!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE fmods
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      INTEGER, PARAMETER :: ncl = 93
      INTEGER, PARAMETER :: nlen = 15
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CONTAINS
      CHARACTER(len=100) FUNCTION fname(class,ext)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Names output files for different binary classes
! Bart Willems, 17/07/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: class
      INTEGER :: j
      CHARACTER(len=*), INTENT(IN) :: ext
      CHARACTER(len=nlen) :: name
      CHARACTER(len=nlen), DIMENSION(0:ncl),PARAMETER :: names = (/ &
           'noname         ','LamSco         ','WEB            ', &
           'WEBwide        ', &
           ('noname         ',j=4,5), &
           'SNII           ', &
           ('noname         ',j=7,10), &
           'ClassicCV      ','GKPerCV        ','SymbCV         ', &
           'sdBCV          ','ThermalCV      ', &
           ('noname         ',j=16,20), &
           'Pre_Algol      ','MSAlgol        ','ColdAlgol      ', &
           'HotAlgol       ', &
           ('noname         ',j=25,30), &
           'NS_LMXB        ','NS_IMXB        ','NS_HMXB        ', &
           'NSWD_XB        ', &
           ('noname         ',j=35,35), &
           'NSnHe_LMXB     ','NSnHe_IMXB     ','NSnHe_HMXB     ', &
           ('noname         ',j=39,40), &
           'BH_LMXB        ','BH_IMXB        ','BH_HMXB        ', &
           'BHWD_XB        ', &
           ('noname         ',j=45,45), &
           'BHnHe_LMXB     ','BHnHe_IMXB     ','BHnHe_HMXB     ', &
           ('noname         ',j=49,50), &
           'S_Symb         ','D_Symb         ', &
           ('noname         ',j=53,54), &
           'WDMS           ', &
           ('noname         ',j=56,60), &
           'WDWD_DD        ','NSWD_DD        ','BHWD_DD        ', &
           'NSNS_DD        ','BHNS_DD        ','BHBH_DD        ', &
           ('noname         ',j=67,70), &
           'BMSP           ','AIC_BMSP       ', &
           ('noname         ',j=73,80), &
           'NS_DLMWXB      ','NS_DIMWXB      ','NS_DHMWXB      ', &
           ('noname         ',j=84,85), &
           'pre_NS_LMXB    ','pre_NS_IMXB    ','pre_NS_HMXB    ', &
           ('noname         ',j=89,90), &
           'BH_DLMWXB      ','BH_DIMWXB      ','BH_DHMWXB      ' &
           /)

      IF ((class < 0).OR.(class > ncl)) THEN
         name = 'noname'
      ELSE
         name = names(class)
      END IF

      fname = name(1:LEN_TRIM(name))//ext(1:LEN_TRIM(ext))

      RETURN
      END FUNCTION fname
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE del_file(name)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Deletes file 'name'                                                  c
! Bart Willems, 01/05/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE
      CHARACTER(len=*), INTENT(IN) :: name
      LOGICAL :: fexst

      INQUIRE(FILE=name,EXIST=fexst)
      IF (fexst) THEN
         OPEN(1,FILE=name(1:LEN_TRIM(name)),STATUS='OLD')
         CLOSE(1,STATUS='DELETE')
      END IF

      RETURN
      END SUBROUTINE del_file
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE fmods
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
