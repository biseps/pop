MODULE extinctionKepler
	IMPLICIT NONE
	CONTAINS
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	ELEMENTAL DOUBLE PRECISION FUNCTION extinctKepler(b,d)
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: b,d
	!V-band extinction (mags per pc)
	DOUBLE PRECISION,PARAMETER :: kv=0.001,hdust=150.0
	DOUBLE PRECISION :: kr,sinb

	kr=kv
	sinb=sin(abs(b))

	!d in pc
	extinctKepler=(hdust*kr/sinb)*(1.0-exp(-(d*sinb)/hdust))

	END FUNCTION extinctKepler

END MODULE extinctionKepler