!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE names
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Bart Willems, 16/07/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE
      INTEGER, PRIVATE :: j
      INTEGER, PARAMETER, PRIVATE :: nl = 15
      INTEGER, PARAMETER, PRIVATE :: nbl = 55
      INTEGER, PARAMETER, PRIVATE :: ncl = 93
      CHARACTER(len=8), DIMENSION(0:nl) :: label = (/' LM MS  ', &
           '   MS   ','   HG   ','   GB   ','  CHeB  ','  EAGB  ', &
           ' TPAGB  ',' nHe MS ',' nHe HG ',' nHe GB ', &
           ' He WD  ',' CO WD  ',' ONe WD ','   NS   ','   BH   ', &
           'Mless SN'/)
      CHARACTER(len=11), DIMENSION(0:nbl) :: blabel = (/ &
           ' Detached  ',('           ',j=1,9), &
           ' RLOF M1   ',' DRLOF M1  ',' TRLOF M1  ',' NRLOF M1  ', &
           '   CE M1   ','   CE M1*  ', &
           ('           ',j=16,19), &
           ' RLOF M2   ',' DRLOF M2  ',' TRLOF M2  ',' NRLOF M2  ', &
           '   CE M2   ','   CE M2*  ', &
           ('           ',j=26,29), &
           ' Contact   ',('           ',j=31,40), &
           ' CE Merger ',' Collision ','Kick merger','RLOF Merger', &
           ('           ',j=45,49), &
           'Single Star',('           ',j=51,54), &
           ' Disrupted '/)
      CHARACTER(len=11), DIMENSION(0:ncl) :: clabel = (/' *         ', &
           'LamSco     ','WEB        ','WEBwide    ', &
           ('           ',j=4,5), &
           'SN II      ', &
           ('           ',j=7,10), &
           'Classic CV ','GK Per CV  ','Symb CV    ','sdB CV     ', &
           'Thermal CV ', &
           ('           ',j=16,20), &
           'Pre Algol  ','MS Algol   ','Cold Algol ','Hot Algol  ', &
           ('           ',j=25,30), &
           'NS LMXB    ','NS IMXB    ','NS HMXB    ','NSWD XB    ', &
           ('           ',j=35,35), &
           'NSnHe LMXB ','NSnHe IMXB ','NSnHe HMXB ', &
           ('           ',j=39,40), &
           'BH LMXB    ','BH IMXB    ','BH HMXB    ','BHWD XB    ', &
           ('           ',j=45,45), &
           'BHnHe LMXB ','BHnHe IMXB ','BHnHe HMXB ', &
           ('           ',j=49,50), &
           'S-Symb     ','D-Symb     ', &
           ('           ',j=53,54), &
           'WDMS       ', &
           ('           ',j=56,60), &
           'WDWD DD    ','NSWD DD    ','BHWD DD    ','NSNS DD    ', &
           'BHNS DD    ','BHBH DD    ', &
           ('           ',j=67,70), &
           'BMSP       ','AIC BMSP   ', &
           ('           ',j=73,80), &
           'NS DLMWXB  ','NS DIMWXB  ','NS DHMWXB  ', &
           ('           ',j=84,85), &
           'pre NS LMXB','pre NS IMXB','pre NS HMXB', &
           ('           ',j=89,90), &
           'BH DLMWXB  ','BH DIMWXB  ','BH DHMWXB  ' &
           /)
           !Number of time points to avg togther, 0 will not output for this phase
           !and a very large number will output only 1 point
           INTEGER,PARAMETER,DIMENSION(0:nl) :: timeSampSing=(/25, &
           25,25,25,25,25, &
           25,25,25,25, &
           25,25,25,25,25, &
           25/)
           INTEGER,PARAMETER,DIMENSION(0:nl) :: timeSampBin=(/500, &
           500,25,40,500,500, &
           500,500,500,500, &
           500,500,500,500,500, &
           500/)
	
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE names
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE output
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         CONTAINS
         SUBROUTINE record(s,b)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Stores output to files numbered 2,3,4                                c
! Bart Willems, 25/04/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE dtypes
         USE dshare
         IMPLICIT NONE
         TYPE (stq), DIMENSION(2), INTENT(IN) :: s
         TYPE (bnq), INTENT(IN) :: b
         DOUBLE PRECISION :: teff

! Store stellar quantities star1
         IF (s(1)%r > s(1)%rl) THEN
            teff = 1000.d0*((1130.d0*s(1)%lum/(s(1)%rl**2.d0))**0.25)
         ELSE
            teff = 1000.d0*((1130.d0*s(1)%lum/(s(1)%r**2.d0))**0.25)
         END IF
         WRITE(2,200) s(1)%tphys,s(1)%kw,s(1)%mass,s(1)%mt, &
              log10(s(1)%lum),log10(s(1)%r),log10(teff), &
              log10(s(1)%rl),s(1)%epoch,s(1)%dmrl
!         write(22,222) s(1)%tphys,s(1)%kw,s(1)%mass,s(1)%mt,
!     &        s(1)%dmw,s(1)%dma

! Store stellar quantities star2
         IF (s(2)%r > s(2)%rl) THEN
            teff = 1000.d0*((1130.d0*s(2)%lum/(s(2)%rl**2.0))**0.25)
         ELSE
            teff = 1000.d0*((1130.d0*s(2)%lum/(s(2)%r**2.0))**0.25)
         END IF
         WRITE(3,200) s(2)%tphys,s(2)%kw,s(2)%mass,s(2)%mt, &
              log10(s(2)%lum),log10(s(2)%r),log10(teff), &
              log10(s(2)%rl),s(2)%epoch,s(2)%dmrl
!         write(23,222) s(2)%tphys,s(2)%kw,s(2)%mass,s(2)%mt,
!     &        s(2)%dmw,s(2)%dma

! Store orbital parameters
         WRITE(4,400) s(1)%tphys,b%kw,s(1)%mt+s(2)%mt,b%Porb*365.d0, &
              b%a,b%ecc

 200     FORMAT (F10.4,1X,I3,1X,6(F10.4,1X),1P,E12.4,1X,1P,E12.4)
 222     FORMAT (F10.4,1X,I3,1X,2(F10.4,1X),1P,E12.4,1X,1P,E12.4)
 400     FORMAT (F10.4,4X,I3,1X,3(F10.4,1X),F7.4)

         RETURN
         END SUBROUTINE record
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE record0
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Writes an empty line preceded by '#' to the output files 2,3,4       c
! Bart Willems, 25/04/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         IMPLICIT NONE

         WRITE(2,'(A)') '#'
         WRITE(3,'(A)') '#'
         WRITE(4,'(A)') '#'

         RETURN
         END SUBROUTINE record0
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE reclog(s,b)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Logs changes in stellar and binary type                              c
! Bart Willems, 16/07/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE dtypes
         USE dshare
!          USE params, ONLY: mmv1,mmv2
!          USE bfuncs, ONLY: bolcor
         IMPLICIT NONE
         TYPE (stq), DIMENSION(2), INTENT(INOUT) :: s
         TYPE (bnq), INTENT(INOUT) :: b
         DOUBLE PRECISION, DIMENSION(2) :: reff
         DOUBLE PRECISION :: teff
         DOUBLE PRECISION :: mv1,mv2,mv,bcsun,mvsun,Lv1,Lv2

! Log binary sequence number
         save_binary(jp)%num = b%seq_num

! Log stellar quantities
         save_binary(jp)%tphys = s(1)%tphys
         save_binary(jp)%kw1 = s(1)%kw
         save_binary(jp)%mass1 = s(1)%mass
         save_binary(jp)%mt1 = s(1)%mt
         save_binary(jp)%epoch1 = s(1)%epoch
         save_binary(jp)%lum1 = s(1)%lum
         save_binary(jp)%xlw1 = s(1)%xlw
         save_binary(jp)%dm1 = s(1)%dmrl
         save_binary(jp)%xldm1 = s(1)%xldm
         s(1)%reff = MIN(s(1)%r,s(1)%rl)
         teff = 1000.d0*((1130.d0*s(1)%lum/(s(1)%reff**2.d0))**0.25)
         s(1)%teff=teff
         save_binary(jp)%teff1 = teff
         save_binary(jp)%reff1 = s(1)%reff
         save_binary(jp)%kw2 = s(2)%kw
         save_binary(jp)%mass2 = s(2)%mass
         save_binary(jp)%mt2 = s(2)%mt
         save_binary(jp)%epoch2 = s(2)%epoch
         save_binary(jp)%lum2 = s(2)%lum
         save_binary(jp)%xlw2 = s(2)%xlw
         save_binary(jp)%dm2 = s(2)%dmrl
         save_binary(jp)%xldm2 = s(2)%xldm
         s(2)%reff = MIN(s(2)%r,s(2)%rl)
         teff = 1000.d0*((1130.d0*s(2)%lum/(s(2)%reff**2.d0))**0.25)
         s(2)%teff=teff
         save_binary(jp)%teff2 = teff
         save_binary(jp)%reff2 = s(2)%reff

! Log orbital parameters
         save_binary(jp)%bkw = b%kw
         save_binary(jp)%cls = b%cls
         save_binary(jp)%Porb = b%Porb*365.d0
         save_binary(jp)%ecc = b%ecc

! Get magntidues from BC
	save_binary(jp)%lum1=s(1)%lum
	save_binary(jp)%lum2=s(2)%lum
! 
!          teff = log10(save_binary(jp)%teff1)
!          save_binary(jp)%bc1 = bolcor(s(1)%kw,s(1)%mt,s(1)%mc,s(1)%lum,teff)
! 
!          teff = log10(save_binary(jp)%teff2)
!          save_binary(jp)%bc2 = bolcor(s(2)%kw,s(2)%mt,s(2)%mc,s(2)%lum,teff)
! 
!          teff = log10(1000.d0*(1130.d0**0.25))
!          bcsun = bolcor(1,1.d0,0.d0,1.d0,teff)
!          mvsun = 4.76d0 - bcsun
! 
!          mv1 = 4.76d0 - 2.5d0*log10(s(1)%lum) - save_binary(jp)%bc1
!          mv2 = 4.76d0 - 2.5d0*log10(s(2)%lum) - save_binary(jp)%bc2
!          Lv1 = 10.d0**(-(mv1-mvsun)/2.5d0)
!          Lv2 = 10.d0**(-(mv2-mvsun)/2.5d0)
! 
!          mv = (mmv1 - mvsun + 2.5d0*log10(Lv1+Lv2) + 5.d0)/5.d0
! !         mv = (mmv1 - mv1 + 5.0d0)/5.d0
! 	   !No longer needed as set in population now
! !          save_binary(jp)%dmin = 10.d0**mv
! 	   save_binary(jp)%dmin=0.d0
!          mv = (mmv2 - mvsun + 2.5d0*log10(Lv1+Lv2) + 5.d0)/5.d0
! !         mv = (mmv2 - mv1 + 5.0d0)/5.d0
! !          save_binary(jp)%dmax = 10.d0**mv
!          save_binary(jp)%dmax=0.d0
! 
! ! Abs magntude of system
!          save_binary(jp)%mag=4.84-2.5d0*log10(Lv1+Lv2)
!          s(1)%mag=mv1
!          s(2)%mag=mv2
!          
!          save_binary(jp)%mag1=mv1
!          save_binary(jp)%mag2=mv2
! 
!          b%mag=save_binary(jp)%mag

         jp = jp + 1
! Did the binary belong to a known class of systems?
! Note this currently needs to be after the increase of jp!!!
         IF (b%cls == 6) THEN
            save_binary(jp-1)%cls = b%cls
         ELSE
             call identify(s%kw,s%mt,s%mc,s%lum,reff, &
                 s%xlw,s%xldm,s%xlcor,b%kw,b%Porb,save_binary(jp-1)%cls,s,b)

         END IF
         IF (jp >= nnlog) warn = 11

         RETURN
         END SUBROUTINE reclog
         
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!         SUBROUTINE wrbout(n,fname)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Writes output stored in bout to disk                                 c
! Bart Willems, 05/03/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!         USE dshare
!         IMPLICIT NONE
!         INTEGER, INTENT(IN) :: n
!         CHARACTER(LEN=*), INTENT(IN) :: fname
!         INTEGER :: j,k
!
!         OPEN(2,FILE=fname,STATUS='UNKNOWN',ACTION='WRITE')
!         WRITE(2,'(A)') '#  Tev(Myr) type      Mo         Mt     '
!     &        //'log10(L)  log10(R) log10(Teff)  log10(Rl)   epoch  '
!     &        //'  dMRL'
!         j = 0
!         loop: DO
!            j = j + 1
!            IF (j >= ip) EXIT loop
!            IF (bout(n,j,1) < 0.0) THEN
!               WRITE(2,200)
!               CYCLE loop
!            END IF
!            WRITE(2,200) (bout(n,j,k),k=1,10)
!         END DO loop
!         CLOSE(2)
! 200     FORMAT (8(F10.4,1X),F12.4,1X,1P,E12.4)
!
!         RETURN
!         END SUBROUTINE wrbout
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!         SUBROUTINE wroout(fname)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Writes output stored in bout to disk                                 c
! Bart Willems, 05/03/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!         USE dshare
!         IMPLICIT NONE
!         CHARACTER(LEN=*), INTENT(IN) :: fname
!         INTEGER :: j,k
!
!         OPEN(4,FILE=fname,STATUS='UNKNOWN',ACTION='WRITE')
!         WRITE(4,'(A)') '#  Tev(Myr)  type      Mb      Porb    '
!     &        //'    a      ecc '
!         j = 0
!         loop: DO
!            j = j + 1
!            IF (j >= ip) EXIT loop
!            IF (oout(j,1) < 0.0) THEN
!               WRITE(4,400)
!               CYCLE loop
!            END IF
!            WRITE(4,400) (oout(j,k),k=1,6)
!         END DO loop
!         CLOSE(4)
! 400     FORMAT (F10.4,4X,F3.0,1X,3(F10.4,1X),F7.4)
!
!         RETURN
!         END SUBROUTINE wroout
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE wrlog(fname)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Writes the log stored in save_binary to the default output device or        c
! to file                                                              c
! Bart Willems, 30/04/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE params
         USE dshare
         USE names
		USE bfuncs
         IMPLICIT NONE

         CHARACTER(LEN=*), INTENT(IN) :: fname

         INTEGER :: j,wrn,fnmbr
			DOUBLE PRECISION :: a

         IF (fname == '*') THEN
            fnmbr = 6
         ELSE
            fnmbr = 3
            OPEN(fnmbr,FILE=fname,STATUS='UNKNOWN',POSITION='APPEND', &
                 ACTION='WRITE')
         END IF

         WRITE(fnmbr,'(A)') '*************************************' &
              //'****************************************'
         WRITE(fnmbr,'(A)') '    Time    M1   Type1     M2   Type2' &
              //'       Porb   Ecc    Status       Class   '
         WRITE(fnmbr,'(A)') '*************************************' &
              //'****************************************'
          WRITE(fnmbr,*) "num=",save_binary(1)%num

         j = 0
         loop: DO
            j = j + 1
            IF ((j >= jp).OR.(j > nnlog)) EXIT loop
            IF (save_binary(j)%tphys == -1.0) THEN
               wrn = save_binary(j)%bkw
               IF ((wrn > 0).AND.(wrn /= 99)) THEN
                  WRITE(fnmbr,*)
                  CALL wrerr(fnmbr,wrn) ! Write warning/error messages
                  WRITE(fnmbr,*)
               END IF
               CYCLE loop
            ELSE IF (save_binary(j)%tphys == -2.0) THEN
               WRITE(fnmbr,*)
               wrn = save_binary(j)%bkw
               IF ((wrn > 0).AND.(wrn /= 99)) THEN
                  CALL wrerr(fnmbr,wrn) ! Write warning/error messages
                  WRITE(fnmbr,*)
               END IF
               CYCLE loop
            END IF

            WRITE(fnmbr,100) save_binary(j)%tphys,save_binary(j)%mt1, &
                 label(save_binary(j)%kw1),save_binary(j)%mt2,label(save_binary(j)%kw2), &
                 save_binary(j)%Porb,save_binary(j)%ecc,blabel(save_binary(j)%bkw), &
                 clabel(save_binary(j)%cls)
!             WRITE(fnmbr,*) save_binary(j)%tphys,save_binary(j)%mt1,save_binary(j)%reff1/rlobe(a,save_binary(j)%mt1,save_binary(j)%mt2),&
!                  label(save_binary(j)%kw1),save_binary(j)%mt2,save_binary(j)%reff2/rlobe(a,save_binary(j)%mt2,save_binary(j)%mt1),label(save_binary(j)%kw2), &
!                  save_binary(j)%Porb,a,save_binary(j)%ecc,blabel(save_binary(j)%bkw), &
!                  clabel(save_binary(j)%cls)
! 		a=Porb2a(save_binary(j)%Porb,save_binary(j)%mt1,save_binary(j)%mt2)
! 		write(fnmbr,*) save_binary(j)%tphys,save_binary(j)%mt1,save_binary(j)%reff1,rlobe(a,save_binary(j)%mt1,save_binary(j)%mt2),save_binary(j)%mt2,&
! 						save_binary(j)%reff2,rlobe(a,save_binary(j)%mt2,save_binary(j)%mt1),&
! 						save_binary(j)%Porb,a,blabel(save_binary(j)%bkw)



         END DO loop

         WRITE(fnmbr,'(A)') '************************************' &
              //'*****************************************'
         IF (ksnii > 0.) WRITE(fnmbr,'(A,1PE12.5)') '  Number of ' &
              //'Type II SN explosions      ',ksnii

         IF (kkick > 0) THEN
            WRITE(fnmbr,'(A,I6)') '  Number of SN kicks calculated  ' &
                 //'         ',kkick
            WRITE(fnmbr,'(A,I6)') '  Number of systems disrupted by ' &
                 //'SN kick  ',kdisr
            WRITE(fnmbr,'(A,I6)') '  Number of systems merged due to' &
                 //' SN kick ',kmerg
            WRITE(fnmbr,'(A)') '************************************' &
                 //'*****************************************'
         END IF

         IF (fname /= '*') THEN
            CLOSE(fnmbr)
         END IF

 100     FORMAT (F16.8,2(1X,F16.8,1X,A8),1X,E16.8,1X,F16.8,2(1X,A11))


         RETURN
         END SUBROUTINE wrlog
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE wrerr(fnmbr,warn)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Writes a warning or error message to the default output device or    c
! to file                                                              c
! Bart Willems, 18/09/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         IMPLICIT NONE
         INTEGER, INTENT(IN) :: fnmbr,warn

         SELECT CASE (warn)
         CASE(1)
            WRITE(fnmbr,*) warn,'   WARNING: Stepsize underflow in evolv2'
         CASE(2)
            WRITE(fnmbr,*) warn,'   WARNING: Unable to determine type of' &
                 //' RLOF'
         CASE(3)
            WRITE(fnmbr,*) warn,'   WARNING: Undefined Radius-Mass exponents'
         CASE(4)
            WRITE(fnmbr,*) warn,'   WARNING: Unimplemented type of mass' &
                 //' transfer'
         CASE(5)
            WRITE(fnmbr,*) warn,'   WARNING: Mass out of range for SSE'
         CASE(6)
            WRITE(fnmbr,*) warn,'   WARNING: Unimplemented type of mass' &
                 //' accretion'
         CASE(7)
            WRITE(fnmbr,*) warn,'   WARNING: Dynamically unstable mass' &
                 //' transfer'
         CASE(8)
            WRITE(fnmbr,*) warn,'   WARNING: dM > 0.2M at start RLOF'
            WRITE(fnmbr,*) warn,'            Unable to find sufficiently' &
                 //' small time step'
         CASE(9)
            WRITE(fnmbr,*) warn,'   WARNING: j > maxstep in evolv2.f'
         CASE(10)
            WRITE(fnmbr,*) warn,'   WARNING: RLOF error: dM > M'
         CASE(11)
            WRITE(fnmbr,*) warn,'   WARNING: save_binary array index out of bounds'
         CASE(12)
            WRITE(fnmbr,*) warn,'   WARNING: Determination of start of RLOF' &
                 //' failed'
         CASE(13)
            WRITE(fnmbr,*) warn,'   WARNING: No convergence on orbital' &
                 //' evolution time step'
         CASE DEFAULT
            IF ((warn > 0).AND.(warn /= 99)) &
                 WRITE(*,*) warn,'   Forgot error message for warn = ',warn
         END SELECT

         RETURN
         END SUBROUTINE wrerr
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE identify(kw,m,mc,lum,r,xlw,xldm,xlcor, &
           bkw,Porb,id,s,b)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Determines to which class a binary belongs                           c
! Bart Willems, 23/11/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE params
         USE dshare
         USE dtypes
         USE bfuncs, ONLY: bolcor
         USE extras, ONLY: Porb2a
         IMPLICIT NONE

         INTEGER, INTENT(IN) :: bkw
         INTEGER, DIMENSION(2), INTENT(IN) :: kw
         DOUBLE PRECISION, INTENT(IN) :: Porb
         DOUBLE PRECISION, DIMENSION(2), INTENT(IN) :: m,mc,r
         DOUBLE PRECISION, DIMENSION(2), INTENT(IN) :: lum
         DOUBLE PRECISION, DIMENSION(2), INTENT(IN) :: xlw,xldm,xlcor

         TYPE (stq), DIMENSION(2), INTENT(INOUT) :: s
         TYPE (bnq), INTENT(INOUT) :: b

         INTEGER :: n,nn,i,i1,i2,id
         DOUBLE PRECISION :: q,teff,teff1,teff2
         DOUBLE PRECISION :: r1,r2,lum1,lum2,Lv1,Lv2
         DOUBLE PRECISION :: bc1,bc2,bcsun,mv1,mv2,mvsun
         DOUBLE PRECISION :: ltot,lec1,lec2,lec,dmv,minF,diff,newMinF,newMaxF
         DOUBLE PRECISION, DIMENSION(2) :: xlum

         id = 0                 ! No classification
	   dmv=0.0d0
		IF (isSingle == 1) then
!  			IF( s(1)%teff .le. 7500.d0 ) THEN
! 		IF( kw(1) < 13 ) THEN
			id = 3
! 			END IF
        	 ELSE
!             	SELECT CASE (bkw)
!             	CASE(0,13,23)
			
!   		   		IF ((kw(1) < 13).AND.(kw(2) < 13) )then
!      		    			IF(s(1)%teff .le. 7500.d0 .or. s(2)%teff .le. 7500.d0) THEN
                  			id = 2
!                     		END IF
!               		END IF
			
!            		 END SELECT

         	END IF

!         SELECT CASE (bkw)
!         CASE(0)                ! Detached systems
!            loop: DO n = 1, 2
!               SELECT CASE (kw(3-n))
!               CASE(0:9)
!                  IF (xlw(3-n) > MIN(10.d0,0.01d0*lum(n))) THEN
!                     SELECT CASE(kw(n))
!                     CASE(0:5)
!                        id = 51 ! S-type symbiotic binary
!                     CASE(6)
!                        id = 52 ! D-type symbiotic binary
!                     END SELECT
!                  END IF
!               CASE(10:12)
!                  SELECT CASE(kw(n))
!                  CASE(0:5)
!                     IF (xlw(3-n) > MIN(10.d0,0.01d0*lum(n))) id = 51
!                  CASE(6)
!                     IF (xlw(3-n) > MIN(10.d0,0.01d0*lum(n))) id = 52
!                  CASE(10:12)
!                     id = 61    ! WD + WD
!                  CASE(13)
!                     id = 62    ! WD + NS
!                     i1 = jp
!                     ll1: DO i = 1, jp-1
!                        IF (save_binary(i)%kw1 == 13) THEN
!                           i1 = i
!                           EXIT ll1
!                        ELSE IF (save_binary(i)%kw2 == 13) THEN
!                           i1 = i
!                           EXIT ll1
!                        END IF
!                     END DO ll1
!                     i2 = i1
!                     ll1b: DO i = jp-1, i1, -1
!                        IF (save_binary(i)%tphys >= 0.0) CYCLE ll1b
!                        i2 = i + 1
!                        EXIT ll1b
!                     END DO ll1b
!                     ll1c: DO i = jp-1, i2, -1
!                        nn = save_binary(i)%bkw - 10*(3-n)
!                        IF ((nn == 2).OR.(nn == 3)) THEN
!                           id = 71 ! BMSP
!                           EXIT ll1c
!                        END IF
!                     END DO ll1c
!                  CASE(14)
!                     id = 63    ! WD + BH
!                  END SELECT
!               CASE(13)
!                  SELECT CASE(kw(n))
!                  CASE(10:12)
!                     id = 62    ! WD + NS
!                     i1 = jp
!                     ll2: DO i = 1, jp-1
!                        IF (save_binary(i)%kw1 == 13) THEN
!                           i1 = i
!                           EXIT ll2
!                        ELSE IF (save_binary(i)%kw2 == 13) THEN
!                           i1 = i
!                           EXIT ll2
!                        END IF
!                     END DO ll2
!                     i2 = i1
!                     ll2b: DO i = jp-1, i1, -1
!                        IF (save_binary(i)%tphys >= 0.0) CYCLE ll2b
!                        i2 = i + 1
!                        EXIT ll2b
!                     END DO ll2b
!                     ll2c: DO i = jp-1, i2, -1
!                        nn = save_binary(i)%bkw - 10*n
!                        IF ((nn == 2).OR.(nn == 3)) THEN
!                           id = 71 ! BMSP
!                           EXIT ll2c
!                        END IF
!                     END DO ll2c
!                  CASE(13)
!                     id = 64    ! NS + NS
!                  CASE(14)
!                     id = 65    ! NS + BH
!                  CASE DEFAULT
!                     IF (xlw(3-n) > xlmin) THEN
!                        IF (m(n) < 1.5d0) THEN
!                           id = 81 ! Detached NS Low-mass Wind XRB
!                        ELSE IF (m(n) < 7.0d0) THEN
!                           id = 82 ! Detached NS Interm-mass Wind XRB
!                        ELSE
!                           id = 83 ! Detached NS High-mass Wind XRB
!                        END IF
!                     END IF
!                  END SELECT
!               CASE(14)
!                  SELECT CASE(kw(n))
!                  CASE(10:12)
!                     id = 63
!                  CASE(13)
!                     id = 65
!                  CASE(14)
!                     id = 66    ! BH + BH
!                  CASE DEFAULT
!                     IF (xlw(3-n) > xlmin) THEN
!                        IF (m(n) < 1.5d0) THEN
!                           id = 91 ! Detached BH Low-mass Wind XRB
!                        ELSE IF (m(n) < 7.0d0) THEN
!                           id = 92 ! Detached BH Interm-mass Wind XRB
!                        ELSE
!                           id = 93 ! Detached BH High-mass Wind XRB
!                        END IF
!                     END IF
!                  END SELECT
!               END SELECT
!               IF (id > 0) EXIT loop
!            END DO loop
!         CASE (12,22)     ! Thermal RLOF
!            n = bkw/10
!            SELECT CASE (kw(3-n))
!            CASE(0:1)           ! Main sequence accretor
!               q = m(n)/m(3-n)
!               IF (q >= 1.d0) THEN
!                  id = 21       ! Pre Algol
!               ELSE
!                  SELECT CASE (kw(n))
!                  CASE(0:1)
!                     id = 22    ! MS Algol
!                  CASE DEFAULT
!                     IF (m(3-n) <= 1.25d0) THEN
!                        id = 23 ! Cold Algol
!                     ELSE
!                        id = 24 ! Hot Algol
!                     END IF
!                  END SELECT
!               ENDIF
!            CASE(10:12)         ! WD accretor
!               SELECT CASE (kw(n))
!               CASE(0:1)
!                  id = 15       ! Thermal CV
!               CASE(2:3)
!                  id = 12       ! GK Per-type CV
!               CASE(4:6)
!                  id = 13       ! Symbiotic-type CV
!               CASE(7:9)
!                  id = 14       ! sdB-type CV
!               END SELECT
!            CASE(13)            ! NS accretor
!               xlum = xlw + xldm ! Total X-ray luminosity
!               IF (xlum(3-n) > xlmin) THEN
!                  SELECT CASE (kw(n))
!                  CASE(0:6)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 31 ! NS Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 32 ! NS Intermediate-mass XRB
!                     ELSE
!                        id = 33 ! NS High-mass XRB
!                     END IF
!                  CASE(7:9)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 36 ! NS nHe Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 37 ! NS nHe Intermediate-mass XRB
!                     ELSE
!                        id = 38 ! NS nHe High-mass XRB
!                     END IF
!                  CASE(10:12)   ! WD donor
!                     id = 34    ! NS WD XRB
!                  END SELECT
!               END IF
!            CASE(14)            ! BH accretor
!               xlum = xlw + xldm ! Total X-ray luminosity
!               IF (xlum(3-n) > xlmin) THEN
!                  SELECT CASE (kw(n))
!                  CASE(0:6)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 41 ! BH Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 42 ! BH Intermediate-mass XRB
!                     ELSE
!                        id = 43 ! BH High-mass XRB
!                     END IF
!                  CASE(7:9)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 46 ! BH nHe Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 47 ! BH nHe Intermediate-mass XRB
!                     ELSE
!                        id = 48 ! BH nHe High-mass XRB
!                     END IF
!                  CASE(10:12)   ! WD donor
!                     id = 44    ! BH WD XRB
!                  END SELECT
!               END IF
!            END SELECT
!         CASE (13,23)     ! Nuclear RLOF
!            n = bkw/10
!            SELECT CASE (kw(3-n))
!            CASE(0:1)           ! Main sequence accretor
!               q = m(n)/m(3-n)
!               IF (q >= 1.d0) THEN
!                  id = 21       ! Pre Algol
!               ELSE
!                  SELECT CASE (kw(n))
!                  CASE(0:1)
!                     id = 22    ! MS Algol
!                  CASE DEFAULT
!                     IF (m(3-n) <= 1.25d0) THEN
!                        id = 23 ! Cold Algol
!                     ELSE
!                        id = 24 ! Hot Algol
!                     END IF
!                  END SELECT
!               ENDIF
!            CASE(10:12)         ! WD accretor
!               SELECT CASE (kw(n))
!               CASE(0:1)
!                  id = 11       ! Classical CV
!               CASE(2:3)
!                  id = 12       ! GK Per-type CV
!               CASE(4:6)
!                  id = 13       ! Symbiotic-type CV
!               CASE(7:9)
!                  id = 14       ! sdB-type CV
!               END SELECT
!            CASE(13)            ! NS accretor
!               xlum = xlw + xldm ! Total X-ray luminosity
!               IF (xlum(3-n) > xlmin) THEN
!                  SELECT CASE (kw(n))
!                  CASE(0:6)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 31 ! NS Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 32 ! NS Intermediate-mass XRB
!                     ELSE
!                        id = 33 ! NS High-mass XRB
!                     END IF
!                  CASE(7:9)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 36 ! NS nHe Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 37 ! NS nHe Intermediate-mass XRB
!                     ELSE
!                        id = 38 ! NS nHe High-mass XRB
!                     END IF
!                  CASE(10:12)   ! WD donor
!                     id = 34    ! NS WD XRB
!                  END SELECT
!               END IF
!            CASE(14)            ! BH accretor
!               xlum = xlw + xldm ! Total X-ray luminosity
!               IF (xlum(3-n) > xlmin) THEN
!                  SELECT CASE (kw(n))
!                  CASE(0:6)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 41 ! BH Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 42 ! BH Intermediate-mass XRB
!                     ELSE
!                        id = 43 ! BH High-mass XRB
!                     END IF
!                  CASE(7:9)
!                     IF (m(n) < 1.5d0) THEN
!                        id = 46 ! BH nHe Low-mass XRB
!                     ELSE IF (m(n) < 7.0d0) THEN
!                        id = 47 ! BH nHe Intermediate-mass XRB
!                     ELSE
!                        id = 48 ! BH nHe High-mass XRB
!                     END IF
!                  CASE(10:12)   ! WD donor
!                     id = 44    ! BH WD XRB
!                  END SELECT
!               END IF
!            END SELECT
!         END SELECT

!         identify = id

         RETURN
         END SUBROUTINE identify
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE output
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
