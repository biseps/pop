MODULE jktebop
!This is adapted code form John Southworths JKTEBOP binary simulator
! avaible at http://www.astro.keele.ac.uk/~jkt/codes/jktebop.html
! References:
! Popper & Etzel (1981AJ.....86..102)
! Etzel (1981psbs.conf..111E)
! Nelson & Davis (1972ApJ...174..617N)
! Southworth et al. (2004MNRAS.351. 1277S)
! Southworth et al. (2004MNRAS.355..986S)
!     *  Southworth et al. (2005MNRAS.363..529S): shows that the Monte Carlo analysis gives reliable results
!     * Bruntt et al. (2006A&A...456..651B): fitting for orbital period and time of minimum light
!     * Southworth et al. (2007A&A...467.1215S): non-linear limb darkening, and inclusion of spectroscopic light ratios and times of minimum light as observations
!     * Southworth et al. (2007MNRAS.379L..11S): linear limb darkening is inadequate for the fitting the HST photometry of the transiting extrasolar planetary system HD 209458
!     * Southworth (2008MNRAS.386.1644S): correlated noise algorithm, cubic-law limb darkening, and a detailed investigation of fourteen transiting extrasolar planetary systems using JKTEBOP
!     * Southworth et al. (2009ApJ...707..167S): inclusion of ecosω and esinω as fitted parameters
!     * Southworth (2010MNRAS.408.1689S): specification of measured values of third light


!-----------------------------------------------------------------------
! V(1) = surface brightness ratio      V(15) = third light
! V(2) = sum of fractional radii       V(16) = phase correction
! V(3) = ratio of stellar radii        V(17) = light scaling factor
! V(4) = linear LD for star A          V(18) = integration ring size (o)
! V(5) = linear LD for star B          V(19) = orbital period (days)
! V(6) = orbital inclination           V(20) = ephemeris timebase (days)
! V(7) = e cos(omega) OR ecentricity   V(21) = nonlinear LD for star A
! V(8) = e sin(omega) OR omega         V(22) = nonlinear LD for star B
! V(9) = gravity darkening 1           VEXTRA(1) = primary star radius
! V(10) = gravity darkening 2          VEXTRA(2) = secondary star radius
! V(11) = primary reflected light      VEXTRA(3) = stellar light ratio
! V(12) = secondary reflected light    VEXTRA(4) = eccentricity
! V(13) = stellar mass ratio           VEXTRA(5) = periastron longitude
! V(14) = tidal lead/lag angle (deg)   VEXTRA(6) = reduced chi-squared
! V(68) = orbital speration in solar radii

! V(23-37) five lots of sine curve [T0,P,amplitude]
! V(38-67) five lots of polynomial [pivot,x,x2,x3,x4,x5] 
!-----------------------------------------------------------------------

	IMPLICIT NONE
	DOUBLE PRECISION,DIMENSION(1:70) :: V

	CONTAINS

	SUBROUTINE jkt(s1,s2,b,i,dis,output)

		USE dtypes
		USE consts
		implicit none

		DOUBLE PRECISION,INTENT(IN) :: i,dis
		INTEGER :: LDTYPE(2),PSINE(5),PPOLY(5)
		INTEGER :: NSINE,NPOLY
		DOUBLE PRECISION,INTENT(OUT),DIMENSION(1:3) :: output

		TYPE (stq), INTENT(IN) :: s1,s2
		TYPE (bnq), INTENT(IN) :: b

            PSINE=0
            PPOLY=0
			NSINE=0
			NPOLY=0

		!Assume quadratic Limb darkening
		LDTYPE(1)=4
		LDTYPE(2)=4
	
		!We assume star is 1 primary
		V=0.d0
!  		V(1)=(10**((s2%mag-s1%mag)/(-2.5)))*(s2%reff/s1%reff)**(-2)
! 		write(*,*) v(1)
 		V(1)=(s2%mag+2.5*log10((pi*s2%reff**2)/(44334448.006896*dis)))/ &
					(s1%mag+2.5*log10((pi*s1%reff**2)/(44334448.006896*dis)))
! 		write(*,*) v(1),(s2%mag+2.5*log10((pi*s2%reff**2)/(44334448.006896*dis))),&
! 						(s1%mag+2.5*log10((pi*s1%reff**2)/(44334448.006896*dis)))

		V(2)=(s1%reff+s2%reff)/b%a
		V(3)=s2%reff/s1%reff
		V(4)=s1%ldc1
		V(5)=s2%ldc1
		V(6)=i*180.d0/pi
		!Assume circular obits
	!WARNING DO NOT USE NON CICRUCLAR ORBIST WITHOUT CHANGING THE THE NUMBER OF
! 	PHASE POINTS CALCULATED
		V(7)=0.d0;V(8)=0.d0
! 		V(7)=b%ecc;V(8)=0.d0

		!Gravity Darkening 
		V(9)=s1%grav
		V(10)=s2%grav

		!Gets set in TASK2 so dont worry
		V(11)=0.d0;V(12)=0.d0
		
		V(13)=s2%mt/s1%mt
		V(14)=0.d0
		V(15)=0.d0

		!Must stay at 0 phase shift
		V(16)=0.0
		V(17)=0.d0
		V(18)=1.0d0
		V(19)=b%Porb
		V(20)=0.d0
		V(21)=s1%ldc2
		V(22)=s2%ldc2
		V(68)=b%a
		v(69)=s1%reff
		V(70)=s2%reff

		CALL TASK2(LDTYPE,output,NSINE,PSINE,NPOLY,PPolY)

	END SUBROUTINE jkt

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

!         SUBROUTINE TASK2 (V,LDTYPE,NSINE,PSINE,NPOLY,PPOLY,output)
	SUBROUTINE TASK2 (LDTYPE,output,NSINE,PSINE,NPOLY,PPolY)
	USE consts
	implicit none                       ! Produces a model light curve
! 	DOUBLE PRECISION,INTENT(INOUT) :: V(68)                        ! IN: light  curve  parameters
	INTEGER,INTENT(IN) :: LDTYPE(2)                   ! IN: LD law type foreach star
	INTEGER :: i,ERROR,aerr                     ! LOCAL: counters & error flag
	DOUBLE PRECISION :: LS,LP                  ! LOCAL: EBOP/GETMODEL  output
	DOUBLE PRECISION :: PHASE                        ! LOCAL:  phase for evaluation
	DOUBLE PRECISION :: GETMODEL                     ! FUNCTION: evaluate the model
	INTEGER,INTENT(INOUT) :: NSINE                       ! OUT: Numbrs of sines and L3s
	INTEGER,INTENT(INOUT) :: PSINE(5)                  ! OUT: Which par for each sine
	INTEGER,INTENT(INOUT) :: NPOLY,PPOLY(5)              ! OUT: Similar for polynomials

	DOUBLE PRECISION :: HJD,magNew,magOld
	DOUBLE PRECISION :: R1,R2
	INTEGER :: NPHASE                   ! LOCAL: number of phases todo
	DOUBLE PRECISION :: MAG
	DOUBLE PRECISION,DIMENSION(1:3),INTENT(OUT) :: output
		DOUBLE PRECISION :: RPA,RPB,EP,RP,ES,RSA,RSB,RS,q,in


	V(19) = 1.0d0           ! Set period to 1.0
	V(20) = 0.0d0           ! Set Tzero to 0.0
	LP = 0.0d0
	LS = 0.0d0
									! NSINE=0 and NPOLY=0 and NUMINT=1
	if ( V(2) >= 0.0d0 ) then
		R1 = V(2) / (1.0d0 + V(3))
		R2 = V(2) / (1.0d0 + (1.0d0/V(3)))
	else
		R1 = abs(V(2))
		R2 = V(3)
	end if
	MAG=GETMODEL(LDTYPE,0,PSINE,0,PPOLY,V(20),1,LP,LS,1,0.0d0)
!       V(11) = 0.4d0 * (LS/(1.0d0-V(15))) * R1**2
!       V(12) = 0.4d0 * (LP/(1.0d0-V(15))) * R2**2

	NPHASE = 10001
!       if ( R1 < 0.01d0 .or. R2 < 0.01d0 ) NPHASE = 100001

!       write(6,'(A40,A40)') ">> The reflection coefficients come from",
!      &                      " the system geometry, not the input file"

!       write(*,'(A47)')"#  PHASE  MAGNITUDE    L1         L2         L3"

		PHASE=0
!         do i = 1,NPHASE
!           PHASE = (i-1) / dble(NPHASE-1)
		HJD = V(20) + PHASE * V(19)
		MAG = GETMODEL(LDTYPE,0,PSINE,0,PPOLY,HJD,1,LP,LS,1,0.0d0)
!  		output=10**(MAG/-2.5)
!          write (*,'(F8.6,4(1X,F10.6))') PHASE,10**(MAG/-2.5),LP,LS,V(15)
!       end do
!       close (62)
		output(1)=10**(MAG/-2.5)


		phase=0.25
		HJD = V(20) + PHASE * V(19)
		MAG = GETMODEL(LDTYPE,0,PSINE,0,PPOLY,HJD,1,LP,LS,1,0.0d0)
		output(2)=10**(MAG/-2.5)


		!This is the point of first contact
		
	if ( V(3) >= 0.0d0 ) then
		RP   = dble( V(2)/(1.0d0+V(3)) )
		RS   = dble( V(2)/(1.0d0+(1.0d0/V(3))) )
	else
		RP   = dble( V(2) )
		RS   = abs( dble( V(3) ) )
	end if
        Q = dble( V(13) )
        CALL BIAX (RP,Q,RPA,RPB,EP)
        CALL BIAX (RS,1.d0/Q,RSA,RSB,ES)

		in=v(6)*pi/180.d0
! 		phase=atan(sqrt((RPB**2*cos(in)**2+RSB**2-V(68)**2*cos(in)**2)/(V(68)**2-RPB**2-RSB**2*sin(in)**2)))/(2.d0*pi)
! 		phase=atan(RPB/(V(68)-RSB))/(2.d0*pi)
		phase=atan((1.d0/(RPB+RSB))-1.d0)/(2.d0*pi)

! 		write(*,*) "j1",in,Q,RPA,RPB,RSB,v(68),PHASE

		HJD = V(20) + PHASE * V(19)
		MAG = GETMODEL(LDTYPE,0,PSINE,0,PPOLY,HJD,1,LP,LS,1,0.0d0)
		output(3)=10**(MAG/-2.5)

!       V(19) = 1.0d0           ! Set period to 1.0
!       V(20) = 0.0d0           ! Set Tzero to 0.0
!                                       ! NSINE=0 and NPOLY=0 and NUMINT=1
!       if ( V(3) >= 0.0d0 ) then
!         R1 = V(2) / (1.0d0 + V(3))
!         R2 = V(2) / (1.0d0 + (1.0d0/V(3)))
!       else
!         R1 = V(2)
!         R2 = abs(V(3))
!       end if
! 
! 	!Note orgianly this was 10001 but that takes long time to run and as were only really want the max value
! !       NPHASE = 1001
! !      if ( R1 < 0.01d0 .or. R2 < 0.01d0 ) NPHASE = 100001
! 
! !       ALLOCATE(MAG(0:NPHASE), STAT=aerr)
! !       IF (aerr > 0) THEN
! !          WRITE(*,*)
! !          WRITE(*,*) '85, ERROR ALLOCATING MEMORY!'
! !          WRITE(*,*)
! !          STOP 'Abnormal program termination!!!'
! !       END IF
! 
!       LP = 0.0d0
!       LS = 0.0d0
! 
! 	MAG=0.d0
! !        MAG(0)=GETMODEL(V,LDTYPE,0,PSINE,0,PPOLY,V(20),1,LP,LS,1,0.0d0)
!       CALL LIGHT (V,LDTYPE,V(20),MAG(0),LP,LS)
! !  	MAG=GETMODEL(V,LDTYPE,0,PSINE,0,PPOLY,V(20),1,LP,LS,1,0.0d0)
!       V(11) = 0.4d0 * (LS/(1.0d0-V(15))) * R1**2
!       V(12) = 0.4d0 * (LP/(1.0d0-V(15))) * R2**2
! 
! !      write (*,'(A40,A40)') ">> The reflection coefficients come from", &
! !                           " the system geometry, not the input file"
! 
! !       write(*,'(A47)')"# PHASE   MAGNITUDE      L1        L2        L3"
! !        do i = 1,NPHASE
! !          PHASE = (i-1) / dble(NPHASE-1)
! !          HJD = V(20) + PHASE  * V(19)
! !   		if(PHASE .gt. 0.1) EXIT
! ! !         MAG(i) = GETMODEL(V,LDTYPE,0,PSINE,0,PPOLY,HJD,1,LP,LS,1,0.0d0)
! ! 
! ! 		CALL LIGHT (V,LDTYPE,HJD,MAG(i),LP,LS)
! ! !           write (*,'(F8.6,4(1X,F16.14))') PHASE,MAG,LP,LS,V(15)
! ! 
! !        end do
! ! !       close (62)
! !  	i=0
!   	magNew=0.d0;magOld=0.d0
! 	DO i=0,10000
! 		PHASE = (i-1) / dble(NPHASE-1)
! 		HJD = V(20) + PHASE  * V(19)
! ! 		i=i+1
! ! 		if(PHASE .gt. 0.1) EXIT
! 		CALL LIGHT (V,LDTYPE,HJD,magNew,LP,LS)
! 
! 		write(*,*) phase,1.0-magNew
! 
! ! 		write(*,*) magNew,magOld
! 		!Look for local maximum
! ! 		if(magNew.gt.magOld)then
! ! 			magOld=magNew
! ! 		else
! !  			magNew=magOld
! ! 			exit
! ! 		end if
! 	END DO

! 	PHASE = 0.0
! 	HJD = V(20) + PHASE  * V(19)
! 	CALL LIGHT (V,LDTYPE,HJD,output,LP,LS)
! 	output=1.0-output

! 	write(*,*) i,magNew,maxval(MAG)
! 	write(*,*) "****"
!  	output=max(magNew-1.d0,0.d0)
!   	output=max(maxval(MAG)-1.d0,0.d0)
! 	output=max(MAG-1.d0,0.d0)
!   	write(*,*) maxval(MAG)-1.d0,output,(maxloc(MAG)-1)/ dble(NPHASE-1),MAG(0),maxval(MAG)
!    	stop

      END SUBROUTINE TASK2

	DOUBLE PRECISION FUNCTION GETMODEL (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,DTYPE,LA,LB,NUMINT,NINTERVAL)
            ! Output a predicted model value according to the parameters
            ! in array V. Precise meaning of the value depends on DTYPE.
            ! DTYPE=1  it outputs an EBOP magnitude for given time
            ! DTYPE=2  it outputs a light ratio for the given time
            ! DTYPE=3  outputs a time of eclipse for the given =CYCLE=
            ! DTYPE=4  it simply outputs the third light value
      implicit none
!       real*8 V(68)                  ! IN: Photometric parameters
      integer LDTYPE(2)             ! IN: LD law type for the two stars
      real*8 TIME                   ! IN: The given TIME, PHASE or CYCLE
      integer DTYPE                 ! IN: 1-6 depending on wanted result
      integer NSINE,PSINE(5)        ! IN: number and parameters of sines
      integer NPOLY,PPOLY(5)        ! IN: number and parameters of polys
      integer NUMINT                ! IN: Number of numerical integratns
      real*8 NINTERVAL              ! IN: Time interval for integrations
      real*8 LA,LB                  ! OUT: Light produced by each star
      real*8 FMAG,LP,LS             ! LOCAL: LIGHT subroutine output
      real*8 ECC,OMEGA,ECOSW,ESINW  ! LOCAL: orbital shape parameters
      real*8 GETMIN,GETPHASE        ! FUNCTIONS
      real*8 FMAGSUM,LASUM,LBSUM
      integer i
      real*8 TIMEIN

      GETMODEL = 0.0d0

            ! DTYPE=1 for light curve datapoint
            ! DTYPE=2 for light ratio
            ! DTYPE=3 for times of minimum light
            ! DTYPE=4 for third light
            ! DTYPE=5 for e*cos(omega)
            ! DTYPE=6 for e*sin(omega)

      if ( DTYPE == 1 ) then
        if ( NUMINT == 1 ) then
         CALL LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,FMAG,LP,LS)
          LA = LP
          LB = LS
          GETMODEL = FMAG

        else if ( NUMINT > 1 ) then
          FMAGSUM = 0.0d0
          LASUM = 0.0d0
          LBSUM = 0.0d0
          CALL LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,FMAG,LP,LS)
          do i = 1,NUMINT
            TIMEIN  =  TIME  -  NINTERVAL / 86400.0d0 / dble(NUMINT) * &
             ( dble(NUMINT) - 2.0d0*dble(i) + 1.0d0 ) / 2.0d0
          CALL LIGHT(LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIMEIN,FMAG,LP,LS)
            FMAGSUM = FMAGSUM + FMAG
            LASUM = LASUM + LP
            LBSUM = LBSUM + LS
          end do
          FMAG = FMAGSUM / NUMINT
          LA = LASUM / NUMINT
          LB = LBSUM / NUMINT
          GETMODEL = FMAG
        else
          write(6,*)"NUMINT is less than 1 in function GETMODEL. Abort."
          write(6,*)"NUMINT =    ", NUMINT
          write(6,*)"NINTERVAL = ", NINTERVAL
          stop
        end if

      else if ( DTYPE == 2 ) then
        CALL LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,TIME,FMAG,LP,LS)
        GETMODEL = LS / LP

      else if ( DTYPE == 3 ) then
        if ( V(7) > 5.0d0 ) then
          ECC = V(7) - 10.0d0
          OMEGA = V(8)
        else
          ECC = sqrt(V(7)**2 + V(8)**2)
          OMEGA = atan2(V(8),V(7)) * 45.0d0 / atan(1.0d0)
          if ( OMEGA < 0.0d0 ) OMEGA = OMEGA + 360.0d0
        end if
        GETMODEL = GETMIN (V(20),V(19),ECC,OMEGA,TIME)

      else if ( DTYPE == 4 ) then
        GETMODEL = V(15)

      else if ( DTYPE == 5 .or. DTYPE == 6 ) then
        if ( V(7) > 5.0d0 ) then
          ECC = V(7)
          OMEGA = V(8)
          ECOSW = (V(7)-10.0d0) * cos(V(8)/57.2957795d0)
          ESINW = (V(7)-10.0d0) * sin(V(8)/57.2957795d0)
          if ( DTYPE == 5 )  GETMODEL = V(7)!ECC
          if ( DTYPE == 6 )  GETMODEL = V(8)!OMEGA
        else
          ECOSW = V(7)
          ESINW = V(8)
          ECC = sqrt(V(7)**2 + V(8)**2)
          OMEGA = atan2(V(8),V(7)) * 45.0d0 / atan(1.0d0)
          if ( OMEGA < 0.0d0 ) OMEGA = OMEGA + 360.0d0
          if ( OMEGA > 360.0d0 ) OMEGA = OMEGA - 360.0d0
          if ( DTYPE == 5 )  GETMODEL = ECOSW
          if ( DTYPE == 6 )  GETMODEL = ESINW
        end if

      else
        GETMODEL = -100.0d0
        write(6,*) "### ERROR: wrong datatype asked for in GETMODEL: ",DTYPE
        STOP
      end if

      END FUNCTION GETMODEL


!=======================================================================
       SUBROUTINE LIGHT (LDTYPE,NSINE,PSINE,NPOLY,PPOLY,HJD,FMAG,LP,LS)
!        SUBROUTINE LIGHT (V,LDTYPE,HJD,FMAG,LP,LS)
	USE consts
      IMPLICIT NONE
!       DOUBLE PRECISION,INTENT(IN) :: V(68),
	DOUBLE PRECISION,INTENT(IN) :: HJD
!       DOUBLE PRECISION :: GETPHASE
      DOUBLE PRECISION,INTENT(OUT) :: LP,LS,FMAG
      DOUBLE PRECISION :: LECL,LE
      DOUBLE PRECISION :: LD1U,LD2U              ! linear LD coeff for each star
      DOUBLE PRECISION :: LD1Q,LD2Q              ! quadratic LD coeff for each star
      DOUBLE PRECISION :: LD1S,LD2S              ! square-root LD coeff for each star
      DOUBLE PRECISION :: LD1L,LD2L              ! logarithmic LD coeff for each star
      DOUBLE PRECISION :: LD1C,LD2C              ! cubic LD coeff for each star
      DOUBLE PRECISION :: LDU,LDQ,LDS,LDL,LDC    ! LD coeffs for the star in question
      INTEGER,INTENT(IN) :: LDTYPE(2)           ! LD law type for both stars
      INTEGER,PARAMETER :: GIMENEZ=3             ! 1 to use original FMAX calculations
                                  ! 2 to use Gimenez' modified calcs
                                  ! 3 to use Gimenez' nonlinear LD calcs
      INTEGER :: i,j
      INTEGER,INTENT(INOUT) :: NSINE,PSINE(5)
      INTEGER,INTENT(INOUT) :: NPOLY,PPOLY(5)
      DOUBLE PRECISION :: PHASE,SINT,SINP,SINA,SINTERM,LPMULT,LSMULT
      DOUBLE PRECISION :: BS,FI,YP,YS,SP,SS,Q,TANGL,EL,DPH,SFACT,DGAM
      DOUBLE PRECISION :: ECOSW,ESINW,E,W
      DOUBLE PRECISION :: FMAXP,FMAXS,DELTP, DELTS, SHORT
!       data GIMENEZ / 3 /
!       data PI,TWOPI,RAD / 3.1415926536E0,6.28318531E0,0.0174532925E0 /
!       data LPMULT,LSMULT / 1.0 , 1.0 /

      DOUBLE PRECISION :: RP,RS,FMN,GAM,GAMN,FMA,FMINP,A,A1,AA,AB1,AA1
      DOUBLE PRECISION :: B2,ALAST,ADISK,AREA,B1,DLP,DLE,COSGAM,COS2
      DOUBLE PRECISION :: COSE,DD,COSW,COSI2,COSV,COSVW,CSVWT,DAREA
      DOUBLE PRECISION :: DGM,DENOM,DGAMA,DISC,E0,DLS,EA,B,ALPHA,D,ES
      DOUBLE PRECISION :: FMA0,EP,FLITE,HEAT,FMINS,HEAT2,OS,OP,OMEGA,OTOT
      DOUBLE PRECISION :: RR,R1,R2,R12,R22,REFL,RPA,RPB,S1,S,RV,RSB
      DOUBLE PRECISION :: SINE,SIN2,SINI2,SINI,SINW,SINVW,SINV,SUM,TANGR
      DOUBLE PRECISION :: THETA,RAS,RK,RSA
      
	LPMULT=1.0
	LSMULT=1.0

! 	NSINE=0
! 	NPOLY=0
! 	PSINE=0
! 	PSINE=0
! 	PPOLY=0
!
!       DETERMINE PRIMARY AND SECONDARY BIAXIAL DIMENSIONS
!       USE SPHERICAL RADII FOR THE COMPUTATION OF ECLIPSE FUNCTIONS
!       USE OBLATENESSES FOR THE COMPUTATION OF THE OUTSIDE ECLIPSE
!       PHOTOMETRIC VARIATIONS WITH LIMB AND GRAVITY DARKENING
!

      if ( V(3) >= 0.0d0 ) then
		!This is R1/a and R2/a
        RP   = dble( V(2)/(1.0d0+V(3)) )
        RS   = dble( V(2)/(1.0d0+(1.0d0/V(3))) )
      else
        RP   = dble( V(2) )
        RS   = abs( dble( V(3) ) )
      end if
!       write (35,*) v(2), v(3), rp, rs
! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !       RATIO  = real(V(3))

      if ( V(7) > 5.0d0 ) then
        ECOSW = dble( (V(7)-10.0d0) * cos(V(8)/57.2957795d0) )
        ESINW = dble( (V(7)-10.0d0) * sin(V(8)/57.2957795d0) )
      else
        ECOSW  = dble(V( 7))
        ESINW  = dble(V( 8))
      end if


      BS     = dble( V(1) )
      FI     = dble( V(6) )
      YP     = dble( V(9) )
      YS     = dble( V(10) )
      SP     = dble( V(11) )
      SS     = dble( V(12) )
      Q      = dble( V(13) )
      TANGL  = dble( V(14) )
      EL     = dble( V(15) )
      DPH    = 1.0 - dble( V(16) )
      SFACT  = dble( V(17) )
      DGAM   = dble( V(18) )

      LD1U = dble(V(4))         ! linear terms
      LD2U = dble(V(5))
      LD1L = 0.0                ! log terms
      LD2L = 0.0
      LD1S = 0.0                ! sqrt terms
      LD2S = 0.0
      LD1Q = 0.0                ! quadratic terms
      LD2Q = 0.0
      LD1C = 0.0                ! cubic terms
      LD2C = 0.0
      if ( LDTYPE(1)==2 ) LD1L = dble(V(21))
      if ( LDTYPE(1)==3 ) LD1S = dble(V(21))
      if ( LDTYPE(1)==4 ) LD1Q = dble(V(21))
      if ( LDTYPE(1)==5 ) LD1C = dble(V(21))
      if ( LDTYPE(2)==2 ) LD2L = dble(V(22))
      if ( LDTYPE(2)==3 ) LD2S = dble(V(22))
      if ( LDTYPE(2)==4 ) LD2Q = dble(V(22))
      if ( LDTYPE(2)==5 ) LD2C = dble(V(22))
      if ( LDTYPE(2)==0 ) then
        LD2U = LD1U
        LD2L = LD1L
        LD2S = LD1S
        LD2Q = LD1Q
        LD2C = LD1C
      end if

      if ( NSINE > 0 ) then
        do i = 1,NSINE
          SINT = V(20+i*3)      ! sine reference time
          SINP = V(21+i*3)      ! sine period
          SINA = V(22+i*3)      ! sine amplitude
!           TWOPI =  atan(1.0d0) * 8.0d0
          SINTERM = SINA * sin( TWOPI * (HJD-SINT) / SINP )
          if ( PSINE(i) ==  1 )  BS = BS * (1.0+SINTERM)
          if ( PSINE(i) ==  2 )  RP = RP * (1.0+SINTERM)
          if ( PSINE(i) ==  3 )  RS = RS * (1.0+SINTERM)
          if ( PSINE(i) ==  6 )  FI = FI + SINTERM
          if ( PSINE(i) == 15 )  EL = EL * (1.0+SINTERM)
          if ( PSINE(i) == 17 )  SFACT = SFACT + SINTERM
          if ( PSINE(i) == -1 )  LPMULT = LPMULT * (1.0+SINTERM)
          if ( PSINE(i) == -2 )  LSMULT = LSMULT * (1.0+SINTERM)
        end do
      end if

      if ( NPOLY > 0 ) then
        do i = 1,NPOLY
          j = 32 + (i*6)
          SINTERM = V(j+1)*(HJD-V(j)) + V(j+2)*((HJD-V(j))**2) + V(j+3)* &
       ((HJD-V(j))**3) + V(j+4)*((HJD-V(j))**4)+ V(j+5)*((HJD-V(j))**5)
          if ( PPOLY(i) ==  1 )  BS = BS + SINTERM   ! Yes this is POLY-
          if ( PPOLY(i) ==  2 )  RP = RP + SINTERM   ! TERM but's called
          if ( PPOLY(i) ==  3 )  RS = RS + SINTERM   ! SINETERM to avoid
          if ( PPOLY(i) ==  6 )  FI = FI + SINTERM    ! proliferation of
          if ( PPOLY(i) == 15 )  EL = EL + SINTERM           ! variables
          if ( PPOLY(i) == 17 )  SFACT = SFACT + SINTERM
          if ( PPOLY(i) == -1 )  LPMULT = LPMULT + SINTERM
          if ( PPOLY(i) == -2 )  LSMULT = LSMULT + SINTERM
        end do
      end if

      PHASE = GETPHASE(HJD,V(19),V(20))

! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! ! !         RS=RP*RATIO
      if ( Q <= 0.0 ) then
        CALL BIAX (RP,0.d0,RPA,RPB,EP)
        CALL BIAX (RS,0.d0,RSA,RSB,ES)
      else
        CALL BIAX (RP,Q,RPA,RPB,EP)
        CALL BIAX (RS,1.d0/Q,RSA,RSB,ES)
      end if

!
!       CORRECT THE OBSERVED PHASE FOR ANY EPOCH ERROR IN EPHEMERIS
!
      THETA=PHASE+DPH
!
      SINI  = SIN(FI*RAD)
      SINI2 = SINI*SINI
      COSI2 = 1.0E0  - SINI2
!
!       TRANSLATE TIDAL LEAD/LAG ANGLE TO RADIANS
      TANGR=TANGL*RAD
!
!    EQUATION 9
!       CONVERT PHASE TO RADIANS
      FMN=THETA*TWOPI
!
!       GET CURRENT VALUES OF E, AND W
      CALL GETEW (ECOSW,ESINW,E,W)
!
!       TEST FOR CIRCULAR ORBIT
!       IF (E)   17,20,17
!    20 COSVW=COS(FMN)
!       SINVW=SIN(FMN)
!       RV=1.0E0
!       GO TO 25

      IF(E .ne. 0.d0) then 
!
!       SOLUTION OF KEPLER'S EQUATION BY DIFFERENTIAL CORRECTIONS
!       (NON-ZERO ECCENTRICITY ONLY . . . )
!
!    EQUATION 6
!
		17 OMEGA = 450.0E0  - W
! 		23 IF (OMEGA - 360.0E0)         22,21,21
! 		21 OMEGA = OMEGA - 360.0E0
! 			GO TO 23
! 		22 OMEGA = OMEGA*RAD

		DO
			if((OMEGA - 360.0E0) .lt.0.d0) exit
			OMEGA = OMEGA - 360.0E0
		END DO
		OMEGA = OMEGA*RAD
			
		!       SINE AND COSINE OF OMEGA
			COSW=COS(OMEGA)
			SINW=SIN(OMEGA)
		!
		!       COMPUTE MEAN ANOMALY CORRECTION TO PHASE
		!       CORRESPONDING TO V=OMEGA=90-W
		!       AT WHICH PHASE COS(V-OMEGA)=1
			E0=ATAN2(SQRT(1.0E0-E*E)*SINW,COSW+E)
		!
		!       MEAN ANOMALY OF MID-PRIMARY ECLIPSE
			FMA0=E0-E*SIN(E0)
		!
		!       MEAN ANOMALY
			FMA=FMN+FMA0
		!    FIRST APPROXIMATION OF ECCENTRIC ANOMALY
			EA=FMA+E*SIN(FMA)
		!
			DO 10 J=1,15
		!       EVALUATE SINE AND COSINE OF ECCENTRIC ANOMALY
			SINE=SIN(EA)
			COSE=COS(EA)
			DENOM=1.0E0-E*COSE
			DISC=FMA-EA+E*SINE
			EA=EA+DISC/DENOM
		!       TEST FOR CONVERGENCE
			IF (ABS(DISC) - 2.0E-05)     15,15,10
		10 CONTINUE
		!
		!
		!       EVALUATE SINE AND COSINE OF TRUE ANOMALY
		15 COSV=(COSE-E)/DENOM
			SINV=SINE*SQRT(1.0E0-E*E)/DENOM
		!
		!       RADIUS VECTOR
			RV = (1.0E0-E*E)/(1.0E0+E*COSV)
		!
		!       THE PHOTOMETRIC PHASE ARGUMENT IN TERMS OF ORBIT PARAMETERS
		!       VW = V-OMEGA
			COSVW=COSV*COSW+SINV*SINW
			SINVW=SINV*COSW-COSV*SINW

      ELSE
      	COSVW=COS(FMN)
     		SINVW=SIN(FMN)
      	RV=1.0E0
     	END IF
!
   25 COS2=COSVW*COSVW
      SIN2=1.0E0-COS2
!
      CSVWT=COS(TANGR)*COSVW-SIN(TANGR)*SINVW
!
!
!       PHOTOMETRIC EFFECTS
!
!

      FMAXP = 0.0
      FMAXS = 0.0
      DELTP = 0.0
      DELTS = 0.0
      SHORT = 0.0

!-----------------------------------------------------------------------
! Alvaro Gimenez and J Diaz-Cordoves have corrected the treatment of LD
! and stellar shapes.  This treatment can be used by putting GIMENEZ=2
! Their teatment for nonlinear LD can be used by putting GIMENEZ=3
!-----------------------------------------------------------------------
! This whole thing affects only the brightness normalisation of the two
! eclipsing stars: any problems here affect the radiative parameters
! but not the geometric parameters (radii, inclination etc).
!-----------------------------------------------------------------------

      if ( GIMENEZ==1 ) then                          ! LINEAR LD ONLY

!       FMAXP=((1.0E0-UP)+0.666666667E0*UP*(1.0E0+0.2E0*EP)) ! Original
!      1      *(1.0E0+3.0E0*YP*EP)/(1.0E0-EP)                ! lines
!       FMAXS=((1.0E0-US)+0.666666667E0*US*(1.0E0+0.2E0*ES)) ! if the
!      1      *(1.0E0+3.0E0*YS*ES)/(1.0E0-ES)                ! stars
!       DELTP=(15.0E0+UP)/(15.0E0-5.0E0*UP)*(1.0E0+YP)*EP    ! are
!       DELTS=(15.0E0+US)/(15.0E0-5.0E0*US)*(1.0E0+YS)*ES    ! oblate
!       SHORT=SINI2*CSVWT*CSVWT

!    26 FMAXP=1.0E0-UP/3.0E0                                 ! Original
!       FMAXS=1.0E0-US/3.0E0                                 ! lines if
!       DELTP=0.0E0                                          ! the stars
!       DELTS=0.0E0                                          ! are
!       SHORT=0.0                                            ! spherical

        if ( Q >= 0.0 ) then
          FMAXP=((1.0E0-LD1U)+0.666666667E0*LD1U*(1.0E0+0.2E0*EP)) &
             *(1.0E0+3.0E0*YP*EP)/(1.0E0-EP)
          FMAXS=((1.0E0-LD2U)+0.666666667E0*LD2U*(1.0E0+0.2E0*ES)) &
             *(1.0E0+3.0E0*YS*ES)/(1.0E0-ES)
          DELTP=(15.0E0+LD1U)/(15.0E0-5.0E0*LD1U)*(1.0E0+YP)*EP
          DELTS=(15.0E0+LD2U)/(15.0E0-5.0E0*LD2U)*(1.0E0+YS)*ES
          SHORT=SINI2*CSVWT*CSVWT
        else
          FMAXP=1.0E0-LD1U/3.0E0
          FMAXS=1.0E0-LD2U/3.0E0
          DELTP=0.0E0
          DELTS=0.0E0
          SHORT=0.0
        end if
!-----------------------------------------------------------------------

      else if ( GIMENEZ==2 ) then                     ! LINEAR LD ONLY

!       FMAXP=(1.0E0-UP*(1.0E0-2.0E0/5.0E0*EP)/3.0E0+YP*EP   ! Original
!      1      *(3.0E0-13.0E0/15.0E0*UP))/(1.0E0-EP)          ! lines
!       FMAXS=(1.0E0-US*(1.0E0-2.0E0/5.0E0*ES)/3.0E0+YS*ES   ! if the
!      1      *(3.0E0-13.0E0/15.0E0*US))/(1.0E0-ES)          ! stars
!       DELTP=(15.0E0+UP)/(15.0E0-5.0E0*UP)*(1.0E0+YP)*EP    ! are
!       DELTS=(15.0E0+US)/(15.0E0-5.0E0*US)*(1.0E0+YS)*ES    ! oblate
!       SHORT=SINI2*CSVWT*CSVWT

!    26 FMAXP=1.0E0-UP/3.0E0                                 ! Original
!       FMAXS=1.0E0-US/3.0E0                                 ! lines if
!       DELTP=0.0E0                                          ! the stars
!       DELTS=0.0E0                                          ! are
!       SHORT=0.0                                            ! spherical

        if ( Q >= 0.0 ) then
          FMAXP=(1.0E0-LD1U*(1.0E0-2.0E0/5.0E0*EP)/3.0E0+YP*EP &
               *(3.0E0-13.0E0/15.0E0*LD1U))/(1.0E0-EP)
          FMAXS=(1.0E0-LD2U*(1.0E0-2.0E0/5.0E0*ES)/3.0E0+YS*ES &
               *(3.0E0-13.0E0/15.0E0*LD2U))/(1.0E0-ES)
          DELTP=(15.0E0+LD1U)/(15.0E0-5.0E0*LD1U)*(1.0E0+YP)*EP
          DELTS=(15.0E0+LD2U)/(15.0E0-5.0E0*LD2U)*(1.0E0+YS)*ES
          SHORT=SINI2*CSVWT*CSVWT
        else
          FMAXP=1.0E0-LD1U/3.0E0
          FMAXS=1.0E0-LD2U/3.0E0
          DELTP=0.0E0
          DELTS=0.0E0
          SHORT=0.0
        end if
!-----------------------------------------------------------------------
! And this is Gimenez's code for including nonlinear LD. He includes
! the linear (UP), quadratic (UP, U2P) and square-root (UP, U3P) laws.
!-----------------------------------------------------------------------

      else if ( GIMENEZ==3 ) then

!      FMAXP=1.0E0-UP*(1.0E0-2.0E0*EP/5.0E0)/3.0E0-
!     1      U2P*(1.0E0-3.0E0*EP/5.0E0)/6.0E0-
!     1      U3P*(1.0E0-4.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP
!     1      *(1.5E0-13.0E0*UP/30.0E0-U2P/5.0E0-23.0E0*U3P/90.0E0)
!      FMAXP=FMAXP/(1.0E0-EP)
!      FMINP=1.0E0-UP*(1.0E0+4.0E0*EP/5.0E0)/3.0E0-
!     1      U2P*(1.0E0+6.0E0*EP/5.0E0)/6.0E0-
!     1      U3P*(1.0E0+8.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP
!     1      *(1.0E0-7.0E0*UP/15.0E0-4.0E0*U2P/15.0E0-13.0E0*U3P/45.0E0)
!      FMINS=1.0E0-US*(1.0E0+4.0E0*ES/5.0E0)/3.0E0-
!     1      U2S*(1.0E0+6.0E0*ES/5.0E0)/6.0E0-
!     1      U3S*(1.0E0+8.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES
!     1      *(1.0E0-7.0E0*US/15.0E0-4.0E0*U2S/15.0E0-13.0E0*U3S/45.0E0)
!      FMAXS=1.0E0-US*(1.0E0-2.0E0*ES/5.0E0)/3.0E0-
!     1      U2S*(1.0E0-3.0E0*ES/5.0E0)/6.0E0-
!     1      U3S*(1.0E0-4.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES
!     1      *(1.5E0-13.0E0*US/30.0E0-U2S/5.0E0-23.0E0*U3S/90.0E0)
!      FMAXS=FMAXS/(1.0E0-ES)
!      DELTP=1.0E0-FMINP/FMAXP
!      DELTS=1.0E0-FMINS/FMAXS
!      SHORT=SINI2*CSVWT*CSVWT

!   26 FMAXP=1.0E0-UP/3.0E0-U2P/6.0E0-U3P/5.0E0
!      FMAXS=1.0E0-US/3.0E0-U2S/6.0E0-U3S/5.0E0
!      DELTP=0.0E0
!      DELTS=0.0E0
!      SHORT=0.0

        if ( Q >= 0.0d0 .or. LDTYPE(1)==1 .or. LDTYPE(1)==5 .or. LDTYPE(2)==1 .or. LDTYPE(2)==5 ) then
          FMAXP=1.0E0-LD1U*(1.0E0-2.0E0*EP/5.0E0)/3.0E0- &
               LD1Q*(1.0E0-3.0E0*EP/5.0E0)/6.0E0- &
               LD1S*(1.0E0-4.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP &
             *(1.5E0-13.0E0*LD1U/30.0E0-LD1Q/5.0E0-23.0E0*LD1S/90.0E0)
          FMAXP=FMAXP/(1.0E0-EP)
          FMINP=1.0E0-LD1U*(1.0E0+4.0E0*EP/5.0E0)/3.0E0- &
               LD1Q*(1.0E0+6.0E0*EP/5.0E0)/6.0E0- &
               LD1S*(1.0E0+8.0E0*EP/9.0E0)/5.0E0+2.0E0*YP*EP &
        *(1.0E0-7.0E0*LD1U/15.0E0-4.0E0*LD1Q/15.0E0-13.0E0*LD1S/45.0E0)
          FMINS=1.0E0-LD2U*(1.0E0+4.0E0*ES/5.0E0)/3.0E0- &
               LD2Q*(1.0E0+6.0E0*ES/5.0E0)/6.0E0- &
               LD2S*(1.0E0+8.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES &
       *(1.0E0-7.0E0*LD2U/15.0E0-4.0E0*LD2Q/15.0E0-13.0E0*LD2S/45.0E0)
          FMAXS=1.0E0-LD2U*(1.0E0-2.0E0*ES/5.0E0)/3.0E0- &
               LD2Q*(1.0E0-3.0E0*ES/5.0E0)/6.0E0- &
               LD2S*(1.0E0-4.0E0*ES/9.0E0)/5.0E0+2.0E0*YS*ES &
             *(1.5E0-13.0E0*LD2U/30.0E0-LD2Q/5.0E0-23.0E0*LD2S/90.0E0)
          FMAXS=FMAXS/(1.0E0-ES)
          DELTP=1.0E0-FMINP/FMAXP
          DELTS=1.0E0-FMINS/FMAXS
          SHORT=SINI2*CSVWT*CSVWT
        else
          FMAXP=1.0-LD1U/3.0-LD1Q/6.0-LD1S/5.0+LD1L*2.0/9.0-LD1C/10.0
          FMAXS=1.0-LD2U/3.0-LD2Q/6.0-LD2S/5.0+LD2L*2.0/9.0-LD2C/10.0
          DELTP=0.0E0
          DELTS=0.0E0
          SHORT=0.0
        end if
!----------------------------------------------------------------------
      end if
!----------------------------------------------------------------------
! Complete original code before the above messing:
! !
! !
! !        PHOTOMETRIC EFFECTS
! !
! !
! !        TEST FOR SIMPLE CASE OF TWO SPHERICAL STARS
!       IF (EP .EQ. 0.  .AND.  ES .EQ. 0.)   GO TO 26
! !
! !        EITHER OR BOTH STARS ARE OBLATE
! !
!       FMAXP=((1.0E0-UP)+0.666666667E0*UP*(1.0E0+0.2E0*EP))
!      1      *(1.0E0+3.0E0*YP*EP)/(1.0E0-EP)
!       FMAXS=((1.0E0-US)+0.666666667E0*US*(1.0E0+0.2E0*ES))
!      1      *(1.0E0+3.0E0*YS*ES)/(1.0E0-ES)
! !        CHANGE IN INTENSITY RATIO DUE TO OBLATENESS RELATED VARIABLES
! !        FROM QUADRATURE TO MINIMUM
! !        FACE ON TO END ON
!       DELTP=(15.0E0+UP)/(15.0E0-5.0E0*UP)*(1.0E0+YP)*EP
!       DELTS=(15.0E0+US)/(15.0E0-5.0E0*US)*(1.0E0+YS)*ES
! !        FORE-SHORTENING FUNCTION OF OBLATENESS
!       SHORT=SINI2*CSVWT*CSVWT
!       GO TO 27
! !
! !        BOTH STARS ARE SPHERICAL
! !
!    26 FMAXP=1.0E0-UP/3.0E0
!       FMAXS=1.0E0-US/3.0E0
!       DELTP=0.0E0
!       DELTS=0.0E0
!       SHORT=0.0
!----------------------------------------------------------------------

!
!        UN-NORMALIZED BRIGHTNESS OF STELLAR COMPONENTS AT QUADRATURE
!    27 OP=PI*RPB*RPB*FMAXP
	OP=PI*RPB*RPB*FMAXP
      OS=PI*RSB*RSB*FMAXS*BS

!       THE NORMALIZING FACTOR
       OTOT=OP+OS
!       BRIGHTNESS CONTRIBUTION FROM EACH COMPONENT
      LP=OP/OTOT*(1.0E0-DELTP*SHORT)
      LS=OS/OTOT*(1.0E0-DELTS*SHORT)
!
!       REFLECTION AND RERADIATION EQUATION
!       IF (SP .EQ. 0.0E0  .AND.  SS .EQ. 0.0E0)   GO TO 28
!       HEAT=SINI*COSVW
!       HEAT2=0.5E0+0.5E0*HEAT*HEAT
!       DLP=SP*(HEAT2+HEAT)
!       DLS=SS*(HEAT2-HEAT)
!       GO TO 29
!    28 DLP=0.0E0
!       DLS=0.0E0

	IF (SP .EQ. 0.0E0  .AND.  SS .EQ. 0.0E0) then
         DLP=0.0E0
         DLS=0.0E0

      ELSE
		HEAT=SINI*COSVW
     	      HEAT2=0.5E0+0.5E0*HEAT*HEAT
      	DLP=SP*(HEAT2+HEAT)
      	DLS=SS*(HEAT2-HEAT)
      END IF

      
!
!       WHICH ECLIPSE COULD THIS BE
!    29 IF (COSVW)         40,40,30

   29 IF(COSVW .gt. 0.d0) then
!
!    PRIMARY ECLIPSE
!
            R1 = RP
		R2 = RS
	!----------------------------------------------------------------------!
	! JKT mod (10/8/2006): the line these replaced was      UU = UP        !
	!----------------------------------------------------------------------!
		LDU = LD1U                                                       !
		LDL = LD1L                                                       !
		LDS = LD1S                                                       !
		LDQ = LD1Q                                                       !
		LDC = LD1C                                                       !
	!----------------------------------------------------------------------!
		LE=LP
		DLE=DLP
! 		GO TO 60
!
!
!    SECONDARY ECLIPSE
!
	else
	      R1 = RS
		R2 = RP
	!-----------------------------------------------------------------------
	! JKT mod (10/8/2006): the line these replaced was      UU = US        !
	!----------------------------------------------------------------------!
		LDU = LD2U                                                       !
		LDL = LD2L                                                       !
		LDS = LD2S                                                       !
		LDQ = LD2Q                                                       !
		LDC = LD2C                                                       !
	!----------------------------------------------------------------------!
		LE=LS
		DLE=DLS
	END IF
!
   60 SUM = 0.0E0
      ALAST = 0.0E0
      AREA=0.0E0
!
!    EQUATION  5
!
      DD = SINVW*SINVW + COSVW*COSVW*COSI2
      IF (DD .LE. 1.0E-06)  DD=0.0
      DD = DD*RV*RV
      D = SQRT(ABS(DD))
      R22 = R2*R2
!
!    EQUATION 17
!
      GAMN = 90.01E0*RAD
      DGAMA = DGAM*RAD
      DGM = DGAMA/2.0E0
      RK = 0.0E0
      GAM = 0.0E0

	
      
   50 GAM = GAM + DGAMA
!       HAS LIMIT OF INTEGRATION BEEN REACHED
      IF (GAM - GAMN)              48,48,49
!
   48 RR = R1*SIN(GAM)
      R12 = RR*RR
!
      AA = 0.0E0
!       ARE THE PROJECTED DISKS CONCENTRIC
      IF (D)                       405,406,405
  406 IF (RR - R2)                 230,230,403
  403 IF (RK - R2)                 404, 49, 49
  404 AA = PI*R22
      GO TO 215
!       TEST FOR NO ECLIPSE
  405 IF (D-R1-R2)                 240,216,216
  216 SUM = 0.0E0
      GO TO 49
!       DECIDE WHICH AREA EQUATIONS FOR NON-CONCENTRIC ECLIPSE
  240 IF (D-RR-R2)                 245,215,215
  245 IF (D-R2+RR)                 230,230,250
  250 IF (R1-R2)                   255,255,280
  255 IF (DD-R22+R12)              205,210,210
  280 IF (D-RR+R2)                 290,260,260
  260 IF (RR-R2)                   255,255,265
  265 IF (DD-R12+R22)              270,210,210

!	
!    EQUATION 12
!
  270 S1 = ABS((R12 - R22 - DD)*0.5E0/D)
      A1 = ABS(R2-S1)
      B2 = ABS(RR-S1-D  )
      AA=PI*R22-(R22*ACOS((R2-A1)/R2) &
        - (R2-A1)*SQRT(2.0E0*R2*A1-A1*A1)) &
        +R12*ACOS((RR-B2)/RR)-(RR-B2)*SQRT(2.0E0*RR*B2-B2*B2)
      GO TO 215
!
  290 IF (R1 - R2 - D)             260,260,295
  295 IF (RK - R2 - D)             300,215,215
  300 RR = R2 + D
      R12 = RR*RR
      GAMN = 0.0E0
      GO TO 260
!
  230 AA = PI*R12
      GO TO 215
!
!    EQUATION 10
!
  205 S = ABS((R12 - R22 + DD)*0.5E0/D)
      A = ABS(RR-S)
      B1 = ABS(R2-S-D)
      A1 = R12*ACOS((RR-A)/RR) - (RR-A)*SQRT(2.0E0*RR*A - A*A)
      AB1 = R22*ACOS((R2-B1)/R2) - (R2-B1)*SQRT(2.0E0*R2*B1-B1*B1)
      AA = PI*R12 - A1 + AB1
      GO TO 215
!
!    EQUATION 1
!
  210 S = ABS((R12 - R22 + DD)*0.5E0/D)
      A = ABS(RR-S)
      B = ABS(S-D+R2)
      A1 = R12*ACOS((RR-A)/RR) - (RR-A)*SQRT(2.0E0*RR*A - A*A)
      AA1 = R22*ACOS((R2-B)/R2) - (R2-B)*SQRT(2.0E0*R2*B - B*B)
      AA = A1 + AA1
!
  215 DAREA = AA - ALAST
!----------------------------------------------------------------------!
! JKT modification (10/9/2006). The removed line was:                  !
!     SUM = SUM + DAREA*(1.0E0  - UU + UU*COS(GAM-DGM))                !
!----------------------------------------------------------------------!
      COSGAM = cos(GAM-DGM)                                            !
      SUM = SUM + DAREA*(1.0 - LDU*(1.0-COSGAM)  &                     !
               - LDL*COSGAM*log(COSGAM) - LDS*(1.0-sqrt(COSGAM))  &    !
              - LDQ*(1.0-COSGAM)**2 - LDC*(1.0-COSGAM)**3)             !
!----------------------------------------------------------------------!
      ALAST = AA
      AREA = AREA + DAREA
!
      RK = RR
      GO TO 50
!
!       LIGHT LOSS FROM ECLIPSE
!
   49 ADISK = PI*R1*R1
!----------------------------------------------------------------------!
! JKT modification (10/9/2006).  See 1992A+A...259..227D for more info.!
! The removed line was:           ALPHA = SUM/(ADISK*(1.0E0-UU/3.0E0)) !
!----------------------------------------------------------------------!
      ALPHA = 1.0 - LDU/3.0 + LDL*2.0/9.0 - LDS/5.0 - LDQ/6.0 - LDC/10.0
      ALPHA = SUM/(ADISK*ALPHA)                                        !
!----------------------------------------------------------------------!
      LECL = ALPHA*LE
      AREA = AREA/ADISK
      REFL=DLP+DLS-AREA*DLE
!
!       THEORETICAL INTENSITY WITH THIRD LIGHT AND QUADRATURE
!       SCALE FACTOR APPLIED
!
!----------------------------------------------------------------------!
! This is the original line from EBOP:
!----------------------------------------------------------------------!
!      FLITE = ((LP+LS-LECL+REFL)*(1.0E0-EL)+EL)*SFACT
!----------------------------------------------------------------------!

      LP = LP * LPMULT               ! sine/poly applied to star 1 light
      LS = LS * LSMULT               ! sine/poly applied to star 2 light
      FLITE = ((LP+LS-LECL+REFL)*(1.0E0-EL)+EL)
      FMAG = -2.5d0 * log10(FLITE) + SFACT

      LP = LP * (1.0E0-EL)           ! account for third light *AFTER*
      LS = LS * (1.0E0-EL)           ! FLITE and FMAG have been found

	END SUBROUTINE LIGHT

!=======================================================================
       SUBROUTINE BIAX (R,Q,A,B,EPS)
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: R,Q
      DOUBLE PRECISION, INTENT(OUT) :: A,B,EPS
		DOUBLE PRECISION :: q13,fq
            ! EBOP subroutine to calculate biaxial ellipsoid dimensions
            ! and oblateness for each star after Chandrasekhar (1933).

      if ( Q <= 0.0 )  then
        A = R
        B = R
        EPS = 0.0
      else
        A = R * ( 1.0 + (1.0 + 7.0*Q)/6.0 * R**3.0)
        B = R * ( 1.0 + (1.0 - 2.0*Q)/6.0 * R**3.0)
!  		write(*,*) "b1",R,Q,A,B,EPS
        EPS = (A - B) / A
        B=( (1.0 - EPS) * R**3.0) ** (1.0/3.0)
        A = B / (1.0 - EPS)
!  		write(*,*) "b2",R,Q,A,B,EPS
      end if

! 	  q13 = (Q)**(1.d0/3.d0)
!       fq = 0.49d0*q13*q13/(0.6d0*q13*q13+LOG(1.d0+q13))
!       B= fq
! 	  A=0.0
! 	  EPS=0.0


      END SUBROUTINE BIAX
!=======================================================================

       SUBROUTINE GETEW (ECOSW,ESINW,E,W)
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: ECOSW,ESINW
      DOUBLE PRECISION, INTENT(OUT) :: E,W
            ! EBOP subroutine to calculate e and w from e(cos)w e(sin)w

      if ( ECOSW==0.0  .and.  ESINW==0.0 ) then
        E = 0.0
        W = 0.0
      else
        W = atan2( ESINW,ECOSW )
        E = sqrt( ESINW*ESINW + ECOSW*ECOSW )
        W = W * 180.0 / 3.1415926536
      end if

      END SUBROUTINE GETEW
!=======================================================================
      DOUBLE PRECISION FUNCTION GETPHASE (HJD,PERIOD,TZERO)
            ! Returns phase from given time and orbital ephemeris
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: HJD,PERIOD,TZERO

       GETPHASE = (HJD - TZERO) / PERIOD
       GETPHASE = GETPHASE - int(GETPHASE)
       if ( GETPHASE < 0.0d0 ) GETPHASE = GETPHASE + 1.0d0

      END FUNCTION GETPHASE
!=======================================================================
       DOUBLE PRECISION FUNCTION GETMIN (TZERO,PERIOD,ECC,OMEGA,CYCLE)
      USE consts
            ! Returns time of minimum for given cycle and ephemeris.  If
            ! the orbit is circular thenthe cycle number is used without
            ! restriction so can refer to any phase. If the orbit is ec-
            ! centric then the cycle number should be integer (indicates
            ! primary minimum) or half-integer (secondary minimum).
      IMPLICIT NONE
      DOUBLE PRECISION,INTENT(IN) :: TZERO,PERIOD           ! IN: reference time, orbital period
      DOUBLE PRECISION,INTENT(IN) :: ECC,OMEGA              ! IN: orbital (e,w) or (ecosw,esinw)
      DOUBLE PRECISION,INTENT(IN) :: CYCLE                  ! IN: cycle number of minimum to use
      DOUBLE PRECISION :: E,W                    ! LOCAL: eccentricity and peri.long.
      DOUBLE PRECISION :: THETA,EE,TANEE         ! LOCAL: true and eccentric anomaly
      DOUBLE PRECISION :: PSEP                   ! LOCAL: phase diff between minima
      DOUBLE PRECISION :: TINY        ! LOCAL: useful variables

      TINY = 1.0d-6

            ! First must deal with the possibility that e and omega are
            ! actually e*cos(omega) and e*sin(omega)

      if ( ECC > 9.0d0 ) then
        E = ECC - 10.0d0
        W = OMEGA
      else
        E = sqrt( OMEGA*OMEGA + ECC*ECC )
	W = atan2( ECC,OMEGA )
      end if

            ! If orbit is circular then simply use the orbital ephemeris
            ! If orbit is eccentric then must calculate the phase diffe-
            ! rence between two successive primary and secondary minima.

      if ( abs(E) < TINY ) then
        GETMIN = TZERO  +  PERIOD * CYCLE
      else
        THETA = 3.0d0*PI/2.0d0 - W
        TANEE = sqrt( (1.0d0-E) / (1.0d0+E) ) * tan(THETA/2.0d0)
        EE = atan(TANEE) * 2.0d0
        PSEP = (EE - E*sin(EE)) / (2.0d0 * PI)
        if ( PSEP < 0.0d0 ) PSEP = PSEP + 1.0d0

        if ( mod(abs(CYCLE),1.0d0) < TINY ) then       ! primary minimum
          GETMIN = TZERO + PERIOD * CYCLE
        else                                         ! secondary minimum
          GETMIN = TZERO + PERIOD * int(CYCLE)
          if ( CYCLE < 1.0d0 ) GETMIN = GETMIN - PERIOD
          GETMIN = GETMIN + PERIOD*PSEP
        end if
      end if

      END FUNCTION GETMIN
!=======================================================================

END MODULE jktebop
