!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	MODULE extinction
	IMPLICIT NONE
	CONTAINS
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Takes the distance and spatial grid point and returns the distance in pc given the extinction in LOS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	DOUBLE PRECISION FUNCTION distanceCalc(dmax,l0deg,b0deg,nlbsub,col,dlong,dlat,extType)
	USE consts
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN) :: dmax,col,dlong,dlat
	INTEGER,INTENT(IN) :: nlbsub
	DOUBLE PRECISION,INTENT(IN),DIMENSION(1:nlbsub) :: l0deg
	DOUBLE PRECISION,INTENT(IN),DIMENSION(1:nlbsub) :: b0deg
	CHARACTER(len=1),INTENT(IN) :: extType

	INTEGER :: jmax,it,mm,nn
	DOUBLE PRECISION :: rdmax,rdmx,rdmx1,rdmx2,avg
	DOUBLE PRECISION, DIMENSION(1:nlbsub) :: fl0
	DOUBLE PRECISION, DIMENSION(1:nlbsub,1:nlbsub) :: fl0b0
	DOUBLE PRECISION :: dd

	fl0=0.d0
	fl0b0=0.d0
	rdmax = dmax/1000.d0
	rdmx1 = 0.d0
	rdmx2 = rdmax
	it = 1
	DO
		rdmx = (rdmx1+rdmx2)/2.0
		DO mm = 1, nlbsub
			DO nn = 1, nlbsub
				fl0b0(mm,nn)=extinct(l0deg(mm),b0deg(nn),rdmx,extType,col)
			IF (fl0b0(mm,nn) < 0.d0) THEN
				fl0b0(mm,nn)=extinct(l0deg(mm),b0deg(nn),2.d0,extType,col)
			END IF
			END DO
		END DO
		DO mm = 1, nlbsub-1
			fl0(mm) = 0.d0
			DO nn = 1, nlbsub-1
			fl0(mm) = fl0(mm)+(COS(b0deg(nn)*pi/180.0)*fl0b0(mm,nn) &
				+COS(b0deg(nn+1)*pi/180.0)*fl0b0(mm,nn+1)) &
				*0.5d0*dlat/DBLE(nlbsub)
			END DO
		END DO
		avg = 0.0
		DO mm = 1, nlbsub-1
			avg = avg + (fl0(mm)+fl0(mm+1)) &
			*0.5d0*dlong/DBLE(nlbsub)
		END DO
		avg = avg/(((l0deg(nlbsub)-l0deg(1))*pi/180.0) &
			*(SIN(b0deg(nlbsub)*pi/180.0)-SIN(b0deg(1)*pi/180.0)))
		dd = rdmax/10.0**(avg/5.0)
		IF (rdmx < dd) THEN
			rdmx1 = rdmx
		ELSE
			rdmx2 = rdmx
		END IF
		IF (ABS(rdmx2 - rdmx1) < 0.001) EXIT
		it = it + 1
	END DO

	distanceCalc=1000.d0*((rdmx2+rdmx1)/2.0)

	RETURN
	END FUNCTION distanceCalc

!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	DOUBLE PRECISION FUNCTION extinct(l,b,d,extType,col)
	USE extinctionHakkila
	USE extinctionDrimmel
	USE extinctionKepler
	USE consts
	IMPLICIT NONE
	!Provides wrapper around the different extinction routines
	DOUBLE PRECISION,INTENT(IN) :: l,b,d,col
	CHARACTER(len=1),INTENT(IN) :: extType
	
	SELECT CASE(extType)
	CASE('h')
	!Hakkila Extinction
		extinct=extinctHakkila(l,b,d)
	CASE('k')
	!Kepler Extinction
		extinct=extinctKepler(b*pi/180.d0,d*1000.0)
	CASE('d')
	!Drimmel Extinction
		extinct=extinctDrimmel(l,b,d)
	CASE DEFAULT
		write(0,*) "Bad extinction letter=",extType
		stop
	END SELECT

	extinct=extinct*col

	END FUNCTION extinct

END MODULE extinction
	