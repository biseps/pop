!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE rlof
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         CONTAINS
         SUBROUTINE rloftp(s,b,zpars)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Procedure to determine the type of Roche lobe overflow               c
! Bart Willems, 23/11/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

         USE precision
         USE dtypes
         USE dshare
         USE params
         USE bfuncs
         IMPLICIT NONE

         DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars
         TYPE (bnq), INTENT(INOUT) :: b
         TYPE (stq), DIMENSION(2), INTENT(IN) :: s

         INTEGER :: n,obkw,it
         DOUBLE PRECISION :: ztL,ztad,zteq,qcdd
         DOUBLE PRECISION :: mcm,m1,m143,dm,dma,x,q

! Initialize variables
         gamma = 1.d0
         it = 0

         n = b%kw/10

		!We want mass transfer from currently less massive component
		if(b%kw==30) THEN
			if(s(1)%mt>s(2)%mt) then
				n=2
			else
				n=1
			end if
		end if

! Mass ratio
         q = s(n)%mt/s(3-n)%mt

! Adiabatic and thermal R-M exponent
         SELECT CASE (s(n)%kw)
         CASE (0,1)
            IF (s(n)%mt < 0.75d0) THEN ! Fully convective stars
               CALL tzetas0(s(n)%mt,ztad,zteq)
!               ztad = -1.d0/3.d0
!               zteq = 1.d0
            ELSE
               CALL tzetas(s(n)%mt,s(n)%r,ztad,zteq,qcdd)
            END IF
         CASE (2)
            CALL tzetas(s(n)%mt,s(n)%r,ztad,zteq,qcdd)
         CASE (3)
            x = 0.3d0          ! Hurley et al. (2000), Eq. (48), Z=0.02
            mcm = s(n)%mc/s(n)%mt
            ztad = -x+2.d0*mcm**5 ! Hurley et al. (2001), Eq. (56)
            IF (s(n)%mt < 1.5d0) THEN
               zteq = 0.d0
            ELSE
               zteq = -0.24d0
            END IF
         CASE (4)
            ztad = 15.d0
            zteq = 15.d0
         CASE (5,6)
            x = 0.33d0        ! Hurley et al. (2000), Eq. (74), Z=0.02
            mcm = s(n)%mc/s(n)%mt
            ztad = -x+2.d0*mcm**5
            zteq = -10.d0
         CASE (7,8,9)
            x = 0.33d0
            mcm = s(n)%mc/s(n)%mt
            ztad = -x+2.d0*mcm**5
            zteq = 0.d0
         CASE (10,11,12)
            m1 = s(n)%mt/1.433d0 ! Mass in units of Chandrasekhar mass
            m143 = m1**(4.d0/3.d0)
            ztad = -(1.d0/3.d0)*(1.d0+m143)/(1.d0-m143)
            zteq = ztad
         CASE DEFAULT
            warn = 3
            RETURN
         END SELECT

         loop: DO
            it = it + 1
            IF (it > 10) THEN
               IF ((b%kw - 10*n == 2).AND.(obkw - 10*n == 3)) THEN
                  b%kw = 10*n + 3
                  EXIT loop
               ELSE IF ((b%kw - 10*n == 3).AND.(obkw - 10*n == 2)) THEN
				!Shouldnt this be +2 as we want transistion from Nuc to themral?
                  b%kw = 10*n + 2
                  EXIT loop
               ELSE
				  if(b%kw==30) EXIT loop
                  b%kw = 10*n
                  warn = 2
                  EXIT loop
               END IF
            END IF
            obkw = b%kw
            ztL = rmlob(s(n)%mt,s(3-n)%mt) ! Roche-lobe index
            IF (ztad < ztL) THEN
               b%kw = 10*n + 1 ! Dynamical mass transfer
            ELSE IF ((zteq < ztL).AND.(ztL < ztad)) THEN
               b%kw = 10*n + 2  ! Thermal mass transfer
            ELSE IF ((ztL < zteq).AND.(ztL < ztad)) THEN
               b%kw = 10*n + 3  ! Nuclear mass transfer
            ELSE
               b%kw = 10*n
               warn = 4
               EXIT loop
            ENDIF
            IF (s(n)%kw < 10) THEN
               IF (b%kw - 10*n == 2) THEN
                  IF (q < 5.d0/6.d0) b%kw = 10*n + 3
               END IF
            END IF
            IF (b%kw == obkw) EXIT loop
  			IF(obkw==30) b%kw=30
            dm = mtrans(b%kw,s(n)%kw,s(n)%mt,s(n)%r, &
                 s(n)%lum,s(n)%mc,s(n)%rl) ! Mass-transfer rate
            dma = maccr(s(n)%kw,dm,s(3-n)%kw,s(3-n)%mass,s(3-n)%mt, &
                 s(3-n)%r,s(3-n)%lum,s(3-n)%mc,zpars,b%kw) ! Mass-accretion r
            gamma = 1.d0 + dma/dm
         END DO loop

         IF (b%kw - 10*n == 1) THEN
            SELECT CASE (s(n)%kw)
            CASE (2:6,8:9)
               b%kw = 10*n + 4 ! Common envelope
               IF (warn == 6) warn = 0
            CASE DEFAULT
               warn = 7
            END SELECT
         END IF

         IF (warn > 0) RETURN

         IF ((s(n)%kw == 1).OR.(s(n)%kw == 2)) THEN
! Check for delayed dynamical instability
            IF (b%kw - 10*n == 2) THEN
!               q = s(n)%mt/s(3-n)%mt
!               qcdd = 2.5d0
               IF (q > qcdd) THEN
                  SELECT CASE (s(n)%kw)
                  CASE (2:6,8:9)
                     b%kw = 10*n + 4 ! Common envelope
                  CASE DEFAULT
                     b%kw = 10*n + 1 ! Dynamical mass transfer
                     warn = 7
                  END SELECT
               END IF
            END IF
         END IF

         RETURN
         END SUBROUTINE rloftp
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE transfer(n,os,s,b,dt,zpars)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Procedure to transfer mass from one component to the other           c
! Bart Willems, 28/11/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE dtypes
         USE dshare
         USE params
         USE bfuncs
         USE comenv
         USE output
         IMPLICIT NONE

         INTEGER, INTENT(IN) :: n
         DOUBLE PRECISION, INTENT(IN) :: dt
         DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars
         TYPE (bnq), INTENT(INOUT) :: b
         TYPE (stq), DIMENSION(2), INTENT(INOUT) :: os,s

         DOUBLE PRECISION :: q1,oq1
         DOUBLE PRECISION :: mb,dmb
         DOUBLE PRECISION :: djej

! Total mass of the binary before mass transfer
         mb = s(1)%mt+s(2)%mt

! 		write(*,*) b%kw,s(n)%tphys
! Mass-transfer rate
         s(n)%dmrl = mtrans(b%kw,s(n)%kw,s(n)%mt,s(n)%r, &
              s(n)%lum,s(n)%mc,s(n)%rl)
! 		write(*,*) n,s(n)%dmrl,dt,s(n)%dmrl*dt,s(n)%mt,s(n)%tphys
         IF (ABS(s(n)%dmrl*dt) > s(n)%mt) warn = 10
         IF (warn > 0) RETURN
         s(n)%xldm = 0.d0


! Update stellar quantities and age of the star after mass transfer
         CALL update(s(n)%kw,s(n)%mass,s(n)%mt,s(n)%lum,s(n)%r, &
              s(n)%mc,s(n)%rc,s(n)%tphys,s(n)%epoch,s(n)%dmrl*dt, &
              s(n)%dmrl,zpars,s(3-n)%kw)

! Mass-accretion rate
         s(3-n)%dmrl = maccr(s(n)%kw,s(n)%dmrl,s(3-n)%kw,s(3-n)%mass, &
              s(3-n)%mt,s(3-n)%r,s(3-n)%lum,s(3-n)%mc,zpars,b%kw)
         IF (warn > 0) RETURN
! Mass-accretion luminosity
         s(3-n)%xldm = xrlum(s(n)%mt,s(3-n)%mt,s(3-n)%dmrl,s(3-n)%r,b%a)
! Update stellar quantities and age of the star after mass accretion
         CALL update(s(3-n)%kw,s(3-n)%mass,s(3-n)%mt,s(3-n)%lum, &
              s(3-n)%r,s(3-n)%mc,s(3-n)%rc,s(3-n)%tphys,s(3-n)%epoch, &
              s(3-n)%dmrl*dt,s(3-n)%dmrl,zpars,s(n)%kw)
         IF (s(3-n)%kw == 15) RETURN

! Mass lost from the binary
         gamma = 1.d0 + s(3-n)%dmrl/s(n)%dmrl
         dmb = gamma*s(n)%dmrl

! Change orbital angular momentum due to RLOF
         djej = nu*b%jorb*dmb/mb
         djej = djej*dt

         b%jorb = MAX(0.d0,b%jorb+djej)
         b%a = aorbit(b%jorb,b%ecc,s(1)%mt,s(2)%mt)
         b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)

! Check for spiral-in
         IF (b%a == 0.d0) THEN
!            b%kw = 44
            b%kw = 42
            warn = 99
            RETURN
         END IF

! Calculate the radii of the Roche lobes
         s(1)%rl = rlobe(b%a,s(1)%mt,s(2)%mt)
         s(2)%rl = rlobe(b%a,s(2)%mt,s(1)%mt)

! Check if binary is still in RLOF
         IF (b%kw - 10*n /= 2) THEN
            IF (s(n)%r < s(n)%rl) THEN
               b%kw = 0
               RETURN
            ELSE IF (s(3-n)%r > s(3-n)%rl) THEN
               SELECT CASE (s(n)%kw)
               CASE (2:6,8:9)
                  b%kw = 10*n + 4  ! Common envelope
                  CALL ce(s,b,zpars)
                  RETURN
               CASE DEFAULT
                  b%kw = 30
				  if(contactExit(s,b).eqv. .TRUE.) THEN
! 					write(*,*) "1"
					RETURN
				end if
               END SELECT
            END IF
         END IF

! Redetermine the type of RLOF when the mass ratio changes
         oq1 = (os(n)%mt/os(3-n)%mt)-5.d0/6.d0
         q1 = (s(n)%mt/s(3-n)%mt)-5.d0/6.d0
         IF (oq1*q1 < 0.d0) THEN
            nu = s(n)%mt/s(3-n)%mt
            CALL rloftp(s,b,zpars)
            IF (b%kw - 10*n == 4) CALL ce(s,b,zpars)
         ELSE IF ((b%kw - 10*n == 2).AND.(q1 < 0.d0)) THEN
            b%kw = 10*n + 3
         END IF

         RETURN
         END SUBROUTINE transfer
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      DOUBLE PRECISION FUNCTION mtrans(bkw,kw,mt,r,lum,mc,rl)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Function to calculate the mass-transfer rate
! Bart Willems, 07/10/01

      USE precision
      USE params
      USE dshare
      USE bfuncs
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: bkw,kw
      DOUBLE PRECISION, INTENT(IN) :: mt,r,lum,mc,rl
      DOUBLE PRECISION :: dm,dmd,dmth,dmnu

! Nuclear Mdot
      dmnu = nmtrans(kw,mt,r,rl)
! Thermal Mdot
      dmth = mt/thk(kw,mt,rl,mc,lum)
! Dynamical Mdot
      dmd = mt/tdyn(mt,rl)
! 		write(*,*) dmd,dmth,dmnu

      SELECT CASE (bkw)
      CASE (11,21)
         dm = MIN(dmd,dmnu)
      CASE (12,22)
         SELECT CASE (kw)
         CASE(2:6,8:9)
            dm = MIN(dmth,dmnu)
         CASE DEFAULT
            dm = MIN(dmd,dmnu)
         END SELECT
      CASE (13,23)
         dm = dmnu
      CASE (14,15,24,25)
         dm = 0.d0              ! Mdot not relevant for CE
	  CASE (30) !Contact assume thermal timescale
! 		 write(*,*) dmd,dmth,dmnu
! 		write(*,*) dmth,dmd,dmnu
   		dm=MIN(dmth,MIN(dmd,dmnu))
! 		write(*,*) dmth,dmd,dmnu,dm
! 		dm=10**-10
     CASE DEFAULT
      	 WRITE(0,*) 'ERROR rlof.f90 mtrans 295'
         WRITE(0,*) 'ILLEGAL CALL TO MTRANS'
         WRITE(0,*) bkw,kw,mt,r,rl
	     WRITE(0,*) 'END ERROR'
!         CALL exit(0)
         STOP 
      END SELECT

      IF (dm < 0.d0) THEN
         mtrans = -1.0d-32
      ELSE
         mtrans = -dm
      END IF

! 		write(*,*) mt,dmd,dmth,dmnu,mtrans,r/rl

      RETURN
      END FUNCTION mtrans
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      DOUBLE PRECISION FUNCTION nmtrans(kw,m,r,rl)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Function to evaluate the nuclear mass transfer rate in solar
! masses per year
! Bart Willems, 15/12/00

      USE precision
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: kw
      DOUBLE PRECISION, INTENT(IN) :: m,r,rl
      DOUBLE PRECISION :: f

      f = 3.d-6*(MIN(m,5.d0))**2
      IF (kw >= 10) f = f*1.d3/MAX(r,1.d-4)
      nmtrans = f*(LOG(r/rl))**3

      RETURN
      END FUNCTION nmtrans
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      DOUBLE PRECISION FUNCTION maccr(kw1,dm,kw2,mass,mt,r,lum,mc, &
           zpars,bkw)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Function to calculate the mass-accretion rate
! Bart Willems, 07/10/01

      USE precision
      USE dshare
      USE params
      USE bfuncs
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: kw1,kw2,bkw
      DOUBLE PRECISION, INTENT(IN) :: dm,mass,mt,r,lum,mc
      DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars
      DOUBLE PRECISION :: dmedd,dma
      DOUBLE PRECISION :: tau1,tau2

! 		write(*,*) kw2,kw1
      IF (cflag > 0) THEN                  ! Non-conservative mass trans
         SELECT CASE (kw2)
         CASE(0,1,2,4)
            tau1 = mt/ABS(dm)
            tau2 = thk(kw2,mt,r,mc,lum)
            dma = -MIN(10.d0*tau1/tau2,1.d0)*dm
         CASE(3,5,6)
            dma = -dm
         CASE(7)
            SELECT CASE (kw1)
            CASE(7:10)
               tau1 = mt/ABS(dm)
               tau2 = thk(kw2,mt,r,mc,lum)
               dma = -MIN(10.d0*tau1/tau2,1.d0)*dm
            CASE DEFAULT
               tau1 = mt/ABS(dm)
               tau2 = thk(kw2,mt,r,mc,lum)
               dma = -MIN(10.d0*tau1/tau2,1.d0)*dm
               warn = 6
            END SELECT
         CASE(8,9)
            SELECT CASE (kw1)
            CASE(7:10)
               dma = -dm
            CASE DEFAULT
               dma = -dm
               warn = 6
            END SELECT
         CASE(10,11,12)
            SELECT CASE (kw1)
            CASE(0:6)
               IF (ABS(dm) < wdsal) THEN
                  dma = -gnov*dm
               ELSE IF (ABS(dm) < wdsah) THEN
                  dma = -dm*(1.d0-gwd)
               ELSE
                  dma = -dm*(1.d0-gwd)
               END IF
            CASE(7:10)
               dma = -dm*(1.d0-gwd)
            CASE DEFAULT
               dma = -dm*(1.d0-gwd)
               IF (gwd < 1.d0) warn = 6
            END SELECT
            IF (ewd == 0) THEN  ! Impose Eddington limit
               dmedd = edding(r,zpars)
               dma = MIN(dma,dmedd)
            END IF
         CASE(13)
! Allow only dnsm solar masses to be accreted onto a NS
            IF (mt > mass + dnsm) THEN
               dma = 0.d0
            ELSE
               IF (gns < 0.d0) THEN
                  tau1 = mt/ABS(dm)
                  dma = dnsm/tau1
               ELSE
                  dma = -dm*(1.d0-gns)
               END IF
            END IF
            IF (ens == 0) THEN ! Impose Eddington limit
               dmedd = edding(r,zpars)
               dma = MIN(dma,dmedd)
            END IF
         CASE(14)
            dma = -dm*(1.d0-gbh)
            IF (ebh == 0) THEN ! Impose Eddington limit
               dmedd = edding(r,zpars)
               dma = MIN(dma,dmedd)
            END IF
         CASE DEFAULT
            warn = 6
            dma = 0.d0
      END SELECT
      ELSE   ! Fully conservative mass transfer
         dma = -dm
      END IF

	if(bkw==30) THEN
		dma=-dm !Contact systems assume fully conserative
	end if


      maccr = dma

      RETURN
      END FUNCTION maccr
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE tzetas(m,r,zad,zth,qcdd)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Determine the adiabatic and thermal radius-mass exponents by         c
! interpolation of the tables given by Hjellming (1989)                c
! The tables are only valid for stars with Z=0.02 that are on the      c
! main sequence, the Hertzsprung gap, and the red-giant branch.        c
! Bart Willems, 05/07/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE precision
      USE extras
      IMPLICIT NONE

      INTEGER :: lo,hi
      INTEGER :: j,k1,k2

      DOUBLE PRECISION, INTENT(IN) :: m,r
      DOUBLE PRECISION, INTENT(OUT) :: zad,zth,qcdd
      DOUBLE PRECISION :: lgr,rc
      DOUBLE PRECISION, DIMENSION(2) :: izad,izth,iqcdd
      DOUBLE PRECISION, DIMENSION(10) :: mm = (/0.75,0.9,1.25,1.5,2.0, &
           3.0,5.0,7.5,10.0,20.0/)
      DOUBLE PRECISION, DIMENSION(280) :: lgrr = (/ &
           -0.1783,-0.1466,-0.1093,-0.0330,-0.0041,0.0259,0.0558, &
           0.0829,0.1075,0.1578,0.1882,0.2611,0.3421,0.4807,0.5845, &
           0.6964,0.9052,1.1696,1.2867,1.3867,1.4867,1.5867,1.6867, &
           1.7613,1.8613,2.0500,2.1599,2.1837, &
           -0.0928,-0.0561,-0.0119,0.0487,0.1162,0.1683,0.2023,0.2325, &
           0.2698,0.3291,0.4057,0.4922,0.6954,0.8082,0.9108,1.1388, &
           1.4061,1.5114,1.5514,1.6114,1.6514,1.7114,1.8114,1.8514, &
           1.9114,2.0646,2.1246,2.1542, &
           0.0959,0.1528,0.1942,0.2260,0.2419,0.2783,0.3224,0.3841, &
           0.3935,0.3981,0.4020,0.4102,0.4277,0.4632,0.5132,0.5687, &
           0.6291,0.6963,0.7679,0.9210,1.2244,1.3244,1.4244,1.5244, &
           1.6695,1.8695,2.1015,2.1235, &
           0.1321,0.2181,0.2952,0.3273,0.3478,0.3478,0.3478,0.3478, &
           0.3478,0.3669,0.4177,0.4530,0.4856,0.5152,0.5178,0.5178, &
           0.5178,0.5323,0.5677,0.6642,0.9003,1.0376,1.3854,1.5484, &
           1.7133,1.8791,2.0401,2.0848, &
           0.1930,0.2787,0.3735,0.4360,0.4360,0.4597,0.5200,0.5819, &
           0.6445,0.6963,0.7196,0.7381,0.7381,0.7381,0.7381,0.7381, &
           0.7381,0.7593,0.8075,0.8561,0.9549,1.2327,1.2800,1.4953, &
           1.5689,1.6689,1.7973,1.8125, &
           0.2912,0.3460,0.4052,0.4704,0.5422,0.5440,0.5733,0.6568, &
           0.7474,0.8169,0.9255,1.0329,1.0552,1.0611,1.0611,1.0611, &
           1.0611,1.0611,1.0762,1.1188,1.1749,1.2302,1.2821,1.2921, &
           1.3882,1.3995,1.4095,1.4612, &
           0.4199,0.5141,0.5656,0.6207,0.6806,0.6806,0.6878,0.7790, &
           0.9096,1.1307,1.2803,1.4660,1.5430,1.5487,1.5554,1.5667, &
           1.5911,1.6493,1.7103,1.7437,1.7539,1.7639,1.7739,1.7839, &
           1.7939,1.8039,1.8139,1.8191, &
           0.5227,0.5911,0.6686,0.7567,0.7883,0.8263,0.9069,1.0367, &
           1.1272,1.3621,1.6956,1.9492,1.9703,1.9886,2.0384,2.1352, &
           2.1395,2.1452,2.1495,2.1552,2.1595,2.1652,2.1695,2.1752, &
           2.1795,2.1825,2.1852,2.1864, &
           0.5949,0.6746,0.7679,0.8780,0.9201,1.0995,1.1997,1.3282, &
           1.6279,1.9275,2.1896,2.2465,2.2712,2.4242,2.4262,2.4282, &
           2.4302,2.4313,2.4322,2.4342,2.4382,2.4413,2.4442,2.4461, &
           2.4481,2.4513,2.4526,2.4541, &
           0.7641,0.8417,0.9535,1.0723,1.1266,1.1266,1.1349,1.3130, &
           1.4943,1.7048,1.8028,1.9486,2.0153,2.1621,2.3786,2.5447, &
           2.6387,2.7918,2.8888,2.9321,2.9558,2.9658,2.9717,2.9758, &
           2.9772,2.9858,2.9872,2.9881/)
      DOUBLE PRECISION, DIMENSION(280) :: zad0 = (/ &
           0.73E+00,0.94E+00,0.11E+01,0.13E+01,0.13E+01,0.15E+01, &
           0.15E+01,0.96E+00,0.76E+00,0.50E+00,0.34E+00,0.19E+00, &
           0.11E+00,0.90E-01,0.80E-01,0.70E-01,0.11E+00,0.16E+00, &
           0.16E+00,0.16E+00,0.16E+00,0.16E+00,0.16E+00,0.16E+00, &
           0.16E+00,0.16E+00,0.16E+00,0.16E+00, &
           0.19E+01,0.27E+01,0.34E+01,0.33E+01,0.26E+01,0.18E+01, &
           0.11E+01,0.66E+00,0.33E+00,0.16E+00,0.50E-01,0.30E-01, &
           0.30E-01,0.30E-01,0.30E-01,0.40E-01,0.40E-01,0.40E-01, &
           0.40E-01,0.40E-01,0.40E-01,0.40E-01,0.40E-01,0.40E-01, &
           0.40E-01,0.40E-01,0.40E-01,0.40E-01, &
           0.20E+01,0.20E+01,0.20E+01,0.20E+01,0.20E+01,0.48E+02, &
           0.27E+02,0.51E+01,0.22E+01,0.10E+01,0.79E+00,0.35E+00, &
           0.25E+00,0.40E-01,-0.30E-01,-0.50E-01,-0.50E-01,-0.60E-01, &
           -0.70E-01,-0.80E-01,-0.80E-01,-0.80E-01,-0.80E-01,-0.80E-01, &
           -0.80E-01,-0.80E-01,-0.80E-01,-0.80E-01, &
           0.20E+01,0.20E+01,0.20E+01,0.20E+01,0.20E+01,0.20E+01, &
           0.20E+01,0.20E+01,0.20E+01,0.23E+01,0.23E+01,0.43E+02, &
           0.26E+02,0.11E+02,0.34E+01,0.34E+01,0.34E+01,0.12E+00, &
           0.00E+00,-0.90E-01,-0.16E+00,-0.12E+00,-0.12E+00,-0.12E+00, &
           -0.12E+00,-0.12E+00,-0.12E+00,-0.12E+00, &
           0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.13E+02, &
           0.13E+02,0.13E+02,0.13E+02,0.13E+02,0.13E+02,-0.50E-01, &
           -0.50E-01,-0.50E-01,-0.16E+00,-0.18E+00,-0.18E+00,-0.19E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.70E+00,0.28E+00,0.60E-01,-0.60E-01,-0.11E+00,-0.11E+00, &
           -0.18E+00,-0.18E+00,-0.18E+00,-0.19E+00, &
           0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.82E+01,0.40E+01,0.23E+01,0.10E+01,0.38E+00,0.11E+00, &
           -0.40E-01,-0.90E-01,-0.90E-01,-0.90E-01,-0.90E-01,-0.90E-01, &
           -0.90E-01,-0.90E-01,-0.90E-01,-0.90E-01, &
           0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.55E+01,0.95E+00,0.95E+00,0.95E+00,0.95E+00,0.95E+00, &
           0.95E+00,0.95E+00,0.95E+00,0.95E+00,0.95E+00,0.95E+00, &
           0.95E+00,0.95E+00,0.95E+00,0.95E+00, &
           0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01,0.40E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01,0.23E+01, &
           0.23E+01,0.23E+01,0.23E+01,0.23E+01/)
      DOUBLE PRECISION, DIMENSION(280) :: zth0 = (/ &
           0.86E+00,0.87E+00,0.88E+00,0.91E+00,0.87E+00,0.82E+00, &
           0.78E+00,0.74E+00,0.70E+00,0.56E+00,0.48E+00,0.28E+00, &
           0.20E+00,0.70E-01,0.00E+00,-0.30E-01,-0.10E+00,-0.19E+00, &
           -0.19E+00,-0.19E+00,-0.19E+00,-0.19E+00,-0.19E+00,-0.19E+00, &
           -0.19E+00,-0.19E+00,-0.19E+00,-0.19E+00, &
           0.13E+01,0.13E+01,0.12E+01,0.12E+01,0.11E+01,0.10E+01, &
           0.88E+00,0.73E+00,0.55E+00,0.55E+00,0.55E+00,0.55E+00, &
           0.10E+00,0.40E-01,-0.10E-01,-0.17E+00,-0.22E+00,-0.22E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           0.84E+00,0.94E+00,0.10E+01,0.10E+01,0.11E+01,0.96E+00, &
           0.84E+00,0.11E+01,0.94E+00,0.89E+00,0.84E+00,0.71E+00, &
           0.44E+00,0.30E+00,0.11E+00,0.70E-01,0.10E-01,-0.20E-01, &
           -0.50E-01,-0.11E+00,-0.25E+00,-0.25E+00,-0.25E+00,-0.25E+00, &
           -0.25E+00,-0.25E+00,-0.25E+00,-0.25E+00, &
           0.78E+00,0.35E+00,0.35E+00,0.35E+00,0.38E+00,0.38E+00, &
           0.38E+00,0.38E+00,0.38E+00,-0.32E+00,-0.28E+00,0.00E+00, &
           0.53E+00,0.14E+01,0.12E+01,0.12E+01,0.12E+01,0.52E+00, &
           0.23E+00,0.10E-01,-0.10E+00,-0.16E+00,-0.29E+00,-0.29E+00, &
           -0.28E+00,-0.28E+00,-0.29E+00,-0.29E+00, &
           0.57E+00,0.36E+00,0.11E+00,-0.12E+00,-0.12E+00,-0.54E+00, &
           -0.59E+00,-0.86E+00,-0.15E+01,-0.14E+01,-0.32E+00,0.17E+01, &
           0.12E+00,0.12E+00,0.12E+00,0.12E+00,0.12E+00,0.12E+00, &
           0.12E+00,0.50E-01,-0.10E+00,-0.18E+00,-0.20E+00,-0.30E+00, &
           -0.30E+00,-0.30E+00,-0.30E+00,-0.30E+00, &
           0.59E+00,0.47E+00,0.31E+00,0.12E+00,-0.21E+00,-0.44E+00, &
           -0.64E+00,-0.16E+01,-0.24E+01,-0.24E+01,-0.42E+01,-0.42E+01, &
           -0.42E+01,0.35E+01,0.00E+00,0.00E+00,0.00E+00,0.00E+00, &
           0.00E+00,0.00E+00,-0.10E+00,-0.13E+00,-0.16E+00,-0.16E+00, &
           -0.15E+00,-0.19E+00,-0.19E+00,-0.19E+00, &
           0.60E+00,0.43E+00,0.28E+00,0.50E-01,-0.18E+00,-0.18E+00, &
           -0.37E+00,-0.13E+01,-0.27E+01,-0.60E+01,-0.87E+01,-0.12E+02, &
           -0.70E+01,-0.66E+01,-0.66E+01,-0.54E+01,-0.38E+01,0.00E+00, &
           -0.90E-01,-0.14E+00,-0.19E+00,-0.19E+00,-0.19E+00,-0.19E+00, &
           -0.19E+00,-0.19E+00,-0.19E+00,-0.19E+00, &
           0.59E+00,0.42E+00,0.21E+00,-0.70E-01,-0.44E+00,-0.44E+00, &
           -0.12E+01,-0.22E+01,-0.40E+01,-0.17E+02,-0.10E+01,-0.10E+01, &
           -0.75E+00,-0.26E+00,-0.26E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           0.59E+00,0.40E+00,0.11E+00,-0.32E+00,-0.57E+00,-0.18E+01, &
           -0.30E+01,-0.55E+01,-0.83E+01,-0.12E+02,-0.10E+01,0.10E-01, &
           0.94E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           -0.22E+00,-0.22E+00,-0.22E+00,-0.22E+00, &
           0.59E+00,0.38E+00,0.00E+00,-0.52E+00,-0.92E+00,-0.92E+00, &
           -0.92E+00,-0.10E+01,-0.16E+01,-0.23E+01,-0.31E+01,-0.44E+01, &
           -0.54E+01,-0.77E+01,-0.11E+02,-0.22E+02,-0.10E+01,-0.10E+01, &
           0.29E+01,0.82E+00,-0.16E+00,-0.16E+00,-0.16E+00,-0.16E+00, &
           -0.16E+00,-0.16E+00,-0.16E+00,-0.16E+00/)
      DOUBLE PRECISION, DIMENSION(280) :: qcdd0 = (/ &
           0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02, &
           0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02, &
           0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02, &
           0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02,0.10E+02, &
           0.10E+02,0.10E+02,0.10E+02,0.10E+02, &
           2.05,2.30,2.55,2.80,2.90,2.90,2.90,2.90, &
           2.90,2.90,2.90,2.90,2.90,2.90, &
           2.90,2.90,2.90,2.90,2.90,2.90, &
           2.90,2.90,2.90,2.90,2.90,2.90, &
           2.90,2.90, &
           2.90,3.15,3.25,3.25,3.28,3.72,3.85,3.40,3.40,3.40, &
           3.40,3.40,3.40,3.40,3.40,3.40, &
           3.40,3.40,3.40,3.40,3.40,3.40, &
           3.40,3.40,3.40,3.40,3.40,3.40, &
           2.85,3.15,3.40,3.47,3.52,3.52,3.52,3.52,3.52,3.83,3.99, &
           4.10,3.80,3.52,3.00,3.00,3.00,3.00,3.00, &
           3.00,3.00,3.00,3.00,3.00,3.00, &
           3.00,3.00,3.00, &
           2.70,2.97,3.25,3.42,3.42,3.69,3.90,4.05,4.15,3.73,3.55, &
           3.40,3.40,3.40,3.40,3.40,3.40,2.40,2.40,2.40, &
           2.40,2.40,2.40,2.40,2.40,2.40, &
           2.40,2.40, &
           2.52,2.67,2.85,3.02,3.20,3.37,3.44,3.65,3.79,3.90,3.90, &
           3.38,3.28,3.25,3.25,3.25,3.25,3.25,3.00,3.00, &
           3.00,3.00,3.00,3.00,3.00,3.00, &
           3.00,3.00, &
           2.27,2.54,2.67,2.70,2.95,2.95,3.12,3.32,3.26,3.15,3.10, &
           3.04,3.02,3.02,3.02,3.02,3.02,3.02, &
           3.02,3.02,3.02,3.02,3.02,3.02, &
           3.02,3.02,3.02,3.02, &
           2.10,2.28,2.47,2.68,2.85,3.00,3.13,3.33,3.47,3.67,3.67, &
           3.67,3.67,3.67,3.67,3.67,3.67,3.67, &
           3.67,3.67,3.67,3.67,3.67,3.67, &
           3.67,3.67,3.67,3.67, &
           1.95,2.17,2.42,2.67,2.87,3.21,3.40,3.47,3.62,1.72,0.054, &
           0.052,0.044,0.044,0.044,0.044,0.044,0.044,0.044,0.044, &
           0.044,0.044,0.044,0.044,0.044,0.044,0.044,0.044, &
           1.62,1.82,2.07,2.37,2.55,2.55,2.57,3.04,3.52,3.90,4.08, &
           4.37,4.5,4.5,4.5,4.5,4.5,4.5,4.5,4.5,4.5,4.5,4.5,4.5, &
           4.5,4.5,4.5,4.5/)

! Tables are in log10(r)
      lgr = LOG10(r)

      CALL locate(mm,10,m,j)
      IF (j == 0) THEN
         lo=1
         hi=28
         CALL locate(lgrr(lo:hi),28,lgr,k1)
         IF (k1 == 0) THEN
            k1 = lo
            zad = zad0(k1)
            zth = zth0(k1)
            qcdd = qcdd0(k1)
         ELSE IF (k1 == 28) THEN
            k1 = hi
            zad = zad0(k1)
            zth = zth0(k1)
            qcdd = qcdd0(k1)
         ELSE
            k1 = lo + k1 - 1
            rc = (zad0(k1+1)-zad0(k1))/(lgrr(k1+1)-lgrr(k1))
            zad = rc*(lgr-lgrr(k1))+zad0(k1)
            rc = (zth0(k1+1)-zth0(k1))/(lgrr(k1+1)-lgrr(k1))
            zth = rc*(lgr-lgrr(k1))+zth0(k1)
            rc = (qcdd0(k1+1)-qcdd0(k1))/(lgrr(k1+1)-lgrr(k1))
            qcdd = rc*(lgr-lgrr(k1))+qcdd0(k1)
         END IF
      ELSE IF (j == 10) THEN
         lo=253
         hi=280
         CALL locate(lgrr(lo:hi),28,lgr,k1)
         IF (k1 == 0) THEN
            k1 = lo
            zad = zad0(k1)
            zth = zth0(k1)
            qcdd = qcdd0(k1)
         ELSE IF (k1 == 28) THEN
            k1 = hi
            zad = zad0(k1)
            zth = zth0(k1)
            qcdd = qcdd0(k1)
         ELSE
            k1 = lo + k1 - 1
            rc = (zad0(k1+1)-zad0(k1))/(lgrr(k1+1)-lgrr(k1))
            zad = rc*(lgr-lgrr(k1))+zad0(k1)
            rc = (zth0(k1+1)-zth0(k1))/(lgrr(k1+1)-lgrr(k1))
            zth = rc*(lgr-lgrr(k1))+zth0(k1)
            rc = (qcdd0(k1+1)-qcdd0(k1))/(lgrr(k1+1)-lgrr(k1))
            qcdd = rc*(lgr-lgrr(k1))+qcdd0(k1)
         END IF
      ELSE
         lo=28*(j-1)+1
         hi=28*j
         CALL locate(lgrr(lo:hi),28,lgr,k1)
         IF (k1 == 0) THEN
            k1 = lo
            izad(1) = zad0(k1)
            izth(1) = zth0(k1)
            iqcdd(1) = qcdd0(k1)
         ELSE IF (k1 == 28) THEN
            k1 = hi
            izad(1) = zad0(k1)
            izth(1) = zth0(k1)
            iqcdd(1) = qcdd0(k1)
         ELSE
            k1 = lo + k1 - 1
            rc = (zad0(k1+1)-zad0(k1))/(lgrr(k1+1)-lgrr(k1))
            izad(1) = rc*(lgr-lgrr(k1))+zad0(k1)
            rc = (zth0(k1+1)-zth0(k1))/(lgrr(k1+1)-lgrr(k1))
            izth(1) = rc*(lgr-lgrr(k1))+zth0(k1)
            rc = (qcdd0(k1+1)-qcdd0(k1))/(lgrr(k1+1)-lgrr(k1))
            iqcdd(1) = rc*(lgr-lgrr(k1))+qcdd0(k1)
         END IF

         lo=lo+28
         hi=hi+28
         CALL locate(lgrr(lo:hi),28,lgr,k2)
         IF (k2 == 0) THEN
            k2 = lo
            izad(2) = zad0(k1)
            izth(2) = zth0(k1)
            iqcdd(2) = qcdd0(k1)
         ELSE IF (k2 == 28) THEN
            k2 = hi
            izad(2) = zad0(k1)
            izth(2) = zth0(k1)
            iqcdd(2) = qcdd0(k1)
         ELSE
            k2 = lo + k2 - 1
            rc = (zad0(k2+1)-zad0(k2))/(lgrr(k2+1)-lgrr(k2))
            izad(2) = rc*(lgr-lgrr(k2))+zad0(k2)
            rc = (zth0(k2+1)-zth0(k2))/(lgrr(k2+1)-lgrr(k2))
            izth(2) = rc*(lgr-lgrr(k2))+zth0(k2)
            rc = (qcdd0(k2+1)-qcdd0(k2))/(lgrr(k2+1)-lgrr(k2))
            iqcdd(2) = rc*(lgr-lgrr(k2))+qcdd0(k2)
         END IF

         rc = (izad(2)-izad(1))/(mm(j+1)-mm(j))
         zad = rc*(m-mm(j))+izad(1)
         rc = (izth(2)-izth(1))/(mm(j+1)-mm(j))
         zth = rc*(m-mm(j))+izth(1)
         rc = (iqcdd(2)-iqcdd(1))/(mm(j+1)-mm(j))
         qcdd = rc*(m-mm(j))+iqcdd(1)

      END IF

      RETURN
      END SUBROUTINE tzetas
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE tzetas0(m,zad,zth)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Determine the adiabatic and thermal radius-mass exponents for        c
! low-mass ZAMS stars by interpolation of the tables given by          c
! Hjellming (1989). The tables are only valid for stars with Z=0.02.   c
! Bart Willems, 03/08/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE precision
      USE extras
      IMPLICIT NONE

      INTEGER :: j
      DOUBLE PRECISION, INTENT(IN) :: m
      DOUBLE PRECISION, INTENT(OUT) :: zad,zth
      DOUBLE PRECISION :: rc
      DOUBLE PRECISION, DIMENSION(13) :: mm = (/0.25,0.30,0.34,0.36, &
           0.38,0.40,0.45,0.50,0.55,0.60,0.65,0.70,0.75/)
      DOUBLE PRECISION, DIMENSION(13) :: zad0 = (/-0.35,-0.35,-0.35, &
           -0.35,-0.35,-0.32,-0.25,-0.14,0.0,0.17,0.32,0.52,0.73/)
      DOUBLE PRECISION, DIMENSION(13) :: zth0 = (/0.72,0.70,0.70, &
           0.70,0.77,0.79,0.92,1.13,1.13,1.07,0.86,0.86,0.86/)

      CALL locate(mm,13,m,j)

      IF (j == 0) THEN
         zad = zad0(1)
         zth = zth0(1)
      ELSE IF (j == 13) THEN
         zad = zad0(13)
         zth = zth0(13)
      ELSE
         rc = (zad0(j+1)-zad0(j))/(mm(j+1)-mm(j))
         zad = rc*(m-mm(j))+zad0(j)
         rc = (zth0(j+1)-zth0(j))/(mm(j+1)-mm(j))
         zth = rc*(m-mm(j))+zth0(j)
      END IF

      RETURN
      END SUBROUTINE tzetas0


!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!Checks if we should assume a contact system has merged
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
		LOGICAL FUNCTION contactExit(s,b)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
		USE dshare
		USE dtypes
		USE params
		IMPLICIT NONE
		TYPE (bnq), INTENT(IN) :: b
		TYPE (stq), DIMENSION(2), INTENT(IN) :: s

			contactExit=.TRUE.
		!Are we contact?
! 		if (b%kw==30) THEN
! 
! 			!Skip Non-MS systems
! 			if (s(1)%kw >1 .or. s(2)%kw > 1) then
! !  			write(*,*) "*"
! 			contactExit=.TRUE.
! 			end if
! 			!If p is less than 0.2 days
! 			if (b%Porb*365.0<0.2)then
! !  			write(*,*) "**"
! 			contactExit=.TRUE.
! 			end if
! 			!If we exceed a critcal mass ratio (Rasio 1995)
! 			if ((MIN(s(1)%mt,s(2)%mt)/MAX(s(1)%mt,s(2)%mt)) < 0.1)then
! !  			write(*,*) "***"
! 			contactExit=.TRUE.
! 			end if
! 		else
! 			contactExit=.FALSE.
! 		end if


		if(contactExit.eqv. .TRUE.) warn=99

		END FUNCTION contactExit

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE rlof
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

