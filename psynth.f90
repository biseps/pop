!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE counters
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Counters used for population synthesis
! Bart Willems, 27/05/01
      USE precision
      IMPLICIT NONE
      INTEGER, PARAMETER, PRIVATE :: ncl = 93
      INTEGER :: total
      INTEGER :: tkkick,tkdisr,tkmerg
      INTEGER :: twarn
      INTEGER :: nnw,nlast
      INTEGER, DIMENSION(0:ncl) :: tclass
      DOUBLE PRECISION :: tksnii
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE counters
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE synthesis
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CONTAINS
      SUBROUTINE calibration(class,ext)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Stores type II SN events                                             c
! Bart Willems, 20/09/02                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE names
      USE fmods
      USE params
      USE dshare

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: class
      CHARACTER(len=*), INTENT(IN) :: ext
      INTEGER, PARAMETER :: chlen = 80
      INTEGER :: j,j1,j2
      DOUBLE PRECISION :: wgt
      CHARACTER(len=50) :: name0

      name0 = fname(class,'_calib'//ext)

! Determine branch-off point if supernova kicks have occurred
      j1 = jp
      j2 = jp + 1
      IF (kkick > 0) THEN
         j = 0
         loop1: DO
            j = j + 1
            IF (j >= jp) EXIT loop1
            IF ((save_binary(j)%kw1 >= 13).OR.(save_binary(j)%kw2 >= 13)) THEN
               j1 = MAX(1,j-1)
               j2 = j
               EXIT loop1
            END IF
         END DO loop1
      END IF

      j = 0
      wgt = 0.d0
      loop2: DO
         j = j + 1
         IF (j >= jp) EXIT loop2
         IF (save_binary(j)%tphys < 0.0) THEN
            IF (kkick > 0) j2 = j + 1
            CYCLE loop2
         END IF

         IF (class /= save_binary(j)%cls) CYCLE loop2

         IF (j <= j1) THEN
            wgt = 1.d0
         ELSE
            wgt = 1.d0/DBLE(nkick)
         END IF

! Store parameters
!          OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!               POSITION='APPEND',ACTION='WRITE')
!          WRITE(1,100) save_binary(j)%num,save_binary(j)%tphys,save_binary(1)%mt1, &
!               save_binary(1)%mt2,save_binary(1)%Porb,wgt
!          CLOSE(1)

      END DO loop2

 100  FORMAT (F12.4,1X,5(E12.5,1X))

      RETURN
      END SUBROUTINE calibration
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE stats(class,ext,track)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Determines the initial parameters leading to a given type of         c
! binaries. In addition, the routine stores the formation channel      c
! leading to the binary.                                               c
! Bart Willems, 26/11/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
		USE consts
		USE counters
		USE names
		USE fmods
		USE dshare
		USE extras
		!       USE input
		USE error
		USE dtypes
		USE params
		!       USE transits
! 		USE magnitude
		USE randomNum
! 		USE BC
! 		USE spectral
      
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: class,track
      CHARACTER(len=*), INTENT(IN) :: ext

      INTEGER, PARAMETER :: chlen = 80,minStep=5
      INTEGER :: numSteps,minStep2

      INTEGER :: j,jd,jsn,jce,jnext,i,x,jmax,k,increment,endTime,l
      INTEGER :: j1,j2,jj,kk,nrpt
      INTEGER :: nce1,nce2,nce
      INTEGER :: lo,hi,lerr,n,ii
      
      
      DOUBLE PRECISION :: q,a,ad,at,t1,t2,dmint,dmind,dmaxt,dmaxd,s
      DOUBLE PRECISION :: fms1,fms2,fPs,time,mag,tmax
      DOUBLE PRECISION :: md1,md2,rrt1,rrt2,rrd1,rrd2,rr1,rr2,Pd
!      DOUBLE PRECISION, DIMENSION(1:numTransits) :: feclipse
!      DOUBLE PRECISION :: IMF,IMRD,IOSD,Porb2a

	DOUBLE PRECISION :: avgBin,avgSin1,avgSin2

      CHARACTER(len=chlen) :: str,chn
      CHARACTER(len=100) :: name0
      CHARACTER(len=100), DIMENSION(16) :: name
      
      TYPE (stq),DIMENSION(1:2) :: star
      TYPE (bnq) :: b

		chn=''
		str=''
! 	DOUBLE PRECISION,DIMENSION(:,:),ALLOCATABLE :: magSing
! 	DOUBLE PRECISION,DIMENSION(:),ALLOCATABLE :: magBin
! 	TYPE(spectralType),DIMENSION(1:2) :: spec

      nce=0;nce1=0;nce2=0
      name(1) = fname(class,'_birth'//ext)
      name(2) = fname(class,'_death'//ext)
      name(3) = fname(class,'_preSN'//ext)
      name(4) = fname(class,'_pstSN'//ext)
      name(5) = fname(class,'_preCE1'//ext)
      name(6) = fname(class,'_pstCE1'//ext)
      name(7) = fname(class,'_preCE2'//ext)
      name(8) = fname(class,'_pstCE2'//ext)
      name(9) = fname(class,'_preCE3'//ext)
      name(10) = fname(class,'_pstCE3'//ext)
      name(11) = fname(class,'_init'//ext)
      name(12) = fname(class,'_extra'//ext)
      name(13) = fname(class,'_pop'//ext)
      name(14) = fname(class,'_transit'//ext)
      name(15) = fname(class,'_kepler'//ext)
!       name(16) = fname(class,'_mag'//ext)
      

		str=''
		chn=''
! Determine branch-off point if supernova kicks have occurred
      j1 = jp
      j2 = jp + 1
      IF (kkick > 0) THEN
         j = 0
         loop1: DO
            j = j + 1
            IF (j >= jp) EXIT loop1
            IF ((save_binary(j)%kw1 >= 13).OR.(save_binary(j)%kw2 >= 13)) THEN
               j1 = MAX(1,j-1)
               j2 = j
               EXIT loop1
            END IF
         END DO loop1
      END IF

      j = 0
      loop2: DO
         j = j + 1
         IF (j >= jp) EXIT loop2
         IF (save_binary(j)%tphys < 0.0) THEN
            IF (kkick > 0) j2 = j + 1
            CYCLE loop2
         END IF

         IF (class /= save_binary(j)%cls) CYCLE loop2
! Check for conspicuous repeats in channel
!HMMMMMMMMMMMMMM???????????????????????????????
!          jj = MIN(j,j1)
!          nrpt = 0
!          DO kk = 1, jj-2
!             IF ((save_binary(kk)%tphys > 0.d0).AND. &
!                  (save_binary(kk)%bkw == save_binary(kk+2)%bkw).AND. &
!                  (save_binary(kk)%bkw /= save_binary(kk+1)%bkw)) THEN
!                nrpt = nrpt + 1
!                IF (nrpt > 10) THEN
!                   WRITE(*,*) 'Conspicuous repeats in channel 1!!!'
!                   WRITE(*,*) save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb
!                   EXIT loop2
!                END IF
!             ELSE
!                nrpt = 0
!             END IF
!          ENDDO
!          jj = MIN(j+1,j2)
!          DO kk = jj, j-2
!             IF ((save_binary(kk)%tphys > 0.d0).AND. &
!                  (save_binary(kk)%bkw == save_binary(kk+2)%bkw).AND. &
!                  (save_binary(kk)%bkw /= save_binary(kk+1)%bkw)) THEN
!                nrpt = nrpt + 1
!                IF (nrpt > 10) THEN
!                   WRITE(*,*) 'Conspicuous repeats in channel 2!!!'
!                   WRITE(*,*) save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb
!                   EXIT loop2
!                END IF
!             ELSE
!                nrpt = 0
!             END IF
!          ENDDO
!          jj = MIN(j,j1)
!          nrpt = 0
!          DO kk = 1, jj-2
!             IF ((save_binary(kk)%tphys > 0.d0).AND. &
!                  (save_binary(kk)%cls == save_binary(kk+2)%cls).AND. &
!                  (save_binary(kk)%cls /= save_binary(kk+1)%cls)) THEN
!                nrpt = nrpt + 1
!                IF (nrpt > 10) THEN
!                   WRITE(*,*) 'Conspicuous repeats in channel 3!!!'
!                   WRITE(*,*) save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb
!                   WRITE(*,*) save_binary(kk)%tphys, save_binary(kk)%cls,save_binary(kk+1)%cls,save_binary(kk+2)%cls
!                   EXIT loop2
!                END IF
!             ELSE
!                nrpt = 0
!             END IF
!          ENDDO
!          jj = MIN(j+1,j2)
!          DO kk = jj, j-2
!             IF ((save_binary(kk)%tphys > 0.d0).AND. &
!                  (save_binary(kk)%bkw == save_binary(kk+2)%bkw).AND. &
!                  (save_binary(kk)%bkw /= save_binary(kk+1)%bkw)) THEN
!                nrpt = nrpt + 1
!                IF (nrpt > 10) THEN
!                   WRITE(*,*) 'Conspicuous repeats in channel 4!!!'
!                   WRITE(*,*) save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb
!                   EXIT loop2
!                END IF
!             ELSE
!                nrpt = 0
!             END IF
!          ENDDO

! Contribution to the population of this binary class
         tclass(class) = tclass(class) + 1

! Track formation channel
         IF (track > 0) THEN

            chn = ''

            n = chlen
            jj = MIN(j,j1)
            CALL code(1,jj,n,str,nce1,lerr)
            IF (lerr > 0) EXIT loop2
            lo = 1
            hi = n
            chn(lo:hi) = str(1:hi)

            jj = MIN(j+1,j2)
            IF (jj <= j) THEN
               n = chlen - n
               CALL code(jj,j,n,str,nce2,lerr)
               IF (lerr > 0) EXIT loop2
               lo = hi + 1
               hi = lo + n - 1
               chn(lo:hi) = str(1:hi)
            END IF

            IF (nce1 + nce2 > 3) THEN
               chn = '0'
            END IF

         ELSE
            chn = ''
            lo = 1
            hi = 1
         END IF

! 	write(*,*) chn

! Store initial parameters
         name0 = name(11)
!          OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!               POSITION='APPEND',ACTION='WRITE')
!          WRITE(1,111) save_binary(j)%num,save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb
!          CLOSE(1)

! Store parameters at birth
         name0 = name(1)
!          OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!               POSITION='APPEND',ACTION='WRITE')
!          WRITE(1,100) save_binary(j)%num,save_binary(j)%tphys,save_binary(j)%mt1, &
!               save_binary(j)%kw1,save_binary(j)%mt2,save_binary(j)%kw2,save_binary(j)%Porb, &
!               save_binary(j)%bkw,chn(1:hi)
!          CLOSE(1)
         
         t1=save_binary(j)%tphys

! Store parameters at death
         jd = j
         loop3: DO
            jd = jd + 1
            IF ((jd >= jp).OR.(save_binary(jd)%tphys < 0.0)) THEN
               jd = jd - 1
               jnext = jd
               EXIT loop3
            ELSE
               jnext = jd - 1
            END IF
            IF (class /= save_binary(jd)%cls) EXIT loop3
            IF (save_binary(j)%kw1 /= save_binary(jd)%kw1) EXIT loop3
            IF (save_binary(j)%kw2 /= save_binary(jd)%kw2) EXIT loop3
            IF (save_binary(j)%bkw /= save_binary(jd)%bkw) EXIT loop3
         END DO loop3

         IF ((save_binary(jd-1)%kw1 < 10).AND.(save_binary(jd)%kw1 == 13)) THEN
            name0 = name(2)
!             OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!                  POSITION='APPEND',ACTION='WRITE')
!             WRITE(1,101) save_binary(j)%num,save_binary(jd-1)%tphys,save_binary(jd-1)%mt1, &
!                  save_binary(jd-1)%kw1,save_binary(jd-1)%mt2,save_binary(jd-1)%kw2, &
!                  save_binary(jd-1)%Porb,save_binary(jd-1)%bkw
!             CLOSE(1)
            
! Store some additional quantities at birth and death
            name0 = name(12)
!             OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!                  POSITION='APPEND',ACTION='WRITE')
!             WRITE(1,103) save_binary(j)%num,save_binary(j)%reff1,save_binary(j)%bc1, &
!                  save_binary(j)%reff2,save_binary(j)%bc2,save_binary(j)%dmin,save_binary(j)%dmax, &
!                  save_binary(jd-1)%reff1,save_binary(jd-1)%bc1,save_binary(jd-1)%reff2, &
!                  save_binary(jd-1)%bc2,save_binary(jd-1)%dmin,save_binary(jd-1)%dmax
!             CLOSE(1)
            jmax=jd-1
            rrd1=save_binary(jd-1)%reff1
            rrd2=save_binary(jd-1)%reff2
            Pd=save_binary(jd-1)%Porb
            md1=save_binary(jd-1)%mt1
            md2=save_binary(jd-1)%mt2
            dmint=save_binary(j)%dmin
            dmaxt=save_binary(j)%dmax
            dmind=save_binary(jd-1)%dmin
            dmaxd=save_binary(jd-1)%dmax
            t2=save_binary(jd-1)%tphys
            
         ELSE IF ((save_binary(jd-1)%kw2 < 10).AND.(save_binary(jd)%kw2 == 13)) THEN
            name0 = name(2)
!             OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!                  POSITION='APPEND',ACTION='WRITE')
!             WRITE(1,101) save_binary(j)%num,save_binary(jd-1)%tphys,save_binary(jd-1)%mt1, &
!                  save_binary(jd-1)%kw1,save_binary(jd-1)%mt2,save_binary(jd-1)%kw2, &
!                  save_binary(jd-1)%Porb,save_binary(jd-1)%bkw
!             CLOSE(1)
! Store some additional quantities at birth and death
            name0 = name(12)
!             OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!                  POSITION='APPEND',ACTION='WRITE')
!             WRITE(1,103) save_binary(j)%num,save_binary(j)%reff1,save_binary(j)%bc1, &
!                  save_binary(j)%reff2,save_binary(j)%bc2,save_binary(j)%dmin,save_binary(j)%dmax, &
!                  save_binary(jd-1)%reff1,save_binary(jd-1)%bc1,save_binary(jd-1)%reff2, &
!                  save_binary(jd-1)%bc2,save_binary(jd-1)%dmin,save_binary(jd-1)%dmax
!             CLOSE(1)
            jmax=jd-1
            rrd1=save_binary(jd-1)%reff1
            rrd2=save_binary(jd-1)%reff2
            Pd=save_binary(jd-1)%Porb
            md1=save_binary(jd-1)%mt1
            md2=save_binary(jd-1)%mt2
            dmint=save_binary(j)%dmin
            dmaxt=save_binary(j)%dmax
            dmind=save_binary(jd-1)%dmin
            dmaxd=save_binary(jd-1)%dmax
            t2=save_binary(jd-1)%tphys
            
         ELSE
            name0 = name(2)
!             OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!                  POSITION='APPEND',ACTION='WRITE')
!             WRITE(1,101) save_binary(j)%num,save_binary(jd)%tphys,save_binary(jd)%mt1, &
!                  save_binary(jd)%kw1,save_binary(jd)%mt2,save_binary(jd)%kw2,save_binary(jd)%Porb, &
!                  save_binary(jd)%bkw
!             CLOSE(1)
! Store some additional quantities at birth and death
            name0 = name(12)
!             OPEN(1,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN', &
!                  POSITION='APPEND',ACTION='WRITE')
!             WRITE(1,103) save_binary(j)%num,save_binary(j)%reff1,save_binary(j)%bc1, &
!                  save_binary(j)%reff2,save_binary(j)%bc2,save_binary(j)%dmin,save_binary(j)%dmax, &
!                  save_binary(jd)%reff1,save_binary(jd)%bc1,save_binary(jd)%reff2, &
!                  save_binary(jd)%bc2,save_binary(jd)%dmin,save_binary(jd)%dmax
!             CLOSE(1)
            jmax=jd
            rrd1=save_binary(jd)%reff1
            rrd2=save_binary(jd)%reff2
            Pd=save_binary(jd)%Porb
            md1=save_binary(jd)%mt1
            md2=save_binary(jd)%mt2
            dmint=save_binary(j)%dmin
            dmaxt=save_binary(j)%dmax
            dmind=save_binary(jd)%dmin
            dmaxd=save_binary(jd)%dmax
            t2=save_binary(jd)%tphys
         END IF

!Store only whats needed for population
         name0 = name(13)
         OPEN(13,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN',POSITION='APPEND',ACTION='WRITE')
         name0=name(15)
         OPEN(15,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN',POSITION='APPEND',ACTION='WRITE')
!          name0=name(16)
!          OPEN(16,FILE=name0(1:LEN_TRIM(name0)),STATUS='UNKNOWN',POSITION='APPEND',ACTION='WRITE')
  
 		t1=save_binary(j)%tphys

	i=max(j,1)

! 	jmax=jmax-1
	output: DO
!         write(*,*) save_binary(i)%bkw
		IF(i.ge.jmax .or. save_binary(i)%tphys .lt. 0.d0) EXIT output
		!Pre-intialise to zero
		star%mt=0.d0;star%reff=0.d0;star%teff=0.d0;star%lum=0.d0
		b%porb=0.d0
		b%a=0.d0
! 		mag=0.d0
		!Sum up all elements that are same system, same star types and same binary type
		IF(i==1) THEN
			minStep2=1
		ELSE IF ((save_binary(i)%kw1 == 2 .or. save_binary(i)%kw2 == 2) .or. &
                 (save_binary(i)%kw1/=save_binary(i-1)%kw1 .or. &
                  save_binary(i)%kw2/=save_binary(i-1)%kw2)) THEN
			minStep2=1
		ELSE
			minStep2=floor(unif(5.d0,25.d0))
		END IF
		!Allways avg atleast the total number of points divided by the minstep ie for singles thats
		!25/5 so at least 5 points while for binaries its 250/5 so at least 50 points
	
		if(isSingle==1) then
			k=floor(unif(dble(i+minStep2),dble(MIN(jmax,i+timeSampSing(save_binary(i)%kw1)))))
		else
			k=floor(unif(dble(i+minStep2),dble(MIN(jmax,i+timeSampBin(save_binary(i)%kw1)))))
		end if
		k=MIN(k,jmax)

		!Try and average all points during mass transfer phases together
! 		if(save_binary(i)%bkw==30.or.&
! 			(save_binary(i)%bkw>=10.and.save_binary(i)%bkw<=13).or.&
! 			(save_binary(i)%bkw>=20.and.save_binary(i)%bkw<=23)) THEN
!   			k=MIN(k+100,jmax)
! 		END IF
		
		DO l=i,k
			if(save_binary(l)%tphys .lt. 0.d0) EXIT
			tmax=MIN(save_binary(l+1)%tphys,tphysMax)

!			write(*,*) j,i,k,l,jmax,save_binary(l)%tphys,tphysmax
! 			mag=mag+10**(save_binary(l)%mag/(-2.5))*(tmax-save_binary(l)%tphys)
			!Sparsely populate star(1),star(2), and b 
			star(1)%mt=star(1)%mt+save_binary(l)%mt1*(tmax-save_binary(l)%tphys)
			star(1)%reff=star(1)%reff+save_binary(l)%reff1*(tmax-save_binary(l)%tphys)
			star(1)%teff=star(1)%teff+log10(save_binary(l)%teff1)*(tmax-save_binary(l)%tphys)
! 			star(1)%mag=star(1)%mag+save_binary(l)%mag1*(tmax-save_binary(l)%tphys)
			star(1)%lum=star(1)%lum+log10(save_binary(l)%lum1)*(tmax-save_binary(l)%tphys)

			star(2)%mt=star(2)%mt+save_binary(l)%mt2*(tmax-save_binary(l)%tphys)
			star(2)%reff=star(2)%reff+save_binary(l)%reff2*(tmax-save_binary(l)%tphys)
			star(2)%teff=star(2)%teff+log10(save_binary(l)%teff2)*(tmax-save_binary(l)%tphys)
! 			star(2)%mag=star(2)%mag+save_binary(l)%mag2*(tmax-save_binary(l)%tphys)
			star(2)%lum=star(2)%lum+log10(save_binary(l)%lum2)*(tmax-save_binary(l)%tphys)

			b%porb=b%porb+save_binary(l)%porb*(tmax-save_binary(l)%tphys)
		END DO
! 		k=l-1
		
!		write(*,*) "*",j,i,k,l,jmax,save_binary(l)%tphys,tphysmax
		!Dont want div by zero
		tmax=MIN(tmax,tphysMax)
		IF(tmax-save_binary(i)%tphys .gt. 0.d0 ) THEN
		
! 			mag=mag/(tmax-save_binary(i)%tphys)
! 			mag=-2.5*log10(mag)
			star(1)%mt=star(1)%mt/(tmax-save_binary(i)%tphys)
			star(1)%reff=star(1)%reff/(tmax-save_binary(i)%tphys)
			star(1)%teff=star(1)%teff/(tmax-save_binary(i)%tphys)
! 			star(1)%mag=star(1)%mag/(tmax-save_binary(i)%tphys)
			star(1)%lum=star(1)%lum/(tmax-save_binary(i)%tphys)

			star(2)%mt=star(2)%mt/(tmax-save_binary(i)%tphys)
			star(2)%reff=star(2)%reff/(tmax-save_binary(i)%tphys)
			star(2)%teff=star(2)%teff/(tmax-save_binary(i)%tphys)
! 			star(2)%mag=star(2)%mag/(tmax-save_binary(i)%tphys)
			star(2)%lum=star(2)%lum/(tmax-save_binary(i)%tphys)
			
			b%porb=b%porb/(tmax-save_binary(i)%tphys)

			star(1)%teff=10**(star(1)%teff)
			star(1)%lum=10**(star(1)%lum)
			star(2)%teff=10**(star(2)%teff)
			star(2)%lum=10**(star(2)%lum)
		ELSE
! 			As the time points are the same need to reset them to proper non zero values
! 			mag=10**(save_binary(i)%mag/(-2.5))
			star(1)%mt=save_binary(i)%mt1
			star(1)%reff=save_binary(i)%reff1
			star(1)%teff=save_binary(i)%teff1
! 			star(1)%mag=save_binary(i)%mag1
			star(1)%lum=save_binary(i)%lum1

			star(2)%mt=save_binary(i)%mt2
			star(2)%reff=save_binary(i)%reff2
			star(2)%teff=save_binary(i)%teff2
! 			star(2)%mag=save_binary(i)%mag2
			star(2)%lum=save_binary(i)%lum2
			b%porb=save_binary(i)%porb
! 			mag=-2.5*log10(mag)
		END IF
		b%a=porb2a(b%porb/365.0,star(1)%mt,star(2)%mt)
		star(1)%kw=save_binary(i)%kw1
		star(2)%kw=save_binary(i)%kw2

			!pop file
		WRITE(13,104) save_binary(i)%num,           &
                      save_binary(1)%mt1,           &
                      save_binary(1)%mt2,           &
                      save_binary(1)%Porb,          &
                      save_binary(i)%tphys,         &
                      save_binary(k)%tphys,         &
                      chn(1:hi)

! 			CALL getSpecType(star(1))
! 			CALL getSpecType(star(2))
! 			CALL getMagBC(star,b)

! 			CALL getMagBC(star%mt,star%reff,star%teff,star%lum,star%kw,metallicity,isSingle,magSing,magBin,spec)

			!Kepler file
			
		WRITE(15,106) save_binary(i)%num,star(1)%mt,star(1)%reff,star(1)%teff,star(1)%lum,&
		star(2)%mt,star(2)%reff,star(2)%teff,star(2)%lum,&
		b%porb,save_binary(i)%tphys,save_binary(k)%tphys,&
		massTransfered(chn(1:hi)),&
! 			trim(spec(1)%class)//trim(spec(1)%subclass)//trim(spec(1)%Lumin)," ",&
! 			trim(spec(2)%class)//trim(spec(2)%subclass)//trim(spec(2)%Lumin)," ",&
		save_binary(i)%kw1,save_binary(i)%kw2,save_binary(i)%bkw,metallicity,&
		chn(1:hi)

			!Mag file
! 			write(16,'(F12.4,1X,3(E18.10,1X))') save_binary(i)%num,magSing(1,1),magSing(1,2),magBin(1)
! 			DEALLOCATE(magSing,magBin)
		i=k
!		write(*,*) "**",j,i,k,l,jmax,save_binary(l)%tphys,tphysmax
!		if(k==jmax) exit output
	END DO output

!  	i=i-1

! 	do ii=j,i
! 		write(*,*) ii,save_binary(ii)%bkw,save_binary(ii)%tphys,save_binary(ii+1)%tphys
! 	end do

	!Handle Contact binary output
! 	if (save_binary(i)%bkw==30) THEN
! ! 		write(*,*) save_binary(i-1:i+1)%bkw
! ! 		write(*,*) save_binary(i-1:i+1)%tphys
! 		!Overwrite channel code to be o for overcontact
! 		!Only want to do this once yet bkw will be 30 twice
! ! 		chn(hi-2:hi-2)="o"
! ! 		chn(hi:hi)="o"
! 
! ! 		chn(hi+1:hi+1)=chn(hi-5:hi-5)
! ! 		chn(hi+2:hi+2)="o"
! ! 		chn(hi+3:hi+3)=chn(hi-3:hi-2)
! ! 		chn(hi+4:hi+4)="o"
! 
! 		if(save_binary(i+1)%tphys .lt. 0.0 .or. save_binary(i+1)%tphys==save_binary(i)%tphys) THEN
! 		!Skip these systems
! 			CLOSE(13)
! 			CLOSE(15)
!  			CLOSE(16)
! 			return
! 		end if
! 
! 		chn(hi+1:hi+2)=btype(save_binary(i)%kw1,save_binary(i)%bkw)
! 		chn(hi+3:hi+4)=btype(save_binary(i)%kw2,save_binary(i)%bkw)
! 		!Pop file
! 		WRITE(13,104) save_binary(i)%num,save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb,save_binary(i)%tphys,save_binary(i+1)%tphys,chn(1:hi+4)
! 		!Kepler file
! 		WRITE(15,106) save_binary(i)%num,save_binary(i)%mt1,save_binary(i)%reff1,save_binary(i)%teff1,save_binary(i)%lum1,&
! 		save_binary(i)%mt2,save_binary(i)%reff2,save_binary(i)%teff2,save_binary(i)%lum2,&
! 		b%porb,save_binary(i)%tphys,save_binary(i+1)%tphys,&
! 		massTransfered(chn(1:hi+4)),&
! 		save_binary(i)%kw1,save_binary(i)%kw2,save_binary(i)%bkw,metallicity,&
! 		chn(1:hi+4)
! 		CLOSE(13)
! 		CLOSE(15)
!  		CLOSE(16)
! 		return
! 	END IF




	CLOSE(13)
	CLOSE(15)
 	CLOSE(16)

         
! Set loop index for the continuation of the cycle
         j = jnext

      END DO loop2

 100  FORMAT (F12.4,1X,E12.5,1X,3(E12.5,1X,I3,1X),A)
 101  FORMAT (F12.4,1X,E12.5,1X,3(E12.5,1X,I3,1X))
 102  FORMAT (F12.4,1X,E12.5,1X,2(E12.5,1X,I3,1X),3(E12.5,1X),I3)
 103  FORMAT (F12.4,1X,12(E12.5,1X))
 104  FORMAT (F12.4,1X,5(E18.10,1X),A)
 105  FORMAT (F12.4,1X,(E18.10,1X))
 106  FORMAT (F12.4,1X,11(E18.10,1X),I3,X,3(I3,X),E12.5,1X,A)
 111  FORMAT (F12.4,1X,3(E12.5,1X))
 112  FORMAT (F12.4,1X,6(E12.5,1X),A)

      RETURN
      END SUBROUTINE stats
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	!Returns 1 if system has undergone mass transfer by this point in time
	!Returns 0 if system hasn't undergone mass transfer by this point in time
	INTEGER FUNCTION massTransfered(chn)
	IMPLICIT NONE
      CHARACTER(*), INTENT(IN) :: chn

	!Perform a string search for the various mass transfer codes
	!Look for E,r,d,t,n in chn  string

	IF( scan(chn,'E').ne.0 .or.scan(chn,'r').ne.0 .or.scan(chn,'d').ne.0 &
 .or.scan(chn,'t').ne.0 .or.scan(chn,'n').ne.0 .or.scan(chn,'o').ne.0) then
		massTransfered=1
	ELSE
		massTransfered=0
	END IF
	
	END FUNCTION massTransfered

      
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE code(kmin,kmax,n,chn,nce,lerr)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Turns a formation channel into a string
! Bart Willems, 18/07/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE dtypes
      USE dshare

      IMPLICIT NONE

      INTEGER, INTENT(IN) :: kmin,kmax
      INTEGER, INTENT(OUT) :: nce,lerr
      INTEGER, INTENT(INOUT) :: n
      CHARACTER(len=n), INTENT(OUT) :: chn

!      DOUBLE PRECISION, PARAMETER :: dmx = 0.001d0
!      DOUBLE PRECISION, PARAMETER :: dPx = 0.001d0
      DOUBLE PRECISION, PARAMETER :: dmx = 0.10d0
      DOUBLE PRECISION, PARAMETER :: dPx = 0.10d0

      INTEGER :: i,k,kk
      INTEGER :: lo,hi,old
      DOUBLE PRECISION :: m0,mt,dm
      DOUBLE PRECISION :: P0,Pt,dP

! 	write(*,*) "int",kmin,kmax,n
! Count number of common envelope phases
      nce = 0

! Record first step
      lo = 1
      hi = 4
      CALL chklen(n,hi,lerr)
      IF (lerr > 0) RETURN
      SELECT CASE (save_binary(kmin)%bkw)
      CASE(12,13)
         chn(lo:lo+1) = btype(save_binary(kmin)%kw1,save_binary(kmin)%bkw)
         chn(hi-1:hi) = ktype(save_binary(kmin)%kw2)
      CASE(14,15)
         nce = nce + 1
         chn(lo:lo+1) = btype(save_binary(kmin)%kw1,save_binary(kmin)%bkw)
         chn(hi-1:hi) = ktype(save_binary(kmin)%kw2)
      CASE(22,23)
         chn(lo:lo+1) = ktype(save_binary(kmin)%kw1)
         chn(hi-1:hi) = btype(save_binary(kmin)%kw2,save_binary(kmin)%bkw)
      CASE(24,25)
         nce = nce + 1
         chn(lo:lo+1) = ktype(save_binary(kmin)%kw1)
         chn(hi-1:hi) = btype(save_binary(kmin)%kw2,save_binary(kmin)%bkw)
      CASE DEFAULT
!         chn(lo:lo+1) = ktype(save_binary(kmin)%kw1)
!         chn(hi-1:hi) = ktype(save_binary(kmin)%kw2)
		chn(lo:lo+1) = btype(save_binary(kmin)%kw1,save_binary(kmin)%bkw)
		chn(hi-1:hi) = btype(save_binary(kmin)%kw2,save_binary(kmin)%bkw)
      END SELECT
      old = save_binary(kmin)%bkw

! 		write(*,*) "--",chn
      kk = 0

! Start while loop
      k = kmin
      kloop: DO
         k = k + 1
         IF (k > kmax) EXIT kloop

         IF (k == kmax) THEN
            kk = kk + 1
            lo = 4*kk + 1
            hi = lo + 3
            CALL chklen(n,hi,lerr)
            IF (lerr > 0) RETURN
            SELECT CASE (save_binary(kmax)%bkw)
            CASE(12,13)
               chn(lo:lo+1) = btype(save_binary(kmax)%kw1,save_binary(kmax)%bkw)
               chn(hi-1:hi) = ktype(save_binary(kmax)%kw2)
            CASE(14,15)
               nce = nce + 1
               chn(lo:lo+1) = btype(save_binary(kmax)%kw1,save_binary(kmax)%bkw)
               chn(hi-1:hi) = ktype(save_binary(kmax)%kw2)
            CASE(22,23)
               chn(lo:lo+1) = ktype(save_binary(kmax)%kw1)
               chn(hi-1:hi) = btype(save_binary(kmax)%kw2,save_binary(kmax)%bkw)
            CASE(24,25)
               nce = nce + 1
               chn(lo:lo+1) = ktype(save_binary(kmax)%kw1)
               chn(hi-1:hi) = btype(save_binary(kmax)%kw2,save_binary(kmax)%bkw)
            CASE DEFAULT
!                chn(lo:lo+1) = ktype(save_binary(kmax)%kw1)
!                chn(hi-1:hi) = ktype(save_binary(kmax)%kw2)
           		chn(lo:lo+1) = btype(save_binary(kmax)%kw1,save_binary(kmax)%bkw)
           		chn(hi-1:hi) = btype(save_binary(kmax)%kw2,save_binary(kmax)%bkw)
            END SELECT
            IF (kk > 1) THEN
               IF (chn(lo:lo+1) == chn(lo-4:lo-3)) THEN
                  SELECT CASE (chn(hi-1:hi))
                  CASE('A*','B*','C*','M*')
                     IF (chn(hi-5:hi-4) == '**') THEN
                        chn(lo-4:hi-4) = chn(lo:hi)
                        kk = kk-1
                        lo = 4*kk + 1
                        hi = lo + 3
                     END IF
                  CASE('D*')
                     IF (chn(hi-5:hi-4) == 'He') THEN
                        chn(lo-4:hi-4) = chn(lo:hi)
                        kk = kk - 1
                        lo = 4*kk + 1
                        hi = lo + 3
                     END IF
                  END SELECT
               ELSE IF (chn(hi-1:hi) == chn(hi-5:hi-4)) THEN
                  SELECT CASE (chn(lo:lo+1))
                  CASE('A*','B*','C*','M*')
                     IF (chn(lo-4:lo-3) == '**') THEN
                        chn(lo-4:hi-4) = chn(lo:hi)
                        kk = kk-1
                        lo = 4*kk + 1
                        hi = lo + 3
                     END IF
                  CASE('D*')
                     IF (chn(lo-4:lo-3) == 'He') THEN
                        chn(lo-4:hi-4) = chn(lo:hi)
                        kk = kk - 1
                        lo = 4*kk + 1
                        hi = lo + 3
                     END IF
                  END SELECT
               END IF
            END IF
            EXIT kloop
         END IF

         SELECT CASE (save_binary(k)%bkw)
         CASE(0)
            SELECT CASE (save_binary(k-1)%bkw)
            CASE(12,13)
               IF ((save_binary(k+1)%bkw == 12).OR.(save_binary(k+1)%bkw == 13)) THEN
                  SELECT CASE(save_binary(k)%kw1)
                  CASE(0,1,2,5,7,8)
                     IF ((save_binary(k)%kw1 == save_binary(k+1)%kw1).OR. &
                          (save_binary(k)%kw1 == save_binary(k+1)%kw1+1)) CYCLE kloop
                  CASE DEFAULT
                     IF (save_binary(k)%kw1 == save_binary(k+1)%kw1) CYCLE kloop
                  END SELECT
               END IF
            CASE(14,15,24,25)
               kk = kk + 1
               lo = 4*kk + 1
               hi = lo + 3
               CALL chklen(n,hi,lerr)
               IF (lerr > 0) RETURN
!                chn(lo:lo+1) = ktype(save_binary(k)%kw1)
!                chn(hi-1:hi) = ktype(save_binary(k)%kw2)
 		 chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
               old = save_binary(k)%bkw
            CASE(22,23)
               IF ((save_binary(k+1)%bkw == 22).OR.(save_binary(k+1)%bkw == 23)) THEN
                  SELECT CASE(save_binary(k)%kw2)
                  CASE(0,1,2,5,7,8)
                     IF ((save_binary(k)%kw2 == save_binary(k+1)%kw2).OR. &
                          (save_binary(k)%kw2 == save_binary(k+1)%kw2+1)) CYCLE kloop
                  CASE DEFAULT
                     IF (save_binary(k)%kw2 == save_binary(k+1)%kw2) CYCLE kloop
                  END SELECT
               END IF
            CASE DEFAULT
               IF ((save_binary(k)%kw1 /= save_binary(k-1)%kw1) &
                    .OR.(save_binary(k)%kw2 /= save_binary(k-1)%kw2)) THEN
                  kk = kk + 1
                  lo = 4*kk + 1
                  hi = lo + 3
                  CALL chklen(n,hi,lerr)
                  IF (lerr > 0) RETURN
!                   chn(lo:lo+1) = ktype(save_binary(k)%kw1)
!                   chn(hi-1:hi) = ktype(save_binary(k)%kw2)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
                  old = save_binary(k)%bkw
                  IF (kk > 1) THEN
                     IF (chn(lo:lo+1) == chn(lo-4:lo-3)) THEN
                        SELECT CASE (chn(hi-5:hi-4))
                        CASE('A*','B*','C*','M*')
                           IF (chn(hi-1:hi) == '**') THEN
                              kk = kk-1
                              lo = 4*kk + 1
                              hi = lo + 3
                           END IF
                        CASE('D*')
                           IF (chn(hi-1:hi) == 'He') THEN
                              kk = kk - 1
                              lo = 4*kk + 1
                              hi = lo + 3
                           END IF
                        END SELECT
                     ELSE IF (chn(hi-1:hi) == chn(hi-5:hi-4)) THEN
                        SELECT CASE (chn(lo-4:lo-3))
                        CASE('A*','B*','C*','M*')
                           IF (chn(lo:lo+1) == '**') THEN
                              kk = kk-1
                              lo = 4*kk + 1
                              hi = lo + 3
                           END IF
                        CASE('D*')
                           IF (chn(lo:lo+1) == 'He') THEN
                              kk = kk - 1
                              lo = 4*kk + 1
                              hi = lo + 3
                           END IF
                        END SELECT
                     END IF
                  END IF
               END IF
            END SELECT
         CASE(12,13)
            IF ((old /= 12).AND.(old /= 13)) THEN ! Record major RLOF ph
               m0 = save_binary(k)%mt1
               mt = save_binary(kmax)%mt1
               P0 = save_binary(k)%Porb
               Pt = save_binary(kmax)%Porb
               DO i = k+1, kmax
                  IF ((save_binary(i)%bkw == 12).OR.(save_binary(i)%bkw == 13)) CYCLE
                  mt = save_binary(i)%mt1
                  Pt = save_binary(i)%Porb
                  EXIT
               END DO
               dm = ABS(m0-mt)/m0
               dP = ABS(P0-Pt)/P0
               IF (((dm > dmx).AND.(dP > dPx)).OR.((i == kmax).AND. &
                    ((save_binary(kmax)%bkw == 12).OR. &
                    (save_binary(kmax)%bkw == 13)))) THEN
                  kk = kk + 1
                  lo = 4*kk + 1
                  hi = lo + 3
                  CALL chklen(n,hi,lerr)
                  IF (lerr > 0) RETURN
!                   chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
!                   chn(hi-1:hi) = ktype(save_binary(k)%kw2)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
                  old = save_binary(k)%bkw
                  IF (kk > 1) THEN
                     SELECT CASE (chn(lo:lo+1))
                     CASE('A*','B*','C*','M*')
                        IF ((chn(lo-4:lo-3) == '**').AND. &
                              (chn(hi-1:hi) == chn(hi-5:hi-4))) THEN
                           chn(lo-4:hi-4) = chn(lo:hi)
                           kk = kk-1
                           lo = 4*kk + 1
                           hi = lo + 3
                        END IF
                     CASE('D*')
                        IF ((chn(lo-4:lo-3) == 'He').AND. &
                              (chn(hi-1:hi) == chn(hi-5:hi-4))) THEN
                           chn(lo-4:hi-4) = chn(lo:hi)
                           kk = kk - 1
                           lo = 4*kk + 1
                           hi = lo + 3
                        END IF
                     END SELECT
                  END IF
               END IF
            END IF
         CASE(22,23)
            IF ((old /= 22).AND.(old /= 23)) THEN ! Record major RLOF ph
               m0 = save_binary(k)%mt2
               mt = save_binary(kmax)%mt2
               P0 = save_binary(k)%Porb
               Pt = save_binary(kmax)%Porb
               DO i = k+1, kmax
                  IF ((save_binary(i)%bkw == 22).OR.(save_binary(i)%bkw == 23)) CYCLE
                  mt = save_binary(i)%mt2
                  Pt = save_binary(i)%Porb
                  EXIT
               END DO
               dm = ABS(m0-mt)/m0
               dP = ABS(P0-Pt)/P0
               IF (((dm > dmx).AND.(dP > dPx)).OR.((i == kmax).AND. &
                    ((save_binary(kmax)%bkw == 22).OR. &
                    (save_binary(kmax)%bkw == 23)))) THEN
                  kk = kk + 1
                  lo = 4*kk + 1
                  hi = lo + 3
                  CALL chklen(n,hi,lerr)
                  IF (lerr > 0) RETURN
!                   chn(lo:lo+1) = ktype(save_binary(k)%kw1)
!                   chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
                  old = save_binary(k)%bkw
                  IF (kk > 1) THEN
                     SELECT CASE (chn(hi-1:hi))
                     CASE('A*','B*','C*','M*')
                        IF ((chn(hi-5:hi-4) == '**').AND. &
                              (chn(lo:lo+1) == chn(lo-4:lo-3))) THEN
                           chn(lo-4:hi-4) = chn(lo:hi)
                           kk = kk - 1
                           lo = 4*kk + 1
                           hi = lo + 3
                        END IF
                     CASE('D*')
                        IF ((chn(hi-5:hi-4) == 'He').AND. &
                              (chn(lo:lo+1) == chn(lo-4:lo-3))) THEN
                           chn(lo-4:hi-4) = chn(lo:hi)
                           kk = kk - 1
                           lo = 4*kk + 1
                           hi = lo + 3
                        END IF
                     END SELECT
                  END IF
               END IF
            END IF
         CASE(14,15)
            nce = nce + 1
            SELECT CASE (chn(lo:lo+1))
            CASE('A*','B*','C*','D*','M*')
!                chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
!                chn(hi-1:hi) = ktype(save_binary(k)%kw2)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
            CASE DEFAULT
               kk = kk + 1
               lo = 4*kk + 1
               hi = lo + 3
               CALL chklen(n,hi,lerr)
               IF (lerr > 0) RETURN
!                chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
!                chn(hi-1:hi) = ktype(save_binary(k)%kw2)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
            END SELECT
            old = save_binary(k)%bkw
            IF (kk > 1) THEN
               SELECT CASE (chn(lo:lo+1))
               CASE('AE','BE','CE','ME')
                  IF ((chn(lo-4:lo-3) == '**').AND. &
                       (chn(hi-1:hi) == chn(hi-5:hi-4))) THEN
                     chn(lo-4:hi-4) = chn(lo:hi)
                     kk = kk-1
                     lo = 4*kk + 1
                     hi = lo + 3
                  END IF
               CASE('DE')
                  IF ((chn(lo-4:lo-3) == 'He').AND. &
                       (chn(hi-1:hi) == chn(hi-5:hi-4))) THEN
                     chn(lo-4:hi-4) = chn(lo:hi)
                     kk = kk - 1
                     lo = 4*kk + 1
                     hi = lo + 3
                  END IF
               END SELECT
            END IF
         CASE(24,25)
            nce = nce + 1
            SELECT CASE (chn(hi-1:hi))
            CASE('A*','B*','C*','D*','M*')
!                chn(lo:lo+1) = ktype(save_binary(k)%kw1)
!                chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
               chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
            CASE DEFAULT
               kk = kk + 1
               lo = 4*kk + 1
               hi = lo + 3
               CALL chklen(n,hi,lerr)
               IF (lerr > 0) RETURN
!                chn(lo:lo+1) = ktype(save_binary(k)%kw1)
!                chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
 		  chn(lo:lo+1) = btype(save_binary(k)%kw1,save_binary(k)%bkw)
              chn(hi-1:hi) = btype(save_binary(k)%kw2,save_binary(k)%bkw)
            END SELECT
            old = save_binary(k)%bkw
            IF (kk > 1) THEN
               SELECT CASE (chn(hi-1:hi))
               CASE('AE','BE','CE','ME')
                  IF ((chn(hi-5:hi-4) == '**').AND. &
                       (chn(lo:lo+1) == chn(lo-4:lo-3))) THEN
                     chn(lo-4:hi-4) = chn(lo:hi)
                     kk = kk - 1
                     lo = 4*kk + 1
                     hi = lo + 3
                  END IF
               CASE('DE')
                  IF ((chn(hi-5:hi-4) == 'He').AND. &
                       (chn(lo:lo+1) == chn(lo-4:lo-3))) THEN
                     chn(lo-4:hi-4) = chn(lo:hi)
                     kk = kk - 1
                     lo = 4*kk + 1
                     hi = lo + 3
                  END IF
               END SELECT
            END IF
         END SELECT

         IF (kk > 0) THEN
            IF (chn(lo:hi) == chn(lo-4:hi-4)) THEN
               kk = kk - 1
               lo = 4*kk + 1
               hi = lo + 3
            END IF
         END IF
         IF (kk > 0) THEN
            IF (chn(lo:hi) == '****') THEN
               kk = kk - 1
               lo = 4*kk + 1
               hi = lo + 3
            END IF
         END IF

      END DO kloop

      IF (kk > 0) THEN
         IF (chn(lo:hi) == chn(lo-4:hi-4)) THEN
            kk = kk - 1
            lo = 4*kk + 1
            hi = lo + 3
         END IF
      END IF

      n = hi

      RETURN
      END SUBROUTINE code
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CHARACTER(len=2) FUNCTION ktype(k)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Bart Willems, 27/06/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: k
      CHARACTER(len=2) :: str
      
      SELECT CASE(k)
      CASE(7,8,9)
         str = 'He'
      CASE(10,11,12)
         str = 'WD'
      CASE(13)
         str = 'NS'
      CASE(14)
         str = 'BH'
      CASE(15)
         str = 'XX'
      CASE(0,1)
         str = 'A*'
      CASE(2,3)
         str = 'B*'
      CASE(4)
         str = 'M*'
      CASE(5,6)
         str = 'C*'
!      CASE DEFAULT
!         IF (k < 10) THEN
!            WRITE(str,'(2I1)') 0,k
!         ELSE
!            WRITE(str,'(I2)') k
!         END IF
      END SELECT

      ktype = str

      RETURN
      END FUNCTION ktype
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CHARACTER(len=2) FUNCTION btype(k,bk)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Bart Willems, 03/06/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: k,bk
      CHARACTER(len=2) :: str

      SELECT CASE(bk)
      CASE(14,15,24,25)
         str(2:2) = 'E'
      CASE(10,20)
      	str(2:2)='r'
      CASE(11,21)
      	str(2:2)='d'
      CASE(12,22)
      	str(2:2)='t'
      CASE(13,23)
      	str(2:2)='n'
      CASE(30)
      	str(2:2)='o'
      CASE DEFAULT
         str(2:2) = '*'
      END SELECT

      SELECT CASE(k)
      CASE(0,1)
         str(1:1) = 'A'
      CASE(2,3)
         str(1:1) = 'B'
      CASE(4)
         str(1:1) = 'M'
      CASE(5,6)
         str(1:1) = 'C'
      CASE(7,8,9)
         str(1:2) = 'He'
      CASE(10,11,12)
         str(1:2) = 'WD'
      CASE(13)
         str(1:2) = 'NS'
      CASE(14)
         str(1:1) = 'I'
      CASE DEFAULT
         str(1:1) = '?'
      END SELECT

      btype = str

      RETURN
      END FUNCTION btype
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE chklen(n,hi,lerr)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Check length of channel string
! Bart Willems, 02/06/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE dshare
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: n,hi
      INTEGER, INTENT(OUT) :: lerr

      IF (hi > n) THEN
         lerr = 1
         OPEN(8,FILE='CHANN.ERR',STATUS='UNKNOWN',POSITION='APPEND', &
              ACTION='WRITE')
         WRITE(8,*) 'Channel too long!!!'
         WRITE(8,*) save_binary(1)%mt1,save_binary(1)%mt2,save_binary(1)%Porb
         CLOSE(8)
      ELSE
         lerr = 0
      END IF

      RETURN
      END SUBROUTINE chklen
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE countwarn
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Counts the number of binaries where the evolution was halted with a  c
! warning                                                              c
! Bart Willems, 01/05/01                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE counters
      USE dshare
      IMPLICIT NONE
      INTEGER :: j,wrn

      j = 0
      loop: DO
         j = j + 1
         IF (j >= jp) EXIT loop
         IF (save_binary(j)%tphys < 0.0) THEN
            wrn = save_binary(j)%bkw
            IF ((wrn > 0).AND.(wrn /= 99)) twarn = twarn + 1
         END IF
      END DO loop

      RETURN
      END SUBROUTINE countwarn
      
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE synthesis
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

