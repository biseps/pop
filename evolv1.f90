!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE evolves
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CONTAINS
      SUBROUTINE evolv1(kw,mass,mt,r,lum,mc,rc,epoch,tphys, &
           tphysf,rl,z,zpars,timeFlag)
!-------------------------------------------------------------c
!
!     Evolves a single star.
!     Mass loss is an option.
!     The timestep is not constant but determined by certain criteria.
!     Plots the HRD and variables as a function of time.
!
!     Written by Jarrod Hurley 26/08/97 at the Institute of
!     Astronomy, Cambridge.
!
!     Bart Willems, 2/11/00: replaced common block /POINTS/ by
!               module 'params' and set rflag = 1, nv = 2 for
!               integration in BiSEC
!                   7/11/00: moved ospin initialization to evolv2
!                   8/11/00: spin adjustment due to magnetic braking
!               and mass loss now occurs after the new mass, radius,
!               etc have been calculated
!                  19/11/00: moved output to evolv2
!                  20/11/00: moved spin related quantities to evolv2
!               and put the whole routine in a module
!
!-------------------------------------------------------------c
!
!     STELLAR TYPES - KW
!
!        0 - deeply or fully convective low mass MS star
!        1 - Main Sequence star
!        2 - Hertzsprung Gap
!        3 - First Giant Branch
!        4 - Core Helium Burning
!        5 - First Asymptotic Giant Branch
!        6 - Second Asymptotic Giant Branch
!        7 - Main Sequence Naked Helium star
!        8 - Hertzsprung Gap Naked Helium star
!        9 - Giant Branch Naked Helium star
!       10 - Helium White Dwarf
!       11 - Carbon/Oxygen White Dwarf
!       12 - Oxygen/Neon White Dwarf
!       13 - Neutron Star
!       14 - Black Hole
!       15 - Massless Supernova
!
!-------------------------------------------------------------c
      USE params
		USE randomNum
      implicit none
!
      integer kw,it,j,kwold,rflag
      integer nv
      parameter(nv=2)
!
      real*8 mass,z,aj
      real*8 epoch,tphys,tphys2,tmold,tbgold
      real*8 mt,tm,tn,tphysf
      real*8 tscls(20),lums(10),GB(10),zpars(20)
      real*8 r,lum,mc,rc,m0,r1,lum1,mc1,rc1
      real*8 dt,dtm,dtr,dr,dtdr,dms,dml,mt2,rl
      REAL*8 mt0,mass0          ! BW 16/05/01
      real*8 tol
      parameter(tol=1.0d-10)
      real*8 ajhold,rm0,eps,alpha2
      parameter(eps=1.0d-06,alpha2=0.09d0)
      real*8 mlwind,vrotf,menvf

	  DOUBLE PRECISION :: minTime,maxTime
      DOUBLE PRECISION,PARAMETER :: absMinTime=1.d-3
      LOGICAL :: timeFlag
	real*8 thookf
      external mlwind,vrotf,menvf,thookf
!
      dtm = 0.d0
!      r = 0.d0
!      lum = 0.d0
!      mc = 0.d0
      mc1 = 0.d0
!      rc = 0.d0
      rflag = 1
!
      do 10 , j = 1,nv
!
         if(mflag.ne.0.and.j.gt.1)then
!
! Calculate mass loss from the previous timestep.
!
            dt = 1.0d+06*dtm  ! dtm is in units of 10**6 yrs
            dms = mlwind(kw,lum,r,mt,mc,rl,z,neta,bwind,wmag)*dt
            if(kw.lt.10)then
               dml = mt - mc
               if(dml.lt.dms)then      ! Mass lost cannot exceed envelop
                  dtm = (dml/dms)*dtm  ! Time to loose the whole envelop
                  dms = dml
               endif
            endif
         else
            dms = 0.d0
         endif
!
! Limit to 1% mass loss.
!
         if(dms.gt.0.01*mt)then
            dtm = 0.01d0*mt*dtm/dms
            dms = 0.01d0*mt
         endif

!
! Update mass and time and reset epoch for a MS (and possibly a HG) star.
!
         if(dms.gt.0.0)then
            mt = mt - dms
            if(kw.le.2.or.kw.eq.7)then
               m0 = mass
               mc1 = mc
               mass = mt
               tmold = tm
               tbgold = tscls(1)
               CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
               if(kw.eq.2)then
                  if(GB(9).lt.mc1)then
                     mass = m0
                  else
                     epoch = tm + (tscls(1) - tm)*(ajhold-tmold)/ &
                                  (tbgold - tmold)
                     epoch = tphys - epoch
                  endif
               else
                  epoch = tphys - ajhold*tm/tmold
               endif
            endif
         endif

         tphys2 = tphys        ! Record current time
!          if(dtm >0.0 .AND. timeFlag) dtm=(MAX(dtm*unif(minTime,maxTime),absMinTime))
           tphys = tphys +dtm   ! Increase time by dtm
!  		tphys=tphys+dtm

!
! Find the landmark luminosities and timescales as well as setting
! the GB parameters.
!
         aj = tphys - epoch
         CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
!
! Find the current radius, luminosity, core mass and stellar type
! given the initial mass, current mass, metallicity and age
!
         kwold = kw
         CALL hrdiag(mass,aj,mt,tm,tn,tscls,lums,GB,zpars, &
                     r,lum,kw,mc,rc)
!
! If mass loss has occurred and no type change then check that we
! have indeed limited the radius change to 10%.
!
         if(kw.eq.kwold.and.dms.gt.0.0.and.rflag.ne.0)then
            mt2 = mt + dms      ! Mass before mass loss
            dml = dms/dtm       ! dM/dt
            it = 0
 20         dr = r - rm0
            if(ABS(dr).gt.0.1*rm0)then
               it = it + 1
               if(it.eq.20.and.kw.eq.4) goto 30
               if(it.gt.30)then
               	  WRITE(0,*) 'ERROR evolv1.f90 evolv1 159'
                  WRITE(0,*)' DANGER1! ',it,kw,mass,dr,rm0
                  WRITE(0,*)' STOP: EVOLV1 FATAL ERROR 1'
!                  CALL exit(0)
                  STOP
               endif
               dtdr = dtm/ABS(dr)                ! |dR/dt|**(-1)
               dtm = alpha2*MAX(rm0,r)*dtdr
               if(it.ge.20) dtm = 0.5d0*dtm
               if(dtm.lt.1.0d-07*aj) goto 30
               dms = dtm*dml                     ! Mass lost during dtm
! Update mass and reset epoch
               mt = mt2 - dms
               if(kw.le.2.or.kw.eq.7)then
                  mass = mt
                  CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
                  if(kw.eq.2)then
                     if(GB(9).lt.mc1)then
                        mass = m0
                     else
                        epoch = tm + (tscls(1) - tm)*(ajhold-tmold)/ &
                                     (tbgold - tmold)
                        epoch = tphys2 - epoch
                     endif
                  else
                     epoch = tphys2 - ajhold*tm/tmold
                  endif
               endif
               tphys = tphys2 + dtm
               aj = tphys - epoch
               mc = mc1
               CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
               CALL hrdiag(mass,aj,mt,tm,tn,tscls,lums,GB,zpars, &
                           r,lum,kw,mc,rc)
               goto 20
            endif
 30         continue
         endif
!
         if (kw.eq.15) goto 90  ! Massless supernova remnant, endpoint o
!
! Reset epoch.
!
         epoch = tphys - aj
!
         if (tphys.ge.tphysf) goto 90
!
! Record radius and current age.
!
         rm0 = r
         ajhold = aj
         if(kw.ne.kwold) kwold = kw
!
! Base new time scale for changes in radius & mass on stellar type.
!
!BW dtr is the time remaining for the current evolutionary phase
         if(kw.le.1)then
            dtm = pts1*tm
            dtr = tm - aj
         elseif(kw.eq.2)then
            dtm = pts3*(tscls(1) - tm)
            dtr = tscls(1) - aj
         elseif(kw.eq.3)then
            if(aj.lt.tscls(6))then
               dtm = pts2*(tscls(4) - aj)
            else
               dtm = pts2*(tscls(5) - aj)
            endif
            dtr = MIN(tscls(2),tn) - aj
         elseif(kw.eq.4)then
            dtm = pts2*tscls(3)
            dtr = MIN(tn,tscls(2) + tscls(3)) - aj
         elseif(kw.eq.5)then
            if(aj.lt.tscls(9))then
               dtm = pts2*(tscls(7) - aj)
            else
               dtm = pts2*(tscls(8) - aj)
            endif
            dtr = MIN(tn,tscls(13)) - aj
         elseif(kw.eq.6)then
            if(aj.lt.tscls(12))then
               dtm = pts2*(tscls(10) - aj)
            else
               dtm = pts2*(tscls(11) - aj)
            endif
            dtm = MIN(dtm,0.005d0)
            dtr = tn - aj
         elseif(kw.eq.7)then
            dtm = pts3*tm
            dtr = tm - aj
         elseif(kw.eq.8.or.kw.eq.9)then
            if(aj.lt.tscls(6))then
               dtm = pts2*(tscls(4) - aj)
            else
               dtm = pts2*(tscls(5) - aj)
            endif
            dtr = tn - aj
         else
            dtm = MAX(0.1d0,aj*10.d0)
            dtr = dtm
         endif
!
! Check for type change.
!
         it = 0
         mass0 = mass           ! BW 16/05/01
         mt0 = mt               ! BW 16/05/01
         if((dtr-dtm).le.tol.and.kw.le.9)then
!
! Check final radius for too large a jump.
!
            aj = MAX(aj,aj*(1.d0-eps)+dtr)
            mc1 = mc
            CALL hrdiag(mass,aj,mt,tm,tn,tscls,lums,GB,zpars, &
                        r1,lum1,kw,mc1,rc1)

            IF ((kwold.eq.2).AND.(kw.eq.7)) THEN ! BW 16/05/01
               kw = kwold       ! BW 16/05/01
               mass = mass0     ! BW 16/05/01
               mt = mt0         ! BW 16/05/01
               goto 50          ! BW 16/05/01
            END IF              ! BW 16/05/01

            dr = r1 - rm0
            if(ABS(dr).gt.0.1*rm0)then
               dtm = dtr - ajhold*eps
               dtdr = dtm/ABS(dr)
               dtm = alpha2*MAX(r1,rm0)*dtdr
               goto 40
            else
               dtm = dtr
               goto 50
            endif
         endif
!
! Limit to a 10% increase in radius assuming no further mass loss
! and thus that the pertubation functions due to small envelope mass
! will not change the radius.
!
 40      aj = ajhold + dtm
         mc1 = mc
         CALL hrdiag(mass,aj,mt,tm,tn,tscls,lums,GB,zpars, &
                     r1,lum1,kw,mc1,rc1)

         IF ((kwold.eq.2).AND.(kw.eq.7)) THEN ! BW 16/05/01
            kw = kwold          ! BW 16/05/01
            mass = mass0        ! BW 16/05/01
            mt = mt0            ! BW 16/05/01
            goto 50             ! BW 16/05/01
         END IF                 ! BW 16/05/01

         dr = r1 - rm0
         it = it + 1
         if(it.eq.20.and.kw.eq.4) goto 50
         if(it.gt.30)then
         	WRITE(0,*) 'ERROR evolv1.f90 evolv1 314'
            WRITE(0,*)' DANGER2! ',it,kw,mass,dr,rm0
            WRITE(0,*)' END ERROR'
!            CALL exit(0)
            STOP
         endif
         if(ABS(dr).gt.0.1*rm0)then
            dtdr = dtm/ABS(dr)
            dtm = alpha2*MAX(rm0,r1)*dtdr
            if(it.ge.20) dtm = 0.5d0*dtm
            goto 40
         endif
!
 50      continue
!
!
! Choose minimum of time-scale and remaining interval (> 100 yrs).
!
! 		write(*,*) "e1",dtm,1.0d-07*aj,tphysf,tphys,tphysf-tphys

		!Make sure we shrink the timestep near the MS hook so that we can resolve it properly and thus enter
		! the HG phase at the proper location
!  		if((kw==1 .or. kw==2) .and. tphys > thookf(mass)*tscls(1))then
 		if(kw==2 )then
			minTime=0.3d0
			maxTime=0.6d0
		else
			minTime=0.75d0
			maxTime=1.d0
		end if

         if(timeFlag) dtm=(MAX(dtm*unif(minTime,maxTime),absMinTime))
         dtm = MAX(dtm,1.0d-07*aj)
         dtm = MIN(dtm,tphysf-tphys)

! 		if (kw < 3 .and. kw/=0) then
! 		write(*,*) kw,dtm,tscls(1),tphys,tphys2,thookf(mass)*tscls(1)
! 		end if
! 		dtm=MAX(dtm,absMinTime)

!
 10   continue
!
 90   continue
!
!
      RETURN
      END SUBROUTINE evolv1
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE evolves
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
