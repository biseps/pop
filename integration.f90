!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	MODULE integration
		USE error
	CONTAINS
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE polint(xa,ya,n,x,y,dy,caller)
	IMPLICIT NONE
	INTEGER :: n,NMAX
	DOUBLE PRECISION :: dy,x,y,xa(n),ya(n)
	PARAMETER (NMAX=32)
	INTEGER :: i,m,ns
	DOUBLE PRECISION :: den,dif,dift,ho,hp,w,c(NMAX),d(NMAX)
	CHARACTER(len=6),INTENT(IN) :: caller
	ns=1
	dif=abs(x-xa(1))
	DO i=1,n
		dift=abs(x-xa(i))
		if(dift.lt.dif)then
			ns=i
			dif=dift
		end if
		c(i)=ya(i)
		d(i)=ya(i)
	END DO
	
	y=ya(ns)
	ns=ns-1
	outer:DO m=1,n-1
		inner:DO i=1,n-m
			ho=xa(i)-x
			hp=xa(i+m)-x
			w=c(i+1)-d(i)
			den=ho-hp
			if(den.eq.0.) then
				WRITE(0,*) 'ERROR integration.f90 polint 35'
				write(0,*) "Failure in polint ",caller
				write(0,*) xa,ya,n,x,y,dy
				WRITE(0,*) 'END ERROR'
				call setError()
				stop
			end if
			den=w/den
			d(i)=hp*den
			c(i)=ho*den
		END DO inner
		if(2*ns.lt.n-m)then
			dy=c(ns+1)
		else
			dy=d(ns)
			ns=ns-1
		end if
		y=y+dy
	END DO outer
	return
	END SUBROUTINE polint


	PURE SUBROUTINE create_bins (x0, dx, n, bin_edges)
        ! Determine bin edges for equi-distantly spaced bins
        ! Bart Willems,  19/01/02

        IMPLICIT NONE

        DOUBLE PRECISION,                   INTENT(IN)  :: x0
        DOUBLE PRECISION,                   INTENT(IN)  :: dx
        INTEGER,                            INTENT(IN)  :: n
        DOUBLE PRECISION,  DIMENSION(n+1),  INTENT(OUT) :: bin_edges
        INTEGER :: i

        do i = 1, n+1

            bin_edges(i) = x0 + DBLE(i-1) * dx

        end do

        return

	END SUBROUTINE create_bins



!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	DOUBLE PRECISION FUNCTION hlp2(d,l,b1,b2,R0,z0,hR,hz,dl)
	USE distributions
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: d,l,b1,b2,dl
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION :: s

	CALL qrombb(galdisc,d,l,b1,b2,s,R0,z0,hR,hz,dl)

	hlp2 = s

	RETURN
	END FUNCTION hlp2
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	DOUBLE PRECISION FUNCTION hlp1(d,l1,l2,b1,b2,R0,z0,hR,hz)
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: d,l1,l2,b1,b2
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION :: s

	CALL qrombl(hlp2,d,l1,l2,b1,b2,s,R0,z0,hR,hz)

	hlp1 = s

	RETURN
	END FUNCTION hlp1
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	DOUBLE PRECISION FUNCTION integrate_galactic_field(d1,d2,l1,l2,b1,b2, &
	R0,z0,hR,hz)
	USE polygon
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Integrate the Galactic disc double exponential density distribution
! between the distances d1 and d2, for a given galactic field
! delimited by the Galactic longitudes l1 and l2 and the Galactic
! latitudes b1 and b2.
!
! R0 is the distance of the sun to the Galactic center in pc
! z0 is the height of the sun above the Galactic plane in pc
! hR is the scale length of the Galactic disc in pc
! hz is the scale height of the Galactic disc in pc
!
! Bart Willems, 30/07/03
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	IMPLICIT NONE
	INTEGER :: i
	INTEGER,PARAMETER :: n=4
	DOUBLE PRECISION, INTENT(IN) :: d1,d2,l1,l2,b1,b2
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION :: s
	DOUBLE PRECISION,DIMENSION(0:n-1) :: x,y
	LOGICAL,DIMENSION(0:n-1) :: res

! 	x(0)=l2
! 	x(1)=l2
! 	x(2)=l1
! 	x(3)=l1
! 
! 	y(0)=b1
! 	y(1)=b2
! 	y(2)=b2
! 	y(3)=b1
! 
! 	DO i=0,n-1
! 		res(i)=PNPOLY(x(i),y(i))
! 	end do
! 
! 	IF((.not.res(0).and..not.res(1)).and.(.not.res(2).and..not.res(3))) THEN
! 	!No points inside
! 		integrate_galactic_field=0.0d0
! 		return
! 	END IF

	CALL qrombd(hlp1,d1,d2,l1,l2,b1,b2,s,R0,z0,hR,hz)

	integrate_galactic_field = s

	RETURN
	END FUNCTION integrate_galactic_field
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE qrombd(func,a,b,l1,l2,b1,b2,ss,R0,z0,hR,hz)
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: a,b,l1,l2,b1,b2
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION, INTENT(OUT) :: ss
	INTEGER, PARAMETER :: JMAX = 32, JMAXP=JMAX+1,K=5,KM=K-1
	DOUBLE PRECISION, PARAMETER :: EPS = 1.d-3
	DOUBLE PRECISION, EXTERNAL :: func
	DOUBLE PRECISION :: dss,h(JMAXP),s(JMAXP)
!U    USES trapzd
	INTEGER :: j
	h(1)=1.0
	DO j=1,2*k
	call trapzdd(func,a,b,l1,l2,b1,b2,s(j),j,R0,z0,hR,hz)
	if (j.ge.k) then
		call polint(h(j-KM),s(j-KM),K,0.0d0,ss,dss,"qrombd")
		if (abs(dss).le.EPS*abs(ss))THEN
!          		write(*,*) "bd",j
			return
		endif
	endif
	if(j.ne.2*k)THEN
	s(j+1)=s(j)
	h(j+1)=0.25*h(j)
	END IF
	END DO
!       write(0,'(6(e12.5,1x))') a,b,l1,l2,b1,b2,dss,EPS*abs(ss)
!       call setError()
!       write(0,*) 'too many steps in qrombd'
!       stop 
	END SUBROUTINE qrombd
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE qrombl(func,d,a,b,b1,b2,ss,R0,z0,hR,hz)
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: a,b,d,b1,b2
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION, INTENT(OUT) :: ss
	INTEGER, PARAMETER :: JMAX = 32, JMAXP=JMAX+1,K=5,KM=K-1
	DOUBLE PRECISION, PARAMETER :: EPS = 1.d-3

	DOUBLE PRECISION, EXTERNAL :: func
	DOUBLE PRECISION :: dss,h(JMAXP),s(JMAXP)
!U    USES trapzd
	INTEGER :: j
	h(1)=1.0
	DO j=1,2*k
	call trapzdl(func,d,a,b,b1,b2,s(j),j,R0,z0,hR,hz)
	if (j .ge.k) then
		call polint(h(j-KM),s(j-KM),K,0.0d0,ss,dss,"qrombl")
		if (abs(dss).le.EPS*abs(ss))THEN
!          		write(*,*) "bl",j
		return
		endif
	endif
	if(j.ne.2*k)THEN
	s(j+1)=s(j)
	h(j+1)=0.25*h(j)
	END IF
	END DO
!       write(0,'(5(e12.5,1x))') d,a,b,b1,b2,dss,EPS*abs(ss)
!       call setError()
!       write(0,*) 'too many steps in qrombl'
!       stop 
	END SUBROUTINE qrombl
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE qrombb(func,d,l,a,b,ss,R0,z0,hR,hz,dl)
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: a,b,d,l,dl
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION, INTENT(OUT) :: ss
	INTEGER, PARAMETER :: JMAX = 32, JMAXP=JMAX+1,K=5,KM=K-1
	DOUBLE PRECISION, PARAMETER :: EPS = 1.d-3
	DOUBLE PRECISION, EXTERNAL :: func
	DOUBLE PRECISION :: dss,h(JMAXP),s(JMAXP)
!U    USES trapzd
	INTEGER :: j
	h(1)=1.0
	DO j=1,2*k
	call trapzdb(func,d,l,a,b,s(j),j,R0,z0,hR,hz,dl)
	if (j .ge.k) then
		call polint(h(j-KM),s(j-KM),K,0.0d0,ss,dss,"qrombb")
		if (abs(dss).le.EPS*abs(ss))THEN
!          		write(*,*) "bb",j
! 			write(*,*) "*",j,abs(dss),abs(ss),EPS*abs(ss)
			return
		endif
! 		write(*,*) j,abs(dss),abs(ss),EPS*abs(ss),d,l,(a-b)/2.0
	endif
	if(j.ne.2*k)THEN
	s(j+1)=s(j)
	h(j+1)=0.25*h(j)
	END IF
	END DO
!       write(0,'(4(e12.5,1x))') d,l,a,b,dss,EPS*abs(ss)
!       call setError()
!       write(0,*) 'too many steps in qrombb'
!       stop 
	END SUBROUTINE qrombb
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE trapzdd(func,a,b,l1,l2,b1,b2,s,n,R0,z0,hR,hz)
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: n
	DOUBLE PRECISION, INTENT(IN) :: a,b,l1,l2,b1,b2
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION, INTENT(INOUT) :: s
	DOUBLE PRECISION, EXTERNAL :: func
	INTEGER :: it,j
	DOUBLE PRECISION :: del,total,tnm,x
	if (n.eq.1) then
	s=0.5*(b-a)*(func(a,l1,l2,b1,b2,R0,z0,hR,hz) &
		+func(b,l1,l2,b1,b2,R0,z0,hR,hz))
	else
	it=2**(n-2)
	tnm=it
	del=(b-a)/tnm
	x=a+0.5*del
	total=0.
	do j=1,it
	total=total+func(x,l1,l2,b1,b2,R0,z0,hR,hz)
	x=x+del
	END DO
	s=0.5*(s+(b-a)*total/tnm)
	endif
	return
	END SUBROUTINE trapzdd
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE trapzdl(func,d,a,b,b1,b2,s,n,R0,z0,hR,hz)
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: n
	DOUBLE PRECISION, INTENT(IN) :: a,b,d,b1,b2
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION, INTENT(INOUT) :: s
	DOUBLE PRECISION, EXTERNAL :: func
	INTEGER :: it,j
	DOUBLE PRECISION :: del,total,tnm,x,dl

	dl=b-a
	if (n.eq.1) then
	s=0.5*(b-a)*(func(d,a,b1,b2,R0,z0,hR,hz,dl) &
		+func(d,b,b1,b2,R0,z0,hR,hz,dl))
	else
	it=2**(n-2)
	tnm=it
	del=(b-a)/tnm
	x=a+0.5*del
	total=0.
	do j=1,it
	total=total+func(d,x,b1,b2,R0,z0,hR,hz,del)
	x=x+del
	END DO
	s=0.5*(s+(b-a)*total/tnm)
	endif
	return
	END SUBROUTINE trapzdl
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	SUBROUTINE trapzdb(func,d,l,a,b,s,n,R0,z0,hR,hz,dl)
	IMPLICIT NONE
	INTEGER, INTENT(IN) :: n
	DOUBLE PRECISION, INTENT(IN) :: a,b,d,l,dl
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION, INTENT(INOUT) :: s
	DOUBLE PRECISION, EXTERNAL :: func
	INTEGER :: it,j
	DOUBLE PRECISION :: del,total,tnm,x,db
	db=b-a
	if (n.eq.1) then
	s=0.5*(b-a)*(func(d,l,a,R0,z0,hR,hz,dl,db)+func(d,l,b,R0,z0,hR,hz,dl,db))
	else
	it=2**(n-2)
	tnm=it
	del=(b-a)/tnm
	x=a+0.5*del
	total=0.
	do j=1,it
	total=total+func(d,l,x,R0,z0,hR,hz,dl,del)
	x=x+del
	END DO
	s=0.5*(s+(b-a)*total/tnm)
	endif
	return
	END SUBROUTINE trapzdb
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	END MODULE INTEGRATION
