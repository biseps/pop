MODULE extinctionDrimmel
	USE consts
    IMPLICIT NONE
    INTEGER,PARAMETER ::  npix=393216
    DOUBLE PRECISION,PARAMETER :: zsun=0.015d0, xsun=-8.d0
    DOUBLE PRECISION,PARAMETER :: deg_rad=pi/180.d0,ep=23.389291111d0
    DOUBLE PRECISION,PARAMETER :: alphag=192.859458333d0, deltag=27.12825d0,l_gp=122.9285d0


!     3D Grids from SWG-RD-02
    REAL*4,DIMENSION(151,151,151) :: avgrid 
    REAL*4,DIMENSION(101,201,51) :: avloc

!     3D Grids from SWG-RD-03

!     Large scale
    REAL*4,DIMENSION(151,151,51) :: avdisk, avspir
    REAL*4,DIMENSION(76,151,51) :: avori

!     Local scale
    REAL*4,DIMENSION(101,201,51) :: avori2
    REAL*4,DIMENSION(31,31,51) :: avdloc

!     Rescaling factors
    REAL*4,DIMENSION(npix) :: glonold,glatold,rf,glon,glat,rfac
    INTEGER,DIMENSION(npix) :: ncompold,pixnum,ncomp
    INTEGER :: intDrim=0

    CONTAINS


    SUBROUTINE preIntDrimmExt()
	  IMPLICIT NONE

		INTEGER             :: i
        LOGICAL             :: fileExists
        CHARACTER(len=52)   :: drimmel_folder='/padata/beta/users/rfarmer/tables/drimmelExtinction/'


        ! Make sure we have the correct path
        fileExists=.false.
        inquire(FILE=drimmel_folder // 'avdisk.dat', exist=fileExists)
        if(fileExists .eqv. .false.) then
            write(0, *) "Can't find drimmel extinction file avdisk.dat!"
            write(0, *) "Is drimmel_folder correct? "
            write(0, *) "drimmel_folder =  ", drimmel_folder
            write(0, *) "Exiting program"
            stop
        end if


        ! Another check that we have the correct path
        fileExists=.false.
        inquire(FILE=drimmel_folder // 'rf_allsky.dat', exist=fileExists)
        if(fileExists .eqv. .false.) then
            write(0, *) "Can't find drimmel extinction file rf_allsky.dat!"
            write(0, *) "Is drimmel_folder correct? "
            write(0, *) "drimmel_folder =  ", drimmel_folder
            write(0, *) "Exiting program"
            stop
        end if


		open(11, file=drimmel_folder // 'avdisk.dat', status='old',  form='unformatted')
		open(12, file=drimmel_folder // 'avspir.dat', status='old',  form='unformatted')
		open(13, file=drimmel_folder // 'avori.dat',  status='old',  form='unformatted')
		open(14, file=drimmel_folder // 'avori2.dat', status='old',  form='unformatted')
		open(15, file=drimmel_folder // 'avdloc.dat', status='old',  form='unformatted')

		read(11) avdisk
		read(12) avspir
		read(13) avori
		read(14) avori2
		read(15) avdloc

		close(11)
		close(12)
		close(13)
		close(14)
		close(15)

		open(16,file=drimmel_folder // 'rf_allsky.dat', status='old',form='unformatted')
		read(16) pixnum, ncompold, glonold, glatold, rf
		close(16)

		do i = 1, npix
			ncomp(1+pixnum(i)) = ncompold(i)
			glon(1+pixnum(i))  = glonold(i)
			glat(1+pixnum(i))  = glatold(i)
			rfac(1+pixnum(i))  = rf(i)
		end do

		intDrim = 1

    END SUBROUTINE



!NUmbers in degrees and kpc
	DOUBLE PRECISION FUNCTION extinctDrimmel(long,lat,dis)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: long,lat,dis
		DOUBLE PRECISION :: l,b,d,ll,bb,abs_d,abs_o,abs_s
		DOUBLE PRECISION :: d1,d2,d3,dmax
		DOUBLE PRECISION :: d_ori,dmax_ori,dori1,dori2,dori3
		DOUBLE PRECISION :: z,z_ori,x,y,x_ori,y_ori
		DOUBLE PRECISION :: x_gc,x_gc_ori,x_he
		DOUBLE PRECISION :: f_i,f_j,f_k
		INTEGER,PARAMETER :: itype=3
		INTEGER :: i,j,k,ip,jp,kp,n_cen,icomponent
		INTEGER*4 :: ipix
		CHARACTER(len=1) :: afact
		DOUBLE PRECISION,DIMENSION(2,2,2) :: a
		DOUBLE PRECISION,DIMENSION (3) :: delta,n,fac

		!Check files initlized?
		if(intDrim /= 1) THEN
			CALL preIntDrimmExt()
			intDrim=1
		end if

! To avoid sin/cos equal zero in the denominator
		l=long
		b=lat
		d=dis

		if(b.eq.0.d0.or.b.eq.90.d0.or.b.eq.-90.d0) THEN
			b=b+1.d-10
		END IF
		if(l.eq.0.d0.or.l.eq.180.d0.or.l.eq.90.d0.or.l.eq.270.d0)THEN
			l=l+1.d-10
		end if

!     ------------------------------------------------------------
!
!     If the star is located out of the large-scale grid, its 
!    distance is reduced to the maximum size of the grid 
!
!    ------------------------------------------------------------


		ll=l*deg_rad
		bb=b*deg_rad

!		c     Greatest distance of the grid in z 
			d1=0.5d0/dabs(dsin(bb))-zsun/dsin(bb)

!		C     Greatest distance of the grid in y
			d2=15.d0/dabs(dcos(ll))-xsun/dcos(ll)
		d2=d2/dabs(dcos(bb))


!		c     Greatest distance of the grid in x
			d3=15.d0/dabs(dsin(ll))
		d3=d3/dabs(dcos(bb))


!		c     We choose the lower maximum distance
			dmax=min(d1,d2)
			dmax=min(dmax,d3)

!		c     If the star distance is greater than dmax
!		c     we decrease this distance to d=dmax

			if(d.gt.dmax) then
				d=dmax
			endif

!		c     ------------------------------------------------------------
!		c
!		c     Conversion from (l,b,d) to (x,y,z)   
!		c
!		c     ------------------------------------------------------------


		x=d*dcos(bb)*dcos(ll)
		y=d*dcos(bb)*dsin(ll)
		z=d*dsin(bb)+zsun

		x_gc = x + xsun
		x_he = x


!		c      Defined only to consider the non-centered origin of the 
!		c      large scale grid for the Orion Arm

		n_cen = 0

!		c     ------------------------------------------------------------
!		c
!		c     Computation of the rescaling factors  (SWG-RD-03)
!		c
!		c     ------------------------------------------------------------

!		c          fac(1)    !  disk component rescaling factor
!		c          fac(2)    !  spiral component rescaling factor
!		c          fac(3)    !  orion component rescaling factor

		do i=1,3
			fac(i)=1.d0
		enddo

		call ll2pix (l,b,ipix)

!		c     print *, glon(ipix+1),glat(ipix+1),ipix+1
		icomponent=ncomp(ipix+1)
		fac(icomponent)=rfac(ipix+1)
!		c     ------------------------------------------------------------
!		c
!		c     Computation of the Av considering rescaling factors(SWG-RD-03)
!		c
!		c     ------------------------------------------------------------

!		c     Disk component:

!		c       Large scale disk component:

		if ((dabs(x).gt.0.75d0).or.(dabs(y).gt.0.75d0)) then 

			delta(1)= 0.2d0
			delta(2)= 0.2d0
			delta(3)=0.02d0

			n(1)=151
			n(2)=151
			n(3)= 51

			call point (x_gc,y,z,n,delta,i,j,k,n_cen,f_i,f_j,f_k) 

			do ip = 1,2
				do jp = 1,2
					do kp = 1,2
						a(ip,jp,kp)=avdisk(i+ip-1,j+jp-1,k+kp-1)
					end do
				end do
			end do

		else 

!		c       Local scale disk component:

			delta(1)=0.05d0
			delta(2)=0.05d0
			delta(3)=0.02d0

			n(1)=31
			n(2)=31
			n(3)=51

			call point (x_he,y,z,n,delta,i,j,k,n_cen,f_i,f_j,f_k) 

			do ip = 1,2
				do jp = 1,2
					do kp = 1,2
						a(ip,jp,kp)=avdloc(i+ip-1,j+jp-1,k+kp-1)
					end do
				end do
			end do

		endif 

		call interp (a,f_i,f_j,f_k,abs_d)

!		c     Spiral component:

		delta(1)= 0.2d0
		delta(2)= 0.2d0
		delta(3)=0.02d0

		n(1)=151
		n(2)=151
		n(3)= 51
		call point (x_gc,y,z,n,delta,i,j,k,n_cen,f_i,f_j,f_k) 

		do ip = 1,2
			do jp = 1,2
				do kp = 1,2
					a(ip,jp,kp)=avspir(i+ip-1,j+jp-1,k+kp-1)
				end do
			end do
		end do 

		call interp (a,f_i,f_j,f_k,abs_s)

!		c     Orion arm component:

!		c       Large scale orion arm component:

		if ((dabs(x).gt.1.d0).or.(dabs(y).gt.2.d0)) then 

!		c     Greatest distance of the grid in z 
			dori1=0.5d0/dabs(dsin(bb))-zsun/dsin(bb)

!		c     Greatest distance of the grid in y
			dori2=3.75d0/dabs(dsin(ll))
			dori2=dori2/dabs(dcos(bb))

!		c     Greatest distance of the grid in x
			if(cos(ll).gt.0.d0)dori3=2.375/dabs(dcos(ll))
			if(cos(ll).lt.0.d0)dori3=1.375/dabs(dcos(ll))
			dori3=dori3/dabs(dcos(bb))

!		c     We choose the lower maximum distance
			dmax_ori=min(dori1,dori2)
			dmax_ori=min(dmax_ori,dori3)

!		c        print*, 'dmax_ori=', dmax_ori

!		c     If the star distance is greater than dmax_ori
!		c     we decrease this distance to d=dmax_ori

			if(d.gt.dmax_ori) then
				d_ori=dmax_ori
			else
				d_ori=d
			endif

!		c     Conversion from (l,b,d) to (x,y,z)   

			x_ori=d_ori*dcos(bb)*dcos(ll)
			y_ori=d_ori*dcos(bb)*dsin(ll)
			z_ori=d_ori*dsin(bb)+zsun

			x_gc_ori = x_ori + xsun

			delta(1)= 0.05d0
			delta(2)= 0.05d0
			delta(3)=0.02d0

			n(1)= 76 
			n(2)=151
			n(3)= 51

			n_cen=1

			call point (x_gc_ori,y_ori,z_ori,n,delta,i,j,k,n_cen,f_i,f_j,f_k) 


			do ip = 1,2
				do jp = 1,2
					do kp = 1,2

					if(x_gc_ori.gt.0.d0) then
						a(ip,jp,kp)=0.d0
					else
						a(ip,jp,kp)=avori(i+ip-1,j+jp-1,k+kp-1)
					endif
					end do
				end do
			end do

		else 

!		c       Local scale orion arm component:

			delta(1)=0.02d0
			delta(2)=0.02d0
			delta(3)=0.02d0

			n(1)=101
			n(2)=201
			n(3)=51

			call point (x_he,y,z,n,delta,i,j,k,n_cen,f_i,f_j,f_k) 


			do ip = 1,2
				do jp = 1,2
					do kp = 1,2
						a(ip,jp,kp)=avori2(i+ip-1,j+jp-1,k+kp-1)
					end do
				end do
			end do

		endif 

		call interp (a,f_i,f_j,f_k,abs_o)

!		c       Computation of the total absorption 

		extinctDrimmel = fac(1)*abs_d + fac(2)*abs_s + fac(3)*abs_o

	END FUNCTION extinctDrimmel

    SUBROUTINE coord (itype, xx, yy, l, b)
	  IMPLICIT NONE
	  INTEGER,INTENT(IN) :: itype
	  DOUBLE PRECISION,INTENT(INOUT) :: xx,yy
	  DOUBLE PRECISION,INTENT(OUT) :: l,b
	  DOUBLE PRECISION :: a_g,d_g,alpha,delta,epp

	  !     itype = 1        !Ecliptic Coordinates
	  !     itype = 2        !Equatorial Coordinates
	  !     itype = 3        !Galactic Coordinates

	  if(itype.eq.3) then
		l=xx
		b=yy
	  else

	  !       Conversion from ecliptic to equatorial

		if(itype.eq.1) then

		    xx=xx*deg_rad
		    yy=yy*deg_rad
		    epp=ep*deg_rad

		delta=(dasin(dsin(yy)*dcos(epp)+dcos(yy)*dsin(epp)*dsin(xx)))
		alpha=dacos(dcos(yy)*dcos(xx)/dcos(delta))

		else
		    alpha=xx*deg_rad
		    delta=yy*deg_rad

		endif

		!	Conversion from equatorial to galactic

		a_g=alphag*deg_rad
		d_g=deltag*deg_rad

		b=dasin(dsin(delta)*dsin(d_g)+dcos(delta)*dcos(d_g)*dcos((a_g-alpha)))/deg_rad
		l=l_gp-dasin(-dcos(delta)*dsin((a_g-alpha))/dcos(b))/deg_rad
	  endif

      END SUBROUTINE coord

!     ..........................................................

!     Computation of the grid point & interpolation factors
!     ..........................................................

      SUBROUTINE point (x,y,z,n,delta,i,j,k,n_cen,f_i,f_j,f_k)
! 	  implicit double precision (a-h,o-z)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: x,y,z
		DOUBLE PRECISION,DIMENSION(3),INTENT(IN) :: delta,n
		INTEGER :: i,j,k
		INTEGER,INTENT(IN) :: n_cen
		DOUBLE PRECISION,INTENT(OUT) :: f_i,f_j,f_k
		DOUBLE PRECISION :: exacti,exactj,exactk


        if(n_cen.ne.1) then
            exacti=x/delta(1)+(n(1)-1.d0)/2.d0+1.d0
        else
            exacti=x/delta(1)+2.5d0*(n(1)-1.d0)+1.d0
        endif

        exactj=y/delta(2)+(n(2)-1.d0)/2.d0+1.d0
        exactk=z/delta(3)+(n(3)-1.d0)/2.d0+1.d0

		i=int(exacti)
		j=int(exactj)
		k=int(exactk)

        if(i.eq.n(1))i=i-1
        if(j.eq.n(2))j=j-1
        if(k.eq.n(3))k=k-1

        if(i.eq.0)i=i+1
        if(j.eq.0)j=j+1
        if(k.eq.0)k=k+1

		f_i=exacti-i
		f_j=exactj-j
		f_k=exactk-k

      END SUBROUTINE point
!     ..........................................................

!     Linear interpolation inside the grids
!     ..........................................................

	SUBROUTINE  interp (a,f_i,f_j,f_k,res)
!		implicit double precision (a-h,o-z)
		IMPLICIT NONE
		DOUBLE PRECISION,DIMENSION(2,2,2),INTENT(IN) :: a
		DOUBLE PRECISION,INTENT(IN) :: f_i,f_j,f_k
		DOUBLE PRECISION,INTENT(OUT) :: res
		DOUBLE PRECISION :: p1,p2,p3,p4,p12,p34


		p1=a(1,1,1)+(a(2,1,1)-a(1,1,1))*f_i

		p2=a(1,2,1)+(a(2,2,1)-a(1,2,1))*f_i

		p3=a(1,1,2)+(a(2,1,2)-a(1,1,2))*f_i

		p4=a(1,2,2)+(a(2,2,2)-a(1,2,2))*f_i

		p12=p1+(p2-p1)*f_j

		p34=p3+(p4-p3)*f_j

		res=p12+(p34-p12)*f_k

	END SUBROUTINE interp
!     ..........................................................

!	Computation of the pixel number
!	(Using some implemented COBE routines)

!     ..........................................................

	SUBROUTINE ll2pix (longitude,latitude,pixel)
!
!  NAME:
!    lonlat_to_pixel
!
!  PURPOSE:
!        Routine returns pixel number given the longitude and latitude.
!        Additional inputs are the coordinate system and output SKYCUBE
!        resolution. Note, all coordinates are at epoch=J2000.
!
!  CALLING SEQUENCE:
!    ll2pix (longitude, latitude, resolution) > pixel number
!
!  INPUT:
!    longitude, latitude
!    resolution - Quad-cube resolution
!    coord - output coordinate system
!
!  OUTPUT:
!    pixel number
!
!  SUBROUTINES CALLED:
!    PIXEL_NUMBER, INCUBE, AXISXY, BIT_SET
!    CONV_G2E,CONV_Q2E
!
!  REVISION HISTORY
!
     IMPLICIT NONE
!    CHARACTER(len=1) :: *    COORD         ! Coordinate System
		INTEGER*4   ::   RESOLUTION    ! Quad-cube resolution
		INTEGER*8   ::   ISTAT         ! Status
! 		INTEGER*8   ::  ll2uv         ! Routine convert lon/lat to unit vector
		INTEGER*4   ::   PIXEL         ! Pixel number
		REAL*8,DIMENSION(3)  :: VECTOR    ! Temp. unit vector to center of pixel
		REAL*8,DIMENSION(3)  :: OVECTOR    ! Unit vector to center of pixel
		!      real*4         PIXEL_NUMBER  ! Routine computes pixel_numbers
		!      real*4         CONV_Q2E, CONV_G2E !Convert from galactic to ecliptic
		!                                        !and equatorial to ecliptic
		REAL*8 :: longitude,latitude  !output longitude and latitude

!
      istat=ll2uv(longitude,latitude,vector)

      call CONV_G2E(vector,ovector)

      resolution=9
      call PIXEL_NUMBER(ovector,resolution,pixel)
      return
      END SUBROUTINE ll2pix
!
      SUBROUTINE PIXEL_NUMBER(ovector,resolution,pixel)
!
!    Routine to return the pixel number corresponding to the input unit
!  vector in the given resolution.  Adapted from the SUPER_PIXNO routine
!  by E. Wright, this routine determines the pixel number in the maximum
!  resolution (15), then divides by the appropriate power of 4 to determine
!  the pixel number in the desired resolution.
!*
!*     PROLOGUE:
!*
!h     Date    Version   SPR#   Programmer    Comments
!h     ----    -------   ----   ----------    --------
!h
!
      Implicit NONE
!
!  Arguments:
!
      Real*8,INTENT(IN),DIMENSION(3)   ::OVECTOR ! Unit vector of position of interest
      Integer*4,INTENT(IN) ::    RESOLUTION    ! Resolution of quad cube
      Integer*4,INTENT(OUT) ::    PIXEL         ! Pixel number (0 -> 6*((2**(res-1))**2)
!
!  Variables:
!
      Integer*4 ::     FACE2             ! I*2 Cube face number (0-5)
      Integer*4 ::     FACE4             ! I*4 Cube face number
      Real*4   ::     X,Y               ! Coordinates on cube face
      Integer*4,DIMENSION(128) :: IX
	  INTEGER*4,DIMENSION(128) :: IY   ! Bit tables for calculating I,J
      Integer*4 ::    I,J               ! Integer pixel coordinates
      Integer*4 ::    IP,ID
      Integer*4 ::    FOUR             ! I*4 '4' - reuired to avoid integer
                                      !  overflow for large pixel numbers
      Integer*4 ::    IL,IH,JL,JH       ! High and low 2-bytes of I & J
      Integer*2 ::    jh1,ih1
!      Integer*4     jishft
!      Integer*2     iishft
!       Real*4,Parameter :: TWO14= 2**14,TWO28= 2**28
!
!
!        Data IX(128) /0/
		IX=0
      if (ix(128) .eq. 0) call BIT_SET(ix,iy,128)
!
      call AXISXY(ovector,face2,x,y)
      face4 = face2
      i = 2.**14 * x
      j = 2.**14 * y
      if (i .gt. 16383) i = 16383
      if (j .gt. 16383) j = 16383
      ih = ishft(i,-7)
      ih1 = ih
      il = i - ishft(ih1,7)
      jh = ishft(j,-7)
      jh1= jh
      jl = j - ishft(jh1,7)
      pixel = ishft(face4,28) + ix(il+1) + iy(jl+1) +ishft((ix(ih+1) + iy(jh+1)),14)
!
!     'Pixel' now contains the pixel number for a resolution of 15.  To
!     convert to the desired resolution, (integer) divide by 4 to the power
!     of the difference between 15 and the given resolution:
!
      pixel = ishft(pixel,-(30-2*resolution))  !  = pixel / 4**(15-resolution)
!
      return
      END SUBROUTINE PIXEL_NUMBER

	SUBROUTINE INCUBE(ALPHA,BETA,X,Y)
!
!*     PROLOGUE:
!*
!h     Date    Version   SPR#   Programmer    Comments
!h     ----    -------   ----   ----------    --------
!h
		IMPLICIT NONE
		REAL*4,INTENT(IN) :: ALPHA,BETA
		Real*4 :: GSTAR_1,M_G,C_COMB
		Real*4,PARAMETER :: GSTAR=1.37484847732
		Real*4,PARAMETER :: G=-0.13161671474
		Real*4,PARAMETER :: M=0.004869491981
		Real*4,PARAMETER :: W1=-0.159596235474
		Real*4,PARAMETER :: C00=0.141189631152
		Real*4,PARAMETER :: C10=0.0809701286525
		Real*4,PARAMETER :: C01=-0.281528535557
		Real*4,PARAMETER :: C11=0.15384112876
		Real*4,PARAMETER :: C20=-0.178251207466
		Real*4,PARAMETER :: C02=0.106959469314
		Real*4,PARAMETER :: D0=0.0759196200467
		Real*4,PARAMETER :: D1=-0.0217762490699
		Real*4,PARAMETER ::  R0=0.577350269
		REAL*4 :: X,Y
		REAL*4 :: AA,BB,A4,B4,ONMAA,ONMBB
		AA=ALPHA**2
		BB=BETA**2
		A4=AA**2
		B4=BB**2
		ONMAA=1.-AA
		ONMBB=1.-BB

        gstar_1 = 1. - gstar
        m_g     = m - g
        c_comb  = c00 + c11*aa*bb

		X = ALPHA * &
            (GSTAR + &
             AA * gstar_1 + &
             ONMAA * (BB * (G + (m_g)*AA + &
     	                     ONMBB * (c_comb + C10*AA + C01*BB + &
                                     C20*A4 + C02*B4)) + &
     	               AA * (W1 - ONMAA*(D0 + D1*AA))))

		Y = BETA * &
            (GSTAR + &
             BB * gstar_1 + &
            ONMBB * (AA * (G + (m_g)*BB + &
                           ONMAA * (c_comb + C10*BB + C01*AA + &
                                     C20*B4+C02*A4)) + &
                      BB * (W1 - ONMBB*(D0 + D1*BB))))
		RETURN
	END SUBROUTINE INCUBE

	SUBROUTINE AXISXY(C,NFACE,X,Y)
		IMPLICIT NONE
!	CONVERTS UNIT VECTOR C INTO NFACE NUMBER (0-5) AND X,Y
!	IN RANGE 0-1
!
		REAL*8,INTENT(IN),DIMENSION(3) :: C
		INTEGER :: NFACE
		REAL*4 :: X,Y
		REAL*4 :: ac3,ac2,ac1,ETA,XI
		AC3=ABS(C(3))
		AC2=ABS(C(2))
		AC1=ABS(C(1))
		IF(AC3.GT.AC2) THEN
			IF(AC3.GT.AC1) THEN
				IF(C(3).GT.0.) THEN
					NFACE=0
					ETA=-C(1)/C(3)
					XI=C(2)/C(3)

				ELSE
					NFACE=5
					ETA=-C(1)/C(3)
					XI=-C(2)/C(3)
				END IF
			ELSE
				IF(C(1).GT.0.) THEN
					NFACE=1
					XI=C(2)/C(1)
					ETA=C(3)/C(1)
				ELSE
					NFACE=3
					ETA=-C(3)/C(1)
					XI=C(2)/C(1)
				ENDIF
			ENDIF
		ELSE
			IF(AC2.GT.AC1) THEN
				IF(C(2).GT.0.) THEN
					NFACE=2
					ETA=C(3)/C(2)
					XI=-C(1)/C(2)
				ELSE
					NFACE=4
					ETA=-C(3)/C(2)
					XI=-C(1)/C(2)
				END IF
			ELSE
				IF(C(1).GT.0.) THEN
					NFACE=1
					XI=C(2)/C(1)
					ETA=C(3)/C(1)
				ELSE
					NFACE=3
					ETA=-C(3)/C(1)
					XI=C(2)/C(1)
				END IF
			ENDIF
		ENDIF

		CALL INCUBE(XI,ETA,X,Y)
		X=(X+1.)/2.
		Y=(Y+1.)/2.
		RETURN
	END SUBROUTINE

	Subroutine BIT_SET(IX,IY,LENGTH)
		IMPLICIT NONE
!
!    Routine to set up the bit tables for use in the pixelization subroutines
!  (extracted and generalized from existing routines).
!
!  Arguments:
!
      Integer*4 :: LENGTH       ! Number of elements in IX, IY
      Integer*4,DIMENSION(length) :: IX   ! X bits
      Integer*4,DIMENSION(length) :: IY   ! Y bits
!
!  Variables:
!
      Integer*4 :: i,j,k,ip,id  ! Loop variables
!
      Do I = 1,length
          j = i - 1
          k = 0
          ip = 1
   10     if (j .eq. 0) then
              ix(i) = k
              iy(i) = 2*k
            else
              id = mod(j,2)
              j = j/2
              k = ip * id + k
              ip = ip * 4
              go to 10
            endif
     END DO

      return
      END SUBROUTINE BIT_SET

      SUBROUTINE CONV_G2E(ivector,ovector)
!
!    Routine to convert galactic coordinates to ecliptic (celestial)
!  coordinates at the epoch=2000.
!
!                          --------------
!
!  Galactic Coordinate System Definition (from Zombeck, "Handbook of
!  Space Astronomy and Astrophysics", page 71):
!
!         North Pole:  12:49       hours right ascension (1950.0)
!                     +27.4        degrees declination   (1950.0)
!
!    Reference Point:  17:42.6     hours right ascension (1950.0)
!                     -28 55'      degrees declination   (1950.0)
!
!                          --------------
!
!*     PROLOGUE:
!*
!h     Date    Version   SPR#   Programmer    Comments
!h     ----    -------   ----   ----------    --------
!h
!h                                            Pole:  12:49
!h                                                  +27.4
!h                                          0 long:  17:42:26.603
!h                                                  -28 55 00.45
!h
!
      Implicit NONE
!
!  Arguments:
!
      Real*8,INTENT(IN) ::     IVECTOR(3)      ! Input coordinate vector
      Real*8,INTENT(OUT) ::     OVECTOR(3)      ! Output coordinate vector
!
!  Program variables:
!
      Real*8     T(3,3)          ! Galactic coordinate transformation matrix
      Integer*4  I,J             ! Loop variables
!
      Data T / -0.054882486d0, -0.993821033d0, -0.096476249d0,& ! 1st column
               0.494116468d0, -0.110993846d0,  0.862281440d0,&! 2nd column
              -0.867661702d0, -0.000346354d0,  0.497154957d0/ ! 3rd column
!
!     Multiply by transformation matrix:
!
		Do I = 1,3
			ovector(i) = 0.0
			Do J = 1,3
				ovector(i) = ovector(i) + ivector(j)*T(i,j)
			end do
		end do

      return
      end subroutine CONV_G2E

!*******************************************************************************
!*     ll2uv
!*
!*     Function: Convert from decimal latitude/longitude to unit three-vector
!*
!*     PROLOGUE:
!*
!h     Date    Version   SPR#   Programmer    Comments
!h     ----    -------   ----   ----------    --------
!h
!*
!*     INPUT DATA:  Decimal latitude and longitude in degrees
!*
!*     OUTPUT DATA: Unit three-vector equivalent
!*
!*     INVOCATION:  status=ll2uv(longitude,latitude,vector)
!*
!*     INPUT PARAMETERS:
!*
!*     Name                Type          Description
!*     ----                ----          -----------
!*
!*     longitude          real*4        decimal longitude
!*     latitude           real*4        decimal latitude
!*
!*     OUTPUT PARAMETERS:
!*
!     Name                Type          Description
!*     ----                ----          -----------
!*
!*     vector             real*4(3)     output vector equivalent
!*
!*     COMMON VARIABLES USED: none
!*
!*     SUBROUTINES CALLED:    none
!*
!*     FUNCTIONS CALLED:      none
!*
!*     PROCESSING METHOD:
!*
!*     BEGIN
!*
!*       Apply standard conversion to input coordinates
!*       Return
!*
!*     END
!*******************************************************************************
!*
      Integer*8 Function ll2uv(longitude,latitude,vector)

!
!  Arguments:
!
      Real*8,INTENT(IN) ::      LONGITUDE  ! longitude in degrees
      Real*8,INTENT(IN) ::      LATITUDE   ! latitude in degrees
      Real*8,DIMENSION(3),INTENT(OUT) ::      VECTOR  ! output coordinate
!
!     Convert:
!
!       deg_rad=3.141592/180.
      vector(1) = dcos(latitude*deg_rad) * dcos(longitude*deg_rad)
      vector(2) = dcos(latitude*deg_rad) * dsin(longitude*deg_rad)
      vector(3) = dsin(latitude*deg_rad)
!     print *, vector
!
!     Done:
!
      ll2uv=1
      return
      end function ll2uv

END MODULE extinctionDrimmel
