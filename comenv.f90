!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE comenv
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CONTAINS
      SUBROUTINE ce(s,b,zpars)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!  Common envelope phase                                               c
!  Rob Mundin, 18/04/01                                                c
!  Bart Willems, 28/06/02                                              c
!  Open University                                                     c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE precision
      USE dtypes
      USE dshare
      USE params
      USE bfuncs
      USE output
      IMPLICIT NONE

      DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars
      TYPE (bnq), INTENT(INOUT) :: b
      TYPE (stq), DIMENSION(2), INTENT(INOUT) :: s

      INTEGER :: n
      INTEGER :: it
      DOUBLE PRECISION :: af
      DOUBLE PRECISION :: tm,tn,aj
      DOUBLE PRECISION, DIMENSION(10) :: lums,GB
      DOUBLE PRECISION, DIMENSION(20) :: tscls

      n = b%kw/10
      it = 0

! Save pre-CE values for output
      IF ((dsave.AND.(s(1)%tphys >=tsave)).OR.asave) &
           CALL record(s,b)
      CALL reclog(s,b)

 1    CONTINUE

! Set the value of lambda
! 1    IF (lflag > 0) THEN
!         lambda = lc(s(n)%mt,s(n)%rl)
!         IF (warn > 0) THEN     ! Not in table
!            b%kw = b%kw + 1
!            warn = 0
!         END IF
!      END IF

! Calculate the final semi-major axis taking into account that
! the companion may also be a giant-type star

      SELECT CASE (s(3-n)%kw)
      CASE(2:6,8:9)
         af = b%a*(s(n)%mc*s(3-n)%mc/(s(n)%mt*s(3-n)%mt)) &
              /(1.d0+(2.d0/(ace*lambda)) &
              *(((s(n)%mt-s(n)%mc)*b%a/(s(3-n)%mt*s(n)%r)) &
              +((s(3-n)%mt-s(3-n)%mc)*b%a/(s(n)%mt*s(3-n)%r))))
      CASE DEFAULT
         af = b%a*((s(n)%mc*s(3-n)%mt)/s(n)%mt)/(s(3-n)%mt &
              +2.d0*(s(n)%mt-s(n)%mc)*b%a/(ace*lambda*s(n)%r))
      END SELECT

! Set the binary parameters to the new semi-major axis

      b%a = af

! All the envelope has been lost
! Set the new mass and radius of the Primary

      s(n)%mt = s(n)%mc
      s(n)%r = s(n)%rc

      aj = s(n)%tphys-s(n)%epoch
      CALL star(s(n)%kw,s(n)%mass,s(n)%mt,tm,tn,tscls,lums,GB,zpars)
      CALL hrdiag(s(n)%mass,aj,s(n)%mt,tm,tn,tscls,lums,GB,zpars, &
           s(n)%r,s(n)%lum,s(n)%kw,s(n)%mc,s(n)%rc)
      s(n)%epoch = s(n)%tphys - aj

! And repeat for the secondary if this was also a giant-type star

      SELECT CASE (s(3-n)%kw)
      CASE(2:6,8:9)
         s(3-n)%mt = s(3-n)%mc
         s(3-n)%r = s(3-n)%rc
         aj = s(3-n)%tphys-s(3-n)%epoch
         CALL star(s(3-n)%kw,s(3-n)%mass,s(3-n)%mt,tm,tn,tscls,lums, &
              GB,zpars)
         CALL hrdiag(s(3-n)%mass,aj,s(3-n)%mt,tm,tn,tscls,lums,GB, &
              zpars,s(3-n)%r,s(3-n)%lum,s(3-n)%kw,s(3-n)%mc,s(3-n)%rc)
         s(3-n)%epoch = s(3-n)%tphys - aj
      END SELECT

      s%xldm = 0.d0

! Recalculate the orbital parameters

      b%Porb = a2Porb(b%a,s(n)%mt,s(3-n)%mt)
      b%jorb = jorbit(b%a,b%ecc,s(1)%mt,s(2)%mt)

! Calculate the radii of the Roche lobes

      s(1)%rl = rlobe(b%a,s(1)%mt,s(2)%mt)
      s(2)%rl = rlobe(b%a,s(2)%mt,s(1)%mt)

! Check if enough envelope has been removed

      IF (s(n)%r > s(n)%rl) THEN
         it = it + 1
         IF (it < 2) THEN
            SELECT CASE (s(n)%kw)
            CASE (2:6,8:9)
               b%kw = 10*n + 4
               GO TO 1
            END SELECT
         END IF
      END IF

! Check for merger

      IF ((s(1)%r > s(1)%rl).OR.(s(2)%r > s(2)%rl).OR. &
           (b%a <= s(1)%r+s(2)%r)) THEN
         b%kw = 41
         warn = 99
         RETURN
      ELSE
         b%kw = 0
      END IF

      RETURN
      END SUBROUTINE ce
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      DOUBLE PRECISION FUNCTION lc(mass, radius)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Routine to determine lambda for the common envelope
! routine. (Using linear interpolation)
!
! RPM 2001
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!      USE precision
!      USE dshare
!      IMPLICIT NONE
!
!      INTEGER :: mloc, rloc, locate
!      INTEGER :: ri
!
!      DOUBLE PRECISION, INTENT(IN) :: mass, radius
!
!      INTEGER, PARAMETER :: num_mass = 8
!      DOUBLE PRECISION, DIMENSION(8) :: mass_vals
!      INTEGER, PARAMETER :: num_rad = 7
!      DOUBLE PRECISION, DIMENSION(7) :: rad_vals
!
!      DOUBLE PRECISION :: m, b
!      DOUBLE PRECISION, DIMENSION(0:1) :: intval
!
!      DOUBLE PRECISION, DIMENSION(num_mass,num_rad) :: lam_table
!
!
! Array holding the mass values for which lambda has been calculated
!      mass_vals = (/3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0/)
! Array holdint the radius values for which lambda has been calculated
!      rad_vals = (/20.0, 40.0, 70.0, 100.0, 200.0, 300.0, 400.0/)
!
! Calculated values of lambda (Dewi & Tauris 2000)
!      data lam_table /0.87, 0.33, 0.34, 0.36, 0.37, 0.39, 0.40, 0.42,
!     &                0.94, 0.64, 0.41, 0.27, 0.28, 0.29, 0.29, 0.31,
!     &                0.97, 0.76, 0.49, 0.35, 0.22, 0.22, 0.23, 0.23,
!     &                0.94, 0.91, 0.54, 0.37, 0.30, 0.19, 0.19, 0.20,
!     &                1.81, 2.32, 1.06, 0.55, 0.28, 0.25, 0.21, 0.22,
!     &                3.44, 5.43, 4.03, 2.07, 0.56, 0.32, 0.25, 0.19,
!     &               16.50, 10.60, 12.00, 6.44, 3.93, 0.56, 0.33, 0.20/
!
!      IF (mass < 3.0 .OR. mass > 10.0) THEN
!         lc=0.5d0
!         warn = 99
!         RETURN
!      END IF
!
!      IF (radius < 20.0 .OR. radius > 400.0) THEN
!         lc=0.5d0
!         warn = 99
!         RETURN
!      END IF
!
! Locate the 2x2 grid square that holds the lambda value we want
!      CALL locate(mass_vals,num_mass,mass,mloc)
!      CALL locate(rad_vals,num_rad,radius,rloc)
!
! linearly interpolate between the two mass values found above
!
!      DO ri=0,1
!         m = (lam_table(mloc+1,rloc+ri)-lam_table(mloc,rloc+ri))
!     &        /(mass_vals(mloc+1)-mass_vals(mloc))
!         b = lam_table(mloc,rloc+ri) - mass_vals(mloc)*m
!         intval(ri) = mass * m + b
!      END DO
!
! linearly interpolate between the two radius values found above
! but using the interpolated values
!
!      m = (intval(1)-intval(0))/(rad_vals(rloc+1)-rad_vals(rloc))
!      b = intval(0) - rad_vals(rloc)*m
!      lc = radius * m + b
!
!      RETURN
!      END FUNCTION lc
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE comenv
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

