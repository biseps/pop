!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE snkick
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         CONTAINS
         SUBROUTINE kick(os,ob,s,b)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Procedure to determine the supernova kick velocity                   c
! Bart Willems, 09/09/01                                               c
!                                                                      c
! Ref: V. Kalogera (1996), ApJ 471, 352                                c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE params
         USE dtypes
         USE dshare
         USE bfuncs
         USE output

         IMPLICIT NONE

         TYPE (bnq), INTENT(IN) :: ob
         TYPE (bnq), INTENT(INOUT) :: b
         TYPE (stq), DIMENSION(2), INTENT(IN) :: os
         TYPE (stq), DIMENSION(2), INTENT(INOUT) :: s

         INTEGER :: n
         DOUBLE PRECISION :: al,be,e2,rp
         DOUBLE PRECISION :: vx,vy,vz
         DOUBLE PRECISION, DIMENSION(2) :: reff

	IF(isSingle == 0) THEN
		IF (nkick > 0) THEN ! Generate random kick velocity
			CALL gaussian(os(1)%mt,os(2)%mt,ob%a,vx,vy,vz)
		ELSE
			vx = 0.d0
			vy = -0.9d0
			vz = 0.d0
		END IF
	
	! Calculate system parameters after SN explosion
		be = (s(1)%mt+s(2)%mt)/(os(1)%mt+os(2)%mt)
		al = be/(2.d0*be-vx**2-(vy+1.d0)**2-vz**2)
		e2 = (vz**2+(vy+1.d0)**2)/(be*al)
	
	! New semi-major axis and orbital eccentricity
		b%a = al*ob%a
		b%ecc = sqrt(1.d0 - e2)
	
	
	! Is the binary disrupted?
		IF ((b%a <= 0.d0).OR.(b%ecc >= 1.d0)) THEN
			b%a = 0.d0
			b%Porb = 0.d0
			b%ecc = 0.d0
			b%jorb = 0.d0
			b%kw = 55
			kdisr = kdisr + 1
			warn = 99
			RETURN
		END IF
	END IF

! Calculate new orbital period and orbital angular momentum
         b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)
         b%jorb = jorbit(b%a,b%ecc,s(1)%mt,s(2)%mt)

! Check for Roche lobe overflow at periastron
         rp = b%a*(1.d0-b%ecc)
         DO n = 1, 2
            s(n)%rl = rlobe(rp,s(n)%mt,s(3-n)%mt)
         END DO

         IF (s(1)%r < s(1)%rl) THEN
            reff(1) = s(1)%r
            IF (s(2)%r < s(2)%rl) THEN ! Detached binary
               reff(2) = s(2)%r
               b%kw = 0
            ELSE                ! M2 fills its Roche lobe
               reff(2) = s(2)%rl
               IF (ob%kw/10 /= 2) THEN
                  IF (s(2)%r > 1.1d0*s(2)%rl) THEN
                     b%kw = 43
                     kmerg = kmerg + 1
                     warn = 99
                  ELSE
                     b%kw = 20
                  END IF
               END IF
               RETURN
            END IF
         ELSE                   ! M1 fills its Roche lobe
            reff(1) = s(1)%rl
            IF (s(2)%r < s(2)%rl) THEN
               reff(2) = s(2)%r
               IF (ob%kw/10 /= 1) THEN
                  IF (s(1)%r > 1.1d0*s(1)%rl) THEN
                     b%kw = 43
                     kmerg = kmerg + 1
                     warn = 99
                  ELSE
                     b%kw = 10
                  END IF
               END IF
               RETURN
            ELSE                ! Contact binary
               reff(2) = s(2)%rl
               b%kw = 43
               kmerg = kmerg + 1
               warn = 99
               RETURN
            END IF
         END IF

! Check for merger
         IF (rp <= reff(1)+reff(2)) THEN
            b%kw = 43
            kmerg = kmerg + 1
            warn = 99
            RETURN
         END IF

! If the binary survives and tides are not taken into account
! then circularize the orbit instantaneously, but conserve the
! orbital angular momentum
         IF (tflag == 0) THEN
! Store values prior to circularization
            CALL reclog(s,b)
            IF (dsave.OR.asave) CALL record(s,b)

            b%ecc = 0.d0
            b%a = aorbit(b%jorb,b%ecc,s(1)%mt,s(2)%mt)
            b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)

! Check for Roche lobe overflow
            DO n = 1, 2
               s(n)%rl = rlobe(b%a,s(n)%mt,s(3-n)%mt)
            END DO

            IF (s(1)%r < s(1)%rl) THEN
               reff(1) = s(1)%r
               IF (s(2)%r < s(2)%rl) THEN ! Detached binary
                  reff(2) = s(2)%r
                  b%kw = 0
               ELSE  ! M2 fills its Roche lobe
                  reff(2) = s(2)%rl
                  IF (s(2)%r > 1.1d0*s(2)%rl) THEN
                     b%kw = 43
                     kmerg = kmerg + 1
                     warn = 99
                  ELSE
                     b%kw = 20
                  END IF
                  RETURN
               END IF
            ELSE     ! M1 fills its Roche lobe
               reff(1) = s(1)%rl
               IF (s(2)%r < s(2)%rl) THEN
                  reff(2) = s(2)%r
                  IF (s(1)%r > 1.1d0*s(1)%rl) THEN
                     b%kw = 43
                     kmerg = kmerg + 1
                     warn = 99
                  ELSE
                     b%kw = 10
                  END IF
                  RETURN
               ELSE  ! Contact binary
                  reff(2) = s(2)%rl
                  jp = jp - 1
                  b%kw = 43
                  kmerg = kmerg + 1
                  warn = 99
                  RETURN
               END IF
            END IF

! Check for merger
            IF (b%a <= reff(1)+reff(2)) THEN
               b%kw = 43
               kmerg = kmerg + 1
               warn = 99
               RETURN
            END IF

         END IF

         RETURN
         END SUBROUTINE kick
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         SUBROUTINE gaussian(m1,m2,a,vx,vy,vz)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Generate a random 3D vector using a Gaussian distribution
! Bart Willems, 27/05/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         USE precision
         USE consts
         USE params
         USE nrtype
         USE nr, ONLY : gasdev

         IMPLICIT NONE

         DOUBLE PRECISION, INTENT(IN) :: m1,m2,a
         DOUBLE PRECISION, INTENT(OUT) :: vx,vy,vz

         DOUBLE PRECISION :: mu = 0.0d0
         DOUBLE PRECISION :: ksi,vorb

! Orbital velocity for the circular orbit prior to the SN explosion
         vorb = sqrt(gn*(m1+m2)/a)
! Convert from solar units and years to km/s
         vorb = vorb*0.02207d0

! Dimensionless dispersion
         ksi = ksig/vorb

! Generate Gaussian velocity components
         CALL gasdev(vx)
         CALL gasdev(vy)
         CALL gasdev(vz)

! Transform into Gaussian distribution with dimensionless velocity
! dispersion ksi=ksig/vorb, with vorb the orbital velocity in the
! pre-supernova circular orbit (Kalogera 1996, ApJ 471, 352)
         vx = vx*ksi + mu
         vy = vy*ksi + mu
         vy = vy*ksi + mu

         RETURN
         END SUBROUTINE gaussian
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE snkick
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
