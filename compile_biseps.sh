#!/bin/bash


# location of source files
#make_file="/padata/beta/users/efarrell/repos/BiSEPS_2.0/source/Makefile_BiSEPS"
#make_vpath="/padata/beta/users/efarrell/repos/BiSEPS_2.0/source"

# make_file="./pop/Makefile_BiSEPS"
# make_vpath="./pop"

# assume source-code here in this directory
make_file="./Makefile_BiSEPS"
make_vpath="./"


# remove old compile
printf '\n remove existing BiSEPS\n\n'
make -f $make_file folder=$make_vpath clobber


# compile BiSEPS
printf '\n compiling BiSEPS...\n\n'
make -f $make_file folder=$make_vpath 

