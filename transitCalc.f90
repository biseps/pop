PROGRAM transitCalc
!Given an input file it will give each binary a number of random inclinations
!and the transit depth
	USE randomNum
	USE dtypes
	USE jktebop
! 	USE magnitude
	USE extras, ONLY : porb2a
	IMPLICIT NONE

	INTEGER :: i,j,k,lineCount,aerr,id,tmpInt
	DOUBLE PRECISION, PARAMETER :: pi = 3.141592654d0
	
	DOUBLE PRECISION :: inclin,dis

	CHARACTER(len=200) :: fin,fin2,fin3
	CHARACTER(len=1000) :: BUFFER
	
	DOUBLE PRECISION,DIMENSION(1:2,1:3) :: transDepth
	TYPE(stq),DIMENSION(1:2):: s
	TYPE(bnq) :: b
	DOUBLE PRECISION :: tmp,num,ran
	CHARACTER(len=10) :: inTrans
	LOGICAL :: fileExists


! 	filterBand='Kp'
	!extract file
	call GETARG(1,fin)
	!Coeffs file
	call GETARG(2,fin2)
	!ranloc file
	call GETARG(3,fin3)

	fin=fin(1:len_trim(fin))
	fin2=fin2(1:len_trim(fin2))
	fin3=fin3(1:len_trim(fin3))
! 	CALL preIntMag()
	CALL preIntRan()

	fileExists=.false.
 	inquire(FILE=fin,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin
 		stop
 	end if

 	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')

	fileExists=.false.
 	inquire(FILE=fin2,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin2
 		stop
 	end if

	fileExists=.false.
 	inquire(FILE=fin3,exist=fileExists)
 	if(fileExists .eqv. .false.) then
 		write(0,*) "No file ", fin3
 		stop
 	end if

 	OPEN(2,FILE=fin2,STATUS='OLD',ACTION='READ')
 	OPEN(50,FILE=fin3,STATUS='OLD',ACTION='READ')

	lineCount=0
	DO WHILE (.true.)
  		read(1, '(A)', end=99) BUFFER
  		lineCount=lineCount+1
      ENDDO
99    CONTINUE
      CLOSE(1)
	
	OPEN(1,FILE=fin,STATUS='OLD',ACTION='READ')
    DO i=1,lineCount

		READ(1,*) id,num,s(1)%mt,s(1)%reff,s(1)%teff,s(1)%lum,&
			s(2)%mt,s(2)%reff,s(2)%teff,s(2)%lum,&
			b%porb,tmp,tmp,tmpInt,BUFFER

		READ(2,*) s(1)%mag,s(1)%ldc1,s(1)%ldc2,s(1)%grav,s(2)%mag,s(2)%ldc1,s(2)%ldc2,s(2)%grav
		READ(50,*) tmp,tmp,tmp,tmp,dis,tmp,tmp
! 		if(i /= 440) cycle

! 		CALL getMagBC(s,b)
! 		call getLDC(s(1)%reff,s(1)%mt,s(1)%teff,s(1)%ldc1,s(1)%ldc2)
! 		call getLDC(s(2)%reff,s(2)%mt,s(2)%teff,s(2)%ldc1,s(2)%ldc2)
! 		s(1)%grav=getGravCoeff(s(1)%reff,s(1)%mt,s(1)%teff)
! 		s(2)%grav=getGravCoeff(s(2)%reff,s(2)%mt,s(2)%teff)
		b%a=porb2a(b%porb/365.0,s(1)%mt,s(2)%mt)

		!Generate random inclcination in range 0-pi
      	inclin=unif(0.d0,pi)
		if (inclin>pi/2.d0) THEN
			 inclin=inclin-pi/2.d0
		END IF

!         inclin=(83.8557189702988/180.0)*pi

		transDepth=0.d0
			CALL jkt(s(1),s(2),b,inclin,dis,transDepth(1,:))
			CALL jkt(s(2),s(1),b,inclin,dis,transDepth(2,:))

!  		write(*,*) s(1)%reff,s(2)%reff,b%a,inclin,acos((s(1)%reff+s(2)%reff)/b%a)
		!JKTEBOP has small scale change (reflection) in lightcurve even without transit
		if(inclin<acos((s(1)%reff+s(2)%reff)/b%a)) THEN
			write(*,*) id,b%porb,inclin*180.d0/pi,acos((s(1)%reff+s(2)%reff)/b%a)*180.d0/pi,&
					0.d0,transDepth(1,2)-transDepth(1,3),&
				0.d0,transDepth(2,2)-transDepth(2,3)
 		ELSE
			write(*,*) id,b%porb,inclin*180.d0/pi,acos((s(1)%reff+s(2)%reff)/b%a)*180.d0/pi,&
					transDepth(1,3)-transDepth(1,1),transDepth(1,2)-transDepth(1,3),&
				transDepth(2,3)-transDepth(2,1),transDepth(2,2)-transDepth(2,3)
		END IF
!  		stop
	END DO	
	CLOSE(1)
	CLOSE(2)
	CLOSE(50)

	


END PROGRAM transitCalc