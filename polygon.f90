MODULE polygon
	DOUBLE PRECISION,DIMENSION(0:100) :: polyx,polyy
	DOUBLE PRECISION,DIMENSION(0:3) :: px,py
	INTEGER :: counter

	CONTAINS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Given a polgon (x,y) will return the area enclosed by it and another polygon (px,py)
! Reference Computational geometry in C, O'Rouke 1995
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	DOUBLE PRECISION FUNCTION poly(x,y)
	IMPLICIT NONE
	INTEGER,PARAMETER :: n=4
	INTEGER :: i
	LOGICAL,DIMENSION(0:n-1) :: res
	DOUBLE PRECISION,INTENT(IN),DIMENSION(0:n-1) :: x,y

	DO i=0,n-1
		res(i)=PNPOLY(x(i),y(i))
	end do

	IF((res(0).and.res(1)).and.(res(2).and.res(3))) THEN
	!All points inside polgon
		poly=1.d0
		return
	ELSE IF((.not.res(0).and..not.res(1)).and.(.not.res(2).and..not.res(3))) THEN
	!No points inside
		poly=0.0d0
		return
	END IF

	!Some intermediate state thus run ConvexIntersect
	polyx=0.d0
	polyy=0.d0
	counter=-1
	CALL ConvexIntersect(x,y,n,n)

	poly=area2(polyx,polyy,counter+1)/area2(x,y,n)

	END FUNCTION poly

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	ELEMENTAL LOGICAL FUNCTION PNPOLY(x,y)
	IMPLICIT NONE
	INTEGER :: i,j
	INTEGER,PARAMETER :: N=4
	DOUBLE PRECISION,INTENT(IN) :: x,y

	PNPOLY=.FALSE.

!Check inside area at all
!  	IF(x.lt.MINVAL(px).or.x.gt.MAXVAL(py).or.y.lt.MINVAL(py).or.y.gt.MAXVAL(py)) THEN
!  		PNPOLY=.FALSE.
!  		RETURN
!  	END IF
! ! !Check if on a point
! 	DO i=0,n-1
! 		if (x.eq.px(i).and.y.eq.py(i)) THEN
! 			PNPOLY=.FALSE.
! 			return
! 		end if
! 	END DO


	DO i=0,n-1
		j=MOD(i+1,N)
		IF((py(i).gt.y).neqv. (py(j).gt.y) )THEN
			IF(x.lt.(px(j)-px(i))*(y-py(i))/(py(j)-py(i))+px(i)) THEN
				PNPOLY=.NOT.PNPOLY
			END IF
		END IF

	END DO

	END FUNCTION PNPOLY


	SUBROUTINE ConvexIntersect(x,y,n,m)
	IMPLICIT NONE
	INTEGER,INTENT(IN) :: n,m
	INTEGER :: a,b,a1,b1,i,aa,ba
	DOUBLE PRECISION ,DIMENSION(0:n-1),INTENT(IN) :: x,y
	LOGICAL :: bha,ahb,res
	DOUBLE PRECISION :: pix,piy,ax,ay,bx,by,cross
	!-1 Pin 0 Qin 1 Unknown
	INTEGER :: inflag

! 	DOUBLE PRECISION :: area
! 	INTEGER :: inout
! 	LOGICAL :: left

	a=0;b=0;aa=0;ba=0
	i=0
	inflag=1
	counter=-1
	DO
		IF((aa.ge.n.and.ba.ge.m).or.(aa.ge.2*n).or.(ba.ge.2*m)) EXIT

		a1=MOD(a+n-1,n)
		b1=MOD(b+m-1,m)
		CALL subVec(x(a),y(a),x(a1),y(a1),ax,ay)		
		CALL subVec(px(b),py(b),px(b1),py(b1),bx,by)
		cross=Area(0.d0,0.d0,ax,ay,bx,by)
		bha=Left(x(a1),y(a1),x(a),y(a),px(b),py(b))
		ahb=Left(px(b1),py(b1),px(b),py(b),x(a),y(a))

		CALL intersection(x(a1),y(a1),x(a),y(a),px(b1),py(b1),px(b),py(b),pix,piy,res)

		IF(res)THEN
			IF(inflag==1)THEN
				aa=0
				ba=0
			END IF
			inflag=Inout(ahb,bha,pix,piy,inflag)
		END IF

		IF(cross==0.d0 .and. (.not.bha) .and. (.not.ahb)) THEN
			IF(inflag==-1)THEN
				CALL advance(b,ba,m,inflag==0,px(b),py(b))
			ELSE
				CALL advance(a,aa,n,inflag==-1,x(a),y(a))
			
			END IF
		ELSE IF (cross.ge.0.d0) THEN
			if(bha) THEN
				CALL advance(a,aa,n,inflag==-1,x(a),y(a))
			ELSE
				CALL advance(b,ba,m,inflag==0,px(b),py(b))
			END IF
		ELSE
			IF(ahb)THEN
				CALL advance(b,ba,m,inflag==0,px(b),py(b))
			ELSE
				CALL advance(a,aa,n,inflag==-1,x(a),y(a))
			END IF
		END IF
	END DO
	
	END SUBROUTINE ConvexIntersect

	ELEMENTAL SUBROUTINE subVec(x1,y1,x2,y2,xAns,yAns)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: x1,y1,x2,y2
		DOUBLE PRECISION,INTENT(OUT) :: xAns,yAns
		xAns=x1-x2
		yAns=y1-y2
	END SUBROUTINE subVec

	ELEMENTAL LOGICAL FUNCTION Left(x1,y1,x2,y2,x3,y3)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: x1,y1,x2,y2,x3,y3
		DOUBLE PRECISION :: a
		Left=.false.
		a=area(x1,y1,x2,y2,x3,y3)
		IF(a.gt.0.d0) Left=.true.
	END FUNCTION left

	ELEMENTAL DOUBLE PRECISION FUNCTION area(x1,y1,x2,y2,x3,y3)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: x1,y1,x2,y2,x3,y3
		area=(x1*y2)-(y1*x2)+(y1*x3)-(x1*y3)+(x2*y3)-(x3*y2)
	END FUNCTION area

	SUBROUTINE intersection(x1,y1,x2,y2,x3,y3,x4,y4,pix,piy,res)
		IMPLICIT NONE
		DOUBLE PRECISION,INTENT(IN) :: x1,y1,x2,y2,x3,y3,x4,y4
		DOUBLE PRECISION,INTENT(OUT) :: pix,piy
		DOUBLE PRECISION :: s,t,denom
		LOGICAL,INTENT(OUT) :: res

		res=.false.
		denom=(x1*(y4-y3))+(x2*(y3-y4))+(x4*(y2-y1))+(x3*(y1-y2))
		IF(denom == 0.0) return

		s=((x1*(y4-y3))+(x3*(y1-y4))+(x4*(y3-y1)))/denom
		t=-((x1*(y3-y2))+(x2*(y1-y3))+(x3*(y2-y1)))/denom
	
		pix=x1+s*(x2-x1)
		piy=y1+s*(y2-y1)

		IF(s.ge.0.d0 .and. s.le.1.d0 .and.t.ge.0.d0 .and. t.le.1.d0) res=.true.

	END SUBROUTINE intersection

	INTEGER FUNCTION inout(ahb,bha,pix,piy,inflag)
	IMPLICIT NONE
	LOGICAL,INTENT(IN) :: ahb,bha
	DOUBLE PRECISION,INTENT(IN) :: pix,piy
	INTEGER,INTENT(IN) :: inflag

		counter=counter+1
		polyx(counter)=pix
		polyy(counter)=piy
	
		IF(ahb)THEN
			inout=-1
		ELSE IF (bha) THEN
			inout=0
		ELSE
			inout=inflag
		END IF	
	END FUNCTION

	SUBROUTINE advance(a,aa,n,inside,x,y)
	IMPLICIT NONE
	INTEGER,INTENT(INOUT) :: a,aa
	INTEGER,INTENT(IN) :: n
	LOGICAL,INTENT(IN) :: inside
	DOUBLE PRECISION,INTENT(IN):: x,y

	if(inside) THEN
		counter=counter+1
		polyx(counter)=x
		polyy(counter)=y
	END IF

	aa=aa+1
	a=MOD(a+1,n)

	END SUBROUTINE

	PURE DOUBLE PRECISION FUNCTION area2(x,y,n)
	IMPLICIT NONE
	DOUBLE PRECISION,INTENT(IN),DIMENSION(0:n) :: x,y
	INTEGER,INTENT(IN) :: n
	INTEGER :: i

	area2=0.d0
	DO i=0,n-1
		area2=area2+(x(MOD(i,n))*y(MOD(i+1,n)))-(x(MOD(i+1,n))*y(MOD(i,n)))
	END DO

	area2=abs(area2)*0.5

	END FUNCTION

END MODULE polygon