#!/bin/bash


# assume source-code here in this directory
make_file="./Makefile_population"
make_vpath="./"


# remove old compile
printf '\n remove existing population\n\n'
make -f $make_file folder=$make_vpath clobber


# compile 
printf '\n compiling population\n\n'
make -f $make_file folder=$make_vpath 

