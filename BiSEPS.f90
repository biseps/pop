!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      PROGRAM BiSEPS
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Bi(nary) S(tellar) E(volution) P(opulation) S(ynthesis)              c
!                                                                      c
! Author: Bart Willems, 18/09/01                                       c
!         Department of Physics and Astronomy                          c
!         The Open University                                          c
!         Walton Hall                                                  c
!         Milton Keynes                                                c
!         MK7 6AA                                                      c
!         United Kingdom                                               c
!                                                                      c
!         E-mail: B.Willems@open.ac.uk                                 c
!                                                                      c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! BiSEPS is a population synthesis program based on BiSEC              c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

      USE precision
      USE params
      USE dtypes
      USE dshare
      USE evolveb
      USE output
      USE counters
      USE fmods
      USE synthesis
      USE names
      USE bfuncs
      USE randomNum
!       USE magnitude, ONLY : preIntMag
! 	  USE BC

      IMPLICIT NONE

      INTEGER :: ioerr,aerr,clock
      INTEGER :: i,j,k,n
      INTEGER :: nP,nm1,nm2,nA
      INTEGER :: track
      INTEGER, PARAMETER :: nclass = 2
      INTEGER, DIMENSION(nclass) :: class = (/2,3/)

      DOUBLE PRECISION :: dum
      DOUBLE PRECISION :: m1,m2,P,a
      DOUBLE PRECISION :: m1min,m1max,m2min,m2max,Pmin,Pmax,aMin,aMax
      DOUBLE PRECISION :: dm1,dm2,dP,da
      DOUBLE PRECISION :: tphysf,dtp,tmp
      DOUBLE PRECISION, DIMENSION(20) :: zpars

      TYPE (stq), DIMENSION(2) :: s  ! Stellar parameters
      TYPE (bnq) :: b                ! Binary parameters

      CHARACTER(len=1) :: go
      CHARACTER(len=20) :: model
      CHARACTER(len=25) :: ext
      CHARACTER(len=100) :: name
      CHARACTER(len=100) :: infile,outfile

      LOGICAL :: fexst

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Input from the file 'BiSEPS.in':
!
! masses of the two stars are in solar units
! z is metallicity in the range 0.0001 -> 0.03 where 0.02 is Population I
!      (we assume both stars to have the same z)
! tphysf is the maximum evolution time in Myr
!
! mflag > 0 activates mass loss
! neta is the Reimers mass-loss coefficent
! bwind is the binary enhanced mass-loss parameter
! beta is the wind-velocity parameter
! wmag is an overall multiplication factor for all wind mass-loss rates
!
! P is the orbital period in days
!
! cflag = 0 sets fully conservative mass transfer
!    cflag > 0 sets non-conservative mass transfer
! gnov is the fraction of the transferred mass accreted by a WD
!      in a nova-type system
! 1-gwd, 1-gns, 1-gbh are the fractions of the transferred mass accreted by
! a WD not in a nova-type system, a neutron star (gns < 0 lets the
! program determine the mass accretion rate based on the maximum mass a
! NS is allowed to accrete), and a black hole
!
! ewd, ens, ebh > 0 allows super-Eddington accretion during RLOF
! for white dwarfs, neutron stars, and black holes, respectively
!
! lflag > 0 activates Dewi & Tauris (2000) treatment of CE
! ace is the efficiency parameter for the transfer of orbital energy
!    to the envelope
! lambda is the binding energy parameter for a common envelope phase
!
! etamb is an ad-hoc parameter to change the MB strength
!
! wdsal and wdsah are the lower and upper limits for the mass transfer
!    to induce steady burning on the surface of a WD
! dnsm is the maximum mass a NS is allowed to accrete during RLOF
!
! xlmin is the minimum xray luminosity (in Lsun) a binary needs to emit
! to be classified as an X-ray binary
!
! xlcrm is an estimated upper limit for the coronal X-ray luminosity for
! stars up to the giant branch (xlcrm < 0 flags the determination of the
! upper limit as a function of the stellar radius)
!
! mcnv is the minimum mass of the convective envelope as a fraction of the
! total mass in order for the corona to be X-ray active
!
! nkick is the number of different kicks to compute after a SN explosion
! ksig is the kick velocity dispersion (for a Maxwellian distribution)
!
! ecc is the orbital eccentricity
!
! tflag > 0 activates tides
!
! ospin is the rotational angular velocity of the stars in yr^{-1}. If
! ospin is less than or equal to zero at time zero then evolv2 will set
! an appropriate ZAMS spin. If ospin is greater than zero then it will
! start with that spin regardless of the time. If you want to start at
! time zero with negligible spin then I suggest using a negligible value
! (but greater than 0.001).
!
! The following parameters determine the timesteps chosen in each
! evolution phase as decimal fractions of the time taken in that phase:
!   pts1 - MS
!   pts2 - GB, CHeB, AGB, HeGB
!   pts3 - HG, HeMS
!
! During RLOF the time step is chosen so that the relative change in mass
! is smaller than pts4
!
! nP is the number of grid points for the orbital period
! nm1 is the number of grid points for the mass M1
! nm2 is the number of grid points for the mass M2c
!
      WRITE(*,*)
!      WRITE(*,*)'Available models:'
!      WRITE(*,*)
!      CALL SYSTEM('ls BiSEPS.in.*')
!      WRITE(*,*)
!      WRITE(*,*) 'Model: '
!      model = 'A'
!      READ(*,*) model

!ccccccccccccccccccccccccccccccccccccccccccccccccccc
! get input file from command line, should be abs path
! population_v2.in
! R Farmer 8/11/2010
      call GETARG(1,infile)
      model="1"
!cccccccccccccccccccccccccccccccccccccccccccccccccccc

!      infile = 'BiSEPS.in.'//model(1:LEN_TRIM(model))
      outfile = 'BiSEPS.out.'//model(1:LEN_TRIM(model))

      OPEN(1,FILE=infile(1:LEN_TRIM(infile)),STATUS='OLD', &
           ACTION='READ',IOSTAT=ioerr)
      IF (ioerr > 0) THEN
         WRITE(0,*) 'ERROR BiSEPS.f90 main 160'
         WRITE(0,*) 'ERROR: Input file does not exist!'
         WRITE(0,*) 'END ERROR'
         stop
      END IF

      READ(1,*) m1min,m1max,m2min,m2max,metallicity,tphysf
      READ(1,*) mflag,neta,bwind,beta,wmag
!       READ(1,*) Pmin,Pmax
	READ(1,*) aMin,aMax
      READ(1,*) cflag,gnov,gwd,gns,gbh
      READ(1,*) ewd,ens,ebh
      READ(1,*) wdsal,wdsah,dnsm
      READ(1,*) lflag,ace,lambda
      READ(1,*) etamb
      READ(1,*) xlmin,xlcrm,mcnv
! WASP line: mdmv is minimum decrease in magnitude required to detect transit
!            mmv1 is minimum magnitude detectable
!            mmv2 is maximum magnitude detectable
      READ(1,*) mdmv,mmv1,mmv2
      READ(1,*) nkick,ksig
      READ(1,*) pts1,pts2,pts3,pts4
!       READ(1,*) nP,nm1,nm2
	READ(1,*) nA,nm1,nm2
      READ(1,*) filterBand
!       READ(1,*) minTransit,maxTransit,numTransits
      READ(1,*) wantTransit,isSingle
      CLOSE(1)

!Set up max evolution to store in params module to pass elese where
! cant use tphysf itself as that name is reused
      tphysMax=tphysf

! Make star1 initially the most massive component
      IF (m2min > m1min) THEN
         dum = m1min
         m1min = m2min
         m2min = dum
         dum = m1max
         m1max = m2max
         m2max = dum
         dum = nm1
         nm1 = nm2
         nm2 = dum
      END IF

! No tidal action for now
      tflag = 0

! track > 0 activates channel tracking
      track = 1

! Set parameters which depend on the metallicity
      CALL zcnsts(metallicity,zpars)

! Initialise sequence number
      b%seq_num = 0.d0

! Set the data-save parameter. If dtp is zero then the parameters of the
! star will be stored in the bout and oout arrays at each timestep otherwise
! they will be stored at intervals of dtp. Setting dtp equal to tphysf will
! store data only at the start and end while a value of dtp greater than
! tphysf will mean that no data is stored.

      dtp = 2.d0*tphysf

! Set up variables which control the output

      asave = .false.
      dsave = .false.
      tsave = tphysf

! Allocate memory to arrays
      IF (nkick > 0) THEN
         nnlog = nlog*nkick
      ELSE
         nnlog = 10*nlog
      END IF

      ALLOCATE(save_binary(nnlog+1),STAT=aerr)
      IF (aerr > 0) THEN
         WRITE(0,*) 'ERROR BiSEPS.f90 main 240'
         WRITE(0,*) 'ERROR: Memory allocation failed for save_binary!'
         WRITE(0,*) 'END ERROR'
         STOP 
      END IF

! Set up logarithmic parameter grid

      m1min = log10(m1min)
      m1max = log10(m1max)
      m2min = log10(m2min)
      m2max = log10(m2max)
!       Pmin = log10(Pmin)
!       Pmax = log10(Pmax)
      aMin = log10(aMin)
      aMax = log10(aMax)

      IF (nm1 > 1) THEN
         dm1 = (m1max-m1min)/DBLE(nm1)
      ELSE
         dm1 = 0.0d0
      END IF
      IF (nm2 > 1) THEN
         dm2 = (m2max-m2min)/DBLE(nm2)
      ELSE
         dm2 = 0.0d0
      END IF
!       IF (nP > 1) THEN
!          dP = (Pmax-Pmin)/DBLE(nP-1)
!       ELSE
!          dP = 0.0d0
!       END IF
      IF (nA > 1) THEN
         da = (aMax-aMin)/DBLE(nA)
      ELSE
         da = 0.0d0
      END IF


      ext = '.dat.'//model(1:LEN_TRIM(model))

      CALL del_file(outfile(1:LEN_TRIM(outfile)))

      name = fname(6,'_calib'//ext)
!       CALL del_file(name(1:LEN_TRIM(name)))
!       OPEN(1,FILE=name(1:LEN_TRIM(name)),STATUS='NEW',ACTION='WRITE')
!       WRITE(1,*) '    dlog10(M1)          dlog10(M2) ' &
! !               //'        dlog10(Porb)'
!               //'        dlog10(a)'         
! !          WRITE(1,'(3(E19.12,1X))') dm1,dm2,dP
!          WRITE(1,'(3(E19.12,1X))') dm1,dm2,da
!       write(1,*)
!       CLOSE(1)

		n=1
	  if (isSingle==1) THEN
		class(n)=3
	  ELSE
	  	class(n)=2
	  END IF
!       DO n = 1, nclass
         name = fname(class(n),'_grid'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         OPEN(1,FILE=name(1:LEN_TRIM(name)),STATUS='NEW',ACTION='WRITE')
         WRITE(1,*) '    dlog10(M1)          dlog10(M2) ' &
!               //'        dlog10(Porb)'
              //'        dlog10(a)'         
!          WRITE(1,'(3(E19.12,1X))') dm1,dm2,dP
         WRITE(1,'(3(E19.12,1X))') dm1,dm2,da
         write(1,*)
         CLOSE(1)
         name = fname(class(n),'_birth'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_death'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_preSN'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_pstSN'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_preCE1'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_pstCE1'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_preCE2'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_pstCE2'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_preCE3'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_pstCE3'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_init'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_extra'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_transit'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_kepler'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_pop'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         name = fname(class(n),'_mag'//ext)
         CALL del_file(name(1:LEN_TRIM(name)))
         CALL del_file('testlog.dat')
!       END DO
	
! Intialize LDc and BC tables
! 	CALL preIntBC((/'500'/))

! Initialize random number generator
	CALL preIntRan()

! Initialize counters
      kkick = 0   ! Number of supernova kicks calculated
      kdisr = 0   ! Number of systems disrupted by SN kick
      kmerg = 0   ! Number of systems merged due to SN kick
      total = 0   ! Total number of systems evolved (incl. kick branch o
      tkkick = 0  ! Total number of supernova kicks calculated
      tkdisr = 0  ! Total number of systems disrupted by SN kick
      tkmerg = 0  ! Total number of systems merged due to SN kick
      twarn = 0   ! Total number of warnings
      tclass = 0  ! Total number of systems in a given class of binaries
      ksnii = 0.0 ! Number of type II supernova explosion

      nnw = 0
      nlast = 0

! Start loop over parameter space
!       period: DO i = nP, 1, -1
	semiMajor: DO i = nA,1,-1
         star1: DO j = 1, nm1
            star2: DO k = 1, nm2

!           P = Pmin + DBLE(i-1)*dP
			a= aMin+DBLE(i-1)*da
			m1 = m1min + DBLE(j-1)*dm1
			m2 = m2min + DBLE(k-1)*dm2
!			write(*,*) i,j,k

! Displace grid randomly (except boundaries)
!                IF (i == 1) THEN
!                   P = unif(P,P+dP/2.d0)
!                ELSE IF (i == nP) THEN
!                   P = unif(P-dP/2.d0,P)
!                ELSE
!                   P = unif(P-dP/2.d0,P+dP/2.d0)
!                END IF
               IF (i == 1) THEN
                  a = unif(a,a+da/2.d0)
               ELSE IF (i == na) THEN
                  a = unif(a-da/2.d0,a)
               ELSE
                  a = unif(a-da/2.d0,a+da/2.d0)
               END IF

               IF (j == 1) THEN
                  m1 = unif(m1,m1+dm1/2.d0)
               ELSE IF (j == nm1) THEN
                  m1 = unif(m1-dm1/2.d0,m1)
               ELSE
                  m1 = unif(m1-dm1/2.d0,m1+dm1/2.d0)
               END IF
               IF (k == 1) THEN
                  m2 = unif(m2,m2+dm2/2.d0)
               ELSE IF (k == nm2) THEN
                  m2 = unif(m2-dm2/2.d0,m2)
               ELSE
                  m2 = unif(m2-dm2/2.d0,m2+dm2/2.d0)
               END IF

              p=a2Porb(10**a,10**m1,10**m2)

! Only systems with m2 < m1 need to be evolved
                IF (m2 > m1) CYCLE star2
! 			IF(m2 > m1) stop
! Initialize variables for data storage
               jp = 1
               save_binary%kw1 = 0
               save_binary%kw2 = 0
               save_binary%bkw = 0
               save_binary%cls = 0
               save_binary%tphys = 0.0
               save_binary%mt1 = 0.0
               save_binary%mt2 = 0.0
               save_binary%Porb = 0.0
               save_binary%ecc = 0.0

! Only circular orbits for now
               b%ecc = 0.d0
! Initialize the stars to begin on the ZAMS
               s(1)%mass = 10.d0**m1
               s(2)%mass = 10.d0**m2
               s%mt = s%mass
               s%mc = 0.d0
               s%kw = 1
               s%tphys = 0.d0
               s%epoch = 0.d0
! Let the program set the spin of the stars
               s%ospin = 0.d0
! Express the orbital period in years
!                b%Porb = 10.d0**P/365.d0
 			b%Porb=P

!               WRITE(*,*) 365.d0*b%Porb,s%mass

               CALL evolv2(s,b,tphysf,dtp,metallicity,zpars)

               total = total + 1 + kkick - kkick/MAX(nkick,1)

!              CALL wrlog('testlog.dat')


               CALL calibration(6,ext)

!                DO n = 1, nclass
                  CALL stats(class(n),ext,track)
!                END DO

               CALL countwarn

               tkkick = tkkick + kkick
               tkdisr = tkdisr + kdisr
               tkmerg = tkmerg + kmerg
               tksnii = tksnii + ksnii
               kkick = 0
               kdisr = 0
               kmerg = 0
               ksnii = 0.0

            END DO star2
         END DO star1
      END DO semiMajor

!       OPEN(1,file=outfile(1:LEN_TRIM(outfile)))
!       WRITE(1,*)
!       WRITE(1,'(A)') 'Model: ',infile(1:LEN_TRIM(infile))
!       WRITE(1,*)
!       WRITE(1,'(A)') '***********************************************'
!       WRITE(1,'(A)') '   Class          Total number '
!       WRITE(1,'(A)') '***********************************************'
!       DO n = 1, nclass
!          WRITE(1,'(2X,A11,8X,I10)') clabel(class(n)),tclass(class(n))
!       END DO
!       WRITE(1,'(A)') '***********************************************'
!       WRITE(1,'(A,I10)') '  Total number of systems evolved      ' &
!            //'         ',total
!       WRITE(1,'(A,1PE12.5)') '  Total number of Type II SN ' &
!            //'explosions      ',tksnii
!       WRITE(1,'(A,I10)') '  Total number of SN kicks calculated  ' &
!            //'         ',tkkick
!       WRITE(1,'(A,I10)') '  Total number of systems disrupted by ' &
!            //'SN kick  ',tkdisr
!       WRITE(1,'(A,I10)') '  Total number of systems merged due to' &
!            //' SN kick ',tkmerg
!       WRITE(1,'(A,I10)') '  Total number of warnings             ' &
!            //'         ',twarn
!       WRITE(1,'(A)') '**********************************************'
!       WRITE(1,*)

      INQUIRE(FILE='CHANN.ERR',EXIST=fexst)
      IF (fexst) WRITE(1,*) 'Channel tracking errors written to ' &
           //'CHANN.ERR'
      CLOSE(1)

! Write out the last num in save_binary
      OPEN(100,FILE="biseps.num",STATUS='UNKNOWN',ACTION='WRITE')
      WRITE(100,*) save_binary(1)%num
      CLOSE(100)

      WRITE(*,*)
      WRITE(*,*) 'BiSEPS terminated succesfully '
      WRITE(*,*) 'See ',outfile(1:LEN_TRIM(outfile)),' for results '
      WRITE(*,*)

! Deallocate array memory
      DEALLOCATE(save_binary)

      STOP 'End of program'
      END PROGRAM

