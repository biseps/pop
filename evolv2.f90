!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	MODULE evolveb
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	CONTAINS
		RECURSIVE SUBROUTINE evolv2(s,b,tphysf,dtp,z,zpars)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Evolves a binary                                                     c
!                                                                      c
! Author: Bart Willems, 28/11/01                                       c
!         Department of Physics and Astronomy                          c
!         The Open University                                          c
!         Walton Hall                                                  c
!         Milton Keynes                                                c
!         MK7 6AA                                                      c
!         United Kingdom                                               c
!                                                                      c
!         E-mail: B.Willems@open.ac.uk                                 c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
		USE precision
		USE consts
		USE params
		USE dtypes
		USE dshare
		USE evolves
		USE accrete
		USE bfuncs
		USE rlof
		USE comenv
		USE snkick
		USE output
		USE randomNum

		IMPLICIT NONE

		DOUBLE PRECISION, INTENT(IN) :: tphysf,dtp
		DOUBLE PRECISION, INTENT(IN) :: z
		DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars
		TYPE (bnq), INTENT(INOUT) :: b
		TYPE (stq), DIMENSION(2), INTENT(INOUT) :: s

		INTEGER :: n,nn,n1,n2,dn
		INTEGER :: j,jt,it1,it2,it3
		INTEGER :: nk,nkm,onkick,obkw

		DOUBLE PRECISION :: tphysf0,ktsave
		DOUBLE PRECISION :: dtms,dt,dtb,dtrl
		DOUBLE PRECISION :: djorb,dr,rp
		DOUBLE PRECISION :: q1,oq1,dm,p

		DOUBLE PRECISION, PARAMETER :: tol = 1.d-9

		DOUBLE PRECISION, DIMENSION(2) :: reff,oreff
		DOUBLE PRECISION, DIMENSION(2) :: dtm

		TYPE (bnq) :: ob,kb
		TYPE (stq), DIMENSION(2) :: os,ks

		DOUBLE PRECISION :: vrotf,betaC,it3Max,it2Max
		DOUBLE PRECISION,PARAMETER :: minTime=0.5d0,maxTime=1.d0,absMinTime=1.d-3
		LOGICAL :: timeFlag,t,massTransWarn

		massTransWarn=.False.
! Initialize variables
		s%lum = 0.d0
		s%r = 0.d0
		s%rc = 0.d0
		s%djmb = 0.d0
		s%dmw = 0.d0
		s%dma = 0.d0
		s%xlw = 0.d0
		s%djmw = 0.d0
		s%djma = 0.d0
		s%dmrl = 0.d0
		s%xldm = 0.d0
		s%xlcor = 0.d0

		b%cls = 0
		b%dagr = 0.d0
		b%degr = 0.d0
		b%damw = 0.d0
		b%demw = 0.d0

		IF (kkick > 0) THEN
			b%seq_num = b%seq_num + 1.d0/DBLE(nkick)
		ELSE
			b%seq_num = b%seq_num + 1.d0
		END IF

		warn = 0
		it1 = 0
		it2 = 0
		it3 = 0
		jt = 0
		IF (s(1)%mt > s(2)%mt) THEN
			n1 = 1
			n2 = 2
			dn = 1
		ELSE
			n1 = 2
			n2 = 1
			dn = -1
		END IF

! Calculate stellar quantities at the initial time
		DO n = 1, 2
			timeFlag=.TRUE.
			CALL evolv1(s(n)%kw,s(n)%mass,s(n)%mt,s(n)%r, &
				s(n)%lum,s(n)%mc,s(n)%rc,s(n)%epoch,s(n)%tphys, &
				s(n)%tphys,s(n)%rl,z,zpars,timeFlag)
		END DO

! Mass ratio minus 1
		q1 = (s(1)%mt/s(2)%mt) - 1.d0

! Calculate the Semi-major axis (in Solar radii) and the orbital
!angular momentum at the initial time
		b%a = Porb2a(b%Porb,s(1)%mt,s(2)%mt)
		b%jorb = jorbit(b%a,b%ecc,s(1)%mt,s(2)%mt)

! 		write(*,*) b%a
! If tides are not taken into account, circularize the orbit
! instantaneously but conserve orbital angular momentum
		IF ((tflag == 0).AND.(b%ecc > 0.d0)) THEN
! Store values prior to circularization
			CALL reclog(s,b)
			IF (warn == 11) RETURN
			b%ecc = 0.d0
			b%a = aorbit(b%jorb,b%ecc,s(1)%mt,s(2)%mt)
			b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)
		END IF

! Radii of the Roche lobes (in Solar radii) at the initial time
		DO n = 1, 2
			s(n)%rl = rlobe(b%a,s(n)%mt,s(3-n)%mt)
		END DO

! 		write(*,*) s%mt,s(2)%mt/s(1)%mt
! 		write(*,*) s(1)%r,s(1)%rl,s(2)%r,s(2)%rl,b%kw,b%a,sum(s%r),sum(s%rl)

! Check for Roche lobe overflow
! If RLOF occurs, the radius of the Roche lobe is used as the
! effective stellar radius
		IF (s(1)%r < s(1)%rl) THEN
			reff(1) = s(1)%r
			IF (s(2)%r < s(2)%rl) THEN ! Detached binary
				reff(2) = s(2)%r
				b%kw = 0
				tphysf0 = tphysf
			ELSE                ! M2 fills its Roche lobe
				reff(2) = s(2)%rl
				nu = s(2)%mt/s(1)%mt
				b%kw = 20
				massTransWarn=.True.
			END IF
		ELSE
			massTransWarn=.True.
                 ! M1 fills its Roche lobe
			reff(1) = s(1)%rl
			IF (s(2)%r < s(2)%rl) THEN
				reff(2) = s(2)%r
				nu = s(1)%mt/s(2)%mt
				b%kw = 10
			ELSE                ! Both stars fill their Roche lobe
				reff(2) = s(2)%rl
				SELECT CASE (s(2)%kw)
				CASE (2:6,8:9)
					b%kw = 24 ! Common envelope
				CASE DEFAULT
					SELECT CASE (s(1)%kw)
					CASE (2:6,8:9)
						b%kw = 14  ! Common envelope
					CASE DEFAULT
						b%kw = 30
	! 					s%r=s%rl
						!These systems are just wierd with sum(roche lobe) < a but both stars exceed roche lobe
						!They also generate TRO's on 100yr timescale (just happens to be minimum biseps evolves for...)
						!Therefore i do not trust that these are "real" as such we ignore them
						!Using MESA on these systems, MESA fails after 2 timesteps
						!Rob Farmer 02/04/2013
						warn=99
! 						if(contactExit(s,b).eqv. .TRUE.) THEN
	! 					write(*,*) "5"
! 						return
! 						end if
					END SELECT
				END SELECT
			END IF
		END IF

		if (massTransWarn) THEN
! 			write(*,*) b%kw
			RETURN
		END IF

! 		write(*,*) s(1)%r,s(1)%rl,s(2)%r,s(2)%rl,b%kw,b%a,sum(s%r),sum(s%rl)
! 		stop

! Determine the type of RLOF
		IF ((b%kw == 10).OR.(b%kw == 20).OR.(b%kw==30)) THEN
! Force instantaneous circularisation
			IF (b%ecc > 0.d0) THEN
			b%ecc = 0.d0
			b%a = aorbit(b%jorb,b%ecc,s(1)%mt,s(2)%mt)
			b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)
			s(1)%rl = rlobe(b%a,s(1)%mt,s(2)%mt)
			s(2)%rl = rlobe(b%a,s(2)%mt,s(1)%mt)
			END IF
			CALL rloftp(s,b,zpars) ! Determine the type of RLOF
			dtrl=1.d-9
			tphysf0 = MIN(tphysf,s(1)%tphys+dtrl)
! 			write(*,*) "r",tphysf,s(1)%tphys+dtrl,dtrl
		END IF

! Initialize the spin of the stars: if tides are taken into account
! and the binary is detached, an appropriate ZAMS spin is set for
! both stars (unless the spin is specified in the input file), otherwise
! the stars are assumed to corotate with the orbital motion.
		IF ((tflag /= 0).AND.(b%kw == 0)) THEN
			DO n = 1, 2
			IF ((s(n)%tphys == 0.0).AND.(s(n)%ospin < 0.001)) THEN
				s(n)%ospin = 45.35d0*vrotf(s(n)%mt)/reff(n)
			END IF
			s(n)%jspin = o2jspin(s(n)%mt,reff(n),s(n)%mc, &
					s(n)%rc,s(n)%ospin)
			END DO
		ELSE
			DO n = 1, 2
			s(n)%ospin = 2.d0*pi/b%Porb
			s(n)%jspin = o2jspin(s(n)%mt,reff(n),s(n)%mc, &
					s(n)%rc,s(n)%ospin)
			END DO
		END IF

! Set coronal X-ray luminosity
		DO n = 1, 2
			s(n)%xlcor = corona(s(n)%tphys,s(n)%kw,s(n)%mass,s(n)%mt, &
				s(n)%mc,reff(n),s(n)%epoch,s(n)%ospin,zpars)
		END DO

! Log first time step
		CALL reclog(s,b)

! If necessary, save values for output
		IF ((dsave.AND.(s(1)%tphys >=tsave)).OR.asave) THEN
			CALL record(s,b)
			IF (dsave) tsave = tsave + dtp
		END IF

		IF (warn > 0) THEN
			IF (dsave.OR.asave) CALL record0
			save_binary(jp)%tphys = -1.0
			save_binary(jp)%bkw = warn
			jp = jp + 1
			RETURN
		END IF

		j = 1
		main: DO
!  			write(*,*) s%kw,b%kw,s%mt,s%r,s%r/s%rl,b%a
			j = j + 1
			IF (j > maxstep) THEN
			warn = 9
			EXIT main
			END IF
!           	tphysf0=MAX(tphysf0*unif(minTime,maxTime),absMinTime)
! Remember binary and stellar parameters from the previous time step
			ob = b
			os = s
			oreff = reff
			oq1 = q1
!             write(*,*) "e",tphysf0
			DO n = n1, n2, dn
				timeFlag=.TRUE.
				CALL evolv1(s(n)%kw,s(n)%mass,s(n)%mt,s(n)%r, &
						s(n)%lum,s(n)%mc,s(n)%rc,s(n)%epoch,s(n)%tphys, &
						tphysf0,s(n)%rl,z,zpars,timeFlag)
	!                      write(*,*) "e",n,tphysf0,s(n)%tphys
				tphysf0 = MIN(tphysf0,s(n)%tphys)
			END DO
! 			write(*,*) "t1",tphysf0,s%tphys,s%tphys - os%tphys
			dtm = s%tphys - os%tphys ! Time step made by evolv1

! 			write(*,*) dtm(1),dtm(2)
			IF (dtm(1) < dtm(2)) THEN
				timeFlag=.FALSE.
! Evolve star 2 with smaller time step dtm(1)
				s(2) = os(2)
				tphysf0 = os(2)%tphys + dtm(1)
				CALL evolv1(s(2)%kw,s(2)%mass,s(2)%mt,s(2)%r, &
						s(2)%lum,s(2)%mc,s(2)%rc,s(2)%epoch,s(2)%tphys, &
						tphysf0,s(2)%rl,z,zpars,timeFlag)
				dtms = dtm(1)
				n1 = 1
				n2 = 2
				dn = 1
!  			write(*,*) "DE1",s(1)%mt,s(2)%mt,dtm,dtms
			ELSE IF (dtm(2) < dtm(1)) THEN
				timeFlag=.FALSE.
	! Evolve star 1 with smaller time step dtm(2)
				s(1) = os(1)
				tphysf0 = os(1)%tphys + dtm(2)
				CALL evolv1(s(1)%kw,s(1)%mass,s(1)%mt,s(1)%r, &
						s(1)%lum,s(1)%mc,s(1)%rc,s(1)%epoch,s(1)%tphys, &
						tphysf0,s(1)%rl,z,zpars,timeFlag)
				dtms = dtm(2)
				n1 = 2
				n2 = 1
				dn = -1
!  			write(*,*) "DE2",s(1)%mt,s(2)%mt,dtm,dtms
			ELSE
				dtms = dtm(1)
!  			write(*,*) "DE3",s(1)%mt,s(2)%mt,dtm,dtms
			END IF

! Time step taken in years
			dt = 1.0d+06*dtms

!  			write(*,*) "DE",s(1)%mt,s(2)%mt,dtm,dtms
			IF (dt < tol) THEN
			warn = 1
			EXIT main
			END IF

! Did a supernova explosion occur?
			DO n = n1, n2, dn
			IF ((s(n)%kw >= 13).AND.(os(n)%kw < 13)) THEN
				SELECT CASE (s(n)%kw)
				CASE (15) ! Massless supernova remnant
					IF (os(n)%kw < 7) THEN
						IF (kkick > 0) THEN
						ksnii = ksnii + 1.d0/DBLE(nkick)
						ELSE
						ksnii = ksnii + 1.d0
						END IF
					END IF
					s%rl = 1.d-10
					s%dma = 0.d0
					s%djma = 0.d0
					s%dmrl = 0.d0
					s%xlw = 0.d0
					s%xldm = 0.d0
					s%dmw = 0.d0
					s%djmw = 0.d0
					s%djmb = 0.d0
					s%jspin = 0.d0
					s%ospin = 0.d0
					s%xlcor = 0.d0

					IF (s(3-n)%kw < 13) THEN
						s(3-n)%dmw = (s(3-n)%mt - os(3-n)%mt)/dt
						s(3-n)%djmw = djmass(os(3-n)%r,os(3-n)%ospin, &
							s(3-n)%dmw)
						s(3-n)%djmb = magbr(os(3-n)%tphys,os(3-n)%kw, &
							s(3-n)%mass,os(3-n)%mt,os(3-n)%r, &
							os(3-n)%mc,os(3-n)%epoch,os(3-n)%ospin, &
							zpars,ob%kw)*dt
						s(3-n)%jspin = MAX(0.d0, &
							os(3-n)%jspin+s(3-n)%djmw-s(3-n)%djmb)
						s(3-n)%ospin = j2ospin(s(3-n)%mt,s(3-n)%r, &
							s(3-n)%mc,s(3-n)%rc,s(3-n)%jspin)
						s(3-n)%xlcor = corona(s(3-n)%tphys,s(3-n)%kw, &
							s(3-n)%mass,s(3-n)%mt,s(3-n)%mc,s(3-n)%r, &
							s(3-n)%epoch,s(3-n)%ospin,zpars)
					END IF

					b%kw = 50
					b%cls = 0
					b%Porb = 0.d0
					b%a = 0.d0
					b%ecc = 0.d0
					b%jorb = 0.d0
					b%dagr = 0.d0
					b%degr = 0.d0
					b%damw = 0.d0
					b%demw = 0.d0

! Save pre-SN values
					IF (os(n)%kw < 7) ob%cls = 6
					CALL reclog(os,ob)
					warn = 99
					EXIT main
				CASE(13) ! Neutron star remnant
					IF (os(n)%kw < 7) THEN
						IF (kkick > 0) THEN
						ksnii = ksnii + 1.d0/DBLE(nkick)
						ELSE
						ksnii = ksnii + 1.d0
						END IF
					END IF
! Force fast rotational spin in abscence of tides
					IF (tflag == 0) THEN
						s(n)%ospin = 2.0d+08
						s(n)%jspin = o2jspin(s(n)%mt,s(n)%r,s(n)%mc, &
							s(n)%rc,s(n)%ospin)
					END IF

! Assume SN is instantaneous
					s%djmb = 0.d0
					s%rl = 1.d-10
					s%dmw = 0.d0
					s%dma = 0.d0
					s%djmw = 0.d0
					s%djma = 0.d0
					s%dmrl = 0.d0
					s%xlw = 0.d0
					s%xldm = 0.d0

					b%cls = 0
					b%dagr = 0.d0
					b%degr = 0.d0
					b%damw = 0.d0
					b%demw = 0.d0

					ks = s
					kb = b
					ktsave = tsave

! Save pre-SN values
					IF (os(n)%kw < 7) ob%cls = 6
					CALL reclog(os,ob)
					IF (warn == 11) EXIT main

!cccccccc We 're not interested in NSNS or BH DD's for now cccc
					IF (s(3-n)%kw >= 13) THEN
						warn = 99
						EXIT main
					END IF
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

					IF ((nkick == 0).OR.(os(n)%kw >= 10)) THEN
						nkm = 1
					ELSE
						nkm = nkick
						kkick = kkick + nkick
					END IF
					onkick = nkick
! Branch off the evolution with nkm random supernova kicks
					klp1: DO nk = 1, nkm
						IF (os(n)%kw >= 10) nkick = 0
						CALL kick(os,ob,s,b)
						nkick = onkick
						IF (warn == 11) THEN
						IF (kkick > 0) THEN
							b%seq_num = b%seq_num + 1.d0/DBLE(nkick)
						ELSE
							b%seq_num = b%seq_num + 1.d0
						END IF
						EXIT main
						END IF
						IF (warn > 0) THEN
						s = ks
						b = kb
						IF (kkick > 0) THEN
							b%seq_num = b%seq_num + DBLE(nk)/DBLE(nkick)
						ELSE
							b%seq_num = b%seq_num + DBLE(nk)
						END IF
						warn = 0
						CYCLE klp1
						END IF
						IF (b%kw == 0) THEN
						IF (kkick > 0) THEN
							b%seq_num = b%seq_num - 1.d0/DBLE(nkick)
						ELSE
							b%seq_num = b%seq_num - 1.d0
						END IF
						CALL evolv2(s,b,tphysf,dtp,z,zpars)
						IF (warn == 11) EXIT klp1
						save_binary(jp-1)%tphys = -2.0
						ELSE
						IF (dsave.OR.asave) THEN
							CALL record(s,b)
							CALL record0
						END IF
						CALL reclog(s,b)
						IF (warn == 11) EXIT main
						save_binary(jp)%tphys = -2.0
						save_binary(jp)%bkw = warn
						jp = jp + 1
						IF (jp >= nnlog) THEN
							warn = 11
							EXIT main
						END IF
						END IF
						s = ks
						b = kb
						IF (kkick > 0) THEN
						b%seq_num = b%seq_num + DBLE(nk)/DBLE(nkick)
						ELSE
						b%seq_num = b%seq_num + DBLE(nk)
						END IF
						tsave = ktsave
						warn = 0
					END DO klp1
					RETURN
				CASE(14)
					IF (os(n)%kw < 7) THEN
						IF (kkick > 0) THEN
						ksnii = ksnii + 1.d0/DBLE(nkick)
						ELSE
						ksnii = ksnii + 1.d0
						END IF
					END IF
					IF (tflag == 0) THEN
						s(n)%ospin = 2.0d+08
						s(n)%jspin = o2jspin(s(n)%mt,s(n)%r,s(n)%mc, &
							s(n)%rc,s(n)%ospin)
					END IF

! Assume SN is instantaneous
					s%djmb = 0.d0
					s%rl = 1.d-10
					s%dmw = 0.d0
					s%dma = 0.d0
					s%djmw = 0.d0
					s%djma = 0.d0
					s%dmrl = 0.d0
					s%xlw = 0.d0
					s%xldm = 0.d0

					b%cls = 0
					b%dagr = 0.d0
					b%degr = 0.d0
					b%damw = 0.d0
					b%demw = 0.d0

! Save pre-SN values
					IF (os(n)%kw < 7) ob%cls = 6
					CALL reclog(os,ob)

					onkick = nkick
					nkick = 0
					CALL kick(os,ob,s,b)
					nkick = onkick
					IF (warn == 11) EXIT main
					IF (warn > 0) EXIT main

!cccccccc We 're not interested in BH binaries for now cccccccc
!                     warn = 99
!                     EXIT main
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

					IF (b%kw == 0) THEN
						CALL evolv2(s,b,tphysf,dtp,z,zpars)
						IF (warn == 11) EXIT main
						save_binary(jp-1)%tphys = -2.0
					ELSE
						IF (dsave.OR.asave) THEN
						CALL record(s,b)
						CALL record0
						END IF
						CALL reclog(s,b)
						IF (warn == 11) EXIT main
						save_binary(jp)%tphys = -2.0
						save_binary(jp)%bkw = warn
						jp = jp + 1
						IF (jp >= nnlog) THEN
						warn = 11
						EXIT main
						END IF
					END IF
					RETURN
				END SELECT
			END IF
			END DO

! Mass accreted by the companion
!    dmw = mass-loss rate due to wind
!    dma = mass-accretion rate (Bondi-Hoyle)
			s%dmw = 0.d0
			WHERE (s%kw < 13) s%dmw = (s%mt - os%mt)/dt
			DO n = 1, 2
			IF (s(n)%dmw < 0) THEN
				s(3-n)%dma = mawind(ob%a,ob%ecc,os(n)%mt,oreff(n), &
					s(n)%dmw,os(3-n)%kw,os(3-n)%mt,oreff(3-n),zpars)
! Update stellar quantities and rejuvenate star
				CALL update(s(3-n)%kw,s(3-n)%mass,s(3-n)%mt, &
					s(3-n)%lum,s(3-n)%r,s(3-n)%mc,s(3-n)%rc, &
					s(3-n)%tphys,s(3-n)%epoch,s(3-n)%dma*dt, &
					s(3-n)%dma,zpars,s(n)%kw)
! Wind accretion luminosity
				s(3-n)%xlw = xrlumw(os(3-n)%mt,s(3-n)%dma,oreff(3-n))
! Change of spin angular momentum
				s(n)%djmw = djmass(oreff(n),os(n)%ospin,s(n)%dmw)*dt
				s(3-n)%djma = djmass(oreff(n),os(n)%ospin, &
					s(3-n)%dma)*dt
! Changes of the the orbital parameters
				b%damw(3-n) = daac(ob%a,ob%ecc,os(n)%mt,s(n)%dmw, &
					os(3-n)%mt,s(3-n)%dma)
				b%demw(3-n) = deac(ob%ecc,os(n)%mt,os(3-n)%mt, &
					s(3-n)%dma)
			ELSE
				s(3-n)%dma = 0.d0
				s(3-n)%xlw = 0.d0
				s(n)%djmw = 0.d0
				s(3-n)%djma = 0.d0
				b%damw(3-n) = 0.d0
				b%demw(3-n) = 0.d0
			END IF
			END DO

! Did a supernova explosion occur?
			DO n = n1, n2, dn
			IF ((s(n)%kw >= 13).AND.(os(n)%kw < 13)) THEN
				SELECT CASE (s(n)%kw)
				CASE (15) ! Massless supernova remnant
					IF (os(n)%kw < 7) THEN
						IF (kkick > 0) THEN
						ksnii = ksnii + 1.d0/DBLE(nkick)
						ELSE
						ksnii = ksnii + 1.d0
						END IF
					END IF
					s%rl = 1.d-10
					s%dma = 0.d0
					s%djma = 0.d0
					s%dmrl = 0.d0
					s%xlw = 0.d0
					s%xldm = 0.d0
					s%dmw = 0.d0
					s%djmw = 0.d0
					s%djmb = 0.d0
					s%jspin = 0.d0
					s%ospin = 0.d0
					s%xlcor = 0.d0

					IF (s(3-n)%kw < 13) THEN
						s(3-n)%dmw = (s(3-n)%mt - os(3-n)%mt)/dt
						s(3-n)%djmw = djmass(os(3-n)%r,os(3-n)%ospin, &
							s(3-n)%dmw)
						s(3-n)%djmb = magbr(os(3-n)%tphys,os(3-n)%kw, &
							s(3-n)%mass,os(3-n)%mt,os(3-n)%r, &
							os(3-n)%mc,os(3-n)%epoch,os(3-n)%ospin, &
							zpars,ob%kw)*dt
						s(3-n)%jspin = MAX(0.d0, &
							os(3-n)%jspin+s(3-n)%djmw-s(3-n)%djmb)
						s(3-n)%ospin = j2ospin(s(3-n)%mt,s(3-n)%r, &
							s(3-n)%mc,s(3-n)%rc,s(3-n)%jspin)
						s(3-n)%xlcor = corona(s(3-n)%tphys,s(3-n)%kw, &
							s(3-n)%mass,s(3-n)%mt,s(3-n)%mc,s(3-n)%r, &
							s(3-n)%epoch,s(3-n)%ospin,zpars)
					END IF

					b%kw = 50
					b%cls = 0
					b%Porb = 0.d0
					b%a = 0.d0
					b%ecc = 0.d0
					b%jorb = 0.d0
					b%dagr = 0.d0
					b%degr = 0.d0
					b%damw = 0.d0
					b%demw = 0.d0

! Save pre-SN values
					IF (os(n)%kw < 7) ob%cls = 6
					CALL reclog(os,ob)
					warn = 99
					EXIT main
				CASE(13) ! Neutron star remnant
					IF (os(n)%kw < 7) THEN
						IF (kkick > 0) THEN
						ksnii = ksnii + 1.d0/DBLE(nkick)
						ELSE
						ksnii = ksnii + 1.d0
						END IF
					END IF
! Force fast rotational spin in abscence of tides
					IF (tflag == 0) THEN
						s(n)%ospin = 2.0d+08
						s(n)%jspin = o2jspin(s(n)%mt,s(n)%r,s(n)%mc, &
							s(n)%rc,s(n)%ospin)
					END IF

! Assume SN is instantaneous
					s%djmb = 0.d0
					s%rl = 1.d-10
					s%dmw = 0.d0
					s%dma = 0.d0
					s%djmw = 0.d0
					s%djma = 0.d0
					s%dmrl = 0.d0
					s%xlw = 0.d0
					s%xldm = 0.d0

					b%cls = 0
					b%dagr = 0.d0
					b%degr = 0.d0
					b%damw = 0.d0
					b%demw = 0.d0

					ks = s
					kb = b
					ktsave = tsave

! Save pre-SN values
					IF (os(n)%kw < 7) ob%cls = 6
					CALL reclog(os,ob)
					IF (warn == 11) EXIT main

!cccccccc We 're not interested in NSNS or BH DD's for now cccc
					IF (s(3-n)%kw >= 13) THEN
						warn = 99
						EXIT main
					END IF
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

					IF ((nkick == 0).OR.(os(n)%kw >= 10)) THEN
						nkm = 1
					ELSE
						nkm = nkick
						kkick = kkick + nkick
					END IF
					onkick = nkick
! Branch off the evolution with nkm random supernova kicks
					klp2: DO nk = 1, nkm
						IF (os(n)%kw >= 10) nkick = 0
						CALL kick(os,ob,s,b)
						nkick = onkick
						IF (warn == 11) THEN
						IF (kkick > 0) THEN
							b%seq_num = b%seq_num + 1.d0/DBLE(nkick)
						ELSE
							b%seq_num = b%seq_num + 1.d0
						END IF
						EXIT main
						END IF
						IF (warn > 0) THEN
						s = ks
						b = kb
						IF (kkick > 0) THEN
							b%seq_num = b%seq_num + DBLE(nk)/DBLE(nkick)
						ELSE
							b%seq_num = b%seq_num + DBLE(nk)
						END IF
						warn = 0
						CYCLE klp2
						END IF
						IF (b%kw == 0) THEN
						IF (kkick > 0) THEN
							b%seq_num = b%seq_num - 1.d0/DBLE(nkick)
						ELSE
							b%seq_num = b%seq_num - 1.d0
						END IF
						CALL evolv2(s,b,tphysf,dtp,z,zpars)
						IF (warn == 11) EXIT klp2
						save_binary(jp-1)%tphys = -2.0
						ELSE
						IF (dsave.OR.asave) THEN
							CALL record(s,b)
							CALL record0
						END IF
						CALL reclog(s,b)
						IF (warn == 11) EXIT main
						save_binary(jp)%tphys = -2.0
						save_binary(jp)%bkw = warn
						jp = jp + 1
						IF (jp >= nnlog) THEN
							warn = 11
							EXIT main
						END IF
						END IF
						s = ks
						b = kb
						IF (kkick > 0) THEN
						b%seq_num = b%seq_num + DBLE(nk)/DBLE(nkick)
						ELSE
						b%seq_num = b%seq_num + DBLE(nk)
						END IF
						tsave = ktsave
						warn = 0
					END DO klp2
					RETURN
				CASE(14)
					IF (os(n)%kw < 7) THEN
						IF (kkick > 0) THEN
						ksnii = ksnii + 1.d0/DBLE(nkick)
						ELSE
						ksnii = ksnii + 1.d0
						END IF
					END IF
					IF (tflag == 0) THEN
						s(n)%ospin = 2.0d+08
						s(n)%jspin = o2jspin(s(n)%mt,s(n)%r,s(n)%mc, &
							s(n)%rc,s(n)%ospin)
					END IF

! Assume SN is instantaneous
					s%djmb = 0.d0
					s%rl = 1.d-10
					s%dmw = 0.d0
					s%dma = 0.d0
					s%djmw = 0.d0
					s%djma = 0.d0
					s%dmrl = 0.d0
					s%xlw = 0.d0
					s%xldm = 0.d0

					b%cls = 0
					b%dagr = 0.d0
					b%degr = 0.d0
					b%damw = 0.d0
					b%demw = 0.d0

! Save pre-SN values
					IF (os(n)%kw < 7) ob%cls = 6
					CALL reclog(os,ob)

					onkick = nkick
					nkick = 0
					CALL kick(os,ob,s,b)
					nkick = onkick
					IF (warn == 11) EXIT main
					IF (warn > 0) EXIT main

!cccccccc We 're not interested in BH binaries for now cccccccc
!                     warn = 99
!                     EXIT main
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

					IF (b%kw == 0) THEN
						CALL evolv2(s,b,tphysf,dtp,z,zpars)
						IF (warn == 11) EXIT main
						save_binary(jp-1)%tphys = -2.0
					ELSE
						IF (dsave.OR.asave) THEN
						CALL record(s,b)
						CALL record0
						END IF
						CALL reclog(s,b)
						IF (warn == 11) EXIT main
						save_binary(jp)%tphys = -2.0
						save_binary(jp)%bkw = warn
						jp = jp + 1
						IF (jp >= nnlog) THEN
						warn = 11
						EXIT main
						END IF
					END IF
					RETURN
				END SELECT
			END IF
			END DO

! Calculate the change of spin angular momentum due to magnetic braking
			DO n = 1, 2
				s(n)%djmb = magbr(os(n)%tphys,os(n)%kw,os(n)%mass, &
					os(n)%mt,oreff(n),os(n)%mc,os(n)%epoch, &
					os(n)%ospin,zpars,ob%kw)*dt
			END DO

! Changes of the orbital parameters due to gravitational radiation
			b%dagr = dagrav(ob%a,ob%ecc,os(1)%mt,os(2)%mt)
			b%degr = degrav(ob%a,ob%ecc,os(1)%mt,os(2)%mt)

			b%a = ob%a + (b%damw(1) + b%damw(2) + b%dagr)*dt
			b%a = MAX(0.d0,b%a)
			b%ecc = ob%ecc + (b%demw(1) + b%demw(2) + b%degr)*dt
			b%ecc = MAX(0.d0,b%ecc)

! Calculate the new orbital period and orbital angular momentum
			b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)
			b%jorb = jorbit(b%a,b%ecc,s(1)%mt,s(2)%mt)

! Force magnetic breaking to work on the orbital angular momentum
! in the absence of tides
 			IF (tflag == 0) THEN
				DO n = 1, 2
	! Reduce magnetic braking for well detached systems
					IF (os(n)%r/os(n)%rl < 0.25d0) THEN
						s(n)%djmb = s(n)%djmb*((4.d0*os(n)%r/os(n)%rl)**5)
					END IF
				END DO
				b%jorb = MAX(0.d0,b%jorb-s(1)%djmb-s(2)%djmb)
				b%a = aorbit(b%jorb,b%ecc,s(1)%mt,s(2)%mt)
				b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)
 			END IF

! Limit time step so that the orbital angular momentum doesn't
! change by more than 2% during a fixed evolutionary phase
			IF ((ob%kw == 0).AND.(s(1)%kw == os(1)%kw) .AND.(s(2)%kw == os(2)%kw)) THEN
				djorb = ABS(b%jorb - ob%jorb)
				IF (djorb/ob%jorb > 0.02d0) THEN
					dtb = (dtms/djorb)*0.019d0*ob%jorb
					s = os
					b = ob
					reff = oreff
					q1 = oq1
					j = j - 1
					tphysf0 = MIN(tphysf,os(1)%tphys+dtb)
					jt = jt + 1
					IF (jt < 10) CYCLE main
					warn = 13
					EXIT main
				ELSE
					jt = 0
				END IF
			END IF

! Check for spiral-in
			IF (b%a == 0.d0) THEN
				IF ((b%kw/10 == 1).OR.(b%kw/10 == 2)) THEN
					b%kw = 44
				ELSE
					b%kw = 42
				END IF
				warn = 99
				EXIT main
			END IF

! Calculate the radii of the Roche lobes
			DO n = 1, 2
				s(n)%rl = rlobe(b%a,s(n)%mt,s(3-n)%mt)
			END DO

! Check for Roche lobe overflow
! If RLOF occurs, the radius of the Roche lobe is used as the
! effective stellar radius
			n = ob%kw/10
				if(ob%kw==30) THEN
					if(os(1)%mt>os(2)%mt) then
						n=2
					else
						n=1
					end if
				end if

			IF (ob%kw-10*n /= 2) THEN
				IF (s(1)%r < s(1)%rl) THEN
					reff(1) = s(1)%r
					IF (s(2)%r < s(2)%rl) THEN ! Detached binary
						reff(2) = s(2)%r
						b%kw = 0
						tphysf0 = tphysf
					ELSE          ! M2 fills its Roche lobe
						reff(2) = s(2)%rl
						nu = s(2)%mt/s(1)%mt
						IF (ob%kw/10 /= 2) b%kw = 20
					END IF
				ELSE             ! M1 fills its Roche lobe
					reff(1) = s(1)%rl
					IF (s(2)%r < s(2)%rl) THEN
						reff(2) = s(2)%r
						nu = s(1)%mt/s(2)%mt
						IF (ob%kw/10 /= 1) b%kw = 10
					ELSE          ! Contact binary
						reff(2) = s(2)%rl
						SELECT CASE (s(2)%kw)
						CASE (2:6,8:9)
							b%kw = 24 ! Common envelope
						CASE DEFAULT
							SELECT CASE (s(1)%kw)
							CASE (2:6,8:9)
								b%kw = 14 ! Common envelope
							CASE DEFAULT
								b%kw = 30 !Over contact
								if(contactExit(s,b).eqv. .TRUE.) THEN
	! 								write(*,*) "4"
									exit main
								end if
							END SELECT
						END SELECT
					END IF
				END IF
			ELSE
				n = b%kw/10
				reff(n) = s(n)%rl
				nu = s(n)%mt/s(3-n)%mt
				IF (s(3-n)%r > s(3-n)%rl) THEN
					reff(3-n) = s(3-n)%rl
					SELECT CASE (s(n)%kw)
					CASE (2:6,8:9)
						b%kw = 10*n + 4 ! Common envelope
					CASE DEFAULT
						SELECT CASE (s(3-n)%kw)
						CASE (2:6,8:9)
							b%kw = 10*(3-n) + 4 ! Common envelope
						CASE DEFAULT
							b%kw = 30
							if(contactExit(s,b).eqv. .TRUE.) THEN
	! 					write(*,*) "3"
						exit main
					end if
						END SELECT
					END SELECT
				ELSE
					reff(3-n) = s(3-n)%r
				END IF
			END IF

! Determine the type of RLOF
			IF ((b%kw == 10).OR.(b%kw == 20).or.(b%kw==30)) THEN
! Force instantaneous circularisation
				IF (b%ecc > 0.d0) THEN
					b%ecc = 0.d0
					b%a = aorbit(b%jorb,b%ecc,s(1)%mt,s(2)%mt)
					b%Porb = a2Porb(b%a,s(1)%mt,s(2)%mt)
					s(1)%rl = rlobe(b%a,s(1)%mt,s(2)%mt)
					s(2)%rl = rlobe(b%a,s(2)%mt,s(1)%mt)
				END IF
				n = b%kw/10
				if(b%kw==30) THEN
					if(s(1)%mt>s(2)%mt) then
						n=1
					else
						n=2
					end if
				end if
! 				if(b%kw==30) WHERE(s%r>s%rl) s%r=s%rl
	! Check if radius is not too much larger than Roche lobe radius
				IF ((os(n)%r/os(n)%rl < 0.9999d0).AND.(it1 < 50)) THEN
					obkw = ob%kw - 10*(3-n)
					IF ((obkw/=4).AND.(obkw/=5).AND.(os(n)%kw<10)) THEN
						dr = (s(n)%r/s(n)%rl - os(n)%r/os(n)%rl)/dtms
						dtrl = (0.99991d0 - os(n)%r/os(n)%rl)/dr
						dtrl = MAX(1.d-9,dtrl)
						s = os
						b = ob
						reff = oreff
						q1 = oq1
						j = j - 1
						tphysf0 = MIN(tphysf,os(n)%tphys+dtrl)
						it1 = it1 + 1
						CYCLE main
	! The evolution does not need to be stopped when this proces does
	! not converge (it1 > 30). The only problem might be that the amount
	! of mass transfered during the first time step is too large, but this
	! will be checked a little further down.
					END IF
				END IF
!  				if(b%kw==30) WHERE(s%r>s%rl) s%r=s%rl
! 				write(*,*) s%kw,b%kw,s%mt,s%r,s%r/s%rl,b%a,"*"
				it2Max=30
!  				if(any(save_binary%bkw==30)) n=3-n
				IF ((s(n)%r/s(n)%rl > 1.0001d0).and.(b%kw/=30).and.(ob%kw/=30)) THEN
					obkw = ob%kw - 10*(3-n)
					IF (((obkw /= 4).AND.(obkw /= 5))) THEN
						IF (it2 < it2Max) THEN
							dr = (s(n)%r/s(n)%rl - os(n)%r/os(n)%rl)/dtms
							dtrl = (1.00009d0 - os(n)%r/os(n)%rl)/dr
							dtrl = MAX(1.d-9,dtrl)
							s = os
							b = ob
							reff = oreff
							q1 = oq1
							j = j - 1
							tphysf0 = MIN(tphysf,os(n)%tphys+dtrl)
							it2 = it2 + 1
!         						write(*,*) it2
							CYCLE main
						ELSE

!         				write(*,*) "*",s%kw,b%kw,s%mt,s%r,s%r/s%rl,b%a
							warn = 12
							EXIT main
						END IF
					END IF
				END IF
! 				write(*,*) "*"
!  				if(any(save_binary%bkw==30)) n=3-n
! Make sure first time step is not too large
				CALL rloftp(s,b,zpars) ! Determine the type of RLOF
				IF (warn > 0) EXIT main

				it3Max=30
				if(b%kw==30) it3Max=1000
				obkw = ob%kw - 10*n
				IF ((obkw /= 4).AND.(obkw /= 5)) THEN
					dm = mtrans(b%kw,s(n)%kw,s(n)%mt,s(n)%r, &
						s(n)%lum,s(n)%mc,s(n)%rl) ! Mass-transfer rate
					IF (warn > 0) EXIT main
					dm = ABS(dm)
					IF (dm*dt > 0.01d0*s(n)%mt) THEN
						IF (it3 < it3Max) THEN
							dtrl = dtms-1.d-6*0.001d0*s(n)%mt/dm
							dtrl = MAX(1.d-9,dtrl)
							s = os
							b = ob
							reff = oreff
							q1 = oq1
							j = j - 1
							tphysf0 = MIN(tphysf,os(n)%tphys+dtrl)
							it3 = it3 + 1
							CYCLE main
						ELSE IF (dm*dt/s(n)%mt > 0.2d0) THEN
							warn = 8
							EXIT main
						END IF
					END IF
				END IF
				it1 = 0
				it2 = 0
				it3 = 0
			END IF

			IF ((b%kw/10 == 1).OR.(b%kw/10 == 2).or.(b%kw==30)) THEN
			n = b%kw/10
			if(b%kw==30) THEN
				if(s(1)%mt>s(2)%mt) then
					n=2
				else
					n=1
				end if
			end if
! Follow the appropriate RLOF scenario
			SELECT CASE (b%kw)
			CASE (14,24)
				CALL ce(s,b,zpars)
! Make first time step after CE not too big
				dtrl = 1.d-9
				IF (warn > 0) EXIT main
				tphysf0 = MIN(tphysf,s(n)%tphys+dtrl)
			CASE DEFAULT
				CALL transfer(n,os,s,b,dt,zpars)
				IF (warn > 0) EXIT main

! Did a supernova explosion occur?
				DO nn = n1, n2, dn
					IF ((s(nn)%kw >= 13).AND.(os(nn)%kw < 13)) THEN
						SELECT CASE (s(nn)%kw)
						CASE (15) ! Massless supernova remnant
						IF (os(nn)%kw < 7) THEN
							IF (kkick > 0) THEN
								ksnii = ksnii + 1.d0/DBLE(nkick)
							ELSE
								ksnii = ksnii + 1.d0
							END IF
						END IF
						s%rl = 1.d-10
						s%dma = 0.d0
						s%djma = 0.d0
						s%dmrl = 0.d0
						s%xlw = 0.d0
						s%xldm = 0.d0
						s%dmw = 0.d0
						s%djmw = 0.d0
						s%djmb = 0.d0
						s%jspin = 0.d0
						s%ospin = 0.d0
						s%xlcor = 0.d0

						IF (s(3-nn)%kw < 13) THEN
							s(3-nn)%dmw = (s(3-nn)%mt-os(3-nn)%mt)/dt
							s(3-nn)%djmw = djmass(os(3-nn)%r, &
								os(3-nn)%ospin,s(3-nn)%dmw)
							s(3-nn)%djmb = magbr(os(3-nn)%tphys, &
								os(3-nn)%kw,s(3-nn)%mass,os(3-nn)%mt, &
								os(3-nn)%r,os(3-nn)%mc,os(3-nn)%epoch, &
								os(3-nn)%ospin,zpars,ob%kw)*dt
							s(3-nn)%jspin = MAX(0.d0,os(3-nn)%jspin+ &
								s(3-nn)%djmw-s(3-nn)%djmb)
							s(3-nn)%ospin = j2ospin(s(3-nn)%mt, &
								s(3-nn)%r,s(3-nn)%mc,s(3-nn)%rc, &
								s(3-nn)%jspin)
							s(3-nn)%xlcor = corona(s(3-nn)%tphys, &
								s(3-nn)%kw,s(3-nn)%mass,s(3-nn)%mt, &
								s(3-nn)%mc,s(3-nn)%r,s(3-nn)%epoch, &
								s(3-nn)%ospin,zpars)
						END IF

						b%kw = 50
						b%cls = 0
						b%Porb = 0.d0
						b%a = 0.d0
						b%ecc = 0.d0
						b%jorb = 0.d0
						b%dagr = 0.d0
						b%degr = 0.d0
						b%damw = 0.d0
						b%demw = 0.d0

! Save pre-SN values
						IF (os(nn)%kw < 7) ob%cls = 6
						CALL reclog(os,ob)
						warn = 99
						EXIT main
						CASE(13) ! Neutron star remnant
						IF (os(nn)%kw < 7) THEN
							IF (kkick > 0) THEN
								ksnii = ksnii + 1.d0/DBLE(nkick)
							ELSE
								ksnii = ksnii + 1.d0
							END IF
						END IF
! Force fast rotational spin in abscence of tides
						IF (tflag == 0) THEN
							s(nn)%ospin = 2.0d+08
							s(nn)%jspin = o2jspin(s(nn)%mt,s(nn)%r, &
								s(nn)%mc,s(nn)%rc,s(nn)%ospin)
						END IF

! Assume SN is instantaneous
						s%djmb = 0.d0
						s%rl = 1.d-10
						s%dmw = 0.d0
						s%dma = 0.d0
						s%djmw = 0.d0
						s%djma = 0.d0
						s%dmrl = 0.d0
						s%xlw = 0.d0
						s%xldm = 0.d0

						b%cls = 0
						b%dagr = 0.d0
						b%degr = 0.d0
						b%damw = 0.d0
						b%demw = 0.d0

						ks = s
						kb = b
						ktsave = tsave

! Save pre-SN values
						IF (os(nn)%kw < 7) ob%cls = 6
						CALL reclog(os,ob)
						IF (warn == 11) EXIT main

!cccccccc We 're not interested in NSNS or BH DD's for now cccc
						IF (s(3-nn)%kw >= 13) THEN
							warn = 99
							EXIT main
						END IF
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

						IF ((nkick == 0).OR.(os(nn)%kw >= 10)) THEN
							nkm = 1
						ELSE
							nkm = nkick
							kkick = kkick + nkick
						END IF
						onkick = nkick
! Branch off the evolution with nkm random supernova kicks
						klp3: DO nk = 1, nkm
							IF (os(nn)%kw >= 10) nkick = 0
							CALL kick(os,ob,s,b)
							nkick = onkick
							IF (warn == 11) THEN
								IF (kkick > 0) THEN
									b%seq_num = b%seq_num &
										+ 1.d0/DBLE(nkick)
								ELSE
									b%seq_num = b%seq_num + 1.d0
								END IF
								EXIT main
							END IF
							IF (warn > 0) THEN
								s = ks
								b = kb
								IF (kkick > 0) THEN
									b%seq_num = b%seq_num &
										+ DBLE(nk)/DBLE(nkick)
								ELSE
									b%seq_num = b%seq_num + DBLE(nk)
								END IF
								warn = 0
								CYCLE klp3
							END IF
							IF (kkick > 0) THEN
								b%seq_num = b%seq_num - 1.d0/DBLE(nkick)
							ELSE
								b%seq_num = b%seq_num - 1.d0
							END IF
							CALL evolv2(s,b,tphysf,dtp,z,zpars)
							IF (warn == 11) EXIT klp3
							save_binary(jp-1)%tphys = -2.0
							s = ks
							b = kb
							IF (kkick > 0) THEN
								b%seq_num = b%seq_num &
									+ DBLE(nk)/DBLE(nkick)
							ELSE
								b%seq_num = b%seq_num + DBLE(nk)
							END IF
							tsave = ktsave
							warn = 0
						END DO klp3
						RETURN
						CASE(14)
						IF (os(nn)%kw < 7) THEN
							IF (kkick > 0) THEN
								ksnii = ksnii + 1.d0/DBLE(nkick)
							ELSE
								ksnii = ksnii + 1.d0
							END IF
						END IF
						IF (tflag == 0) THEN
							s(nn)%ospin = 2.0d+08
							s(nn)%jspin = o2jspin(s(nn)%mt,s(nn)%r, &
								s(nn)%mc,s(nn)%rc,s(nn)%ospin)
						END IF

! Assume SN is instantaneous
						s%djmb = 0.d0
						s%rl = 1.d-10
						s%dmw = 0.d0
						s%dma = 0.d0
						s%djmw = 0.d0
						s%djma = 0.d0
						s%dmrl = 0.d0
						s%xlw = 0.d0
						s%xldm = 0.d0

						b%cls = 0
						b%dagr = 0.d0
						b%degr = 0.d0
						b%damw = 0.d0
						b%demw = 0.d0

! Save pre-SN values
						IF (os(nn)%kw < 7) ob%cls = 6
						CALL reclog(os,ob)

						onkick = nkick
						nkick = 0
						CALL kick(os,ob,s,b)
						nkick = onkick
						IF (warn == 11) EXIT main
						IF (warn > 0) EXIT main

!cccccccc We 're not interested in BH binaries for now cccccccc
!                     warn = 99
!                     EXIT main
!cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc

						IF (b%kw == 0) THEN
							CALL evolv2(s,b,tphysf,dtp,z,zpars)
							IF (warn == 11) EXIT main
							save_binary(jp-1)%tphys = -2.0
						ELSE
							IF (dsave.OR.asave) THEN
								CALL record(s,b)
								CALL record0
							END IF
							CALL reclog(s,b)
							IF (warn == 11) EXIT main
							save_binary(jp)%tphys = -2.0
							save_binary(jp)%bkw = warn
							jp = jp + 1
							IF (jp >= nnlog) THEN
								warn = 11
								EXIT main
							END IF
						END IF
						RETURN
						END SELECT
					END IF
				END DO

! Set time step during RLOF

				IF (b%kw == 0) THEN
					dtrl = 1.d-3
				ELSE IF (b%kw /= ob%kw) THEN
					dm = mtrans(b%kw,s(n)%kw,s(n)%mt,s(n)%r, &
						s(n)%lum,s(n)%mc,s(n)%rl) ! Mass-transfer rate
					IF (warn > 0) EXIT main
					dm = ABS(dm)
					IF (dm > 0) THEN
						dtrl = MIN(2.d0*dt,pts4*s(n)%mt/dm)
					ELSE
						dtrl = MIN(2.d0*dt,1.d-3)
					END IF
				ELSE
					dtrl = MIN(2.d0*dt,pts4*s(n)%mt/(ABS(s(n)%dmrl)))
				END IF
				dtrl = 1.d-6*dtrl
				tphysf0 = MIN(tphysf,s(n)%tphys+dtrl)
			END SELECT
! Force synchronisation during RLOF
				s(n)%ospin = 2.d0*pi/b%Porb
				s(3-n)%ospin = s(n)%ospin
				s(n)%jspin = o2jspin(s(n)%mt,s(n)%rl,s(n)%mc, &
						s(n)%rc,s(n)%ospin)
				s(3-n)%jspin = o2jspin(s(3-n)%mt,s(3-n)%r,s(3-n)%mc, &
						s(3-n)%rc,s(3-n)%ospin)
			ELSE
			s%dmrl = 0.d0
			s%xldm = 0.d0
! Keep stars synchronised in the absence of tides
				IF (tflag == 0) THEN
					s(1)%ospin = 2.d0*pi/b%Porb
					s(2)%ospin = s(1)%ospin
					s(1)%jspin = o2jspin(s(1)%mt,s(1)%r,s(1)%mc, &
						s(1)%rc,s(1)%ospin)
					s(2)%jspin = o2jspin(s(2)%mt,s(2)%r,s(2)%mc, &
						s(2)%rc,s(2)%ospin)
				ELSE
! Adjust the spin of the star due to magnetic braking and due to
! changes in mass caused by a stellar wind
					DO n = 1, 2
						s(n)%jspin = MAX(0.d0, &
							os(n)%jspin+s(n)%djmw+s(n)%djma-s(n)%djmb)
						s(n)%ospin = j2ospin(s(n)%mt,s(n)%r,s(n)%mc, &
							s(n)%rc,s(n)%jspin)
					END DO
				END IF
			END IF

! Calculate the effective radii of the stars
 			IF ((b%kw /= 12).AND.(b%kw /= 22).AND.(b%kw /= 30)) THEN
				DO n = 1, 2
					reff(n) = MIN(s(n)%r,rlobe(b%a,s(n)%mt,s(3-n)%mt))
				END DO
				ELSE
				n = b%kw/10
				reff(n) = s(n)%rl
				reff(3-n) = MIN(s(3-n)%r,rlobe(b%a,s(3-n)%mt,s(n)%mt))
			END IF

! Set coronal X-ray luminosity
			DO n = 1, 2
				s(n)%xlcor = corona(s(n)%tphys,s(n)%kw,s(n)%mass,s(n)%mt, &
					s(n)%mc,reff(n),s(n)%epoch,s(n)%ospin,zpars)
			END DO

! Check for merger
			rp = b%a*(1.d0-b%ecc) ! Periastron distance
			IF (rp < reff(1)+reff(2)) THEN
				IF ((b%kw/10 == 1).OR.(b%kw/10 == 2)) THEN
					b%kw = 44
				ELSE
					b%kw = 42
				END IF
				EXIT MAIN
			END IF
			if(rp==reff(1)+reff(2)) THEN
				b%kw=30
				if(contactExit(s,b).eqv. .TRUE.) THEN
! 					write(*,*) "2"
					exit main
				end if
			END IF

! 			p=a2porb(b%a,s(1)%mt,s(2)%mt)*365.0
! 		write(*,*) s%mt,min(s(1)%mt,s(2)%mt)/max(s(1)%mt,s(2)%mt),p,b%kw



! Does the binary belong to a known class of systems?
!                call identify(s%kw,s%mt,s%mc,s%lum,reff,s%xlw,s%xldm, &
!                     s%xlcor,b%kw,b%Porb,b%cls,s,b)

! Keep track of evolutionary changes
!             q1 = (s(1)%mt/s(2)%mt) - 1.d0
!             IF ((s(1)%kw /= os(1)%kw).OR.(s(2)%kw /= os(2)%kw) &
!                  .OR.(b%kw /= ob%kw).OR.(b%cls /= ob%cls) &
!                  .OR.(q1*oq1 < 0.d0)) THEN
			CALL reclog(s,b)
			IF (warn == 11) EXIT main
!             END IF

! If necessary, save values for output
			IF ((dsave.AND.(s(1)%tphys >= tsave)).OR.asave) THEN
			CALL record(s,b)
			IF (dsave) tsave = tsave + dtp
			END IF

! Check if maximum evolutionary age is reached
			IF (s(1)%tphys >= tphysf) THEN
			CALL reclog(s,b)
			EXIT main
			END IF

! Check if mass is still in range covered by SSE (some degree of
! extrapolation below 0.1Msun is allowed)
			DO n = 1, 2
			!NOote we check at this agaisnt 0.095 not 0.1
			!as stars in wide star systems may loss enough mass from winds to drop out of valid range and stop evolution
			IF ((s(n)%mt < 0.095d0).OR.(s(n)%mt > 100.d0)) THEN
				warn = 5
				EXIT main
			END IF
			END DO

		END DO main

		IF ((warn > 0).AND.(warn /= 11)) CALL reclog(s,b)

	IF (dsave.OR.asave) CALL record0
		save_binary(jp)%tphys = -1.0
		save_binary(jp)%bkw = warn
		jp = jp + 1

		RETURN
		END SUBROUTINE evolv2

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	END MODULE evolveb
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
