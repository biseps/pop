!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE accrete
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
         CONTAINS
         PURE DOUBLE PRECISION FUNCTION mawind(a,ecc,m1,r1,dm1w,kw2,m2,r2, &
              zpars)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Function to evaluate the mean mass-accretion rate from a wind
! Bart Willems, 09/02/01
!               19/11/01 Added Eddington limit restriction

         USE precision
         USE consts
         USE params
         USE bfuncs
         IMPLICIT NONE

! m1 loses mass through a stellar wind
! m2 (stellar type kw2) accretes mass from the wind
! alfa is the (Bondi-Hoyle) mass-accretion parameter

         INTEGER, INTENT(IN) :: kw2
         DOUBLE PRECISION, INTENT(IN) :: m1,r1,dm1w,m2,r2
         DOUBLE PRECISION, INTENT(IN) :: a,ecc
         DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars

         DOUBLE PRECISION :: v2orb,v2wind,v2
         DOUBLE PRECISION :: dm2a,dmedd
         DOUBLE PRECISION,PARAMETER :: alfa = 1.5d0

! Orbital velocity for a circular orbit
         v2orb = gn*(m1+m2)/a
! Set the wind velocity proportional to the escape velocity
         v2wind = 2.d0*beta*(gn*m1/r1)
         v2 = v2orb/v2wind
! Check fast wind approximation
!         IF (v2 >= 1.d0) THEN
!            WRITE(*,*) 'WARNING: fast wind approximation invalid'
!            WRITE(*,*) '         execution resumes...'
!            WRITE(99,*) 'WARNING: fast wind approximation invalid'
!            WRITE(99,*) '         M1 = ',m1,'M2 = ',m2,'a = ',a
!            WRITE(99,*) '         execution resumes...'
!         END IF

! Mass accreted by the companion (Bondi-Hoyle accretion)
         dm2a = -(1.d0/sqrt(1.d0-ecc**2))*((gn*m2/v2wind)**2) &
              *(alfa/(2.d0*a**2))*(dm1w/(1.d0+v2)**1.5)

! Limit mass accretion rate to maximum 80%
         IF (dm2a > 0.8d0*abs(dm1w)) dm2a = 0.8d0*abs(dm1w)

! Check Eddington limit for accretion onto compact objects
         SELECT CASE (kw2)
         CASE(10,11,12)
            IF (ewd == 0) THEN
               dmedd = edding(r2,zpars)
               dm2a = MIN(dm2a,dmedd)
            END IF
         CASE(13)
            IF (ens == 0) THEN
               dmedd = edding(r2,zpars)
               dm2a = MIN(dm2a,dmedd)
            END IF
         CASE(14)
            IF (ebh == 0) THEN
               dmedd = edding(r2,zpars)
               dm2a = MIN(dm2a,dmedd)
            END IF
         END SELECT

         mawind = dm2a

         RETURN
         END FUNCTION mawind
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE accrete
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
