!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      MODULE bfuncs
      CONTAINS
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Collection of functions and procedures for the BiSEC code            c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION o2jspin(mt,r,mc,rc,ospin)

! Function to calculate the rotational angular momentum from the
! angular velocity
! Bart Willems, 20/11/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: mt,r,mc,rc,ospin
      DOUBLE PRECISION,PARAMETER :: k2 = 0.1d0, k3 = 0.21d0

      o2jspin = ospin*(k2*r*r*(mt-mc)+k3*rc*rc*mc)

      RETURN
      END FUNCTION o2jspin
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION j2ospin(mt,r,mc,rc,jspin)

! Function to calculate the rotational angular velocity from the
! angular momentum
! Bart Willems, 20/11/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: mt,r,mc,rc,jspin
      DOUBLE PRECISION,PARAMETER :: k2 = 0.1d0, k3 = 0.21d0

      j2ospin = jspin/(k2*r*r*(mt-mc)+k3*rc*rc*mc)

      RETURN
      END FUNCTION j2ospin
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      DOUBLE PRECISION FUNCTION magbr(tphys,kw,mass,mt,r,mc, &
           epoch,ospin,zpars,bkw)

! Function to evaluate the change of rotational angular momentum
! due to magnetic braking
! Bart Willems, 16/03/01

      USE precision
      USE params
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: kw,bkw
      DOUBLE PRECISION, INTENT(IN) :: tphys,mass,mt,r,mc,epoch,ospin
      DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars

      DOUBLE PRECISION :: tm,tn,aj
      DOUBLE PRECISION :: menv,menvf,magbr2
      DOUBLE PRECISION, DIMENSION(10) :: lums,GB
      DOUBLE PRECISION, DIMENSION(20) :: tscls


! EF commented out  Nov 6th
     !aj = tphys - epoch
     !CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
     !menv = menvf(kw,mt,mc,aj,tm,tscls(1))
     !IF ((mt > 0.35).AND.(kw < 10)) THEN
        !magbr = 5.83d-16*menv*(r*ospin)**3/mt
!! 	write(*,*) magbr
     !ELSE
        !magbr = 0.d0
     !END IF


! EF uncommented Nov 6th
       IF ((mt > 0.35).AND.(mt < 1.5).AND.(kw < 2)) THEN
          magbr2 = 1.5d-15*etamb*mt*(r**4)*(ospin**3)
       ELSE
          magbr2 = 0.d0
       END IF
 
! 	write(*,*) magbr,magbr2
! 	if(bkw==30) magbr=magbr/10.d0
      RETURN
      END FUNCTION magbr
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION djmass(r,ospin,dm)

! Function to evaluate the change of rotational angular momentum
! due to changes in mass (mass density is assumed to be uniform in
! a spherical shell at the star's surface)
! Bart Willems, 20/11/00
      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: r,ospin,dm

      djmass = (2.d0/3.d0)*dm*r*r*ospin

      RETURN
      END FUNCTION djmass
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION Porb2a(Porb,m1,m2)

! Function to evaluate the semi-major axis from Kepler's 3rd law
! Bart Willems, 30/11/00

! Porb is expressed in years
! m1 and m2 are expressed in solar masses
! a is expressed in solar radii

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: Porb,m1,m2
      DOUBLE PRECISION :: n

      n = 2.d0*pi/Porb
      Porb2a = (gn*(m1+m2)/n**2)**(1.d0/3.d0)

      RETURN
      END FUNCTION Porb2a
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION a2Porb(a,m1,m2)

! Function to evaluate the orbital period from Kepler's 3rd law
! Bart Willems, 30/11/00

! a is expressed in solar radii
! m1 and m2 are expressed in solar masses
! Porb is expressed in years

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,m1,m2

      a2Porb = 2.d0*pi*sqrt(a**3/(gn*(m1+m2)))

      RETURN
      END FUNCTION a2Porb
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION jorbit(a,ecc,m1,m2)

! Function to evaluate the orbital angular momentum
! Bart Willems, 10/11/00

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,ecc,m1,m2

      jorbit = m1*m2*sqrt(gn*a*(1.d0-ecc**2)/(m1+m2))

      RETURN
      END FUNCTION jorbit
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION aorbit(jorb,ecc,m1,m2)

! Function to evaluate the semi-major axis from the orbital angular
! momentum and the orbital eccentricity
! Bart Willems, 01/12/00

! m1 and m2 are expressed in solar masses
! jorb is expressed in Msun Rsun^2 yr^{-1}
! a is expressed in solar radii

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: jorb,ecc,m1,m2

      aorbit = ((m1+m2)/(gn*m1*m1*m2*m2))*jorb*jorb/(1.d0-ecc*ecc)

      RETURN
      END FUNCTION aorbit
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION dagrav(a,ecc,m1,m2)

! Function to evaluate the change of the semi-major axis due to
! gravitational radiation
! Bart Willems, 21/11/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,ecc,m1,m2
      DOUBLE PRECISION :: e2,fe

      e2 = ecc*ecc
      fe = (1.d0+e2*((73.d0/24.d0)+(37.d0/96.d0)*e2))/ &
           (1.d0-e2)**3.5d0
      dagrav = -1.7d-9*(m1*m2*(m1+m2)/a**3)*fe

      RETURN
      END FUNCTION dagrav
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL  DOUBLE PRECISION FUNCTION degrav(a,ecc,m1,m2)

! Function to evaluate the change of the orbital eccentricity due
! to gravitational radiation
! Bart Willems, 21/11/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,ecc,m1,m2
      DOUBLE PRECISION :: e2,fe

      e2 = ecc*ecc
      fe = (1.d0+(121.d0/304.d0)*e2)/(1.d0-e2)**2.5d0
      degrav = -2.6d-9*(m1*m2*(m1+m2)/a**4)*fe*ecc

      RETURN
      END FUNCTION degrav
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION rlobe(a,m1,m2)

! Function to evaluate the radius of the Roche lobe
! Bart Willems, 17/11/00

! m1 and m2 are expressed in solar masses
! a and rlobe are expressed in solar radii

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,m1,m2
      DOUBLE PRECISION :: q13,fq

      q13 = (m1/m2)**(1.d0/3.d0)
      fq = 0.49d0*q13*q13/(0.6d0*q13*q13+LOG(1.d0+q13))
      rlobe = a*fq

      RETURN
      END FUNCTION rlobe
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION daac(a,ecc,m1,dm1,m2,dm2)

! Function to evaluate the change of the semi-major axis due
! to mass accretion
! Bart Willems, 17/11/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: a,ecc,m1,dm1,m2,dm2
      DOUBLE PRECISION :: mb,e2,da

      mb = m1+m2
      e2 = ecc**2
      da = -(dm1/mb)-((2.d0-e2)/m2+(1.d0+e2)/mb)*(dm2/(1.d0-e2))
      daac = a*da

      RETURN
      END FUNCTION daac
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION deac(ecc,m1,m2,dm2)

! Function to evaluate the change of the orbital eccentricity due
! to mass accretion
! Bart Willems, 17/11/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: ecc,m1,m2,dm2
      DOUBLE PRECISION :: mb,de

      mb = m1+m2
      de = -dm2*(1.d0/mb+1.d0/(2.d0*m2))
      deac = ecc*de

      RETURN
      END FUNCTION deac
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION rmlob(m1,m2)

! Function to evaluate the Roche lobe index of m1
! Bart Willems, 18/12/00

      USE precision
      USE params
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: m1,m2
      DOUBLE PRECISION :: q,q13,b,ztL,ztLc

      q = m1/m2
      q13 = q**(1.d0/3.d0)
      b = - (2.d0/3.d0) + (q13*(1.d0+1.2d0*q13*(1.d0+q13)))/ &
           (3.d0*(1.d0+q13)*(0.6d0*q13*q13+LOG(1.d0+q13)))
      ztL = gamma*((2.d0*nu+1.d0)*q/(1.d0+q)+(b-2.d0)*q)
      ztLc = 2.d0*(q-1.d0)-(1.d0+q)*b
      rmlob = ztL + ztLc

      RETURN
      END FUNCTION rmlob
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION thk(kw,m,r,mc,lum)

! Function to evaluate the Helmholtz-Kelvin time scale in years
! Bart Willems, 15/12/00

      USE precision
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: kw
      DOUBLE PRECISION, INTENT(IN) :: m,r,mc,lum

      SELECT CASE (kw)
      CASE (0,1,7,10,11,12,13,14)
         thk = 3.13d7*m*m/(r*lum)
      CASE (2,3,4,5,6,8,9)
         thk = 3.13d7*m*(m-mc)/(r*lum)
      END SELECT

      RETURN
      END FUNCTION thk
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION tdyn(m,r)

! Function to evaluate the dynamical time scale in years
! Bart Willems, 15/12/00

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: m,r

      tdyn = 5.05d-5*sqrt(r**3/m)

      RETURN
      END FUNCTION tdyn
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION xrlumw(m,dm,r)

! Function to calculuate the accretion luminosity from a stellar wind
! Bart Willems, 25/04/01

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: m,dm,r

      xrlumw = gn*m*dm/r

      RETURN
      END FUNCTION xrlumw
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION xrlum(m1,m2,dm2,r2,a)

! Function to calculuate the accretion luminosity during Roche-lobe
! overflow
!
! Bart Willems, 25/04/01

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: m1,m2,dm2,r2,a
      DOUBLE PRECISION :: x1,x2,rp1,rp2

      x1 = lagl1(m1,m2,a)
      x2 = a - x1
      rp1 = rochp(m1,m2,a,x1,x2)
      x1 = a - r2
      x2 = r2
      rp2 = rochp(m1,m2,a,x1,x2)
      xrlum = dm2*(rp1-rp2)

      RETURN
      END FUNCTION xrlum
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      DOUBLE PRECISION FUNCTION corona(tphys,kw,mass,mt,mc,r,epoch, &
           ospin,zpars)

! Function to calculuate the coronal X-ray flux
!
! Bart Willems, 30/06/02

      USE precision
      USE consts
      USE params
      IMPLICIT NONE

      INTEGER, INTENT(IN) :: kw
      DOUBLE PRECISION, INTENT(IN) :: tphys,mass,mt,mc,r,epoch,ospin
      DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars

      DOUBLE PRECISION :: xlcor
      DOUBLE PRECISION :: tm,tn,aj
      DOUBLE PRECISION :: menv,menvf
      DOUBLE PRECISION, DIMENSION(10) :: lums,GB
      DOUBLE PRECISION, DIMENSION(20) :: tscls

      SELECT CASE (kw)
      CASE (0:3)
         aj = tphys - epoch
         CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
         menv = menvf(kw,mt,mc,aj,tm,tscls(1))
         If ((menv >= mcnv*mt).AND.(ospin > 0.d0)) THEN
            IF (xlcrm < 0.d0) THEN
               xlcor = 1.26d-3*r*r
            ELSE
               xlcor = xlcrm
            END IF
         ELSE
            xlcor = 0.d0
         END IF
      CASE DEFAULT
         xlcor = 0.d0
      END SELECT

      corona = xlcor

      RETURN
      END FUNCTION corona
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION lagl1(m1,m2,a)

! Function to determine the distance of the inner Lagrangian point L1
! from m1 in a binary with a circular orbit
! Bart Willems, 25/04/01

! m1 and m2 are the masses of the stars in solar units
! a is the semi-major axis of the orbit in solar radii

! lagl1 is expressed in solar radii

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: m1,m2,a
      DOUBLE PRECISION :: q,p,x

      IF (m1 < m2) THEN
         q = m1/m2
         IF (q < 0.4) THEN
            p = (q/(3.d0+3.d0*q))**(1.d0/3.d0)
            x = 1.d0-p+(p*p/3.d0)-(5.d0*p**4/12.d0)
         ELSE
            x = 1.d0/(1.0015d0+q**0.4056d0)
         END IF
         x = 1.d0 - x
      ELSE
         q = m2/m1
         IF (q < 0.4) THEN
            p = (q/(3.d0+3.d0*q))**(1.d0/3.d0)
            x = 1.d0-p+(p*p/3.d0)-(5.d0*p**4/12.d0)
         ELSE
            x = 1.d0/(1.0015d0+q**0.4056d0)
         END IF
      END IF

      lagl1 = a*x

      RETURN
      END FUNCTION lagl1
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      PURE DOUBLE PRECISION FUNCTION edding(r,zpars)

! Function to evaluate the Eddington limit for mass accretion in
! solar masses per year
! Bart Willems, 20/12/00

! eddington is expressed in solar masses per year

      USE precision
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: r
      DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars

      edding = 2.08d-3*r/(1.d0+zpars(11))

      RETURN
      END FUNCTION edding
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      ELEMENTAL DOUBLE PRECISION FUNCTION rochp(m1,m2,a,x1,x2)

! Calculates the Roche potential along the line joining the centres of
! the stars. The rotation of both stars is assumed to be synchronized
! with the orbital motion.
! Bart Willems, 25/04/01

! m1 and m2 are the masses of the stars in solar units
! a is the semi-major axis of the orbit in solar radii
! |x| is the distance of the point where the Roche potential has
!     to be evaluated to m1 (also in solar radii)
!     x = 0 is the position of m1 and x = a is the position of m2

! rochp is expressed in solar units

      USE precision
      USE consts
      IMPLICIT NONE
      DOUBLE PRECISION, INTENT(IN) :: m1,m2,a,x1,x2
      DOUBLE PRECISION :: rmu,rpgr,rpsp

      rmu = m2/(m1+m2)
      rpgr = -gn*((m1/ABS(x1))+(m2/ABS(x2)))
      rpsp = -gn*(m1+m2)*(x1-rmu*a)*(x1-rmu*a)/(2.d0*a**3)

      rochp = rpgr + rpsp

      RETURN
      END FUNCTION rochp
      
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
!Function no longer in use, see magnitudes.f90 for replacement
! R Farmer
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
     ELEMENTAL  DOUBLE PRECISION FUNCTION bolcor(kw,mt,mc,lum,log10Teff)

! Calculate the bolometric correction as a function of the effective
! temperature. Based on Pasi Nurmi's corrected routine!
      USE precision
      IMPLICIT NONE

! Pasi's original comments:
!      This conversion is based on P. Flower,1996,ApJ,469,355
!      His original coefficients are WRONG
!      I just made a fit to his tables calculated from his fit ...

      INTEGER, INTENT(IN) :: kw
      DOUBLE PRECISION, INTENT(IN) :: mt,mc,lum,log10Teff
      DOUBLE PRECISION :: mu,bc
      DOUBLE PRECISION, PARAMETER :: kap = -0.5d0
      DOUBLE PRECISION, PARAMETER :: lum0 = 7.0d+04

      SELECT CASE(kw)
      CASE(0:6)
         IF (log10Teff < 3.955d0) THEN
            bc=-0.158528d0-3.42431d0*(log10Teff-4.0d0) &
                 -28.6978d0*(log10Teff-4.0d0)**2 &
                 -112.758d0*(log10Teff-4.0d0)**3 &
                 -196.628d0*(log10Teff-4.0d0)**4
         ELSE
            bc=-0.273555d0-5.00162d0*(log10Teff-4.0d0) &
                 -1.28267d0*(log10Teff-4.0d0)**2
         END IF
         IF (bc < -10d0) bc=-10d0 !Teff ~2500 K
!         IF (log10Teff < 3.4) THEN
!            WRITE(*,*)
!            WRITE(*,*) 'BC out of range 1: ',kw,mt,log10Teff,bc
!            WRITE(*,*)
!         END IF
         IF (log10Teff > 4.8) THEN
            IF (kw > 1) THEN
               mu = ((mt-mc)/mt)*MIN(5.d0,MAX(1.2d0,(lum/lum0)**kap))
               IF (mu < 1.0d0) THEN
                  bc = -5.0d0
!               ELSE
!                  WRITE(*,*)
!                  WRITE(*,*) 'BC out of range 2: ',kw,mt,log10Teff,bc
!                  WRITE(*,*)
               END IF
!            ELSE
!               WRITE(11,*)
!               WRITE(11,*) 'BC out of range 3: ',kw,mt,log10Teff,bc
!               WRITE(11,*)
            END IF
         END IF
      CASE (7:12)
         bc = -5.0d0
      CASE DEFAULT
         bc = -10.d0
      END SELECT

      bolcor = bc

      RETURN
      END FUNCTION bolcor
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE update(kw,mass,mt,lum,r,mc,rc,tphys,epoch, &
           dm,dmdt,zpars,kw2)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Update stellar quantities and age/rejuvenate star after mass         c
! loss/accretion                                                       c
! Bart Willems, 09/06/02                                               c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE precision
      USE params
      IMPLICIT NONE

      INTEGER, INTENT(INOUT) :: kw
      INTEGER, INTENT(IN) :: kw2
      DOUBLE PRECISION, INTENT(IN) :: dm,dmdt
      DOUBLE PRECISION, DIMENSION(20), INTENT(IN) :: zpars
      DOUBLE PRECISION, INTENT(INOUT) :: mass,mt,lum,r,mc,rc
      DOUBLE PRECISION, INTENT(INOUT) :: tphys,epoch

      INTEGER :: it

      DOUBLE PRECISION :: tm,tn,tmold,tbgold,aj,ajold
      DOUBLE PRECISION :: m0,mc1,mtold,mu,muold
      DOUBLE PRECISION :: mche,mcmax,mlo,mhi,Lmlo,Lmhi,Lc,Lmass
      DOUBLE PRECISION, DIMENSION(10) :: lums,GB
      DOUBLE PRECISION, DIMENSION(20) :: tscls

      DOUBLE PRECISION :: mcheif,lmcgbf,tbgbf,lbgbf
      DOUBLE PRECISION :: mbagbf,mheif

      mtold = mt
      mt = mt + dm
! Update mass and epoch for MS and HG stars (cfr evolv1)
      IF ((kw <= 2).OR.(kw == 7)) THEN
         ajold = tphys - epoch
         CALL star(kw,mass,mtold,tm,tn,tscls,lums,GB,zpars)
         m0 = mass
         mc1 = mc
         mass = mt
         tmold = tm
         tbgold = tscls(1)
         CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
         IF (kw == 2) THEN
            IF (GB(9) < mc1) THEN
               mass = m0
            ELSE
               epoch = tm + (tscls(1) - tm)* &
                    (ajold-tmold)/(tbgold - tmold)
               epoch = tphys - epoch
            END IF
         ELSE
            muold = 1.d0
            mu = 1.d0
            IF (dm > 0) THEN
               IF ((mt < 0.35d0).OR.(mt > 1.25d0)) THEN
                  muold = mtold
                  mu = mt
               END IF
            END IF
            epoch = tphys - (muold/mu)*(ajold*tm/tmold)
         END IF
      END IF

! Accretion onto white dwarfs

      IF (dmdt > 0.d0) THEN
         SELECT CASE (kw2) ! Type of the donor star
         CASE(0:6)         ! H-rich donor
            SELECT CASE (kw)
            CASE(10)
               IF (dmdt >= wdsah) THEN ! WD becomes GB star
                  kw = 3
                  mcmax = mcheif(zpars(2),zpars(2),zpars(9))
                  IF (mc > mcmax) THEN
                     mass = mheif(mc,zpars(2),zpars(9))
                  ELSE
                     CALL star(kw,mt,mt,tm,tn,tscls,lums,GB,zpars)
                     Lc = lmcgbf(mc,GB)
                     mlo = mc
                     mhi = zpars(2)
                     Lmlo = lbgbf(mlo) - Lc
                     Lmhi = lbgbf(mhi) - Lc
                     it = 0
                     loop: DO
                        it = it + 1
                        IF (it > 100) THEN
                           WRITE(0,*) 'ERROR: bfuncs.f90 update 641'
                           WRITE(0,*) 'ERROR ACCRETING ONTO WD'
                           WRITE(0,*) '      WD type  = ',kw
                           WRITE(0,*) '      old WD mass  = ',mtold
                           WRITE(0,*) '      new WD mass  = ',mt
                           WRITE(0,*) 'END ERROR'
!                           CALL exit(0)
                           STOP
                        END IF
                        IF (ABS(mhi - mlo) < 0.005d0) EXIT
                        mass = (mlo + mhi)/2.d0
                        Lmass = lbgbf(mass) - Lc
                        IF (Lmass*Lmlo < 0.d0) THEN
                           mhi = mass
                           Lmhi = Lmass
                        ELSE IF (Lmass*Lmhi < 0.d0) THEN
                           mlo = mass
                           Lmlo = Lmass
                        ELSE
                           WRITE(0,*) 'ERROR: bfuncs.f90 update 660'
                           WRITE(0,*) 'ERROR ACCRETING ONTO WD'
                           WRITE(0,*) '      WD type  = ',kw
                           WRITE(0,*) '      old WD mass  = ',mtold
                           WRITE(0,*) '      new WD mass  = ',mt
                           WRITE(0,*) 'END ERROR'
!                           CALL exit(0)
                           STOP
                        END IF
                     END DO loop
                  END IF
                  aj = tbgbf(mass)
                  epoch = tphys - aj
                  IF (mass > zpars(3)) THEN
                           WRITE(0,*) 'ERROR: bfuncs.f90 update 674'
                           WRITE(0,*) 'ERROR ACCRETING ONTO WD'
                           WRITE(0,*) '      WD type  = ',kw
                           WRITE(0,*) '      old WD mass  = ',mtold
                           WRITE(0,*) '      new WD mass  = ',mt
                           WRITE(0,*) 'END ERROR'
!                     CALL exit(0)
                     		STOP
                  END IF
               END IF
            CASE(11,12)
               IF (dmdt >= wdsah) THEN ! WD become TPAGB star
                  IF (mc <= 0.8d0) THEN
                     mche = mc
                  ELSE IF (mc < 1.438d0) THEN
                     mche = (mc-0.448d0)/0.44d0
                  ELSE
                     mche = (mc+0.35d0)/0.773d0
                  END IF
                  mass = mbagbf(mche)
                  IF (mass <= 0.d0) THEN
                     mass = mt
                     aj = 0
                     epoch = tphys - aj
                  ELSE
                     kw = 6
                     CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
                     aj = tscls(13)
                     epoch = tphys - aj
                  END IF
               END IF
            END SELECT
         CASE(7:10)         ! He-rich donor
            SELECT CASE (kw)
            CASE(10)
               IF (mt > 0.7d0) THEN
                  kw = 15
                  mt = 1.0d-10
                  lum = 1.0d-10
                  r = 1.0d-10
                  mc = 1.0d-10
                  rc = 1.0d-10
                  aj = 0.d0
                  epoch = tphys - aj
                  RETURN
               END IF
            CASE(11)
               IF (mt > mass + 0.15d0) THEN
                  kw = 15
                  mt = 1.0d-10
                  lum = 1.0d-10
                  r = 1.0d-10
                  mc = 1.0d-10
                  rc = 1.0d-10
                  aj = 0.d0
                  epoch = tphys - aj
                  RETURN
                END IF
           END SELECT
         END SELECT
      END IF

      aj = tphys - epoch
      CALL star(kw,mass,mt,tm,tn,tscls,lums,GB,zpars)
      CALL hrdiag(mass,aj,mt,tm,tn,tscls,lums,GB, &
           zpars,r,lum,kw,mc,rc)
      epoch = tphys - aj

      RETURN
      END SUBROUTINE update

!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      END MODULE bfuncs
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
