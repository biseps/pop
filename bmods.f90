module precision

    ! kind values for single and double precision real variables

    ! bart willems, 16/10/00

    implicit none

    integer, parameter :: single = kind(0.0)
    integer, parameter :: double = kind(0.d0)

end module precision




module consts

    ! bart willems, 06/05/01

    use precision
    implicit none


    ! gn - newtonian gravitational constant 
    ! in rsun^3 msun^{-1} yr^{-2}
    double precision, parameter :: gn         = 6.67384d-11*5.7195044d18

    ! maximum number of time steps 
    ! used to evolve the binary
    integer, parameter          :: maxstep    = 250000

    double precision, parameter :: pi         = 3.141592654d0
    double precision, parameter :: twopi      = 6.28318531e0
    double precision, parameter :: rad        = 0.0174532925e0
    double precision, parameter :: deg2rad    = 0.0174532925
    double precision, parameter :: ln10       = 2.302585d0

    double precision, parameter :: teffsun    = 5777.d0
    double precision, parameter :: solartocgs = 6.993328363d-5
    double precision, parameter :: msun       = 1.98892d30
    double precision, parameter :: rsun       = 6.955d8

    double precision, parameter :: newtgrav   = 6.67384d-11
    double precision, parameter :: rjup       = 0.100519051
    double precision, parameter :: teffjup    = 160.d0

    ! visual brightness of the sun
    ! defined in http://adsabs.harvard.edu/abs/2010aj....140.1158t
    double precision, parameter :: vsun       = -26.76

    ! defined as vsun + 31.572 + bc_v,sun 
    ! derive bc from tables and use that
    double precision, parameter :: bolomagsun = 4.75

end module consts



module params

    ! bart willems, 27/05/01

    use precision
    implicit none
    save

    ! iseed is the seeding factor for the random number generator
    integer :: iseed = 4

    ! maximum evolution time
    double precision :: tphysmax


    ! mflag > 0 activates mass loss
    ! tflag > 0 activates tides
    ! cflag > 0 sets non-conservative mass transfer
    ! lflag > 0 activates dewi & tauris (2000) treatment of ce
    ! ewd, ens, ebh > 0 allows super-eddington accretion during rlof
    ! for white dwarfs, neutron stars, and black holes, respectively
    integer :: mflag, tflag, cflag, lflag
    integer :: ewd, ens, ebh

    ! neta is the reimers mass-loss coefficient
    ! bwind is the binary enhanced mass-loss parameter
    ! beta is the wind-velocity parameter for mass accretion
    ! wmag is an overall multiplication factor for all wind mass-loss rates
    double precision :: neta, bwind, beta, wmag

    ! gamma is the fraction of the mass lost from the system during rlof
    ! nu measures the specific angular momentum lost during rlof
    ! gnov is the fraction of the transferred mass accreted by a wd
    !    in a nova-type system
    ! 1-gwd, 1-gns, 1-gbh are the fractions of the transferred mass accreted by
    !    a wd not in a nova-type system, a neutron star (gns < 0 lets the
    !    program determine the mass accretion rate based on the maximum mass
    !    a ns is allowed to accrete), and a black hole
    ! wdsal and wdsah are the lower and upper limits for the mass transfer
    !    to induce steady burning on the surface of a wd
    ! dnsm is the maximum mass a ns is allowed to accrete during rlof
    double precision :: gamma, nu
    double precision :: gnov, gwd, gns, gbh
    double precision :: wdsal, wdsah, dnsm

    ! xlmin is the minimum xray luminosity (in lsun) a binary needs to emit
    ! to be classified as an x-ray binary
    ! xlcrm is an estimated upper limit for the coronal x-ray luminosity for
    ! stars up to the giant branch
    ! mcnv is the minimum mass of the convective envelope as a fraction of the
    ! total mass in order for the corona to be x-ray active
    double precision :: xlmin, xlcrm
    double precision :: mcnv

    ! wasp line: mdmv is minimum decrease in magnitude required to detect transit
    ! mmv1 is minimum magnitude detectable
    ! mmv2 is maximum magnitude detectable
    double precision :: mdmv, mmv1, mmv2

    ! ace is the efficiency parameter during common envelope evolution
    ! lambda is the binding energy parameter for a common envelope phase
    double precision :: ace, lambda

    !
    ! etamb is an ad-hoc parameter to change the mb strength
    double precision :: etamb

    ! nkick is the number of different kicks to compute after a sn explosion
    ! ksig is the kick velocity dispersion (for a maxwellian distribution)
    integer :: nkick
    double precision :: ksig

    ! pts1, pts2, pts3, pts4 determine the timesteps chosen to evolve the binary
    double precision :: pts1, pts2, pts3, pts4

    ! do we want transits recorded or not?
    !are we looking at single or bianry systems
    integer :: wanttransit, issingle, numtransits
    character(len=2) :: filterband

    double precision :: metallicity

end module params




module dtypes

    ! bart willems, 23/03/01

    use precision
    use params
    implicit none

    ! type :: spectraltype
    !     character(len=4) :: class, subclass, lumin
    ! end type spectraltype


    ! stellar parameters
    type :: stq                    

        ! stellar type
        integer :: kw               

        ! initial mass
        double precision :: mass   

        ! current mass
        double precision :: mt     

        ! luminosity
        double precision :: lum    

        ! radius
        double precision :: r      

        ! core mass
        double precision :: mc     

        ! core radius
        double precision :: rc     

        ! effective radius
        double precision :: reff   

        ! time
        double precision :: tphys  

        ! start of current evolutionary phas
        double precision :: epoch  

        ! rotational angular velocity
        double precision :: ospin  

        ! rotational angular momentum
        double precision :: jspin  

        ! djspin/dt due to magnetic braking
        double precision :: djmb   

        ! radius of the roche lobe
        double precision :: rl     

        ! wind mass loss rate
        double precision :: dmw    

        ! wind accretion rate
        double precision :: dma    

        ! wind accretion luminosity
        double precision :: xlw    

        ! djspin/dt due to wind mass loss
        double precision :: djmw   

        ! djspin/dt due to wind accretion
        double precision :: djma   

        ! mass trasfer rate due to rlof
        double precision :: dmrl   

        ! mass transfer accretion luminosity
        double precision :: xldm   

        ! coronal x-ray luminosity
        double precision :: xlcor  

        ! effective temperature
        double precision :: teff   

        ! limb darkening parameters
        double precision :: ldc1, ldc2 

        !mag of star in (filterband)
        double precision :: mag 	

        !magnitudes in each 
        ! type(spectraltype) :: spectype ! spectral type of star

        ! gravity darkening coefficent
        double precision :: grav 
    end type stq



    ! binary parameters
    type :: bnq                    
        ! binary type
        integer :: kw               

        ! binary class
        integer :: cls              

        ! orbital period
        double precision :: porb   

        ! semi-major axis
        double precision :: a      

        ! eccentricity
        double precision :: ecc    

        ! orbital angular momentum
        double precision :: jorb   

        ! gravitational radiation => da/dt
        double precision :: dagr   

        ! gravitational radiation => de/dt
        double precision :: degr   

        ! wind loss/accr => da/dt
        double precision, dimension(2) :: damw 

        ! wind loss/accr => de/dt
        double precision, dimension(2) :: demw 

        ! sequence number
        double precision :: seq_num 

        !mag of binary in (filterband)
        double precision :: mag    

        ! luminsoities of both stars
        double precision :: lum1
        double precision :: lum2 
    end type bnq

end module dtypes




module dshare

    ! bart willems, 18/09/01

    use precision
    use params
    implicit none

    save

    type :: summary
        integer          :: kw1     ! stellar type star1
        integer          :: kw2     ! stellar type star2
        integer          :: bkw     ! binary type
        integer          :: cls     ! binary classification

        double precision :: tphys   ! time

        double precision :: mass1   ! initial mass star1
        double precision :: mass2   ! initial mass star2

        double precision :: mt1     ! current mass star1
        double precision :: mt2     ! current mass star2

        double precision :: reff1   ! effective radius star1
        double precision :: reff2   ! effective radius star2

        double precision :: epoch1  ! epoch star1
        double precision :: epoch2  ! epoch star2
        double precision :: porb    ! orbital period
        double precision :: ecc     ! eccentricity
        double precision :: num     ! sequence number

        double precision :: lum1    ! luminosity star1
        double precision :: lum2    ! luminosity star2

        double precision :: teff1   ! effective temperature star1
        double precision :: teff2   ! effective temperature star2

        double precision :: xlw1    ! wind x-ray luminosity star1
        double precision :: xlw2    ! wind x-ray luminosity star2

        double precision :: dm1     ! mass-transfer rate star1
        double precision :: dm2     ! mass-transfer rate star2

        double precision :: xldm1   ! mass transfer luminosity star1
        double precision :: xldm2   ! mass transfer luminosity star2

        double precision :: dmin    ! min observable distance for given magnitude limit
        double precision :: dmax    ! max observable distance for given magnitude limit

        ! double precision :: bc1     ! bolometric correction star1
        ! double precision :: bc2     ! bolometric correction star2
        ! double precision :: mag	  ! abs magnitude of system
        ! double precision :: mag1    ! mag of star 1
        ! double precision :: mag2    ! mag of star 2
    end type summary


    integer, parameter :: nlog = 5000

    integer            :: warn, jp, nnlog
    integer            :: kkick, kdisr, kmerg
    double precision   :: ksnii
    double precision   :: tsave
    logical            :: dsave, asave

    ! type(summary),  allocatable,  dimension(:) :: blog
    type(summary),  allocatable,  dimension(:) :: save_binary


end module dshare
