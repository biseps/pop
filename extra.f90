! R Farmer
! 3/12/2010
! This module holds extra routines

MODULE extras

    CONTAINS




    PURE DOUBLE PRECISION FUNCTION calc_integral (values, n, low, high, bin_widths)
        ! Integral of binned data

        ! values(1:n)   -   contains tabulated function values
        ! low, high     -   integration boundaries

        ! Bart Willems, 21/10/01

        IMPLICIT NONE

        INTEGER,                        INTENT(IN) :: n, low, high
        DOUBLE PRECISION,               INTENT(IN) :: bin_widths
        DOUBLE PRECISION, DIMENSION(n), INTENT(IN) :: values
        INTEGER                                    :: i
        DOUBLE PRECISION                           :: total

        total  = 0.d0
        total  = SUM(values(low:high))

        calc_integral = total * bin_widths

        RETURN
    END FUNCTION calc_integral




    PURE DOUBLE PRECISION FUNCTION get_stellar_density (dist, ndist, dist_star, ifg)

        IMPLICIT NONE

        INTEGER :: idx_star
        INTEGER,                                INTENT(IN) :: ndist
        DOUBLE PRECISION, DIMENSION(1:ndist),   INTENT(IN) :: dist
        DOUBLE PRECISION, DIMENSION(1:ndist),   INTENT(IN) :: ifg
        DOUBLE PRECISION,                       INTENT(IN) :: dist_star


        CALL locate(search_array=dist, n=ndist, search_for=dist_star, found_idx=idx_star)

        if (idx_star .ge. ndist) then
            idx_star = ndist - 1
        end if

        get_stellar_density = ifg(idx_star)                             &
                              +  (dist_star        - dist(idx_star))    &
                              *  (ifg(idx_star+1)  - ifg(idx_star))     &
                              /  (dist(idx_star+1) - dist(idx_star))

    END FUNCTION get_stellar_density




    PURE DOUBLE PRECISION FUNCTION linpol (x, x0, x1, y0, y1)
        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: x, x0, x1, y0, y1

        linpol = y0 + (((x-x0)*y1) - ((x-x0)*y0)) / (x1-x0)

    END FUNCTION linpol




    PURE SUBROUTINE locate (search_array, n, search_for, found_idx)
        IMPLICIT NONE
        INTEGER,            INTENT(IN)  :: n
        DOUBLE PRECISION,   INTENT(IN)  :: search_for, search_array(n)
        INTEGER,            INTENT(OUT) :: found_idx
        INTEGER                            lower, middle, upper

        lower = 0
        upper = n + 1

10      if ((upper - lower) .gt. 1) then
             middle = (upper + lower) / 2

            if ((search_array(n) .ge. search_array(1)) .eqv. &
                (search_for      .ge. search_array(middle))) then
                lower = middle
            else
                upper = middle
            endif

            goto 10
        endif
      
      
        if (search_for .eq. search_array(1)) then
            found_idx = 1

        else if (search_for .eq. search_array(n)) then
            found_idx = n - 1

        else
            found_idx = lower
        endif

        return
    END SUBROUTINE locate




    ELEMENTAL DOUBLE PRECISION FUNCTION get_imf (mass, imf_type)
        ! initial mass function
        IMPLICIT NONE
        DOUBLE PRECISION,INTENT(IN) :: mass
        CHARACTER(len=1),INTENT(IN) :: imf_type

        select case (imf_type)

            case('k')
                get_imf = LOG(10.d0) * mass * imfKroupa(mass)

            case('l')
                get_imf = imfLogNormal(mass)

            case('e')
                get_imf = LOG(10.d0) * mass * imfExp(mass)     	

        end select

    END FUNCTION get_imf




    ELEMENTAL DOUBLE PRECISION FUNCTION imfKroupa (m)
        ! Initial Mass Function
        ! Bart Willems, 06/05/01

        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: m
        DOUBLE PRECISION :: f

        if (m < 0.1d0) then
            f = 0.d0

        else if (m < 0.5d0) then
            f = 0.29056d0 / m**1.3

        else if (m < 1.0d0) then
            f = 0.15571d0 / m**2.2

        else
            f = 0.15571d0 / m**2.7
        end if

        imfKroupa = f

        RETURN
    END FUNCTION imfKroupa




    ELEMENTAL DOUBLE PRECISION FUNCTION imfLogNormal (m)
        ! Log normal IMF from Chabrier 2001
        ! Robert Farmer 08/02/2012

        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: m

        DOUBLE PRECISION,PARAMETER :: a=0.141
        DOUBLE PRECISION,PARAMETER :: m0=0.1
        DOUBLE PRECISION,PARAMETER :: sigma=0.627

        imfLogNormal = a*exp(-((log10(m)-log10(m0))**2)/(2.d0*sigma)**2)

        RETURN
    END FUNCTION imfLogNormal




    ELEMENTAL DOUBLE PRECISION FUNCTION imfExp (m)
        ! exponentianl IMF from Chabrier 2001
        ! Robert Farmer 08/02/2012

        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: m

        DOUBLE PRECISION,PARAMETER :: a     = 3.0
        DOUBLE PRECISION,PARAMETER :: m0    = 716.4
        DOUBLE PRECISION,PARAMETER :: alpha = 3.3
        DOUBLE PRECISION,PARAMETER :: beta  = 0.25

        imfExp = a * m**(-alpha) * exp(-(m0/m)**beta)

        RETURN
    END FUNCTION imfExp
      
      
      
      
    ELEMENTAL DOUBLE PRECISION FUNCTION IMRD (q, s)
        ! Initial Mass Ratio Distribution
        ! Bart Willems, 06/05/01

        ! q = binary mass ratio (mass2 / mass1)
        ! s = a free parameter

        ! From R. Farmer thesis 2014: (page 26/27)
        ! "I consider s = 0 (a flat distribution) as the canonical value for
        ! this work. There is little agreement in the literature as to the
        ! precise form of the IMRD, though it appears to vary as a function of
        ! the primary mass. Janson et al. (2012) finds for M-dwarfs a best fit
        ! is a flat distribution, Duquennoy & Mayor (1991) favours a Gaussian
        ! distribution that peaks at q = 0.25 for G dwarfs while (Raghavan et
        ! al., 2010) suggest a distribution that favours equal sized objects
        ! (s → 1). Finally, Goodwin (2013) argues that this can all be
        ! explained if instead of selecting stars via their primary mass in
        ! the IMF, instead the system mass is used. Combined with a flat mass
        ! ratio distribution this can then give rise to an apparent mass
        ! dependant IMRD."

        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: q,s
        DOUBLE PRECISION :: f

        ! IF (q <= 0.d0) THEN
            ! WRITE(*,*)
            ! WRITE(*,*) 'FATAL ERROR IN FUNCTION IMRD: q <= 0!!!'
            ! WRITE(*,*)
            ! STOP 'Abnormal program termination!!!'
        ! END IF

        ! IF (s == -1.d0) THEN
            ! WRITE(*,*)
            ! WRITE(*,*) 'FATAL ERROR IN FUNCTION IMRD: s = -1!!!'
            ! WRITE(*,*)
            ! STOP 'Abnormal program termination!!!'
        ! END IF

        IF (q > 1.d0) THEN
            f = 0.d0
        ELSE
            f = (s+1.d0) * (q**s)
        END IF

        IMRD = f
        ! write(*,*) IMRD

        RETURN
    END FUNCTION IMRD
      
      
      
      
    ELEMENTAL PURE DOUBLE PRECISION FUNCTION IOSD (a)
        ! Initial Orbital Separation Distribution
        ! Bart Willems, 06/05/01
        !
        ! 'a' is assumed to be expressed in solar radii

        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: a
        DOUBLE PRECISION :: f

        IF ((a >= 3.d0).AND.(a <= 1.d6)) THEN
            f = 0.078636d0/a
        ELSE
            f = 0.d0
        END IF

        IOSD = f

        RETURN
    END FUNCTION IOSD
      



    ELEMENTAL DOUBLE PRECISION FUNCTION Porb2a (Porb,m1,m2)
        ! evaluate semi-major axis from Kepler's 3rd law
        ! Bart Willems, 30/11/00
        !
        ! 'Porb' is expressed in years
        ! 'm1' and 'm2' are expressed in solar masses
        ! 'a' is expressed in solar radii

        IMPLICIT NONE
        DOUBLE PRECISION, INTENT(IN) :: Porb,m1,m2
        DOUBLE PRECISION :: n
        DOUBLE PRECISION, PARAMETER :: pi = 3.141592654d0
        DOUBLE PRECISION, PARAMETER :: gn = 6.67259d-11*5.86676d18

        n = 2.d0*pi/Porb
        Porb2a = (gn*(m1+m2)/n**2)**(1.d0/3.d0)

        RETURN
    END FUNCTION Porb2a
      
      
END MODULE extras




module error
	contains

	subroutine seterror()
        ! write to stderr a error code, can then pipe the stderr to a file
        ! can then check for existence of stderr file to see if code exited 
        ! prematurely
        write(0,*) "1"

        return
	end subroutine seterror
	
	
	
	
	subroutine check_alloc_error (status_code, error_msg)
        ! handle any errors 
        ! from 'allocate' statements

        implicit none
        integer,            intent(in)  :: status_code
		character(len=*),   intent(in)  :: error_msg
	
        if (status_code > 0) then
            ! something went wrong!
            write(0, *)
            write(0, *) error_msg
            write(0, *)
            call seterror()
            stop 'Abnormal program termination!!!'
        end if

        return
	end subroutine check_alloc_error


end module error
      
