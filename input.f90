MODULE input
    ! Module to read 'pop.in' file

	IMPLICIT NONE

    ! Population Input variables
	INTEGER           :: class, total_time_steps, nlon, nlat
	CHARACTER(len=10) :: model
	CHARACTER(len=5)  :: sf_method, disk_age
	DOUBLE PRECISION  :: lon1, lon2, lat1, lat2, imr
	DOUBLE PRECISION  :: R0, z0, hR, hz
	DOUBLE PRECISION  :: t_start, t_end, age_galaxy, T0
	DOUBLE PRECISION  :: minMag, maxMag, bFrac
	CHARACTER(len=1)  :: extType, imfType
	
    ! Extinction color correction Av=Ar/colourCorrect
	DOUBLE PRECISION  :: colourCorrect

	CONTAINS
	
	
	SUBROUTINE read_pop_params(param_file)

        ! read population parameter file

		USE params
		USE consts
		USE polygon
		USE error

		IMPLICIT NONE

		CHARACTER(len=100), INTENT(IN) :: param_file
		INTEGER :: i, k, ioerr
        INTEGER :: id

		OPEN(1, FILE=param_file(1:LEN_TRIM(param_file)), STATUS='OLD', ACTION='READ', IOSTAT=ioerr)

		IF (ioerr > 0) THEN
			WRITE(0, *) 'ERROR (input.f90) (read_pop_params)'
			WRITE(0, *) 'ERROR: Input parameter file (pop.in?) does not exist!'
			WRITE(0, *) 'END ERROR'
			call setError()
			STOP 
		END IF

		READ(1, *)
		READ(1, *) class
		READ(1, *)
		READ(1, *) model
		READ(1, *)
		READ(1, *) sf_method, disk_age, T0
		READ(1, *)
		READ(1, *) t_start, t_end, total_time_steps
		READ(1, *)
		READ(1, *) age_galaxy
		READ(1, *)
		READ(1, *) R0, z0
		READ(1, *)
		READ(1, *) hR, hz
		READ(1, *)
		READ(1, *) lon1, lon2, nlon
		READ(1, *) lat1, lat2, nlat
		READ(1, *)
		READ(1, *) wantTransit, isSingle, numTransits
		READ(1, *)
		READ(1, *) minMag, maxMag

		! Read the 4 points of the kepler ccd
		READ(1, *) (px(k),  k=0, 3)
		READ(1, *) (py(k),  k=0, 3)

		! Extinction type(h, k, d),  IMF type,  imr, colourCorrect
		READ(1, *) extType, imfType, imr, colourCorrect, bFrac

		CLOSE(1)

        ! Switch to radians
		px = px * pi/180.d0
		py = py * pi/180.d0


        ! Convert to lower-case
        k = LEN_TRIM(sf_method)
        DO i = 1, k
            id = IACHAR(sf_method(i:i))
            IF ((id >= 65).AND.(id <= 90)) sf_method(i:i) = ACHAR(id+32)
        END DO


        ! Convert to lower-case
        ! 'disk_age' is the age of the galactic disk (young or old)
        k = LEN_TRIM(disk_age)
        DO i = 1, k
            id = IACHAR(disk_age(i:i))
            IF ((id >= 65).AND.(id <= 90)) disk_age(i:i) = ACHAR(id+32)
        END DO


	END SUBROUTINE read_pop_params


END MODULE input
