!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      PROGRAM BiSEC
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Bi(nary) S(tellar) E(volution) C(ode)                                c
!                                                                      c
! Author: Bart Willems, 18/09/01                                       c
!         Department of Physics and Astronomy                          c
!         The Open University                                          c
!         Walton Hall                                                  c
!         Milton Keynes                                                c
!         MK7 6AA                                                      c
!         United Kingdom                                               c
!                                                                      c
!         E-mail: B.Willems@open.ac.uk                                 c
!                                                                      c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! The BiSEC program uses the SSE (Single Star Evolution) package       c
! written by Hurley et al. (2000)                                      c
!                                                                      c
! Mass range:        0.1 -> 100 Msun                                   c
! Metallicity Range: 0.0001 -> 0.03 (0.02 is solar)                    c
!                                                                      c
! An integer is assigned to each phase of stellar evolution:           c
!                                                                      c
!        0 - deeply or fully convective low mass Main Sequence star    c
!        1 - Main Sequence star                                        c
!        2 - Hertzsprung Gap                                           c
!        3 - First Giant Branch                                        c
!        4 - Core Helium Burning                                       c
!        5 - First Asymptotic Giant Branch                             c
!        6 - Second Asymptotic Giant Branch                            c
!        7 - Main Sequence Naked Helium star                           c
!        8 - Hertzsprung Gap Naked Helium star                         c
!        9 - Giant Branch Naked Helium star                            c
!       10 - Helium White Dwarf                                        c
!       11 - Carbon/Oxygen White Dwarf                                 c
!       12 - Oxygen/Neon White Dwarf                                   c
!       13 - Neutron Star                                              c
!       14 - Black Hole                                                c
!       15 - Massless Supernova                                        c
!                                                                      c
! More details can be found in the paper                               c
!                                                                      c
!     "Comprehensive analytic formulae for stellar evolution as a      c
!      function of mass and metallicity"                               c
!      Hurley J.R., Pols O.R., Tout C.A., 2000, MNRAS, 315, 543        c
!                                                                      c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! The following integers are assigned to each phase of binary          c
! evolution:                                                           c
!                                                                      c
!        0 - Detached binary                                           c
!       10 - Roche-lobe overflow from star 1                           c
!          11 - Dynamical mass transfer from star 1                    c
!          12 - Thermal mass transfer from star 1                      c
!          13 - Nuclear mass transfer from star 1                      c
!          14 - Common envelope due to star 1 (Dewi & Tauris 2000)     c
!          15 - Common envelope due to star 1                          c
!                  (Constant binding-energy parameter)                 c
!       20 - Roche-lobe overflow from star 2                           c
!          21 - Dynamical mass transfer from star 2                    c
!          22 - Thermal mass transfer from star 2                      c
!          23 - Nuclear mass transfer from star 2                      c
!          24 - Common envelope due to star 2 (Dewi & Tauris 2000)     c
!          25 - Common envelope due to star 2                          c
!                  (Constant binding-energy parameter)                 c
!       30 - Contact binary                                            c
!       40 - Merger                                                    c
!          41 - CE merger                                              c
!          42 - Collision                                              c
!          43 - Kick merger                                            c
!          44 - RLOF merger                                            c
!       50 - Single star remnant                                       c
!       55 - Disrupted by supernova explosion                          c
!                                                                      c
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE precision
      USE params
      USE dtypes
      USE dshare
      USE evolveb
      USE output
      USE ran_state, ONLY : ran_seed

      IMPLICIT NONE

      INTEGER :: ioerr,aerr

      DOUBLE PRECISION :: z
      DOUBLE PRECISION :: tphysf,dtp
      DOUBLE PRECISION, DIMENSION(20) :: zpars

      TYPE (stq), DIMENSION(2) :: s  ! Stellar parameters
      TYPE (bnq) :: b                ! Binary parameters
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Input from the file 'BiSEC.in':
!
! masses of the two stars are in solar units
! z is metallicity in the range 0.0001 -> 0.03 where 0.02 is Population I
!      (we assume both stars to have the same z)
! tphysf is the maximum evolution time in Myr
!
! mflag > 0 activates mass loss
! neta is the Reimers mass-loss coefficent
! bwind is the binary enhanced mass-loss parameter
! beta is the wind-velocity parameter
! wmag is an overall multiplication factor for all wind mass-loss rates
!
! Porb is the orbital period in days
! ecc is the orbital eccentricity
!
! cflag = 0 sets fully conservative mass transfer
!    cflag > 0 sets non-conservative mass transfer
! gnov is the fraction of the transferred mass accreted by a WD
!      in a nova-type system
! 1-gwd, 1-gns, 1-gbh are the fractions of the transferred mass accreted by
! a WD not in a nova-type system, a neutron star (gns < 0 lets the
! program determine the mass accretion rate based on the maximum mass a
! NS is allowed to accrete), and a black hole
!
! ewd, ens, ebh > 0 allows super-Eddington accretion during RLOF
! for white dwarfs, neutron stars, and black holes, respectively
!
! lflag > 0 activates Dewi & Tauris (2000) treatment of CE
! ace is the efficiency parameter for the transfer of orbital energy
!    to the envelope during a common envelope phase
! lambda is the binding energy parameter for a common envelope phase
!
! etamb is an ad-hoc parameter to change the MB strength
!
! wdsal and wdsah are the lower and upper limits for the mass transfer
!    to induce steady burning on the surface of a WD
! dnsm is the maximum mass a NS is allowed to accrete during RLOF
!
! xlmin is the minimum xray luminosity (in Lsun) a binary needs to emit
! to be classified as an X-ray binary
!
! xlcrm is an estimated upper limit for the coronal X-ray luminosity for
! stars up to the giant branch (xlcrm < 0 flags the determination of the
! upper limit as a function of the stellar radius)
!
! mcnv is the minimum mass of the convective envelope as a fraction of the
! total mass in order for the corona to be X-ray active
!
! nkick is the number of different kicks to compute after a SN explosion
! ksig is the kick velocity dispersion (for a Maxwellian distribution)
!
! tflag > 0 activates tides
!
! ospin is the rotational angular velocity of the stars in yr^{-1}. If
! ospin is less than or equal to zero at time zero then evolv2 will set
! an appropriate ZAMS spin. If ospin is greater than zero then it will
! start with that spin regardless of the time. If you want to start at
! time zero with negligible spin then I suggest using a negligible value
! (but greater than 0.001).
!
! The following parameters determine the timesteps chosen in each
! evolution phase as decimal fractions of the time taken in that phase:
!   pts1 - MS
!   pts2 - GB, CHeB, AGB, HeGB
!   pts3 - HG, HeMS
!
! During RLOF the time step is chosen so that the relative change in mass
! is smaller than pts4
!
! When a negative mass is entered for the first star, the system is assumed
! to be evolved. The input parameters are then
!   current time
!   initial mass, current mass, type, epoch of star1
!   initial mass, current mass, type, epoch of star2
!
      OPEN(1,FILE='BiSEC.in',STATUS='OLD',ACTION='READ',IOSTAT=ioerr)
      IF (ioerr > 0) THEN
         WRITE(*,*)
         WRITE(*,*) 'ERROR: File ''BiSEC.in'' does not exist!'
         WRITE(*,*)
         STOP 'Abnormal program termination!!!'
      END IF

      READ(1,*) s(1)%mass,s(2)%mass,z,tphysf
      READ(1,*) mflag,neta,bwind,beta,wmag
      READ(1,*) b%Porb,b%ecc
      READ(1,*) cflag,gnov,gwd,gns,gbh
      READ(1,*) ewd,ens,ebh
      READ(1,*) wdsal,wdsah,dnsm
      READ(1,*) lflag,ace,lambda
      READ(1,*) etamb
      READ(1,*) xlmin,xlcrm,mcnv
! WASP line: mdmv is minimum decrease in magnitude required to detect transit
!            mmv1 is minimum magnitude detectable
!            mmv2 is maximum magnitude detectable
      READ(1,*) mdmv,mmv1,mmv2
      READ(1,*) nkick,ksig
      READ(1,*) pts1,pts2,pts3,pts4

! No tidal action for now
      tflag = 0

! Let the program set the spin of the stars
      s%ospin = 0.d0

! Set parameters which depend on the metallicity
      CALL zcnsts(z,zpars)

      IF (s(1)%mass > 0.d0) THEN ! Initialize stars to ZAMS
         s%mt = s%mass
         s%kw = 1
         s%tphys = 0.d0
         s%epoch = 0.d0
      ELSE                      ! Read parameters of evolved stars
         READ(1,*) s(1)%tphys
         s(2)%tphys = s(1)%tphys
         READ(1,*) s(1)%mass,s(1)%mt,s(1)%kw,s(1)%epoch
         READ(1,*) s(2)%mass,s(2)%mt,s(2)%kw,s(2)%epoch
      END IF

      CLOSE(1)

! Initialise sequence number

      b%number = 0.d0

! Set the data-save parameter. If dtp is zero then the parameters of the
! star will be stored in the bout and oout arrays at each timestep otherwise
! they will be stored at intervals of dtp. Setting dtp equal to tphysf will
! store data only at the start and end while a value of dtp greater than
! tphysf will mean that no data is stored.

      dtp = 0.d0
!      dtp = 0.1d0
!      dtp = 2.d0*tphysf

! Allocate memory to arrays
      nnlog = nlog*(nkick+1)

      ALLOCATE(blog(nnlog+1),STAT=aerr)
      IF (aerr > 0) THEN
         WRITE(*,*)
         WRITE(*,*) 'ERROR: Memory allocation failed for blog!'
         WRITE(*,*)
         STOP 'Abnormal program termination!!!'
      END IF

! Set up variables which control the output
      jp = 1
      tsave = s(1)%tphys
      dsave = .true.            ! Save data at intervals dtp
      asave = .false.           ! Save data at each time step
      IF (dtp <= 0.0) THEN
         asave = .true.
         dsave = .false.        ! Takes care of dtp < 0 later on
         tsave = tphysf
      ELSE IF (dtp > tphysf) THEN
         dsave = .false.
         tsave = tphysf
      END IF

      blog%kw1 = 0
      blog%kw2 = 0
      blog%bkw = 0
      blog%cls = 0
      blog%tphys = 0.0
      blog%mt1 = 0.0
      blog%mt2 = 0.0
      blog%Porb = 0.0
      blog%ecc = 0.0

! Open output files
      OPEN(2,FILE='binary1.dat',STATUS='UNKNOWN',ACTION='WRITE')
      WRITE(2,'(A)') '#  Tev(Myr) type      Mo         Mt     ' &
           //'log10(L)  log10(R) log10(Teff)  log10(Rl)   epoch  ' &
           //'  dMRL'

      OPEN(3,FILE='binary2.dat',STATUS='UNKNOWN',ACTION='WRITE')
      WRITE(3,'(A)') '#  Tev(Myr) type      Mo         Mt     ' &
           //'log10(L)  log10(R) log10(Teff)  log10(Rl)   epoch  ' &
           //'  dMRL'

      OPEN(4,FILE='orbit.dat',STATUS='UNKNOWN',ACTION='WRITE')
      WRITE(4,'(A)') '#  Tev(Myr)   type      Mb         Porb    ' &
           //'    a      ecc '

! Initialize random number generator
      CALL ran_seed(sequence=iseed)

! Initialize counters
      kkick = 0   ! Number of supernova kicks calculated
      kdisr = 0   ! Number of systems disrupted by SN kick
      kmerg = 0   ! Number of systems merged due to SN kick

      ksnii = 0.0 ! Number of type II supernova explosion

! Express the orbital period in years
      b%Porb = b%Porb/365.d0

      CALL evolv2(s,b,tphysf,dtp,z,zpars)

! Write log to the default output device
      WRITE(*,*)
      CALL wrlog('*')
      WRITE(*,*)

! Close output files
      CLOSE(2)
      CLOSE(3)
      CLOSE(4)

! Deallocate array memeory
      DEALLOCATE(blog)

      STOP 'End of program'
      END PROGRAM
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
