!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Perform some statistics on the output from channels.f
! Bart Willems, 27/08/03
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	MODULE distributions
		USE error
	CONTAINS
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	PURE SUBROUTINE SFR(t,n,sfm,age,T0,res,imfType)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Star formation rate in Myr^{-1}
! Bart Willems, 06/05/01
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	IMPLICIT NONE
	DOUBLE PRECISION, DIMENSION(1:n),INTENT(IN) :: t
	INTEGER,INTENT(IN) :: n
	INTEGER :: i
	DOUBLE PRECISION,INTENT(IN) :: T0
	DOUBLE PRECISION,DIMENSION(1:n),INTENT(OUT) :: res
	CHARACTER (len=5), INTENT(IN) :: sfm,age
	CHARACTER(len=1),INTENT(IN) :: imfType
	DOUBLE PRECISION :: s

	SELECT CASE(imfType)
	case('k')
      	s=9.2d0
      case('l')
      	s=13.786d0
      case('e')
      	s=54.69d0     	
      end select


	SELECT CASE(sfm(1:LEN_TRIM(sfm)))
	CASE('ctu')
		SELECT CASE(age(1:LEN_TRIM(age)))
		CASE('young')
			DO i=1,n-1
				IF((t(i)+t(i+1))/2.d0.lt.T0)THEN
					res(i)=0.d0
				ELSE
					res(i)=s
				END IF
			END DO
		CASE('old')
			DO i=1,n-1
				IF((t(i)+t(i+1))/2.d0.gt.T0)THEN
					res(i)=0.d0
				ELSE
					res(i)=s
				END IF
			END DO
		END SELECT
	CASE('burst')
			DO i=1,n-1
				IF(t(i).gt.0.d0) then
					res(i)=0.d0
				ELSE
					res(i)=1.d0/(t(i+1)-t(i))
				END IF
			END DO
	END SELECT		

	res=res*1.d6

	RETURN
	END SUBROUTINE SFR


!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	DOUBLE PRECISION FUNCTION galdisc(d,l,b,R0,z0,hR,hz,dl,db)
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Calculate the product of the Galactic disc double exponential density
! distribution and the Jacobian (R,z) -> (d,l,b) for a given distance d
! (in pc), Galactic longitude l (in radians), and  Galactic latitude b
! (in radians).
!
! R0 is the distance of the sun to the Galactic center in pc
! z0 is the height of the sun above the Galactic plane in pc
! hR is the scale length of the Galactic disc in pc
! hz is the scale height of the Galactic disc in pc
!
! Bart Willems, 30/07/03
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
	USE polygon
	IMPLICIT NONE
	DOUBLE PRECISION, INTENT(IN) :: d,l,b,dl,db
	DOUBLE PRECISION, INTENT(IN) :: R0,z0,hR,hz
	DOUBLE PRECISION,PARAMETER :: pi=3.141592654d0
	DOUBLE PRECISION :: R,z,weight
	DOUBLE PRECISION :: f,f0,fR,fz,tmp
	DOUBLE PRECISION,DIMENSION(0:3) :: x,y

	R=0.0

	x(0)=l+dl/2.0
	x(1)=l+dl/2.d0
	x(2)=l-dl/2.d0
	x(3)=l-dl/2.d0

	y(0)=b-db/2.d0
	y(1)=b+db/2.d0
	y(2)=b+db/2.d0
	y(3)=b-db/2.d0

	weight=poly(x,y)

	tmp=(d*d*COS(b)*COS(b)-2.d0*d*R0*COS(b)*COS(l)+R0*R0)
	if(tmp .gt. 0.0) THEN
		R=sqrt(tmp)
	END IF

	z = d*SIN(b)+z0

	f0 = 1.d0/(4.d0*pi*hz*hR*hR)
	fR = EXP(-R/hR)
	fz = EXP(-ABS(z)/hz)
	f = f0*fR*fz*ABS(d*d*COS(b))

	galdisc = f*weight

	RETURN
	END FUNCTION galdisc

	END MODULE distributions