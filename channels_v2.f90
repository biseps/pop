!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      PROGRAM channels_v2
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! Group formation channels found by BiSEPS
! Bart Willems, 26/08/02
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      USE fmods

      IMPLICIT NONE

      INTEGER :: class,class2,class3
      INTEGER :: kw1,kw2,bkw,kw
      INTEGER :: kkw1,kkw2
      INTEGER :: kw12,kw22,bkw2
      INTEGER :: kw13,kw23,bkw3
      INTEGER*8 :: nmbr,count,nrank
      INTEGER*8 :: i,j,k,n,tmpInt
      INTEGER, PARAMETER :: nmax = 10000 ! Max number of channels
      INTEGER, PARAMETER :: mlen = 500  ! Max length of channel code
      INTEGER, DIMENSION(nmax) :: acount

      DOUBLE PRECISION :: bsn0,bsn,t,mt1,mt2,Pt
      DOUBLE PRECISION :: bsn2,t2,mt12,mt22,Pt2
      DOUBLE PRECISION :: bsn3,t3,mt13,mt23,Pt3
      DOUBLE PRECISION :: frac,tmpD
      
      DOUBLE PRECISION :: fms1,fms2,fps,feclipse
      DOUBLE PRECISION :: dmint,dmaxt,dmind,dmaxd,t1

      CHARACTER(len=2) :: tmp1
      CHARACTER(len=1) :: grp
      CHARACTER(len=10) :: model,tmpC
      CHARACTER(len=15) :: ext
      CHARACTER(len=50) :: fin,fout,fchs,fchd,fin2,fin3
      CHARACTER(len=50), DIMENSION(14) :: iname,oname
      CHARACTER(len=mlen) :: chn,chn2,chn3,split

      LOGICAL :: fexst

      TYPE :: channel
         INTEGER :: rank               ! Number representing the channel
         INTEGER :: nmbr               ! Number of binaries in this chan
         INTEGER, DIMENSION(2) :: nce  ! Number of CE phases in channel
         CHARACTER(len=mlen) :: code   ! Channel code
      END TYPE channel

      TYPE(channel) :: tmp
      TYPE(channel), DIMENSION(0:nmax) :: db  ! Channel database

! read input

!      WRITE(*,*)
!      WRITE(*,*) 'Binary class:'
!      READ(*,'(I3)') class
!      WRITE(*,*)
!      WRITE(*,*) 'BiSEPS input model:'
!      READ(*,'(A10)') model
!      WRITE(*,*)

!      WRITE(*,*)
!      WRITE(*,*) 'Group binaries according to '
!      WRITE(*,*)
!      WRITE(*,*) '   1: differences in channels'
!      WRITE(*,*) '   2: number of ce phases of each component'
!      WRITE(*,*) '   3: channels in input file'
!      WRITE(*,*) '   4: presence in other group'
!      WRITE(*,*) '   5: white dwarf type'
!      WRITE(*,*) '   6: presence in other group & white dwarf type'
!      WRITE(*,*) '   7: presence in two other groups'
!      WRITE(*,*) '   8: presence in two other groups & white dwarf type'
!      WRITE(*,*) '   9: different primary and secondary pairs'
!      WRITE(*,*)
!      WRITE(*,*) 'Make your choice: '
!      READ(*,'(A1)') grp
!      WRITE(*,*)

!ccccccccccccccccccccccccccccccccccccccccccccccc
! EDIT: RFarmer 10/11/10 Make this cmd line driven
! binary type
      call GETARG(1,tmp1)
! model number
      call GETARG(2,iname(12))
! option from list just below
      call GETARG(3,grp)
      READ(tmp1,'(I3)') class
!cccccccccccccccccccccccccccccccccccccccccccccc

      IF ((grp == '4').OR.(grp == '6')) THEN
         WRITE(*,*) 'Binary class of second group:'
         READ(*,'(I3)') class2
         WRITE(*,*)
      END IF

      IF ((grp == '7').OR.(grp == '8')) THEN
         WRITE(*,*) 'Binary class of second group:'
         READ(*,'(I3)') class2
         WRITE(*,*)
         WRITE(*,*) 'Binary class of third group:'
         READ(*,'(I3)') class3
         WRITE(*,*)
      END IF

! Setup filenames

      ext = '.dat.'//model(1:LEN_TRIM(model))
!       iname(1) = fname(class,'_birth'//ext)
!      iname(2) = fname(class,'_death'//ext)
!      iname(3) = fname(class,'_preSN'//ext)
!      iname(4) = fname(class,'_pstSN'//ext)
!      iname(5) = fname(class,'_preCE1'//ext)
!      iname(6) = fname(class,'_pstCE1'//ext)
!      iname(7) = fname(class,'_preCE2'//ext)
!      iname(8) = fname(class,'_pstCE2'//ext)
!      iname(9) = fname(class,'_preCE3'//ext)
!      iname(10) = fname(class,'_pstCE3'//ext)
!      iname(11) = fname(class,'_init'//ext)
!        iname(12) = fname(class,'_pop'//ext)
! 		iname(13)=fname(class,'_kepler'//ext)
      IF ((grp == '4').OR.(grp == '6')) THEN
         fin2 = fname(class2,'_birth'//ext)
      END IF
      IF ((grp == '7').OR.(grp == '8')) THEN
         fin2 = fname(class2,'_birth'//ext)
         fin3 = fname(class3,'_birth'//ext)
      END IF

      ext = '_dat.'//model(1:LEN_TRIM(model))
!       oname(1) = fname(class,'_birth'//ext)
!      oname(2) = fname(class,'_death'//ext)
!      oname(3) = fname(class,'_preSN'//ext)
!      oname(4) = fname(class,'_pstSN'//ext)
!      oname(5) = fname(class,'_preCE1'//ext)
!      oname(6) = fname(class,'_pstCE1'//ext)
!      oname(7) = fname(class,'_preCE2'//ext)
!      oname(8) = fname(class,'_pstCE2'//ext)
!      oname(9) = fname(class,'_preCE3'//ext)
!      oname(10) = fname(class,'_pstCE3'//ext)
!      oname(11) = fname(class,'_init'//ext)
!        oname(12) = fname(class,'_pop'//ext)
! 		oname(13)=fname(class,'_kepler'//ext)
		
      fin = iname(12)
      fout = fin(1:LEN_TRIM(fin))//'.chn'
      fchs = fin(1:LEN_TRIM(fin))//'.chs'
      fchd = fin(1:LEN_TRIM(fin))//'.chd'

      nmbr = 0                  ! Initialize counter
      count = 0                 ! Initialize counter
      db = channel(0,0,0,'')    ! Initialize channel database

      IF ((grp == '1').OR.(grp == '2')) THEN

! Count the number of different channels and store them in db
         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
 !1       READ(1,100,END=11) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
 1       READ(1,*,END=11) tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpInt,tmpC,tmpC,chn
         nmbr = nmbr + 1
         IF (count > 0) THEN
            DO i = 1, count
               IF (chn == db(i)%code) EXIT
               IF (i == count) THEN
                  count = count + 1
                  db(count)%code = chn
               END IF
            END DO
         ELSE
            count = count + 1
            db(count)%code = chn
         END IF
         IF (count >= nmax) THEN
            WRITE(*,*)
            WRITE(*,*) 'ERROR: count >= nmax!!!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF
         GO TO 1
 11      CONTINUE
         CLOSE(1)

! Put channels in alphabetical order

         CALL sortc(db(1:count)%code,count,mlen)

! Put 'trash' channel first (if it exists)

         DO i = 1, count
            IF (db(i)%code == '0') THEN
               tmp = db(i)
               DO j = i, 2, -1
                  db(j) = db(j-1)
               END DO
               db(1) = tmp
            END IF
         END DO

      END IF

      SELECT CASE(grp)
      CASE('1')

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
!          OPEN(2,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
!               ACTION='WRITE')
! 2       READ(1,100,END=22) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
2          READ(1,*,END=22) tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpD,tmpInt,tmpC,tmpC,chn
         DO i = 1, count
            IF (chn == db(i)%code) THEN
               db(i)%rank = i
               db(i)%nmbr = db(i)%nmbr + 1
!               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,db(i)%rank
! 		WRITE(2,105) bsn,fms1,fms2,fps,t1,t2,db(i)%rank
               EXIT
            ELSE IF (i == count) THEN
               WRITE(*,*) '1, ERROR GROUPING BINARIES!!!'
               WRITE(*,*) '   ',chn
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END IF
         END DO
         GO TO 2
 22      CONTINUE
         CLOSE(1)
!          CLOSE(2)

! Write summary files
		write(*,*) fchs,fchd
         OPEN(4,file=fchs(1:LEN_TRIM(fchs)),STATUS='UNKNOWN', &
              ACTION='WRITE')
         OPEN(50,file=fchd(1:LEN_TRIM(fchd)),STATUS='UNKNOWN', &
              ACTION='WRITE')
         WRITE(4,*) fchs(1:LEN_TRIM(fchs))
         WRITE(4,*)
         WRITE(4,300) 'Total number of systems in file: ',nmbr
         WRITE(4,300) 'Total number of different channels: ',count
         WRITE(4,*)
         DO j = 1, count
            WRITE(4,301) 'Channel: ',db(j)%rank
            chn = split(db(j)%code,mlen)
            write(4,302) chn
            frac = DBLE(db(j)%nmbr)/DBLE(nmbr)
            WRITE(4,303) 'Number of binaries in channel: ', &
                 db(j)%nmbr,' = ',100.d0*frac,'%'
            WRITE(4,*)
            WRITE(50,500) db(j)%rank,db(j)%code
         END DO
         CLOSE(4)
         CLOSE(50)

      CASE('2')

! Determine the number of CE phases in each channel

         DO i = 1, count
            chn  = db(i)%code
            IF (chn == '0') db(i)%nce = -1
            n = LEN_TRIM(chn)
            DO j = 1, n-1, 2
               SELECT CASE(chn(j:j+1))
               CASE('AE','BE','CE','DE','ME')
                  k = (j-1)/2
                  IF (2*(k/2) == k) THEN ! k is even
                     k = 1
                  ELSE ! k is odd
                     k = 2
                  END IF
                  db(i)%nce(k) = db(i)%nce(k) + 1
               END SELECT
            END DO
         END DO

! Group channels according to the number of CE phases

         nrank = 0 ! number of channel groups
         nrank = nrank + 1
         db(1)%rank = nrank
         DO i = 2, count
            DO j = 1, i-1
               IF ((db(i)%nce(1) == db(j)%nce(1)).AND. &
                    (db(i)%nce(2) == db(j)%nce(2))) THEN
                  db(i)%rank = db(j)%rank
                  EXIT
               ELSE IF (j == i-1) THEN
                  nrank = nrank + 1
                  db(i)%rank = nrank
               END IF
            END DO
         END DO

! Group binaries

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')
 3       READ(1,100,END=33) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         DO i = 1, count
            IF (chn == db(i)%code) THEN
               db(i)%nmbr = db(i)%nmbr + 1
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,db(i)%rank
               EXIT
            ELSE IF (i == count) THEN
               WRITE(*,*) '2, ERROR GROUPING BINARIES!!!'
               WRITE(*,*) '   ',chn
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END IF
         END DO
         GO TO 3
 33      CONTINUE
         CLOSE(1)
         CLOSE(2)

! Write summary file

         OPEN(4,file=fchs(1:LEN_TRIM(fchs)),STATUS='UNKNOWN', &
              ACTION='WRITE')
         OPEN(5,file=fchd(1:LEN_TRIM(fchd)),STATUS='UNKNOWN', &
              ACTION='WRITE')
         WRITE(4,*) fchs(1:LEN_TRIM(fchs))
         WRITE(4,*)
         WRITE(4,300) 'Total number of systems in file: ',nmbr
         WRITE(4,300) 'Total number of different channels: ',count
         WRITE(4,300) 'Total number of channel groups  : ',nrank
         WRITE(4,*)
         DO i = 1, nrank
            WRITE(4,301) 'Channel group: ',i
            WRITE(4,*)
            n = 0
            DO j = 1, count
               IF (db(j)%rank == i) THEN
                  chn = split(db(j)%code,mlen)
                  WRITE(4,302) chn,db(j)%nce
                  WRITE(5,500) db(j)%rank,db(j)%code
                  n = n + db(j)%nmbr
               END IF
            END DO
            frac = DBLE(n)/DBLE(nmbr)
            WRITE(4,*)
            WRITE(4,303) 'Number of binaries in group: ', &
                 n,' = ',100.d0*frac,'%'
            WRITE(4,*)
         END DO
         CLOSE(4)
         CLOSE(5)

      CASE('3')

         INQUIRE(FILE=fchd(1:LEN_TRIM(fchd)),EXIST=fexst)
         IF (.NOT.fexst) THEN
            WRITE(*,*) 'Channel input file does not exist!!!'
            WRITE(*,*)
            STOP 'Abnormal program termination!!!'
         END IF
         OPEN(3,file=fchd(1:LEN_TRIM(fchd)),STATUS='OLD', &
              ACTION='READ')
 4       READ(3,500,END=44) db(count+1)%rank,db(count+1)%code
         count = count + 1
         GO TO 4
 44      CONTINUE
         CLOSE(3)

! Group binaries

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')
 5       READ(1,100,END=55) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         DO i = 1, count
            IF (chn == db(i)%code) THEN
               db(i)%nmbr = db(i)%nmbr + 1
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,db(i)%rank
               EXIT
            ELSE IF (i == count) THEN
               WRITE(*,*) '3, ERROR GROUPING BINARIES!!!'
               WRITE(*,*) '   ',chn
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END IF
         END DO
         GO TO 5
 55      CONTINUE
         CLOSE(1)
         CLOSE(2)

      CASE('4')

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fin2(1:LEN_TRIM(fin2)),STATUS='OLD',ACTION='READ')
         OPEN(3,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')

         READ(1,100,END=66) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         READ(2,100,END=66) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
              bkw2,chn2

 6       IF (bsn < bsn2) THEN
            WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
            READ(1,100,END=66) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         ELSE IF (bsn2 < bsn) THEN
            READ(2,100,END=88) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
                 bkw2,chn2
         ELSE
 7          WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,1
            bsn0 = bsn
            READ(1,100,END=66) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
            IF (bsn == bsn0) GO TO 7
         END IF

         GO TO 6

 88      CONTINUE

 8       WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
         READ(1,100,END=66) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         GO TO 8

 66      CONTINUE

         CLOSE(1)
         CLOSE(2)
         CLOSE(3)

      CASE('5')

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')
 51      READ(1,100,END=56) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         IF ((kw1 > 9).AND.(kw1 < 13)) THEN
            kw = kw1
         ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
            kw = kw2
         ELSE
            kw = -10
         END IF
         SELECT CASE(kw)
         CASE(10)
            WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,1
         CASE(11)
            WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
         CASE(12)
            WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,3
         END SELECT
         GO TO 51
 56      CONTINUE
         CLOSE(1)
         CLOSE(2)

      CASE('6')

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fin2(1:LEN_TRIM(fin2)),STATUS='OLD',ACTION='READ')
         OPEN(3,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')

         READ(1,100,END=67) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         READ(2,100,END=67) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
              bkw2,chn2

 61      IF (bsn < bsn2) THEN
            IF ((kw1 > 9).AND.(kw1 < 13)) THEN
               kw = kw1
            ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
               kw = kw2
            ELSE
               kw = -10
            END IF
            SELECT CASE(kw)
            CASE(10)
               WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
            CASE(11)
               WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,4
            CASE(12)
               WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,6
            END SELECT
            READ(1,100,END=67) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         ELSE IF (bsn2 < bsn) THEN
            READ(2,100,END=89) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
                 bkw2,chn2
         ELSE
 71         IF ((kw1 > 9).AND.(kw1 < 13)) THEN
               kw = kw1
            ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
               kw = kw2
            ELSE
               kw = -10
            END IF
            SELECT CASE(kw)
            CASE(10)
               WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,1
            CASE(11)
               WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,3
            CASE(12)
               WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,5
            END SELECT
            bsn0 = bsn
            READ(1,100,END=67) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
            IF (bsn == bsn0) GO TO 71
         END IF

         GO TO 61

 89      CONTINUE

 81      IF ((kw1 > 9).AND.(kw1 < 13)) THEN
            kw = kw1
         ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
            kw = kw2
         ELSE
            kw = -10
         END IF
         SELECT CASE(kw)
         CASE(10)
            WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
         CASE(11)
            WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,4
         CASE(12)
            WRITE(3,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,6
         END SELECT
         READ(1,100,END=67) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         GO TO 81

 67      CONTINUE

         CLOSE(1)
         CLOSE(2)
         CLOSE(3)

      CASE('7')

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fin2(1:LEN_TRIM(fin2)),STATUS='OLD',ACTION='READ')
         OPEN(3,FILE=fin3(1:LEN_TRIM(fin3)),STATUS='OLD',ACTION='READ')
         OPEN(4,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')

         READ(1,100,END=63) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         READ(2,100,END=63) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
              bkw2,chn2
         READ(3,100,END=63) bsn3,t3,mt13,kw13,mt23,kw23,Pt3, &
              bkw3,chn3

 62      IF (bsn < bsn2) THEN
            IF (bsn < bsn3) THEN
               WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,4
               READ(1,100,END=63) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
            ELSE IF (bsn3 < bsn) THEN
               READ(3,100,END=83) bsn3,t3,mt13,kw13,mt23,kw23,Pt3, &
                    bkw3,chn3
            ELSE
 74            WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,3
               bsn0 = bsn
               READ(1,100,END=63) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
               IF (bsn == bsn0) GO TO 74
            END IF
         ELSE IF (bsn2 < bsn) THEN
            READ(2,100,END=84) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
                 bkw2,chn2
         ELSE
            IF (bsn < bsn3) THEN
               WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
               READ(1,100,END=63) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
            ELSE IF (bsn3 < bsn) THEN
               READ(3,100,END=83) bsn3,t3,mt13,kw13,mt23,kw23,Pt3, &
                    bkw3,chn3
            ELSE
 72            WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,1
               bsn0 = bsn
               READ(1,100,END=63) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
               IF (bsn == bsn0) GO TO 72
            END IF
         END IF

         GO TO 62

 83      CONTINUE

         bsn3 = 1e32
         GO TO 62

 84      CONTINUE

         bsn2 = 1e32
         GO TO 62

 63      CONTINUE

         CLOSE(1)
         CLOSE(2)
         CLOSE(3)
         CLOSE(4)

      CASE('8')

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fin2(1:LEN_TRIM(fin2)),STATUS='OLD',ACTION='READ')
         OPEN(3,FILE=fin3(1:LEN_TRIM(fin3)),STATUS='OLD',ACTION='READ')
         OPEN(4,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')

         READ(1,100,END=463) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
         READ(2,100,END=463) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
              bkw2,chn2
         READ(3,100,END=463) bsn3,t3,mt13,kw13,mt23,kw23,Pt3, &
              bkw3,chn3

462      IF (bsn < bsn2) THEN
            IF (bsn < bsn3) THEN
               IF ((kw1 > 9).AND.(kw1 < 13)) THEN
                  kw = kw1
               ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
                  kw = kw2
               ELSE
                  kw = -10
               END IF
               SELECT CASE(kw)
               CASE(10)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,4
               CASE(11)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,8
               CASE(12)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,12
               END SELECT
               READ(1,100,END=463) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
            ELSE IF (bsn3 < bsn) THEN
               READ(3,100,END=483) bsn3,t3,mt13,kw13,mt23,kw23,Pt3, &
                    bkw3,chn3
            ELSE
474            IF ((kw1 > 9).AND.(kw1 < 13)) THEN
                  kw = kw1
               ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
                  kw = kw2
               ELSE
                  kw = -10
               END IF
               SELECT CASE(kw)
               CASE(10)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,3
               CASE(11)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,7
               CASE(12)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,11
               END SELECT
               bsn0 = bsn
               READ(1,100,END=463) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
               IF (bsn == bsn0) GO TO 474
            END IF
         ELSE IF (bsn2 < bsn) THEN
            READ(2,100,END=484) bsn2,t2,mt12,kw12,mt22,kw22,Pt2, &
                 bkw2,chn2
         ELSE
            IF (bsn < bsn3) THEN
               IF ((kw1 > 9).AND.(kw1 < 13)) THEN
                  kw = kw1
               ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
                  kw = kw2
               ELSE
                  kw = -10
               END IF
               SELECT CASE(kw)
               CASE(10)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,2
               CASE(11)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,6
               CASE(12)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,10
               END SELECT
               READ(1,100,END=463) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
            ELSE IF (bsn3 < bsn) THEN
               READ(3,100,END=483) bsn3,t3,mt13,kw13,mt23,kw23,Pt3, &
                    bkw3,chn3
            ELSE
472            IF ((kw1 > 9).AND.(kw1 < 13)) THEN
                  kw = kw1
               ELSE IF ((kw2 > 9).AND.(kw2 < 13)) THEN
                  kw = kw2
               ELSE
                  kw = -10
               END IF
               SELECT CASE(kw)
               CASE(10)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,1
               CASE(11)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,5
               CASE(12)
                  WRITE(4,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,9
               END SELECT
               bsn0 = bsn
               READ(1,100,END=463) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
               IF (bsn == bsn0) GO TO 472
            END IF
         END IF

         GO TO 462

483      CONTINUE

         bsn3 = 1e32
         GO TO 462

484      CONTINUE

         bsn2 = 1e32
         GO TO 462

463      CONTINUE

         CLOSE(1)
         CLOSE(2)
         CLOSE(3)
         CLOSE(4)

      CASE('9')

         acount = 0

         OPEN(1,FILE=fin(1:LEN_TRIM(fin)),STATUS='OLD',ACTION='READ')
         OPEN(2,FILE=fout(1:LEN_TRIM(fout)),STATUS='UNKNOWN', &
              ACTION='WRITE')
 710     READ(1,100,END=711) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,chn
!         IF (Pt > 20.d0) THEN
!            WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,999
!            GO TO 710
!         END IF
         IF (bkw == 0) THEN
            n = 0
         ELSE IF ((bkw == 13).OR.(bkw == 23)) THEN
            n = 100
         ELSE
            n = 200
            WRITE(*,*)
            WRITE(*,*) 'WARNING: n = 200!!!'
            WRITE(*,*)
         END IF
         IF (kw1 > kw2) THEN
            kkw1 = kw1
            kkw2 = kw2
         ELSE
            kkw1 = kw2
            kkw2 = kw1
         END IF
         SELECT CASE(kkw1)
         CASE(0)
            SELECT CASE(kkw2)
            CASE(0)
               k = 1
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(1)
            SELECT CASE(kkw2)
            CASE(0)
               k = 2
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 3
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(2)
            SELECT CASE(kkw2)
            CASE(0)
               k = 4
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 5
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 6
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(3)
            SELECT CASE(kkw2)
            CASE(0)
               k = 7
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 8
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 9
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 10
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(4)
            SELECT CASE(kkw2)
            CASE(0)
               k = 11
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 12
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 13
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 14
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 15
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(5)
            SELECT CASE(kkw2)
            CASE(0)
               k = 16
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 17
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 18
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 19
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 20
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 21
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(6)
            SELECT CASE(kkw2)
            CASE(0)
               k = 22
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 23
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 24
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 25
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 26
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 27
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 28
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(7)
            SELECT CASE(kkw2)
            CASE(0)
               k = 29
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 30
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 31
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 32
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 33
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 34
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 35
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(7)
               k = 36
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(8)
            SELECT CASE(kkw2)
            CASE(0)
               k = 37
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 38
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 39
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 40
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 41
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 42
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 43
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(7)
               k = 44
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(8)
               k = 45
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(9)
            SELECT CASE(kkw2)
            CASE(0)
               k = 46
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 47
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 48
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 49
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 50
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 51
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 52
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(7)
               k = 53
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(8)
               k = 54
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(9)
               k = 55
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(10)
            SELECT CASE(kkw2)
            CASE(0)
               k = 56
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 57
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 58
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 59
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 60
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 61
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 62
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(7)
               k = 63
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(8)
               k = 64
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(9)
               k = 65
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(10)
               k = 66
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(11)
            SELECT CASE(kkw2)
            CASE(0)
               k = 67
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 68
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 69
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 70
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 71
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 72
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 73
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(7)
               k = 74
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(8)
               k = 75
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(9)
               k = 76
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(10)
               k = 77
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(11)
               k = 78
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE(12)
            SELECT CASE(kkw2)
            CASE(0)
               k = 79
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(1)
               k = 80
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(2)
               k = 81
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(3)
               k = 82
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(4)
               k = 83
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(5)
               k = 84
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(6)
               k = 85
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(7)
               k = 86
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(8)
               k = 87
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(9)
               k = 88
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(10)
               k = 89
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(11)
               k = 90
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE(12)
               k = 91
               WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,k+n
               acount(k+n) = acount(k+n) + 1
            CASE DEFAULT
               WRITE(*,*) 'Error grouping binaries!!!'
               WRITE(*,*)
               STOP 'Abnormal program termination!!!'
            END SELECT
         CASE DEFAULT
            WRITE(*,*) 'k1 > 12!!!'
            WRITE(*,*)
            WRITE(2,101) bsn,t,mt1,kw1,mt2,kw2,Pt,bkw,999
            GO TO 710
         END SELECT
         GO TO 710
 711     CONTINUE
         CLOSE(1)
         CLOSE(2)

! Write summary file

         OPEN(4,file=fchs(1:LEN_TRIM(fchs)),STATUS='UNKNOWN', &
              ACTION='WRITE')
         DO i = 1, nmax
            IF (acount(i) == 0) CYCLE
            WRITE(4,304) 'Channel ',i,' : ',acount(i),' binaries'
         END DO
         CLOSE(4)

      CASE DEFAULT

         WRITE(*,*) 'Illegal input!!!'
         WRITE(*,*)
         STOP 'Abnormal program termination!!!'

      END SELECT

 100  FORMAT (F12.4,1X,E12.5,1X,3(E12.5,1X,I3,1X),A)
 101  FORMAT (F12.4,1X,E12.5,1X,3(E12.5,1X,I3,1X),I5)
 300  FORMAT (A,I10)
 301  FORMAT (4X,A,I3)
 302  FORMAT (8X,A44,2X,I3,I3)
 303  FORMAT (8X,A,I10,A,F5.2,A)
 304  FORMAT (4X,A,I4,A,I7,A)
 500  FORMAT (I3,1X,A44)
 
 104  FORMAT (F12.4,1X,5(E18.12,1X),A)
 105  FORMAT (F12.4,1X,5(E18.12,1X),I5)

      STOP 'END of program'
      END PROGRAM channels_v2
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      CHARACTER(len=100) FUNCTION split(str,mlen)
! Put space in str after every 4th character
! Bart Willems, 16/07/01
      IMPLICIT NONE
      INTEGER, INTENT(IN) :: mlen
      CHARACTER(len=mlen), INTENT(IN) :: str
      CHARACTER(len=mlen) :: tmp
      INTEGER :: i,k,n

      tmp = ''
      n = LEN_TRIM(str)
      k = 1
      DO i = 1, n, 4
         tmp(k:k+3) = str(i:i+3)
         tmp(k+4:k+4) = ' '
         k = k+5
      END DO

      split = tmp

      RETURN
      END FUNCTION split
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
      SUBROUTINE sortc (array,n,mlen)
!
!  Purpose:
!    To sort a character array into ascending order using a
!    selection sort.
!
!  Record of revisions:
!      Date       Programmer          Description of change
!      ====       ==========          =====================
!    11/15/95    S. J. Chapman        Original code
!    10/07/01    B. Willems           Added string length as calling parameter
!
      IMPLICIT NONE

! Declare calling parameters:
      INTEGER, INTENT(IN) :: n     ! Number of values
      INTEGER, INTENT(IN) :: mlen ! Length of the strings
      CHARACTER(len=mlen), DIMENSION(n), INTENT(INOUT) :: array
                                          ! Array to be sorted
! Declare local variables:
      INTEGER :: i              ! Loop index
      INTEGER :: iptr           ! Pointer to smallest value
      INTEGER :: j              ! Loop index
      CHARACTER(len=mlen) :: temp     ! Temp variable for swaps

! Sort the array
      outer: DO i = 1, n-1
! &
   !  ind the minimum value in array(i) through array(n)
         iptr = i
         inner: DO j = i+1, n
            minval: IF ( array(j) < array(iptr) ) THEN
               iptr = j
            END IF minval
         END DO inner
! &
   !  ptr now points to the minimum value, so swap array(iptr) &
   !  ith array(i) if i /= iptr.
         swap: IF ( i /= iptr ) THEN
            temp        = array(i)
            array(i)    = array(iptr)
            array(iptr) = temp
         END IF swap

      END DO outer

      RETURN
      END SUBROUTINE sortc
!ccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
