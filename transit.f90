!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Calculates the probility system will transit at different
! transit depths
! R Farmer 01/08/2011
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
MODULE transits

      CONTAINS
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Wraps round getLDC and bisect2 to perfom a bisection search looking
! for the required inclicnations to give a certain
! transit depth
! R.Farmer 02/2011
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	SUBROUTINE transitProb (s,b)
      USE dtypes
      USE params
      USE magnitude
      USE consts

	IMPLICIT NONE
	INTEGER :: i,j,k,aerr
	DOUBLE PRECISION, DIMENSION(:,:),ALLOCATABLE :: inclin
	DOUBLE PRECISION, DIMENSION(:),ALLOCATABLE :: deltaF
	DOUBLE PRECISION :: minL,maxL,minminL,diff
	TYPE (stq), DIMENSION(2),INTENT(INOUT) :: s
      TYPE (bnq), INTENT(INOUT) :: b

      ALLOCATE(inclin(1:numTransits,1:4),deltaF(0:numTransits),STAT=aerr)
      IF (aerr > 0) THEN
         WRITE(*,*)
         WRITE(*,*) '0, ERROR ALLOCATING MEMORY! in transits'
         WRITE(*,*)
         STOP 'Abnormal program termination!!!'
      END IF

	!Get LDC values
	call getLDC(s(1)%reff,s(1)%mt,s(1)%teff,s(1)%ldc1,s(1)%ldc2)
	call getLDC(s(2)%reff,s(2)%mt,s(2)%teff,s(2)%ldc1,s(2)%ldc2)

! 	write(*,*) s(1)%ldc1,s(1)%ldc2
! 	write(*,*) s(2)%ldc1,s(2)%ldc2

	minminL=log10(minTransit)
 	diff=(log10(maxTransit)-log10(minTransit))/(numTransits*1.d0)

	inclin=0.d0

! 	write(*,*) minminL,diff

	DO i=0,numTransits
		deltaF(i)=10**(minminL+((i)*diff))
	END DO

! 	write(*,*) deltaF

	DO  i=1,numTransits
 		!Get inlcination for this transit depth on star in position 1
		inclin(i,1)=bisect2(s(1),s(2),b,deltaF(i-1),deltaF(i))
		inclin(i,2)=bisect2(s(2),s(1),b,deltaF(i-1),deltaF(i))
	end do

! 	write(*,*) inclin(1,1),inclin(1,2)

	!Set max to 90 deg
	WHERE(inclin(:,1).lt.0.d0) inclin(:,1)=pi/2.d0
	WHERE(inclin(:,2).lt.0.d0) inclin(:,2)=pi/2.d0

	!Calculate probaility
	FORALL (i=1:numTransits, inclin(i,1) .gt. 0.d0 .and. inclin(i+1,1) .gt.0.d0) inclin(i,3)=DCOS(inclin(i,1))-DCOS(inclin(i+1,1))
	FORALL (i=1:numTransits, inclin(i,2) .gt. 0.d0 .and. inclin(i+1,2) .gt.0.d0) inclin(i,4)=DCOS(inclin(i,2))-DCOS(inclin(i+1,2))

	!Dont be less than zero
	WHERE(inclin(:,3).lt.0.d0) inclin(:,3)=0.d0
	WHERE(inclin(:,4).lt.0.d0) inclin(:,4)=0.d0

	b%prob=0.d0

 	FORALL (i=1:numTransits) b%prob(i)=max(inclin(i,3),inclin(i,4))

	DEALLOCATE(inclin,deltaF)

	END SUBROUTINE transitProb


!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
! Perfom a bisection search between two transit depths searching for an appriaote
! inclcination
! R.Farmer 02/2011
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
      DOUBLE PRECISION FUNCTION bisect2(s1,s2,b,minL,maxL)

	USE dtypes
	USE jktebop
	USE consts

	IMPLICIT NONE
	DOUBLE PRECISION :: imid,imin,imax,imin_out
	DOUBLE PRECISION, PARAMETER :: eps=1.0d-2,Ieps=1.0d-10
	DOUBLE PRECISION :: d,delmin,delmid,delmax
	DOUBLE PRECISION :: prob
	DOUBLE PRECISION,INTENT(IN) ::minL,maxL
	INTEGER :: ExitValue

	TYPE (stq), INTENT(IN) :: s1,s2
        TYPE (bnq), INTENT(IN) :: b

	INTEGER :: steps
	!Star 1 is eclipsed by star 2

	!Dont want contact systems
	if(b%a.lt.(s1%reff+s2%reff))then
		bisect2=-9.d0
		return
	end if

	!Both
	imax=pi/2.d0
	imin=dacos((s1%reff+s2%reff)/b%a)

! 	write(*,*) s1%r,s2%r,b%a
! 	write(*,*) imin*180/pi,imax*180/pi

	if(imin.gt.pi/2.d0.or.imax.gt.pi/2.d0)then
		bisect2=-99.9
		return
	end if

	delmax=jkt(s1,s2,b,imax)
	if(delmax.lt.minL) then
		bisect2=-999.9
		return
	end if

	!Perform bisection search to get the value

	deLmid=0.d0
	DO steps=0, 500
		imid=imin+((imax-imin)/2.d0)
		deLmid=jkt(s1,s2,b,imid)
! 		write(*,*) deLmid,delmax,imin*180/pi,imid*180/pi,imax*180/pi
		if(abs(imin-imax) .lt. Ieps ) THEN
			if(abs((deLmid-minL)/minL) .le. eps ) then
			!Convergered
				imin_out=imid
				EXIT
			else if (delmid .gt.minL .and. delmid .lt.maxL) then
			!Not converged but still visible
				imin_out=imid
				EXIT
			else
			!Not converged 
				imin_out=-1.d0
				EXIT
			end if
		end if
		if(deLmid.gt.minL)then
			!We have bigger change than we want so decrease i
			imax=imid
		else if (deLmid.lt.minL)then
			!We have smaller change than we want to increase i
			imin=imid
		else
		!Only if deLmid=L  which is unlikely but possible
			imin_out=imid
			exit
		end if
	END DO


	bisect2=imin_out

	END FUNCTION bisect2

END MODULE transits